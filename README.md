# Sistema-Padaria
Projeto de Sistema de Gestão para Confeitarias e Padarias. Criado e desenvolvido no 1º semestre de 2019 por alunos da [*Universidade Regional do Noroeste do Estado do Rio Grande do Sul, Unijuí*](https://www.unijui.edu.br/), para a disciplina de Projeto Integrador II.

###Alunos
- Carlos Valdiero
- Éderson Siqueira
- Jean Carlos
- Rodrigo Vianna

##Disciplina
A disciplina Projeto Integrador II faz parte de um conjunto de três disciplinas que integram conteúdos de **programação** e **desenvolvimento** cursados na graduação.
A partir de um projeto, uma documentação, sorteado entre os grupos, os alunos devem desenvolver com base nesse projeto, podendo fazer algumas modificações quando necessário. Essa documentação foi elaborada na disciplina de Projeto Integerador I, a qual engloba conhecimentos de **engenharia de software**.

##Projeto

##Sistema

##Instalação