package base;

/**
 *
 * @author Rodrigo Vianna
 * @since 07/05/2019
 */
public class CampoException extends Exception {

    public CampoException(String message) {
        super(message);
    }

}
