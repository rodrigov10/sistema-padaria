package base;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author Rodrigo Vianna
 * @since 23/05/2019
 */
public class Criptografia {

    public static String getMD5(String valor) {
        try {
            MessageDigest m = MessageDigest.getInstance("MD5");
            m.update(valor.getBytes(), 0, valor.length());
            return new BigInteger(1, m.digest()).toString(16);
        } catch (NoSuchAlgorithmException e) {
        }
        return null;
    }

}
