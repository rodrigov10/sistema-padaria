package base;

import static base.Sistema.SEP;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

/**
 *
 * @author Rodrigo Vianna
 * @since 24/03/2019
 *
 */
public class Propriedades {

    /**
     * Propriedades do sistema.
     */
    private static Properties propriedades;

    /**
     * Local do arquivo de propriedades.
     */
    private static final String PROPERTIES_FILE = Sistema.USER_LOCAL_SYSTEM + SEP + "config.properties";

    /**
     * Valida as propriedades do sistema.
     *
     * @throws IOException
     */
    private static void validarProperties() throws IOException {
        if (propriedades == null) {
            configurarProperties();
        }
    }

    /**
     * Configura o arquivo de propriedades e o cria caso não exista.
     *
     * @throws IOException lança throws
     */
    public static void configurarProperties() throws IOException {
        propriedades = new Properties();
        File fileProp = new File(PROPERTIES_FILE);
        if (!fileProp.exists()) {
            criarProperties(fileProp);
        }
        try (FileInputStream file = new FileInputStream(fileProp)) {
            propriedades.load(file);
        }
    }

    /**
     * Cria o arquivo de propriedades.
     *
     * @param fileProp Arquivo de propriedades
     * @throws FileNotFoundException
     * @throws IOException
     */
    private static void criarProperties(File fileProp) throws FileNotFoundException, IOException {
        propriedades.setProperty("prop.server.password", "");
        propriedades.setProperty("prop.server.host", "");
        propriedades.setProperty("prop.server.login", "");
        propriedades.setProperty("prop.log.localsave", Sistema.USER_LOCAL_SYSTEM + "\\log");
        propriedades.setProperty("prop.login.autologin", "");
        propriedades.setProperty("prop.login.lastuser", "");
        propriedades.setProperty("prop.login.lastpass", "");
        try (FileOutputStream fos = new FileOutputStream(fileProp)) {
            propriedades.store(fos, "Propriedades do Sistema");
        }
    }

    /**
     * Retorna uma propriedade.
     *
     * @param propertie Chave da propriedade
     * @return String do valor da propriedade
     * @throws IOException lança throw
     */
    public static String getProperty(String propertie) throws IOException {
        validarProperties();
        return propriedades.getProperty(propertie);
    }

    /**
     *
     * @param propertie Chave da propriedade
     * @param defaultValue Valor padrão caso não encontre um valor no arquivo.
     * @return String do valor da propriedade
     * @throws IOException lança throw
     */
    public static String getProperty(String propertie, String defaultValue) throws IOException {
        validarProperties();
        return propriedades.getProperty(propertie, defaultValue);
    }

    /**
     * Seta uma propriedade e salvo no arquivo.
     *
     * @param chave Chave da propriedade.
     * @param valor Valor da propriedade.
     */
    public static void setProperty(String chave, String valor) {
        try {
            validarProperties();
            propriedades.setProperty(chave, valor);
            try (FileOutputStream fos = new FileOutputStream(PROPERTIES_FILE)) {
                propriedades.store(fos, "Propriedades do Sistema");
            }
        } catch (IOException ex) {
            //Ignora
        }

    }

    /**
     * Recarrega o atributo {@link Propriedades#propriedades} a partir do
     * arquivo salvo no computador.
     *
     * @throws IOException lança throw
     */
    public static void recarregarProperties() throws IOException {
        configurarProperties();
    }
}
