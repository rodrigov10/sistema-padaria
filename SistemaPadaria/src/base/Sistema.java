package base;

import java.io.File;

/**
 *
 * @author Rodrigo Vianna
 * @since 23/03/2019
 */
public class Sistema {

    /**
     * Diretório da pasta do usuário do computador.
     */
    public static final String SEP = File.separator;
    /**
     * Diretório da pasta do usuário do computador.
     */
    public static final String USER_LOCAL_SYSTEM = System.getProperty("user.home") + SEP + "SistemaPadaria";

    /**
     * Nome do usuário do computador.
     */
    public static final String USER_NAME = System.getProperty("user.name");

}
