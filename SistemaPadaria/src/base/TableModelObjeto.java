package base;

import base.bd.ObjetoID;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

/**
 *
 * @author Rodrigo Vianna
 * @param <O> Passa objetoID
 * @since 25/05/2019
 */
public class TableModelObjeto<O extends ObjetoID> extends DefaultTableModel {

    private final TableColumnModel tcm;

    public TableModelObjeto(TableColumnModel tcm) {
        this.tcm = tcm;
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return false;
    }

    public TableColumn getColumn(Object columnIdentifier) {
        return tcm.getColumn(tcm.getColumnIndex(columnIdentifier));
    }

    public O getObjeto(int row) {
        return (O) getValueAt(row, getColumnCount() - 1);
    }

    public void reset() {
        setNumRows(0);
    }

}
