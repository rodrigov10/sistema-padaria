package base;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 *
 * @author Rodrigo Vianna
 * @since 23/05/2019
 */
public class Valor extends Number {

    public static final Valor VALOR_CEMD2 = new Valor("100,00");
    public static final Valor VALOR_ZEROD2 = new Valor("0,00");
    public static final Valor VALOR_ZEROD0 = new Valor("0");

    private final BigDecimal valor;

    public Valor(String valor) {
        if (valor == null || valor.isEmpty()) {
            this.valor = VALOR_ZEROD2.getBigDecimal();
        } else {
            this.valor = new BigDecimal(valor.replace(".", "").replace(",", "."));
        }
    }
    
    public Valor(Double valor) {
        if (valor == null) {
            this.valor = VALOR_ZEROD2.getBigDecimal();
        } else {
            this.valor = new BigDecimal(valor);
        }
    }

    public Valor(Double valor, int decimais) {
        if (valor == null) {
            this.valor = VALOR_ZEROD2.getBigDecimal().setScale(decimais, RoundingMode.HALF_UP);
        } else {
            this.valor = new BigDecimal(valor).setScale(decimais, RoundingMode.HALF_UP);
        }
    }

    public Valor(BigDecimal valor) {
        if (valor == null) {
            this.valor = VALOR_ZEROD2.getBigDecimal();
        } else {
            this.valor = valor;
        }
    }

    public Valor(long valor, int decimais) {
        this.valor = new BigDecimal(valor).movePointLeft(decimais);
    }

    public Valor(long valor) {
        this.valor = new BigDecimal(valor);
    }

    public Valor mais(Valor oValor) {
        return new Valor(this.valor.add(oValor.valor).setScale(2, RoundingMode.HALF_UP));
    }

    public Valor menos(Valor oValor) {
        return new Valor(this.valor.subtract(oValor.valor).setScale(2, RoundingMode.HALF_UP));
    }

    public Valor div(Valor oValor) {
        return new Valor(this.valor.divide(oValor.valor).setScale(2, RoundingMode.HALF_UP));
    }

    public Valor mult(Valor oValor) {
        return new Valor(this.valor.multiply(oValor.valor).setScale(2, RoundingMode.HALF_UP));
    }

    public BigDecimal getBigDecimal() {
        return this.valor;
    }

    @Override
    public String toString() {
        return valor.toString().replace(".", ",");
    }

    @Override
    public int intValue() {
        return this.valor.intValue();
    }

    @Override
    public long longValue() {
        return this.valor.longValue();
    }

    @Override
    public float floatValue() {
        return this.valor.floatValue();
    }

    @Override
    public double doubleValue() {
        return this.valor.doubleValue();
    }

}
