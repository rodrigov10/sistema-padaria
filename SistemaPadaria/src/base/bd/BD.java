package base.bd;

import base.Propriedades;
import base.log.Log;
import base.msg.JAlerta;
import base.utils.SwingUtils;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Esta classe contém os dados do BD
 * @author Rodrigo Vianna
 * @since 10/04/2019
 */
public class BD {

    private static String host = "";
    private static String login = "";
    private static String password = "";

    private static String nomeBancoDados = "";
    private static String versaoBancoDados = "";
    private static String usuarioBancoDados = "";
    private static String urlBancoDados = "";
    private static String driverBancoDados = "";
    private static String driverVersaoBancoDados = "";
    private static String bancoDados = "";

    public static String getNomeBancoDados() {
        return nomeBancoDados;
    }

    public static String getVersaoBancoDados() {
        return versaoBancoDados;
    }

    public static String getUsuarioBancoDados() {
        return usuarioBancoDados;
    }

    public static String getUrlBancoDados() {
        return urlBancoDados;
    }

    public static String getDriverBancoDados() {
        return driverBancoDados;
    }

    public static String getDriverVersaoBancoDados() {
        return driverVersaoBancoDados;
    }

    public static String getBancoDados() {
        return bancoDados;
    }

    private static void validarMetaDados(Connection c) throws SQLException {
        DatabaseMetaData metaData = c.getMetaData();
        nomeBancoDados = metaData.getDatabaseProductName();
        versaoBancoDados = metaData.getDatabaseProductVersion();
        usuarioBancoDados = metaData.getUserName();
        urlBancoDados = metaData.getURL();
        driverBancoDados = metaData.getDriverName();
        driverVersaoBancoDados = metaData.getDriverVersion();
        if (urlBancoDados == null || urlBancoDados.isEmpty()) {
            bancoDados = "";
        } else {
            String[] split = urlBancoDados.split("/");
            bancoDados = split[split.length - 1];
        }

    }

    public static boolean configurarBD() {
        Log.log("Configurando banco de dados...");

        try {
            login = Propriedades.getProperty("prop.server.login");
            password = Propriedades.getProperty("prop.server.password");
            host = Propriedades.getProperty("prop.server.host");
        } catch (IOException ex) {
            JAlerta.getAlertaErro(SwingUtils.getJanelaFocada(), "Erro ao configurar banco de dados:", ex);
            return false;
        }
        try (Connection c = DriverManager.getConnection(host, login, password)) {
            if (!c.isClosed()) {
                validarMetaDados(c);
                Log.log("Conectado ao banco de dados " + bancoDados + '!');
            }
            return true;
        } catch (SQLException ex) {
            Log.log("Erro na conexão dos banco de dados:", new BDException(ex));
            return false;
        }
    }

    public static void encerrarConexoes() {
        Conexao.LISTA_CONEXOES.forEach((conexao) -> {
            conexao.close();
        });
    }

    public static Connection getConnection() throws BDException {
        if (host.isEmpty() || login.isEmpty() || password.isEmpty()) {
            configurarBD();
        }
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException ex) {
            Log.log("Erro ao retornar a conexão com o banco - ", ex);
        }
        try {
            return DriverManager.getConnection(host, login, password);
        } catch (SQLException ex) {
            throw new BDException(ex);
        }
    }
}
