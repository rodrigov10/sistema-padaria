package base.bd;

import java.sql.SQLException;

/**
 * Exception do BD
 * @author Rodrigo Vianna
 * @since 31/05/2019
 */
public class BDException extends Exception {

    public BDException(SQLException ex) {
        super(ex);
    }
}
