package base.bd;

import base.log.Log;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Classe que realiza a conexão com o BD
 *
 * @author Rodrigo Vianna
 * @since 10/04/2019
 */
public class Conexao implements AutoCloseable {

    protected static final List<Conexao> LISTA_CONEXOES = Collections.synchronizedList(new ArrayList<Conexao>(2));
    private static int lastTicket = 0;
    private final Connection connection;
    private final int ticket;
    private Statement stmt;

    /**
     * Método que cria uma nova conexão.
     *
     * @return Retona uma nova conexão
     * @throws BDException lança throws
     */
    public static Conexao getConexao() throws BDException {
        Conexao c = new Conexao();
        c.logConexao("Conexão criada!");
        return c;
    }

    private static synchronized int getNewTicket() {
        return lastTicket++;
    }

    private Conexao() throws BDException {
        try {
            this.ticket = getNewTicket();
            this.connection = BD.getConnection();
            this.stmt = this.connection.createStatement();
        } catch (SQLException e) {
            throw new BDException(e);
        }
    }

    public static int update(SQL sql) throws BDException {
        try (Conexao c = new Conexao()) {
            c.logConexao(sql);
            return c.stmt.executeUpdate(sql.toString());
        } catch (SQLException ex) {
            throw new BDException(ex);
        }
    }

    public int getTicket() {
        return ticket;
    }

    public ResultSet executeQuery(SQL sql) throws BDException {
        try {
            logConexao(sql);
            return stmt.executeQuery(sql.toString());
        } catch (SQLException ex) {
            throw new BDException(ex);
        }
    }

    private void logConexao(SQL sql) {
        Log.log('C' + String.valueOf(ticket) + '{' + sql.toString() + ";}");
    }

    private void logConexao(String msg) {
        Log.log('C' + String.valueOf(ticket) + " " + msg);
    }

    private void logConexao(String msg, Exception ex) {
        Log.log('C' + String.valueOf(ticket) + " " + msg, ex);
    }

    /**
     * Método que fecha a conexão.
     */
    @Override
    public void close() {
        try {
            if (connection != null && !connection.isClosed()) {
                connection.close();
            }
            if (stmt != null && !stmt.isClosed()) {
                stmt.close();
            }
            LISTA_CONEXOES.remove(this);
            logConexao("Conexão fechada!");
        } catch (SQLException eSql) {
            logConexao("Erro ao fechar conexão:", new BDException(eSql));
        }
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 19 * hash + this.ticket;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Conexao other = (Conexao) obj;
        return this.ticket == other.ticket;
    }

}
