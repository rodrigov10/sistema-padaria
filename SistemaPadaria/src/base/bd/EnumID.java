package base.bd;

/**
 *
 * @author Rodrigo
 * @since 21/05/2019
 */
public interface EnumID {

    Integer getCodigo();
}
