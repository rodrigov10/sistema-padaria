package base.bd;

/**
 *
 * @author Rodrigo
 * @since 21/05/2019
 */
public interface ObjetoID {

    void setId(Integer id);

    Integer getId();
}
