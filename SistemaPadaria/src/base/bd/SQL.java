package base.bd;

import base.Valor;
import base.utils.DataUtils;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Rodrigo Vianna
 * @since 10/04/2019
 */
public class SQL {

    private final StringBuilder sb = new StringBuilder();

    private final HashMap<String, Object> parametros = new HashMap(5);

    private String sqlGerado = null;

    public SQL() {
    }

    public SQL(String sql) {
        sb.append("\n").append(sql);
    }

    public void add(String sql) {
        sb.append("\n").append(sql);
    }

    private static String setParametro(Object o) {
        if (o == null) {
            return "null";
        }
        if (o instanceof ObjetoID) {
            return String.valueOf(((ObjetoID) o).getId());
        } else if (o instanceof EnumID) {
            return String.valueOf(((EnumID) o).getCodigo());
        } else if (o instanceof Valor) {
            return String.valueOf(((Valor) o).getBigDecimal());
        } else if (o instanceof Timestamp) {
            return String.valueOf(adicionarAspas(DataUtils.dataToString((Timestamp) o, DataUtils.FORMATO_TIMESTAMPBD)));
        } else if (o instanceof Date) {
            return String.valueOf(adicionarAspas(DataUtils.dataToString((Date) o, DataUtils.FORMATO_DATEBD)));
        } else if (o instanceof Boolean) {
            return String.valueOf((Boolean) o);
        } else if (o instanceof Double) {
            return String.valueOf((Double) o);
        } else if (o instanceof Float) {
            return String.valueOf((Float) o);
        } else if (o instanceof Long) {
            return String.valueOf((Long) o);
        } else if (o instanceof Integer) {
            return String.valueOf((Integer) o);
        } else if (o instanceof Short) {
            return String.valueOf((Short) o);
        } else if (o instanceof Byte) {
            return String.valueOf((Byte) o);
        } else {
            return String.valueOf(adicionarAspas(o.toString()));
        }
    }

    private static String adicionarAspas(String string) {
        return "'" + string + "'";
    }

    public void setParametro(String key, Object value) {
        parametros.put(key, value);
    }

    private void gerarSql() {
        final String sqlBruto = sb.toString();
        final Matcher matcher = Pattern.compile("(?<!:)([\\\\]?:\\w+)").matcher(sqlBruto);
        final StringBuffer sqlProcessado = new StringBuffer();
        while (matcher.find()) {
            String parametro = matcher.group(1);
            String chave = parametro.substring(1);
            if (!parametros.containsKey(chave)) {
                if (parametro.startsWith("\\")) {
                    matcher.appendReplacement(sqlProcessado, chave);
                    continue;
                }
                throw new IllegalArgumentException("Não foi enccontrada a chave '" + chave + "' no SQL.\nVerifique:" + sqlBruto + "\n" + sqlProcessado.toString());
            }
            String valor = setParametro(parametros.get(chave));
            matcher.appendReplacement(sqlProcessado, valor.replace("\\", "\\\\").replace("$", "\\$"));
        }
        matcher.appendTail(sqlProcessado);
        sqlGerado = sqlProcessado.toString();
    }

    @Override
    public String toString() {
        if (sqlGerado == null) {
            gerarSql();
        }
        return sqlGerado;
    }

}
