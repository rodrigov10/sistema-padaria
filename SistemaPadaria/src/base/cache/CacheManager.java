package base.cache;

import base.log.Log;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Rodrigo
 * @since 19/06/2019
 * @param <C> CacheObjeto
 */
public final class CacheManager<C extends CacheObjeto> {

    private static final HashMap<String, CacheManager> CACHES = new HashMap<>(5);

    public static CacheManager getInstance(Class clas) {
        synchronized (CACHES) {
            String cacheClassType = clas.getName();
            if (CACHES.containsKey(cacheClassType)) {
                return CACHES.get(cacheClassType);
            }
            CacheManager cache = new CacheManager(cacheClassType);
            CACHES.put(cacheClassType, cache);
            return cache;
        }
    }

    /**
     * Limpa todos os gerenciadores de cache da lista.
     *
     * @see CacheManager#CACHES
     * @see CacheManager#CACHE
     */
    protected static void clear() {
        synchronized (CACHES) {
            Log.log("[CACHE]Iniciando limpeza de cache... ");
            CACHES.keySet().forEach((string) -> {
                CACHES.get(string).clearCache();
            });
        }
    }

    private final HashMap<Object, C> CACHE = new HashMap(20);
    private final String cacheClassType;

    /**
     * Construtor privado.
     * <p>
     * Uma nova instância deve ser criada pelo método {@link CacheManager#getInstance()
     * }, pois nele a nova instância é armazenada na lista de gereciadores
     * criados.</p>
     *
     * @see CacheManager#CACHES
     */
    private CacheManager(String cacheClassType) {
        this.cacheClassType = cacheClassType;
    }

    /**
     * Adiciona ou atualiza um objeto da cache de acordo com seu identificador.
     *
     * @param cacheObjeto objeto a ser armazenado.
     * @see CacheManager#CACHE
     */
    public void put(C cacheObjeto) {
        if (cacheObjeto != null && cacheObjeto.getId() != null) {
            synchronized (CACHE) {
                CACHE.put(cacheObjeto.getId(), cacheObjeto);
                Log.log("[CACHE]Armazenada " + cacheObjeto.getClass().getName() + ": " + cacheObjeto.toString());
            }
        }
    }

    /**
     * Remove um objeto do map e o retorna.
     *
     * @param keyObjeto identificador do objeto em cache.
     * @return o objeto em cache ou null caso não exista.
     * @see CacheManager#CACHE
     */
    public C remove(Object keyObjeto) {
        if (keyObjeto != null) {
            synchronized (CACHE) {
                C object = CACHE.remove(keyObjeto);
                if (object == null) {
                    return null;
                }
                Log.log("[CACHE]Removida " + cacheClassType + ": " + object.toString());
                return object;
            }
        }
        return null;
    }

    /**
     * Retorna um dos objetos em cache a partir do seu identificador.
     *
     * @param keyObjeto identificador do objeto em cache.
     * @return o objeto em cache ou null caso não exista.
     * @see CacheManager#CACHE
     */
    public C get(Object keyObjeto) {
        if (keyObjeto != null) {
            synchronized (CACHE) {
                C object = CACHE.get(keyObjeto);
                if (object == null) {
                    return null;
                }
                Log.log("[CACHE]Retornado " + cacheClassType + ": " + object.toString());
                return object;
            }
        }
        return null;
    }

    /**
     * Retorna uma lista com todos os objetos em cache.
     *
     * @return lista de objetos.
     */
    public List<C> getObjetos() {
        synchronized (CACHE) {
            return new ArrayList(CACHE.values());
        }
    }

    /**
     * Limpa o map de objetos em cache.
     *
     * @see CacheManager#CACHE
     */
    private void clearCache() {
        synchronized (CACHE) {
            Log.log("[CACHE]Limpando cache de" + cacheClassType + "...");
            CACHE.clear();
        }
    }

}
