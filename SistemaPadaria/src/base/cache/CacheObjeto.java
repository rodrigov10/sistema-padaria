package base.cache;

/**
 *
 * @author Rodrigo
 * @param <I> tipo de objeto
 * @since 19/06/2019
 */
public interface CacheObjeto<I> {

    I getId();

}
