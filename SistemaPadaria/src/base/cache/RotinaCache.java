package base.cache;

import base.rotina.RotinaInterface;

/**
 *
 * @author Rodrigo
 * @since 19/06/2019
 */
public class RotinaCache implements RotinaInterface {

    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(900000);
                CacheManager.clear();
            } catch (InterruptedException e) {
            }
        }
    }

}
