package base.log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import base.Propriedades;
import base.Sistema;
import static base.Sistema.SEP;
import base.utils.DataUtils;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

/**
 *
 * @author Rodrigo Vianna
 * @since 13/03/2019
 */
public class Log {

    private static String localSave;

    private static Nivel nivel = Nivel.DESLIGADO;

    private Log() {
    }

    /**
     * Realiza a configuração do log.
     *
     * @throws IOException lança throws
     */
    public static void configurarLog() throws IOException {
        localSave = Propriedades.getProperty("prop.log.localsave");
        if (localSave == null || localSave.isEmpty()) {
            localSave = Sistema.USER_LOCAL_SYSTEM + SEP +"log";
            alterarLocalLog(localSave);
        }

        File file = new File(localSave);
        if (!file.exists()) {
            file.mkdirs();
        }
        ligar();
        log("Local de Log: " + localSave);
    }

    /**
     * Realiza o log do sistema.
     *
     * @param mensagem passa mensagem
     */
    public static void log(String mensagem) {
        log(mensagem, null);
    }

    /**
     * Realiza o log do sistema, com exceção.
     *
     * @param mensagem passa mensagem
     * @param ex passa Throwable
     */
    public synchronized static void log(String mensagem, Throwable ex) {
        if (nivel.equals(Nivel.DESLIGADO)) {
            return;
        }

        final Date data = new Date();
        if (ex != null) {
            mensagem = "[" + DataUtils.dataToString(data, DataUtils.FORMATO_DATAHORASEG) + "] - " + throwableToString(mensagem, ex);
            System.err.println(mensagem);
        } else {
            mensagem = "[" + DataUtils.dataToString(data, DataUtils.FORMATO_DATAHORASEG) + "] - " + mensagem;
            System.out.println(mensagem);
        }

        if (localSave == null || localSave.isEmpty()) {
            return;
        }

        File file = new File(getDirectoryLog(data));
        if (!file.exists()) {
            file.mkdirs();
        }
        escreverArquivo(file, DataUtils.dataToString(data, DataUtils.FORMATO_LOGNAME) + ".log", mensagem);
    }

    /**
     * Transforma a pilha da exceção mais a mensagem informada em uma única
     * String formatada.
     *
     * @param msg
     * @param th
     * @return Mensagem formatada
     */
    private static String throwableToString(String msg, Throwable th) {
        if (nivel.equals(Nivel.INFO)) {
            if (msg == null || msg.isEmpty()) {
                return new StringBuilder().append(th.getMessage()).toString();
            } else {
                return new StringBuilder().append(msg).append(": \n").append(th.getMessage()).toString();
            }
        }
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        th.printStackTrace(new PrintStream(b));
        if (msg == null || msg.isEmpty()) {
            return new StringBuilder().append(b.toString()).toString();
        } else {
            return new StringBuilder().append(msg).append(": \n").append(b.toString()).toString();
        }
    }

    /**
     * Liga o log.
     */
    public static void ligar() {
        nivel = Nivel.ERRO;
    }

    /**
     * Desliga o log.
     */
    public static void desligar() {
        nivel = Nivel.DESLIGADO;
    }

    /**
     * Altera o nível do log
     *
     * @param nivel passa nivel
     */
    public static void setNivel(Nivel nivel) {
        if (nivel == null) {
            return;
        }
        Log.nivel = nivel;
    }

    /**
     * Altera no arquivo properties o local de criação do log
     *
     * @param local passa local
     */
    public static void alterarLocalLog(String local) {
        if (local == null || local.isEmpty()) {
            return;
        }
        File file = new File(local);
        if (file.isFile()) {
            return;
        }
        if (!file.exists()) {
            file.mkdirs();
        }
        Propriedades.setProperty("prop.log.localsave", local);
        localSave = local;
        log("Local de Log: " + localSave);
    }

    /**
     * Grava sobre o arquivo de log ou cria um novo.
     *
     * @param destino
     * @param nomeArquivo
     * @param mensagem
     */
    private static void escreverArquivo(File destino, String nomeArquivo, String mensagem) {
        try {
            try (FileWriter fw = new FileWriter(new File(destino.getAbsolutePath() + SEP + nomeArquivo), true); BufferedWriter bw = new BufferedWriter(fw); PrintWriter pw = new PrintWriter(bw)) {
                pw.println(mensagem);
                pw.flush();
            }
        } catch (IOException ex) {
            //Ignora
        }
    }

    /**
     * Retorna o diretório de gravação do log com o nome do arquivo.
     *
     * @param data
     * @return String do diretório
     */
    private static String getDirectoryLog(Date data) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd");
        String dia = dateFormat.format(data);

        dateFormat.applyPattern("MM");
        String mes = dateFormat.format(data);

        dateFormat.applyPattern("yyyy");
        String ano = dateFormat.format(data);
        return localSave + SEP + ano + SEP + mes + SEP + dia;
    }

    public static enum Nivel {
        /**
         * Log desligado
         */
        DESLIGADO,
        /**
         * Apenas informações. Exceções mostram apenas a mensagem, sem a pilha.
         */
        INFO,
        /**
         * Mostra a pilha da exceção.
         */
        ERRO;
    }

}
