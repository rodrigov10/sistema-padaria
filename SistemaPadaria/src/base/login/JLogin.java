package base.login;

import base.Propriedades;
import base.msg.JAlerta;
import base.utils.FecharEsc;
import base.utils.SwingUtils;
import java.awt.Component;
import java.io.IOException;

/**
 * Define a tela de login do sistema.
 *
 * @author Rodrigo Vianna
 * @since 23/03/2019
 */
public class JLogin extends javax.swing.JDialog implements FecharEsc {

    /**
     * Define uma única instância do login no sistema.
     */
    private static JLogin jLogin;

    /**
     * Retorna uma instância da tela de Login
     *
     * @return Tela de Login.
     */
    public static JLogin getInstance() {
        if (jLogin == null) {
            jLogin = new JLogin((java.awt.Dialog) null, true);
        }
        jLogin.limpar();
        if (Sessao.getUsuarioLogado() != null) {
            jLogin.jTextField_Nome.setText(Sessao.getUsuarioLogado() == null ? "" : Sessao.getUsuarioLogado().getNomeUsuario());
            jLogin.jTextField_Nome.selectAll();
        }
        jLogin.jTextField_Nome.requestFocusInWindow();
        return jLogin;
    }

    /**
     * Define se o sistema foi logado corretamente.
     */
    private boolean ok = false;

    /**
     * Construtor que recebe uma janela pai e se é ou não modal.
     *
     * @param parent
     * @param modal
     */
    private JLogin(java.awt.Window parent, boolean modal) {
        super(parent, modal ? ModalityType.APPLICATION_MODAL : ModalityType.MODELESS);
        initComponents();
        inicializar();
    }

    /**
     * Inicializa a tela
     */
    private void inicializar() {
        SwingUtils.inicializarTela(this);
        jTextField_Nome.requestFocusInWindow();

        new Thread(() -> {
            autoLogin();
        }, "Auto-Login").start();
    }

    /**
     * Tenta fazer o auto-login
     */
    private void autoLogin() {
        try {
            SwingUtils.iniciarCursorProcessamento(this);
            SwingUtils.desabilitarCampos(getCampos());
            int i = Integer.parseInt(Propriedades.getProperty("prop.login.autologin"));
            if (i == 1) {
                jCheckBox_Lembrar.setSelected(true);
                ok = Sessao.autoLogin();
                if (ok) {
                    fechar();
                }
            }
        } catch (IOException | NumberFormatException e) {
        } finally {
            SwingUtils.habilitarCampos(getCampos());
            SwingUtils.finalizarCursorProcessamento(this);
        }
    }

    /**
     * Valida o login e acessa o sistema
     */
    private void validarLogin() {
        try {
            SwingUtils.iniciarCursorProcessamento(this);
            SwingUtils.desabilitarCampos(getCampos());
            final boolean autoLogin = jCheckBox_Lembrar.isSelected();
            ok = Sessao.logarSistema(jTextField_Nome.getText(), jPasswordField.getPassword());
            if (ok) {
                if (autoLogin) {
                    Sessao.setAutoLogin();
                } else {
                    Sessao.limparAutoLogin();
                }
                fechar();
            }
        } catch (UsuarioException | SenhaException ex) {
            JAlerta.getAlertaErro(this, ex.getMessage());
            if (ex instanceof UsuarioException) {
                jTextField_Nome.selectAll();
                jTextField_Nome.requestFocusInWindow();
            } else if (ex instanceof SenhaException) {
                jPasswordField.selectAll();
                jPasswordField.requestFocusInWindow();
            }
        } finally {
            SwingUtils.habilitarCampos(getCampos());
            SwingUtils.finalizarCursorProcessamento(this);
        }
    }

    /**
     * Retorna se o login foi validado.
     *
     * @return True para verdadeiro
     */
    public boolean isOk() {
        return ok;
    }

    /**
     * Retorna os componentes de tela que devem ser bloqueados.
     *
     * @return componentes
     */
    private Component[] getCampos() {
        return new Component[]{jTextField_Nome, jPasswordField, jCheckBox_Lembrar, jButton_Entrar};
    }

    /**
     * Limpa a tela.
     */
    private void limpar() {
        jTextField_Nome.setText("");
        jPasswordField.setText("");
    }

    /**
     * Fecha a tela com Esc
     */
    @Override
    public void fechar() {
        setVisible(false);
        dispose();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jCheckBox_Lembrar = new javax.swing.JCheckBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Login");
        setMaximumSize(new java.awt.Dimension(380, 150));
        setMinimumSize(new java.awt.Dimension(380, 150));
        setPreferredSize(new java.awt.Dimension(380, 150));

        jPanel_North.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel_North.setMaximumSize(new java.awt.Dimension(1000, 30));
        jPanel_North.setMinimumSize(new java.awt.Dimension(200, 30));
        jPanel_North.setPreferredSize(new java.awt.Dimension(400, 30));
        jPanel_North.setLayout(new java.awt.BorderLayout());

        jLabel_Icon.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel_Icon.setIcon(new javax.swing.ImageIcon(getClass().getResource("/base/icones/key_24.png"))); // NOI18N
        jLabel_Icon.setMaximumSize(new java.awt.Dimension(30, 30));
        jLabel_Icon.setMinimumSize(new java.awt.Dimension(30, 30));
        jLabel_Icon.setPreferredSize(new java.awt.Dimension(30, 30));
        jPanel_North.add(jLabel_Icon, java.awt.BorderLayout.WEST);

        jPanel_Titulo.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 10, 1, 1));
        jPanel_Titulo.setMaximumSize(new java.awt.Dimension(1000, 36));
        jPanel_Titulo.setMinimumSize(new java.awt.Dimension(200, 36));
        jPanel_Titulo.setPreferredSize(new java.awt.Dimension(400, 36));
        jPanel_Titulo.setLayout(new java.awt.BorderLayout());

        jLabel_Titulo.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel_Titulo.setText("Acesso ao sistema");
        jPanel_Titulo.add(jLabel_Titulo, java.awt.BorderLayout.CENTER);

        jPanel_North.add(jPanel_Titulo, java.awt.BorderLayout.CENTER);

        getContentPane().add(jPanel_North, java.awt.BorderLayout.NORTH);

        jPanel_Center.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createTitledBorder(""), javax.swing.BorderFactory.createEmptyBorder(10, 10, 10, 10)));
        jPanel_Center.setLayout(new javax.swing.BoxLayout(jPanel_Center, javax.swing.BoxLayout.Y_AXIS));

        jPanel_Nome.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jPanel_Nome.setMaximumSize(new java.awt.Dimension(500, 22));
        jPanel_Nome.setMinimumSize(new java.awt.Dimension(100, 22));
        jPanel_Nome.setPreferredSize(new java.awt.Dimension(500, 22));
        jPanel_Nome.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 1));

        jLabel_Nome.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabel_Nome.setText("Usuário:");
        jLabel_Nome.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabel_Nome.setMaximumSize(new java.awt.Dimension(60, 16));
        jLabel_Nome.setMinimumSize(new java.awt.Dimension(60, 16));
        jLabel_Nome.setPreferredSize(new java.awt.Dimension(60, 16));
        jPanel_Nome.add(jLabel_Nome);

        jTextField_Nome.setMaximumSize(new java.awt.Dimension(250, 20));
        jTextField_Nome.setMinimumSize(new java.awt.Dimension(250, 20));
        jTextField_Nome.setPreferredSize(new java.awt.Dimension(250, 20));
        jPanel_Nome.add(jTextField_Nome);

        jPanel_Center.add(jPanel_Nome);

        jPanel_Nome1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jPanel_Nome1.setMaximumSize(new java.awt.Dimension(500, 22));
        jPanel_Nome1.setMinimumSize(new java.awt.Dimension(100, 22));
        jPanel_Nome1.setPreferredSize(new java.awt.Dimension(500, 22));
        jPanel_Nome1.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 1));

        jLabel_Senha.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabel_Senha.setText("Senha:");
        jLabel_Senha.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabel_Senha.setMaximumSize(new java.awt.Dimension(60, 16));
        jLabel_Senha.setMinimumSize(new java.awt.Dimension(60, 16));
        jLabel_Senha.setPreferredSize(new java.awt.Dimension(60, 16));
        jPanel_Nome1.add(jLabel_Senha);

        jPasswordField.setMaximumSize(new java.awt.Dimension(250, 20));
        jPasswordField.setMinimumSize(new java.awt.Dimension(250, 20));
        jPasswordField.setPreferredSize(new java.awt.Dimension(250, 20));
        jPanel_Nome1.add(jPasswordField);

        jPanel_Center.add(jPanel_Nome1);

        jPanel_Nome2.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jPanel_Nome2.setMaximumSize(new java.awt.Dimension(500, 22));
        jPanel_Nome2.setMinimumSize(new java.awt.Dimension(100, 22));
        jPanel_Nome2.setPreferredSize(new java.awt.Dimension(500, 22));
        jPanel_Nome2.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 1));

        jLabel_Espaco.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabel_Espaco.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabel_Espaco.setMaximumSize(new java.awt.Dimension(50, 16));
        jLabel_Espaco.setMinimumSize(new java.awt.Dimension(50, 16));
        jLabel_Espaco.setPreferredSize(new java.awt.Dimension(50, 16));
        jPanel_Nome2.add(jLabel_Espaco);

        jCheckBox_Lembrar.setText("Lembrar");
        jCheckBox_Lembrar.setToolTipText("Na próxima vez, irá entrar automaticamente neste usuário");
        jCheckBox_Lembrar.setMaximumSize(new java.awt.Dimension(90, 23));
        jCheckBox_Lembrar.setMinimumSize(new java.awt.Dimension(90, 23));
        jCheckBox_Lembrar.setPreferredSize(new java.awt.Dimension(90, 23));
        jPanel_Nome2.add(jCheckBox_Lembrar);

        jPanel_Center.add(jPanel_Nome2);

        getContentPane().add(jPanel_Center, java.awt.BorderLayout.CENTER);

        jPanel_South.setMaximumSize(new java.awt.Dimension(1000, 36));
        jPanel_South.setMinimumSize(new java.awt.Dimension(200, 36));
        jPanel_South.setPreferredSize(new java.awt.Dimension(400, 36));

        jButton_Entrar.setText("Acessar");
        jButton_Entrar.setMaximumSize(new java.awt.Dimension(95, 23));
        jButton_Entrar.setMinimumSize(new java.awt.Dimension(95, 23));
        jButton_Entrar.setPreferredSize(new java.awt.Dimension(95, 23));
        jButton_Entrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_EntrarActionPerformed(evt);
            }
        });
        jPanel_South.add(jButton_Entrar);

        getContentPane().add(jPanel_South, java.awt.BorderLayout.SOUTH);

        setSize(new java.awt.Dimension(396, 189));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton_EntrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_EntrarActionPerformed
        new Thread(() -> {
            validarLogin();
        }, "Validação de Login").start();
    }//GEN-LAST:event_jButton_EntrarActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private final javax.swing.JButton jButton_Entrar = new javax.swing.JButton();
    private javax.swing.JCheckBox jCheckBox_Lembrar;
    private final javax.swing.JLabel jLabel_Espaco = new javax.swing.JLabel();
    private final javax.swing.JLabel jLabel_Icon = new javax.swing.JLabel();
    private final javax.swing.JLabel jLabel_Nome = new javax.swing.JLabel();
    private final javax.swing.JLabel jLabel_Senha = new javax.swing.JLabel();
    private final javax.swing.JLabel jLabel_Titulo = new javax.swing.JLabel();
    private final javax.swing.JPanel jPanel_Center = new javax.swing.JPanel();
    private final javax.swing.JPanel jPanel_Nome = new javax.swing.JPanel();
    private final javax.swing.JPanel jPanel_Nome1 = new javax.swing.JPanel();
    private final javax.swing.JPanel jPanel_Nome2 = new javax.swing.JPanel();
    private final javax.swing.JPanel jPanel_North = new javax.swing.JPanel();
    private final javax.swing.JPanel jPanel_South = new javax.swing.JPanel();
    private final javax.swing.JPanel jPanel_Titulo = new javax.swing.JPanel();
    private final javax.swing.JPasswordField jPasswordField = new javax.swing.JPasswordField();
    private final javax.swing.JTextField jTextField_Nome = new javax.swing.JTextField();
    // End of variables declaration//GEN-END:variables
}
