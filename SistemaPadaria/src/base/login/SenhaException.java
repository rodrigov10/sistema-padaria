package base.login;

/**
 * Define exceções da {link Sessao} do sistema quando forem relacionadas à
 * senha do {link Usuario}.
 *
 * @author Rodrigo Vianna
 * @since 27/04/2019
 */
public class SenhaException extends Exception {

    public SenhaException() {
    }

    public SenhaException(String message) {
        super(message);
    }

}
