package base.login;

import base.Criptografia;
import base.Propriedades;
import base.bd.BDException;
import base.log.Log;
import base.msg.JAlerta;
import base.usuario.TipoUsuario;
import base.usuario.Usuario;
import base.usuario.UsuarioBD;
import java.io.IOException;

/**
 * Classe responsável pela sessão de usuário em todo o sistema.
 *
 * @author Rodrigo Vianna
 * @since 24/04/2019
 */
public class Sessao {

    /**
     * Define o nome de usuário master.
     */
    private static final String USER_MASTER = "Administrador";
    /**
     * Define a senha do usuário master.
     */
    private static final char[] SENHA_MASTER = new char[]{'P', 'a', 'd', 'a', 'r', 'i', 'a', '1', '0'};

    /**
     * Define se o usuário da sessão logado é master.
     */
    private static boolean usuarioLogadoMaster = false;

    /**
     * Define o usuário logado no sistema.
     */
    private static Usuario usuarioLogado;

    /**
     * Retorna o usuário logado na sessão.
     *
     * @return Usuário logado ou null.
     */
    public static Usuario getUsuarioLogado() {
        return usuarioLogado;
    }

    /**
     * Retorna se o usuário logado é master.
     *
     * @return true ou false
     */
    public static boolean isUsuarioMaster() {
        return usuarioLogadoMaster;
    }

    /**
     * Seta o usuário logado.
     *
     * @param usuarioLogado
     */
    private static void setUsuarioLogado(Usuario usuarioLogado) {
        Sessao.usuarioLogado = usuarioLogado;
    }

    /**
     * Loga um usuário no sistema a partir de sua senha e nome de usuário.
     *
     * @param usuario Nome de Usuário
     * @param senha Senha do Usuário
     * @return {@code true} caso tenha logado com sucesso.
     * @throws UsuarioException Caso o nome de usuário esteja incorreto ou não
     * exista.
     * @throws SenhaException Caso a senha esteja incorreta.
     */
    protected static boolean logarSistema(String usuario, char[] senha) throws UsuarioException, SenhaException {
        if (usuario == null || (usuario = usuario.trim()).isEmpty()) {
            throw new UsuarioException("Usuário não informado");
        }
        if (senha == null) {
            throw new SenhaException("Senha não informada");
        }
        try {
            Log.log("Realizando login...");
            if (validarMaster(usuario, senha)) {
                setUsuarioLogado(criarUsuarioMaster());
                usuarioLogadoMaster = true;
                Log.log("Login realizado com sucesso.");
                return true;
            }
            usuarioLogadoMaster = false;
            Integer id = SessaoBD.existeUsuario(usuario);
            if (id == null) {
                throw new UsuarioException("Usuario não encontrado");
            }
            if (!SessaoBD.validarSenhaMd5(id, String.valueOf(senha))) {
                throw new SenhaException("Senha inválida");
            }
            Usuario user = new Usuario(id);
            UsuarioBD.carregar(user);
            setUsuarioLogado(user);
            Log.log("Login realizado com sucesso.");
            return true;
        } catch (BDException ex) {
            JAlerta.getAlertaErro((java.awt.Dialog) null, "Erro no banco de dados: ", ex);
            return false;
        } finally {
            if (getUsuarioLogado() != null) {
                Log.log("Usuário conectado: " + getUsuarioLogado().toString());
            }
        }
    }

    /**
     * Tenta realizar o auto-login do sistema. É feito de forma silenciosa,
     * qualquer exceção será suprimida.
     *
     * @return {code true} caso seja feito o login.
     */
    protected static boolean autoLogin() {
        try {
            Log.log("Realizando auto-login...");
            String nome = Propriedades.getProperty("prop.login.lastuser");
            String senha = Propriedades.getProperty("prop.login.lastpass");
            if (nome == null || nome.isEmpty() || senha == null || senha.isEmpty()) {
                limparAutoLogin();
                return false;
            }
            if (autoValidarMaster(nome, senha)) {
                setUsuarioLogado(criarUsuarioMaster());
                usuarioLogadoMaster = true;
                Log.log("Auto-login realizado com sucesso.");
                return true;
            }
            usuarioLogadoMaster = false;
            Integer id = SessaoBD.existeUsuario(nome);
            if (id == null) {
                Log.log("Auto-login fracassado: usuário não encontrado.");
                return false;
            }
            if (!SessaoBD.validarSenha(id, senha)) {
                Log.log("Auto-login fracassado: senha inválida.");
                return false;
            }
            Usuario user = new Usuario(id);
            UsuarioBD.carregar(user);
            setUsuarioLogado(user);
            Log.log("Auto-login realizado com sucesso.");
            return true;
        } catch (IOException | BDException e) {
            Log.log("Auto-login fracassado: ", e);
        }
        return true;
    }

    /**
     * Define o Auto-Login para o usuário da sessão.
     */
    public static void setAutoLogin() {
        if (usuarioLogado == null) {
            limparAutoLogin();
            return;
        }
        Propriedades.setProperty("prop.login.autologin", "1");
        Propriedades.setProperty("prop.login.lastuser", usuarioLogado.getNomeUsuario());
        Propriedades.setProperty("prop.login.lastpass", Criptografia.getMD5(usuarioLogado.getSenha()));
    }

    /**
     * Limpa o Auto-Login dos properties.
     */
    public static void limparAutoLogin() {
        Propriedades.setProperty("prop.login.autologin", "0");
        Propriedades.setProperty("prop.login.lastuser", "");
        Propriedades.setProperty("prop.login.lastpass", "");
    }

    /**
     * Cria o usuário master.
     * <p>
     * Usuário master não é obrigatoriamente um usuário persistível no sistema,
     * sendo criado em tempo de execução. Esse usuário é utilizado pelo
     * administrador "master" do sistema, para utilizar no desenvolvimento ou
     * para testes. </p>
     *
     * @return Usuário master criado.
     */
    private static Usuario criarUsuarioMaster() {
        Usuario usuario = new Usuario();
        usuario.setId(0);
        usuario.setNomeUsuario(USER_MASTER);
        usuario.setSenha(String.valueOf(SENHA_MASTER));
        usuario.setTipoUsuario(TipoUsuario.ADMINISTRADOR);
        return usuario;
    }

    /**
     * Valida a senha e nome de usuário informados se correspondem ao do usuário
     * master.
     *
     * @param usuario Nome de Usuário
     * @param senha Senha do Usuário
     * @return {@code true} caso tenha logado com sucesso como master.
     * @throws UsuarioException Caso o nome de usuário esteja incorreto ou não
     * exista.
     * @throws SenhaException Caso a senha esteja incorreta.
     */
    private static boolean validarMaster(String usuario, char[] senha) throws UsuarioException, SenhaException {
        if (!usuario.equals(USER_MASTER)) {
            return false;
        }
        if (senha.length != SENHA_MASTER.length) {
            throw new SenhaException("Senha inválida");
        }
        for (int i = 0; i < senha.length; i++) {
            if (senha[i] != SENHA_MASTER[i]) {
                throw new SenhaException("Senha inválida");
            }
        }
        return true;
    }

    /**
     * Valida a senha e nome de usuário informados se correspondem ao do usuário
     * master. É feito de forma silenciosa
     *
     * @param usuario Nome de Usuário
     * @param senha Senha do Usuário
     * @return {@code true} caso tenha logado com sucesso como master.
     */
    private static boolean autoValidarMaster(String usuario, String senhaMd5) {
        if (!usuario.equals(USER_MASTER)) {
            return false;
        }
        return senhaMd5.equals(Criptografia.getMD5(String.valueOf(SENHA_MASTER)));
    }
}
