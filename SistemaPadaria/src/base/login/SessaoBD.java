package base.login;

import base.Criptografia;
import base.bd.BDException;
import base.bd.Conexao;
import base.bd.SQL;
import base.usuario.UsuarioBD;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Rodrigo Vianna
 * @since 24/04/2019
 */
class SessaoBD {
    
    static Integer existeUsuario(String nomeUsuario) throws BDException {
        SQL sql = new SQL("SELECT id FROM " + UsuarioBD.TBL + " WHERE nomeusuario = :nomeusuario");
        sql.setParametro("nomeusuario", nomeUsuario);
        try (Conexao c = Conexao.getConexao()) {
            try (ResultSet rs = c.executeQuery(sql)) {
                if (rs.next()) {
                    return rs.getInt("id");
                }
                return null;
            } catch (SQLException ex) {
                throw new BDException(ex);
            }
        }
    }
    
    static boolean validarSenhaMd5(Integer idUsuario, String senha) throws BDException {
        SQL sql = new SQL("SELECT CASE WHEN senha = :senha THEN 1 ELSE 0 END i FROM " + UsuarioBD.TBL);
        sql.add("WHERE id = :id");
        sql.setParametro("id", idUsuario);
        sql.setParametro("senha", Criptografia.getMD5(senha));
        try (Conexao c = Conexao.getConexao()) {
            try (ResultSet rs = c.executeQuery(sql)) {
                if (rs.next()) {
                    return rs.getInt("i") == 1;
                }
                return false;
            } catch (SQLException ex) {
                throw new BDException(ex);
            }
        }
    }
    
    static boolean validarSenha(Integer idUsuario, String senha) throws BDException {
        SQL sql = new SQL("SELECT CASE WHEN senha = :senha THEN 1 ELSE 0 END i FROM " + UsuarioBD.TBL);
        sql.add("WHERE id = :id");
        sql.setParametro("id", idUsuario);
        sql.setParametro("senha", senha);
        try (Conexao c = Conexao.getConexao()) {
            try (ResultSet rs = c.executeQuery(sql)) {
                if (rs.next()) {
                    return rs.getInt("i") == 1;
                }
                return false;
            } catch (SQLException ex) {
                throw new BDException(ex);
            }
        }
    }
    
}
