package base.login;

/**
 * Define exceções da {link Sessao} do sistema quando forem relacionadas ao
 * nome do {link Usuario}.
 *
 * @author Rodrigo Vianna
 * @since 27/04/2019
 */
public class UsuarioException extends Exception {

    public UsuarioException() {
    }

    public UsuarioException(String message) {
        super(message);
    }

}
