package base.msg;

import java.awt.Window;
import base.log.Log;
import base.utils.SwingUtils;
import javax.swing.ImageIcon;

/**
 * Janela de alertas do sistema. Monstra mensagens ao usuário e manda para o log
 * do sistema. Pode apenas exibir mensagens, mostrar exceções ou pedir uma
 * cofirmação ao usuário.
 *
 * @author Rodrigo Vianna
 * @since 23/03/2019
 */
public class JAlerta extends javax.swing.JDialog {

    /**
     * Define se a janela está no modo confirmação.
     */
    private boolean modoConfirmacao = false;

    /**
     * Booleano que define se o usuário confirmou ou não, quando for uma janela
     * do tipo confirmação.
     */
    private boolean confirmado = false;

    /**
     * Cria uma classe de alerta.
     *
     * @param parent Tela pai
     * @param modal Se o sistema irá esperar a janela fechar para continuar a
     * execução.
     * @param mensagem Mensagem de alerta
     */
    private JAlerta(java.awt.Window parent, boolean modal, String mensagem) {
        this(parent, modal, mensagem, null);
    }

    /**
     * Cria uma classe de alerta.
     *
     * @param parent Tela pai
     * @param modal Se o sistema irá esperar a janela fechar para continuar a
     * execução.
     * @param mensagem Mensagem de alerta
     * @param ex Exceção a ser exibida, se existir
     */
    private JAlerta(java.awt.Window parent, boolean modal, String mensagem, Exception ex) {
        super(parent, modal ? ModalityType.APPLICATION_MODAL : ModalityType.MODELESS);
        initComponents();
        inicializar(mensagem, ex);
    }

    /**
     * Inicializa a tela
     */
    private void inicializar(String mensagem, Exception ex) {
        SwingUtils.inicializarTela(this);

        if (ex != null) {
            jLabel_Msg.setText("<html>" + mensagem + "<br>" + ex.getMessage() + "</html>");
        } else {
            jLabel_Msg.setText("<html>" + mensagem + "</html>");
        }
        Log.log(mensagem, ex);
    }

    /**
     * Retorna uma janela de alerta do tipo erro, sem exibir a descrição do
     * erro.
     *
     * @param parent Tela pai
     * @param mensagem Mensagem
     */
    public static void getAlertaErro(Window parent, String mensagem) {
        getAlertaErro(parent, mensagem, null);
    }

    /**
     * Retorna uma janela de alerta do tipo erro, exibindo a descrição da
     * exceção.
     *
     * @param parent Tela pai
     * @param mensagem Mensagem
     * @param ex Exceção
     */
    public static void getAlertaErro(Window parent, String mensagem, Exception ex) {
        JAlerta jAlerta = new JAlerta(parent, true, mensagem, ex);
        jAlerta.jLabel_Titulo.setText("Erro");
        jAlerta.setTitle("Erro");
        jAlerta.jLabel_Icon.setIcon(new ImageIcon(jAlerta.getClass().getResource("/base/icones/alert_24.png")));
        jAlerta.jButton_Cancelar.setVisible(false);
        jAlerta.setVisible(true);
    }

    /**
     * Retorna uma janela de alerta que exibe uma mensagem para o usuário.
     *
     * @param parent Tela pai
     * @param mensagem Mensagem
     */
    public static void getAlertaMsg(Window parent, String mensagem) {
        getAlertaMsg(parent, mensagem, "Mensagem");
    }

    /**
     * Retorna uma janela de alerta que exibe uma mensagem para o usuário.
     *
     * @param parent Tela pai
     * @param mensagem Mensagem
     * @param titulo Título da janela
     */
    public static void getAlertaMsg(Window parent, String mensagem, String titulo) {
        JAlerta jAlerta = new JAlerta(parent, true, mensagem);
        jAlerta.jLabel_Titulo.setText(titulo);
        jAlerta.setTitle(titulo);
        jAlerta.jLabel_Icon.setIcon(new ImageIcon(jAlerta.getClass().getResource("/base/icones/info_24.png")));
        jAlerta.jButton_Cancelar.setVisible(false);
        jAlerta.setVisible(true);
    }

    /**
     * Retorna uma janela que exibe uma confirmação para ser escolhida pelo
     * usuário.
     *
     * @param parent Tela pai
     * @param mensagem Mensagem
     * @return true ou false
     */
    public static boolean getAlertaConfirmacao(Window parent, String mensagem) {
        return getAlertaConfirmacao(parent, mensagem, "Confirmar");
    }

    /**
     * Retorna uma janela que exibe uma confirmação para ser escolhida pelo
     * usuário.
     *
     * @param parent Tela pai
     * @param mensagem Mensagem
     * @param titulo Título da janela
     * @return true ou false
     */
    public static boolean getAlertaConfirmacao(Window parent, String mensagem, String titulo) {
        JAlerta jAlerta = new JAlerta(parent, true, mensagem);
        jAlerta.modoConfirmacao = true;
        jAlerta.setTitle(titulo);
        jAlerta.jLabel_Icon.setIcon(new ImageIcon(jAlerta.getClass().getResource("/base/icones/question_24.png")));
        jAlerta.jLabel_Titulo.setText(titulo);
        jAlerta.setVisible(true);
        return jAlerta.confirmado;
    }

    /**
     * Fecha a tela.
     */
    private void fechar() {
        setVisible(false);
        dispose();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Alerta");
        setMaximumSize(new java.awt.Dimension(1000, 700));
        setMinimumSize(new java.awt.Dimension(400, 170));
        setPreferredSize(new java.awt.Dimension(400, 170));

        jPanel_North.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel_North.setMaximumSize(new java.awt.Dimension(1000, 30));
        jPanel_North.setMinimumSize(new java.awt.Dimension(200, 30));
        jPanel_North.setPreferredSize(new java.awt.Dimension(400, 30));
        jPanel_North.setLayout(new java.awt.BorderLayout());

        jLabel_Icon.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel_Icon.setMaximumSize(new java.awt.Dimension(30, 30));
        jLabel_Icon.setMinimumSize(new java.awt.Dimension(30, 30));
        jLabel_Icon.setPreferredSize(new java.awt.Dimension(30, 30));
        jPanel_North.add(jLabel_Icon, java.awt.BorderLayout.WEST);

        jPanel_Titulo.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 10, 1, 1));
        jPanel_Titulo.setMaximumSize(new java.awt.Dimension(1000, 36));
        jPanel_Titulo.setMinimumSize(new java.awt.Dimension(200, 36));
        jPanel_Titulo.setPreferredSize(new java.awt.Dimension(400, 36));
        jPanel_Titulo.setLayout(new java.awt.BorderLayout());

        jLabel_Titulo.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel_Titulo.setText("Título");
        jPanel_Titulo.add(jLabel_Titulo, java.awt.BorderLayout.CENTER);

        jPanel_North.add(jPanel_Titulo, java.awt.BorderLayout.CENTER);

        getContentPane().add(jPanel_North, java.awt.BorderLayout.NORTH);

        jPanel_Center.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createTitledBorder(""), javax.swing.BorderFactory.createEmptyBorder(10, 10, 10, 10)));
        jPanel_Center.setLayout(new java.awt.BorderLayout());

        jLabel_Msg.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabel_Msg.setText("Texto");
        jLabel_Msg.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jPanel_Center.add(jLabel_Msg, java.awt.BorderLayout.CENTER);

        getContentPane().add(jPanel_Center, java.awt.BorderLayout.CENTER);

        jPanel_South.setMaximumSize(new java.awt.Dimension(1000, 36));
        jPanel_South.setMinimumSize(new java.awt.Dimension(200, 36));
        jPanel_South.setPreferredSize(new java.awt.Dimension(400, 36));

        jButton_OK.setText("Ok");
        jButton_OK.setMaximumSize(new java.awt.Dimension(55, 23));
        jButton_OK.setMinimumSize(new java.awt.Dimension(55, 23));
        jButton_OK.setPreferredSize(new java.awt.Dimension(55, 23));
        jButton_OK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_OKActionPerformed(evt);
            }
        });
        jPanel_South.add(jButton_OK);

        jButton_Cancelar.setText("Não");
        jButton_Cancelar.setMaximumSize(new java.awt.Dimension(55, 23));
        jButton_Cancelar.setMinimumSize(new java.awt.Dimension(55, 23));
        jButton_Cancelar.setPreferredSize(new java.awt.Dimension(55, 23));
        jButton_Cancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_CancelarActionPerformed(evt);
            }
        });
        jPanel_South.add(jButton_Cancelar);

        getContentPane().add(jPanel_South, java.awt.BorderLayout.SOUTH);

        setSize(new java.awt.Dimension(416, 209));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton_OKActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_OKActionPerformed
        if (modoConfirmacao) {
            confirmado = true;
        }
        fechar();
    }//GEN-LAST:event_jButton_OKActionPerformed

    private void jButton_CancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_CancelarActionPerformed
        fechar();
    }//GEN-LAST:event_jButton_CancelarActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private final javax.swing.JButton jButton_Cancelar = new javax.swing.JButton();
    private final javax.swing.JButton jButton_OK = new javax.swing.JButton();
    private final javax.swing.JLabel jLabel_Icon = new javax.swing.JLabel();
    private final javax.swing.JLabel jLabel_Msg = new javax.swing.JLabel();
    private final javax.swing.JLabel jLabel_Titulo = new javax.swing.JLabel();
    private final javax.swing.JPanel jPanel_Center = new javax.swing.JPanel();
    private final javax.swing.JPanel jPanel_North = new javax.swing.JPanel();
    private final javax.swing.JPanel jPanel_South = new javax.swing.JPanel();
    private final javax.swing.JPanel jPanel_Titulo = new javax.swing.JPanel();
    // End of variables declaration//GEN-END:variables
}
