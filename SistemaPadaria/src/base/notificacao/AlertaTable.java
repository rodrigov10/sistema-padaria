package base.notificacao;

import base.TableModelGenerico;
import java.awt.event.KeyAdapter;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import javax.swing.JTable;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author Rodrigo Vianna
 * @since 17/06/2019
 */
public class AlertaTable {

    public static AlertaTable alerta;

    public static AlertaTable getAlerta() {
        if (alerta == null) {
            alerta = new AlertaTable();
        }
        return alerta;
    }

    private final ArrayList<Registro> lista = new ArrayList(1);

    private JTable jTable;
    private TableModelGenerico<Registro> tableModel;

    public void setTabelaAlerta(JTable jTable) {
        this.jTable = jTable;
        inicializarTabela();
    }

    private void inicializarTabela() {
        tableModel = new TableModelGenerico(jTable.getColumnModel());
        tableModel.addColumn("Alerta");
        tableModel.addColumn("Registro");

        jTable.setModel(tableModel);

        tableModel.getColumn("Alerta").setPreferredWidth(50);
        jTable.removeColumn(tableModel.getColumn("Registro"));

        jTable.setOpaque(false);

        TableRowSorter sorter = new TableRowSorter(tableModel);
        jTable.setRowSorter(sorter);

        jTable.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getButton() == MouseEvent.BUTTON3 && jTable.getSelectedRow() != -1) {
                    removerRegistro(tableModel.getObjeto(jTable.getSelectedRow()));
                } else if (e.getButton() == MouseEvent.BUTTON1 && e.getClickCount() == 2) {
                    Registro r = tableModel.getObjeto(jTable.getSelectedRow());
                    r.acao.run();
                    removerRegistro(r);
                }
            }

        });
    }

    public void adicionarRegistro(Registro... registro) {
        if (registro == null || tableModel == null) {
            return;
        }
        ArrayList<Registro> listaNotNull = new ArrayList<>(registro.length);
        for (Registro r : registro) {
            if (r != null) {
                listaNotNull.add(r);
            }
        }
        lista.addAll(listaNotNull);
        Collections.sort(lista);
        atualizarTable();
    }

    public void removerRegistro(Registro... registro) {
        if (registro == null || tableModel == null) {
            return;
        }
        ArrayList<Registro> listaNotNull = new ArrayList<>(registro.length);
        for (Registro r : registro) {
            if (r != null) {
                listaNotNull.add(r);
            }
        }
        lista.removeAll(listaNotNull);
        Collections.sort(lista);
        atualizarTable();
    }

    private void atualizarTable() {
        tableModel.reset();

        lista.stream().map((registro) -> {
            Object[] row = new Object[2];
            row[0] = registro.descricao;
            row[1] = registro;
            return row;
        }).forEachOrdered((row) -> {
            tableModel.addRow(row);
        });
    }

}
