package base.notificacao;

/**
 *
 * @author Rodrigo Vianna
 * @since 17/06/2019
 */
public class Registro implements Comparable<Registro> {

    private static long ticketGlobal = 0;

    private static synchronized long getTicket() {
        return ticketGlobal++;
    }

    public Registro(Prioridade prioridade, AcaoAoSelecionar acao, String descricao) {
        this.ticket = getTicket();
        this.prioridade = prioridade;
        this.acao = acao;
        this.descricao = descricao;
    }

    public final long ticket;

    public final Prioridade prioridade;

    public final AcaoAoSelecionar acao;

    public final String descricao;

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 43 * hash + (int) (this.ticket ^ (this.ticket >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Registro other = (Registro) obj;
        return this.ticket == other.ticket;
    }

    @Override
    public int compareTo(Registro o) {
        if (o == null) {
            return 1;
        }
        if (o.prioridade == null && this.prioridade == null) {
            return 0;
        }
        if (o.prioridade != null && this.prioridade == null) {
            return -1;
        }
        if (o.prioridade == null && this.prioridade != null) {
            return 1;
        }
        if (o.prioridade.prio == this.prioridade.prio) {
            return 0;
        }
        if (o.prioridade.prio > this.prioridade.prio) {
            return -1;
        }
        return 1;
    }

    public static enum Prioridade {
        ALTA((byte) 2), MEDIA((byte) 1), BAIXA((byte) 0);

        public final byte prio;

        private Prioridade(byte prio) {
            this.prio = prio;
        }

    }

    public static abstract class AcaoAoSelecionar {

        public abstract void run();
    }

}
