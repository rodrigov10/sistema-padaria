package base.relatorio;

import base.Sistema;
import static base.Sistema.SEP;
import base.log.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

/**
 *
 * @author Rodrigo Vianna
 * @since 11/06/2019
 */
public class AutoReport {

    /**
     * Propriedades da empresa.
     */
    private static Properties propriedades;

    /**
     * Local do arquivo de propriedades.
     */
    private static final String PROPERTIES_FILE = Sistema.USER_LOCAL_SYSTEM + SEP + "empresa.properties";

    /**
     * Valida as propriedades da empresa.
     *
     * @throws IOException
     */
    private static void validarProperties() {
        if (propriedades == null) {
            configurarProperties();
        }
    }

    /**
     * Configura o arquivo de propriedades e o cria caso não exista.
     */
    public static void configurarProperties() {
        propriedades = new Properties();
        File fileProp = new File(PROPERTIES_FILE);
        if (!fileProp.exists()) {
            criarProperties(fileProp);
        }
        try (FileInputStream file = new FileInputStream(fileProp)) {
            propriedades.load(file);
        } catch (Exception e) {
        }
    }

    /**
     * Cria o arquivo de propriedades.
     *
     * @param fileProp Arquivo de propriedades
     * @throws FileNotFoundException
     * @throws IOException
     */
    private static void criarProperties(File fileProp) {
        propriedades.setProperty("empresa.nome", "");
        propriedades.setProperty("empresa.endereco", "");
        propriedades.setProperty("empresa.cidade", "");
        propriedades.setProperty("empresa.contato", "");
        try (FileOutputStream fos = new FileOutputStream(fileProp)) {
            propriedades.store(fos, "Propriedades do Sistema");
        } catch (FileNotFoundException ex) {
            Log.log("Erro: ", ex);
        } catch (IOException ex) {
            Log.log("Erro: ", ex);
        }
    }

    /**
     *
     * @param propertie Chave da propriedade
     * @param defaultValue Valor padrão caso não encontre um valor no arquivo.
     * @return String do valor da propriedade
     * @throws IOException
     */
    private static String getProperty(String propertie, String defaultValue) {
        validarProperties();
        return propriedades.getProperty(propertie, defaultValue);
    }

    /**
     * Seta uma propriedade e salvo no arquivo.
     *
     * @param chave Chave da propriedade.
     * @param valor Valor da propriedade.
     */
    private static void setProperty(String chave, String valor) {
        try {
            validarProperties();
            propriedades.setProperty(chave, valor);
            try (FileOutputStream fos = new FileOutputStream(PROPERTIES_FILE)) {
                propriedades.store(fos, "Propriedades do Sistema");
            }
        } catch (IOException ex) {
            //Ignora
        }

    }

    public static String getNomeEmpresa() {
        return getProperty("empresa.nome", "Nome da Empresa");
    }

    public static String getEnderecoEmpresa() {
        return getProperty("empresa.endereco", "Endereço da Empresa");
    }

    public static String getCidadeEmpresa() {
        return getProperty("empresa.cidade", "Endereço da Empresa");
    }

    public static String getTelefoneEmpresa() {
        return getProperty("empresa.contato", "Contato da Empresa");
    }

    public static void setNomeEmpresa(String s) {
        setProperty("empresa.nome", s);
    }

    public static void setEnderecoEmpresa(String s) {
        setProperty("empresa.endereco", s);
    }

    public static void setCidadeEmpresa(String s) {
        setProperty("empresa.cidade", s);
    }

    public static void setTelefoneEmpresa(String s) {
        setProperty("empresa.contato", s);
    }

}
