package base.rotina;

import base.cache.RotinaCache;
import base.log.Log;
import com.encomenda.RotinaEncomenda;

/**
 *
 * @author Rodrigo Vianna
 * @since 14/06/2019
 */
public class RotinaManager {

    public static void iniciar() {
        Log.log("[ROTINAS]Iniciando Rotinas:");
        Log.log("[ROTINAS]Rotina de Encomendas...");
        new Thread(new RotinaEncomenda(), "Rotina de Encomendas").start();
        Log.log("[ROTINAS]Rotina de Limpeza de Cache...");
        new Thread(new RotinaCache(), "Rotina de Limpeza de Cache").start();
    }

}
