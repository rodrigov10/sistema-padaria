package base.usuario;

/**
 *
 * @author Rodrigo Vianna
 * @since 31/05/2019
 */
public class CampoSenhaException extends Exception {

    private final boolean senhaDeConfirmacao;
    
    CampoSenhaException(String message, boolean senhaDeConfirmacao) {
        super(message);
        this.senhaDeConfirmacao = senhaDeConfirmacao;
    }
    
    public boolean isSenhaDeConfirmacao(){
        return senhaDeConfirmacao;
    }

}
