package base.usuario;

import base.CampoException;
import base.bd.BDException;
import base.msg.JAlerta;
import base.utils.FieldFormatUtils;
import base.utils.SwingUtils;
import java.awt.Component;
import java.awt.event.ItemEvent;

/**
 *
 * @author Rodrigo
 * @since 29/05/2019
 */
public class JUsuario extends javax.swing.JFrame {

    private static JUsuario jUsuario;

    public static JUsuario getInstance() {
        if (jUsuario == null) {
            jUsuario = new JUsuario();
        }
        jUsuario.limparTela();
        return jUsuario;
    }

    private Usuario usuario;

    private JUsuarioConsulta jUsuarioConsulta;

    private boolean processando = false;

    private boolean atualizarTabela = false;

    private JUsuario() {
        initComponents();
        inicializar();
    }

    private void inicializar() {
        SwingUtils.inicializarTela(this);

        FieldFormatUtils.formatarSemEspaco(jTFNome);
        jCBTipoUsuario.addItem(TipoUsuario.ADMINISTRADOR);
        jCBTipoUsuario.addItem(TipoUsuario.NORMAL);

        limparTela();
    }

    ////////////////////////////////////////////////////////////////////////////
    //Usuario
    ////////////////////////////////////////////////////////////////////////////
    private void setUsuario(Usuario usuario) {
        try {
            processando = true;

            jPasswordField_Senha.setText("");
            jPasswordField_Confirmar.setText("");

            this.usuario = usuario;
            jTFNome.setText(usuario == null ? "" : usuario.getNomeUsuario());
            jCBTipoUsuario.setSelectedItem(usuario == null ? TipoUsuario.ADMINISTRADOR : usuario.getTipoUsuario());
            jCheckBox_Ativo.setSelected(usuario == null || usuario.isAtivo());

            if (usuario != null && usuario.isMaster()) {
                SwingUtils.desabilitarCampos(jBGravar, jBExcluir, jBAlterarSenha, jCheckBox_Ativo);
            } else {
                SwingUtils.habilitarCampos(jBGravar, jBExcluir, jBAlterarSenha, jCheckBox_Ativo);
            }

        } finally {
            processando = false;
        }
    }

    private void salvar() {
        try {
            SwingUtils.iniciarCursorProcessamento(this);
            SwingUtils.desabilitarCampos(getCampos());

            if (usuario == null) {
                char[] senha = jPasswordField_Senha.getPassword();
                try {
                    UsuarioController.validarSenha(senha, jPasswordField_Confirmar.getPassword());
                } catch (CampoSenhaException e) {
                    JAlerta.getAlertaErro(this, e.getMessage());
                    if (e.isSenhaDeConfirmacao()) {
                        jPasswordField_Confirmar.requestFocusInWindow();
                    } else {
                        jPasswordField_Senha.requestFocusInWindow();
                    }
                }
                usuario = new Usuario();
                usuario.setSenha(String.valueOf(senha));
            }

            String nome = jTFNome.getText().trim();
            if (!nome.isEmpty()) {
                usuario.setNomeUsuario(nome);
            }
            usuario.setTipoUsuario(jCBTipoUsuario.getItemAt(jCBTipoUsuario.getSelectedIndex()));
            usuario.setAtivo(jCheckBox_Ativo.isSelected());

            try {
                UsuarioController.salvar(usuario);
                atualizarTabela = true;
                JAlerta.getAlertaMsg(this, "Salvo com sucesso.");
            } catch (BDException ex) {
                usuario = null;
                JAlerta.getAlertaErro(this, "Erro ao salvar usuario: ", ex);
            } catch (CampoException ex) {
                usuario = null;
                JAlerta.getAlertaErro(this, ex.getMessage());
            }
        } finally {
            SwingUtils.finalizarCursorProcessamento(this);
            SwingUtils.habilitarCampos(getCampos());
        }
    }

    private void excluir() {
        if (usuario == null) {
            JAlerta.getAlertaMsg(this, "Usuario não informada.");
            return;
        }
        try {
            SwingUtils.iniciarCursorProcessamento(this);
            SwingUtils.desabilitarCampos(getCampos());

            UsuarioController.excluir(usuario.getId());
            atualizarTabela = true;
            limparTela();
        } catch (BDException ex) {
            JAlerta.getAlertaErro(this, "Erro ao buscar usuario: ", ex);
        } finally {
            SwingUtils.finalizarCursorProcessamento(this);
            SwingUtils.habilitarCampos(getCampos());
        }
    }

    private void consultar() {
        try {
            SwingUtils.iniciarCursorProcessamento(this);

            if (jUsuarioConsulta == null) {
                jUsuarioConsulta = JUsuarioConsulta.getInstance(this);
                jUsuarioConsulta.atualizarTabelaThread();
            } else if (atualizarTabela) {
                atualizarTabela = false;
                jUsuarioConsulta.atualizarTabelaThread();
            }
            jUsuarioConsulta.setVisible(true);
            if (jUsuarioConsulta.isRetornar()) {
                Usuario usuario = jUsuarioConsulta.getUsuario();
                setUsuario(usuario);
            }
        } catch (BDException ex) {
            JAlerta.getAlertaErro(this, "Erro ao consultar usuario: ", ex);
            setUsuario(null);
        } finally {
            SwingUtils.finalizarCursorProcessamento(this);
        }
    }

    private void alterarAtivo(ItemEvent evt) {
        try {
            SwingUtils.iniciarCursorProcessamento(this);
            SwingUtils.desabilitarCampos(jCheckBox_Ativo);

            UsuarioController.alterarStatusAtivo(usuario, evt.getStateChange() == ItemEvent.SELECTED);
        } catch (BDException ex) {
            JAlerta.getAlertaErro(this, "Erro ao alterar status: ", ex);
        } finally {
            SwingUtils.finalizarCursorProcessamento(this);
            SwingUtils.habilitarCampos(jCheckBox_Ativo);
        }
    }

    private void alterarSenha(char[] senha, char[] confirmacao) {
        try {
            SwingUtils.iniciarCursorProcessamento(this);
            SwingUtils.desabilitarCampos(jCheckBox_Ativo);

            UsuarioController.alterarSenha(usuario, senha, confirmacao);
        } catch (BDException ex) {
            JAlerta.getAlertaErro(this, "Erro ao alterar senha: ", ex);
        } catch (CampoSenhaException ex) {
            JAlerta.getAlertaErro(this, ex.getMessage());
            if (ex.isSenhaDeConfirmacao()) {
                jPasswordField_Confirmar.requestFocusInWindow();
            } else {
                jPasswordField_Senha.requestFocusInWindow();
            }
        } finally {
            SwingUtils.finalizarCursorProcessamento(this);
            SwingUtils.habilitarCampos(jCheckBox_Ativo);
        }
    }

    private Component[] getCampos() {
        return new Component[]{jBGravar, jBConsultar, jBLimpar, jBExcluir, jBAlterarSenha, jCheckBox_Ativo};
    }

    public void limparTela() {
        setUsuario(null);
    }

    public static void main(String[] args) {
        if (com.Start.iniciarSistema()) {
            new JUsuario().setVisible(true);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Cadastro de Usuário");
        setMaximumSize(new java.awt.Dimension(1000, 700));
        setMinimumSize(new java.awt.Dimension(455, 305));
        setPreferredSize(new java.awt.Dimension(455, 305));

        jPFundo.setLayout(new java.awt.BorderLayout());

        jPPessoal.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Pessoal", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 1, 14))); // NOI18N
        jPPessoal.setPreferredSize(new java.awt.Dimension(450, 491));
        jPPessoal.setLayout(new javax.swing.BoxLayout(jPPessoal, javax.swing.BoxLayout.Y_AXIS));

        jPNome.setMaximumSize(new java.awt.Dimension(32767, 32));
        jPNome.setMinimumSize(new java.awt.Dimension(10, 22));
        jPNome.setPreferredSize(new java.awt.Dimension(438, 32));
        jPNome.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 6));

        jLNome.setText("Nome:");
        jLNome.setMaximumSize(new java.awt.Dimension(50, 20));
        jLNome.setMinimumSize(new java.awt.Dimension(50, 20));
        jLNome.setPreferredSize(new java.awt.Dimension(50, 20));
        jPNome.add(jLNome);

        jTFNome.setMaximumSize(new java.awt.Dimension(250, 20));
        jTFNome.setMinimumSize(new java.awt.Dimension(250, 20));
        jTFNome.setPreferredSize(new java.awt.Dimension(250, 20));
        jPNome.add(jTFNome);

        jCheckBox_Ativo.setText("Ativo");
        jCheckBox_Ativo.setMaximumSize(new java.awt.Dimension(80, 23));
        jCheckBox_Ativo.setMinimumSize(new java.awt.Dimension(80, 23));
        jCheckBox_Ativo.setPreferredSize(new java.awt.Dimension(80, 23));
        jCheckBox_Ativo.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jCheckBox_AtivoItemStateChanged(evt);
            }
        });
        jPNome.add(jCheckBox_Ativo);

        jPPessoal.add(jPNome);

        jPCPF.setMaximumSize(new java.awt.Dimension(32767, 32));
        jPCPF.setMinimumSize(new java.awt.Dimension(10, 22));
        jPCPF.setPreferredSize(new java.awt.Dimension(438, 32));
        jPCPF.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 6));

        jLTipo.setText("Tipo:");
        jLTipo.setMaximumSize(new java.awt.Dimension(50, 20));
        jLTipo.setMinimumSize(new java.awt.Dimension(50, 20));
        jLTipo.setPreferredSize(new java.awt.Dimension(50, 20));
        jPCPF.add(jLTipo);

        jCBTipoUsuario.setMaximumSize(new java.awt.Dimension(100, 20));
        jCBTipoUsuario.setMinimumSize(new java.awt.Dimension(100, 20));
        jCBTipoUsuario.setPreferredSize(new java.awt.Dimension(100, 20));
        jPCPF.add(jCBTipoUsuario);

        jPPessoal.add(jPCPF);

        jPContato.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Alterar Senha", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 1, 14))); // NOI18N
        jPContato.setLayout(new javax.swing.BoxLayout(jPContato, javax.swing.BoxLayout.Y_AXIS));

        jPTelefone.setMaximumSize(new java.awt.Dimension(438, 32));
        jPTelefone.setMinimumSize(new java.awt.Dimension(438, 32));
        jPTelefone.setPreferredSize(new java.awt.Dimension(438, 32));
        jPTelefone.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 6));

        jLTelefone1.setText("Nova senha:");
        jLTelefone1.setMaximumSize(new java.awt.Dimension(80, 20));
        jLTelefone1.setMinimumSize(new java.awt.Dimension(80, 20));
        jLTelefone1.setPreferredSize(new java.awt.Dimension(80, 20));
        jPTelefone.add(jLTelefone1);

        jPasswordField_Senha.setMaximumSize(new java.awt.Dimension(250, 20));
        jPasswordField_Senha.setMinimumSize(new java.awt.Dimension(250, 20));
        jPasswordField_Senha.setPreferredSize(new java.awt.Dimension(250, 20));
        jPTelefone.add(jPasswordField_Senha);

        jPContato.add(jPTelefone);

        jPTelefone2.setMaximumSize(new java.awt.Dimension(438, 32));
        jPTelefone2.setMinimumSize(new java.awt.Dimension(438, 32));
        jPTelefone2.setPreferredSize(new java.awt.Dimension(438, 32));
        jPTelefone2.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 6));

        jLTelefone2.setText("Confirmar:");
        jLTelefone2.setMaximumSize(new java.awt.Dimension(80, 20));
        jLTelefone2.setMinimumSize(new java.awt.Dimension(80, 20));
        jLTelefone2.setPreferredSize(new java.awt.Dimension(80, 20));
        jPTelefone2.add(jLTelefone2);

        jPasswordField_Confirmar.setMaximumSize(new java.awt.Dimension(250, 20));
        jPasswordField_Confirmar.setMinimumSize(new java.awt.Dimension(250, 20));
        jPasswordField_Confirmar.setPreferredSize(new java.awt.Dimension(250, 20));
        jPTelefone2.add(jPasswordField_Confirmar);

        jBAlterarSenha.setText("Alterar");
        jBAlterarSenha.setMaximumSize(new java.awt.Dimension(80, 23));
        jBAlterarSenha.setMinimumSize(new java.awt.Dimension(80, 23));
        jBAlterarSenha.setPreferredSize(new java.awt.Dimension(80, 23));
        jBAlterarSenha.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBAlterarSenhaActionPerformed(evt);
            }
        });
        jPTelefone2.add(jBAlterarSenha);

        jPContato.add(jPTelefone2);

        jPPessoal.add(jPContato);

        jPFundo.add(jPPessoal, java.awt.BorderLayout.WEST);

        getContentPane().add(jPFundo, java.awt.BorderLayout.CENTER);

        jPTitulo.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createEmptyBorder(10, 10, 10, 10), javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED)));

        jLTitulo.setFont(new java.awt.Font("Arial", 1, 24)); // NOI18N
        jLTitulo.setText("Cadastro de Usuário");
        jPTitulo.add(jLTitulo);

        getContentPane().add(jPTitulo, java.awt.BorderLayout.NORTH);

        jBGravar.setMnemonic('G');
        jBGravar.setText("Gravar");
        jBGravar.setMaximumSize(new java.awt.Dimension(95, 23));
        jBGravar.setMinimumSize(new java.awt.Dimension(95, 23));
        jBGravar.setPreferredSize(new java.awt.Dimension(95, 23));
        jBGravar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBGravarActionPerformed(evt);
            }
        });
        jPBotoes.add(jBGravar);

        jBConsultar.setMnemonic('C');
        jBConsultar.setText("Consultar");
        jBConsultar.setMaximumSize(new java.awt.Dimension(95, 23));
        jBConsultar.setMinimumSize(new java.awt.Dimension(95, 23));
        jBConsultar.setPreferredSize(new java.awt.Dimension(95, 23));
        jBConsultar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBConsultarActionPerformed(evt);
            }
        });
        jPBotoes.add(jBConsultar);

        jBLimpar.setMnemonic('L');
        jBLimpar.setText("Limpar");
        jBLimpar.setMaximumSize(new java.awt.Dimension(95, 23));
        jBLimpar.setMinimumSize(new java.awt.Dimension(95, 23));
        jBLimpar.setPreferredSize(new java.awt.Dimension(95, 23));
        jBLimpar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBLimparActionPerformed(evt);
            }
        });
        jPBotoes.add(jBLimpar);

        jBExcluir.setMnemonic('E');
        jBExcluir.setText("Excluir");
        jBExcluir.setMaximumSize(new java.awt.Dimension(95, 23));
        jBExcluir.setMinimumSize(new java.awt.Dimension(95, 23));
        jBExcluir.setPreferredSize(new java.awt.Dimension(95, 23));
        jBExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBExcluirActionPerformed(evt);
            }
        });
        jPBotoes.add(jBExcluir);

        getContentPane().add(jPBotoes, java.awt.BorderLayout.SOUTH);

        setSize(new java.awt.Dimension(471, 344));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jBLimparActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBLimparActionPerformed
        limparTela();
    }//GEN-LAST:event_jBLimparActionPerformed

    private void jBExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBExcluirActionPerformed
        new Thread(() -> {
            excluir();
        }, "Excluir Usuario").start();
    }//GEN-LAST:event_jBExcluirActionPerformed

    private void jBGravarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBGravarActionPerformed
        new Thread(() -> {
            salvar();
        }, "Gravar Usuario").start();
    }//GEN-LAST:event_jBGravarActionPerformed

    private void jCheckBox_AtivoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jCheckBox_AtivoItemStateChanged
        if (processando) {
            return;
        }
        new Thread(() -> {
            alterarAtivo(evt);
        }, "Alterar Status Ativo").start();
    }//GEN-LAST:event_jCheckBox_AtivoItemStateChanged

    private void jBConsultarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBConsultarActionPerformed
        consultar();
    }//GEN-LAST:event_jBConsultarActionPerformed

    private void jBAlterarSenhaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBAlterarSenhaActionPerformed
        if (processando) {
            return;
        }
        new Thread(() -> {
            alterarSenha(jPasswordField_Senha.getPassword(), jPasswordField_Confirmar.getPassword());
        }, "Alterar Senha do Usuário").start();
    }//GEN-LAST:event_jBAlterarSenhaActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private final javax.swing.JButton jBAlterarSenha = new javax.swing.JButton();
    private final javax.swing.JButton jBConsultar = new javax.swing.JButton();
    private final javax.swing.JButton jBExcluir = new javax.swing.JButton();
    private final javax.swing.JButton jBGravar = new javax.swing.JButton();
    private final javax.swing.JButton jBLimpar = new javax.swing.JButton();
    private final javax.swing.JComboBox<TipoUsuario> jCBTipoUsuario = new javax.swing.JComboBox<>();
    private final javax.swing.JCheckBox jCheckBox_Ativo = new javax.swing.JCheckBox();
    private final javax.swing.JLabel jLNome = new javax.swing.JLabel();
    private final javax.swing.JLabel jLTelefone1 = new javax.swing.JLabel();
    private final javax.swing.JLabel jLTelefone2 = new javax.swing.JLabel();
    private final javax.swing.JLabel jLTipo = new javax.swing.JLabel();
    private final javax.swing.JLabel jLTitulo = new javax.swing.JLabel();
    private final javax.swing.JPanel jPBotoes = new javax.swing.JPanel();
    private final javax.swing.JPanel jPCPF = new javax.swing.JPanel();
    private final javax.swing.JPanel jPContato = new javax.swing.JPanel();
    private final javax.swing.JPanel jPFundo = new javax.swing.JPanel();
    private final javax.swing.JPanel jPNome = new javax.swing.JPanel();
    private final javax.swing.JPanel jPPessoal = new javax.swing.JPanel();
    private final javax.swing.JPanel jPTelefone = new javax.swing.JPanel();
    private final javax.swing.JPanel jPTelefone2 = new javax.swing.JPanel();
    private final javax.swing.JPanel jPTitulo = new javax.swing.JPanel();
    private final javax.swing.JPasswordField jPasswordField_Confirmar = new javax.swing.JPasswordField();
    private final javax.swing.JPasswordField jPasswordField_Senha = new javax.swing.JPasswordField();
    private final javax.swing.JTextField jTFNome = new javax.swing.JTextField();
    // End of variables declaration//GEN-END:variables
}
