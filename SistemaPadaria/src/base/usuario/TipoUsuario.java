package base.usuario;

import base.bd.EnumID;

/**
 *
 * @author Carlos
 * @since 17/04/2019
 */
public enum TipoUsuario implements EnumID {
    ADMINISTRADOR(0, "Administrador"),
    NORMAL(1, "Normal");

    public final Integer codigo;
    public final String nome;

    private TipoUsuario(Integer codigo, String nome) {
        this.nome = nome;
        this.codigo = codigo;
    }

    public static TipoUsuario getTipoUsuario(Integer codigo) {
        for (TipoUsuario tipo : values()) {
            if (tipo.codigo.equals(codigo)) {
                return tipo;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return nome;
    }

    @Override
    public Integer getCodigo() {
        return this.codigo;
    }

}
