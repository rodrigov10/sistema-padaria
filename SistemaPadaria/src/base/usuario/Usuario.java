package base.usuario;

import base.Criptografia;
import base.bd.ObjetoID;
import base.cache.CacheObjeto;
import java.util.Objects;

/**
 *
 * @author Carlos
 * @since 17/04/2019
 */
public class Usuario implements ObjetoID, CacheObjeto<Integer> {

    private Integer id;
    private String senha;
    private String nomeUsuario;
    private TipoUsuario tipoUsuario;
    private boolean ativo;
    private boolean master = false;

    public Usuario(Integer id) {
        this.id = id;
    }

    public Usuario() {
    }

    public TipoUsuario getTipoUsuario() {
        return tipoUsuario;
    }

    public void setTipoUsuario(TipoUsuario tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public String getSenha() {
        return senha;
    }

    protected void setSenhaSemMD5(String senha) {
        this.senha = senha;
    }

    public void setSenha(String senha) {
        this.senha = senha == null ? null : Criptografia.getMD5(senha);
    }

    public String getNomeUsuario() {
        return nomeUsuario;
    }

    public void setNomeUsuario(String nomeUsuario) {
        this.nomeUsuario = nomeUsuario;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    protected void setMaster(boolean master) {
        this.master = master;
    }

    public boolean isMaster() {
        return master;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 79 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Usuario other = (Usuario) obj;
        return Objects.equals(this.id, other.id);
    }

    @Override
    public String toString() {
        return nomeUsuario + (master ? " [MASTER]" : "");
    }

}
