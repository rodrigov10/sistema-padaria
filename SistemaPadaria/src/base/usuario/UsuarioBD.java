package base.usuario;

import base.bd.BDException;
import base.bd.Conexao;
import base.bd.SQL;
import base.cache.CacheManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Rodrigo Vianna
 * @since 24/04/2019
 */
public class UsuarioBD {

    public static final String TBL = "tabusuario";
    
    private static final CacheManager<Usuario> CACHE = CacheManager.getInstance(Usuario.class);

    private static Integer getNovoId() throws BDException {
        SQL sql = new SQL("SELECT nextval('tabusuario_id_seq') seq");
        try (Conexao c = Conexao.getConexao()) {
            try (ResultSet rs = c.executeQuery(sql)) {
                if (rs.next()) {
                    return rs.getInt("seq");
                }
            } catch (SQLException ex) {
                throw new BDException(ex);
            }
        }
        return null;
    }

    public static void salvar(Usuario usuario) throws BDException {
        usuario.setId(getNovoId());
        SQL sql = new SQL();
        sql.add("INSERT INTO " + TBL);
        sql.add("(id, senha, nomeusuario, tipo, ativo)");
        sql.add("VALUES(:id,:senha,:nomeusuario,:tipo,:ativo)");
        sql.setParametro("id", usuario);
        sql.setParametro("senha", usuario.getSenha());
        sql.setParametro("nomeusuario", usuario.getNomeUsuario());
        sql.setParametro("tipo", usuario.getTipoUsuario());
        sql.setParametro("ativo", usuario.isAtivo());
        Conexao.update(sql);
        CACHE.put(usuario);
    }

    public static void alterar(Usuario usuario) throws BDException {
        SQL sql = new SQL();
        sql.add("UPDATE " + TBL);
        sql.add("SET nomeusuario = :nomeusuario, tipo = :tipo, ativo = :ativo");
        sql.add("WHERE id = :id");
        sql.setParametro("id", usuario);
        sql.setParametro("nomeusuario", usuario.getNomeUsuario());
        sql.setParametro("tipo", usuario.getTipoUsuario());
        sql.setParametro("ativo", usuario.isAtivo());
        Conexao.update(sql);
        CACHE.put(usuario);
    }

    public static void excluir(long id) throws BDException {
        SQL sql = new SQL("DELETE FROM " + TBL + " WHERE id = :id");
        sql.setParametro("id", id);
        Conexao.update(sql);
        CACHE.remove(id);
    }

    public static void carregar(Usuario usuario) throws BDException {
        Usuario objCache = CACHE.get(usuario.getId());
        if (objCache != null) {
            carregarObjeto(usuario, objCache);
            return;
        }
        SQL sql = new SQL("SELECT * FROM " + TBL + " WHERE id = :id");
        sql.setParametro("id", usuario);
        try (Conexao c = Conexao.getConexao()) {
            try (ResultSet rs = c.executeQuery(sql)) {
                while (rs.next()) {
                    carregarObjeto(usuario, rs);
                }
            } catch (SQLException ex) {
                throw new BDException(ex);
            }
        }
    }
    private static void carregarObjeto(Usuario usuario, Usuario cache) {
        usuario.setId(cache.getId());
        usuario.setNomeUsuario(cache.getNomeUsuario());
        usuario.setSenhaSemMD5(cache.getSenha());
        usuario.setTipoUsuario(cache.getTipoUsuario());
        usuario.setAtivo(cache.isAtivo());
        usuario.setMaster(cache.isMaster());
    }

    private static void carregarObjeto(Usuario usuario, ResultSet rs) throws SQLException {
        usuario.setNomeUsuario(rs.getString("nomeusuario"));
        usuario.setSenhaSemMD5(rs.getString("senha"));
        usuario.setTipoUsuario(TipoUsuario.getTipoUsuario(rs.getInt("tipo")));
        usuario.setAtivo(rs.getBoolean("ativo"));
        usuario.setMaster(rs.getBoolean("master"));
    }

    public static List<Usuario> listar(UsuarioSC filtro) throws BDException {
        SQL sql = new SQL();
        sql.add("SELECT *");
        sql.add("FROM " + TBL);
        sql.add("WHERE TRUE");
        if (filtro.nome != null && !filtro.nome.isEmpty()) {
            sql.add("AND nome LIKE :nomeusuario");
            sql.setParametro("nomeusuario", filtro.nome + "%");
        }
        if (filtro.ativo != null && filtro.ativo) {
            sql.add("AND ativo = true");
        }
        try (Conexao c = Conexao.getConexao()) {
            try (ResultSet rs = c.executeQuery(sql)) {
                List<Usuario> lista = new ArrayList<>();
                while (rs.next()) {
                    Usuario usuario = new Usuario(rs.getInt("id"));
                    carregarObjeto(usuario, rs);
                    lista.add(usuario);
                }
                return lista;
            } catch (SQLException ex) {
                throw new BDException(ex);
            }
        }
    }

    public static void alterarStatusAtivo(Integer id, boolean ativo) throws BDException {
        SQL sql = new SQL();
        sql.add("UPDATE " + TBL);
        sql.add("SET ativo = :ativo");
        sql.add("WHERE id = :id");
        sql.setParametro("id", id);
        sql.setParametro("ativo", ativo);
        Conexao.update(sql);
    }

    public static void alterarSenha(Integer id, String senha) throws BDException {
        SQL sql = new SQL();
        sql.add("UPDATE " + TBL);
        sql.add("SET senha = :senha");
        sql.add("WHERE id = :id");
        sql.setParametro("id", id);
        sql.setParametro("senha", senha);
        Conexao.update(sql);
    }

}
