package base.usuario;

import base.CampoException;
import base.Criptografia;
import base.bd.BDException;

/**
 *
 * @author Rodrigo
 * @since 31/05/2019
 */
public class UsuarioController {

    public static void salvar(Usuario usuario) throws BDException, CampoException {
        if (usuario.getNomeUsuario() == null || usuario.getNomeUsuario().isEmpty()) {
            throw new CampoException("Nome não informado");
        }
        if (usuario.getId() == null) {
            UsuarioBD.salvar(usuario);
        } else {
            if (usuario.getNomeUsuario() == null) {
                throw new CampoException("Nome não informado");
            }
            UsuarioBD.alterar(usuario);
        }
    }

    public static void excluir(Integer id) throws BDException {
        UsuarioBD.excluir(id);
    }

    public static boolean alterarStatusAtivo(Usuario usuario, boolean status) throws BDException {
        if (usuario == null || usuario.getId() == null) {
            return false;
        }
        UsuarioBD.alterarStatusAtivo(usuario.getId(), status);
        usuario.setAtivo(status);
        return true;
    }

    public static boolean alterarSenha(Usuario usuario, char[] senha, char[] confirmacao) throws BDException, CampoSenhaException {
        if (usuario == null || usuario.getId() == null) {
            return false;
        }
        validarSenha(senha, confirmacao);
        String novaSenha = Criptografia.getMD5(String.valueOf(senha));
        UsuarioBD.alterarSenha(usuario.getId(), novaSenha);
        usuario.setSenhaSemMD5(novaSenha);
        return true;
    }

    protected static void validarSenha(char[] senha, char[] confirmacao) throws CampoSenhaException {
        if (senha == null || senha.length == 0) {
            throw new CampoSenhaException("Senha não informada.", false);
        }
        if (confirmacao == null || confirmacao.length == 0) {
            throw new CampoSenhaException("Confirme a senha.", true);
        }
        if (senha.length != confirmacao.length) {
            throw new CampoSenhaException("Senhas diferentes", false);
        }
        for (int i = 0; i < senha.length; i++) {
            if (senha[i] != confirmacao[i]) {
                throw new CampoSenhaException("Senhas diferentes, verifique.", false);
            }
        }
    }
}
