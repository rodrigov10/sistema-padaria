package base.utils;

import java.text.ParseException;
import javax.swing.text.MaskFormatter;

/**
 *
 * @author Rodrigo Vianna
 * @since 22/05/2019
 */
public class CPFUtils {

    public static final String MASK_CPF = "###.###.###-##";

    public static String desformatar(String string) {
        return NumberUtils.desformatar(string);
    }

    public static String formatar(String string) {
        if (string == null || string.isEmpty()) {
            return "";
        }
        try {
            MaskFormatter maskFormatter = new MaskFormatter(MASK_CPF);
            maskFormatter.setValueContainsLiteralCharacters(false);
            maskFormatter.setPlaceholderCharacter(' ');
            return maskFormatter.valueToString(string);
        } catch (ParseException e) {
            return string;
        }
    }
}
