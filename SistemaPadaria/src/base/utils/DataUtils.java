package base.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Rodrigo Vianna
 * @since 13/03/2019
 */
public class DataUtils {

    public static final String FORMATO_COMPETENCIA = "MM/yyyy";

    public static final String FORMATO_DATA = "dd/MM/yyyy";
    public static final String FORMATO_DATA_REDUZIDO = "dd/MM/yy";

    public static final String FORMATO_DATA_EX = "dd 'de' MMMMM 'de' yyyy";
    public static final String FORMATO_DATA_COMPLETO_EX = "EEEE',' dd 'de' MMMMM 'de' yyyy";

    public static final String FORMATO_DATAHORA = "dd/MM/yyyy HH:mm";
    public static final String FORMATO_DATAHORASEG = "dd/MM/yyyy HH:mm:ss";

    public static final String FORMATO_LOGNAME = "EEEE_dd-MM-yy_HH'h'";
    
    public static final String FORMATO_DATEBD = "yyyy-MM-dd";
    public static final String FORMATO_TIMESTAMPBD = "yyyy-MM-dd HH:mm:ss";

    /**
     * Transforma uma data em String no padrão dd/MM/yyyy.
     *
     * @see DataUtils#FORMATO_DATA
     * @param data Data a ser transformada
     * @return String da data
     */
    public static String dataToString(Date data) {
        return dataToString(data, FORMATO_DATA);
    }

    /**
     * Transfomra uma data em String a partir de um formato.
     *
     * @param formato Formato de data para transformar.
     * @param data Data a ser transformada
     * @return String da data
     */
    public static String dataToString(Date data, String formato) {
        if (data == null) {
            return "";
        }
        if (formato == null) {
            formato = FORMATO_DATA;
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat(formato);
        return dateFormat.format(data);
    }
    
    public static Date incrementaDia(Date data, int quantidade) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(data);
        calendar.add(java.util.Calendar.DAY_OF_MONTH, quantidade);
        return calendar.getTime();
    }

}
