package base.utils;

/**
 * Interface para ser utilizada em telas do sistema.
 * <p>
 * Em {@link SwingUtils#inicializarTela(java.awt.Window)
 * } adiciona um evento de tecla para que seja executado o método que fecha a
 * tela, caso implemente essa interface.</p>
 *
 * @author Rodrigo
 */
public interface FecharEsc {

    /**
     * Permite fechar a tela implementada com a tecla Esc.
     *
     * @see SwingUtils#inicializarTela(java.awt.Window)
     */
    void fechar();
}
