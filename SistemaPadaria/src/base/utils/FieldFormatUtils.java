package base.utils;

import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.text.ParseException;
import javax.swing.JFormattedTextField;
import javax.swing.JTextField;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.JTextComponent;
import javax.swing.text.MaskFormatter;
import javax.swing.text.PlainDocument;

/**
 *
 * @author Rodrigo Vianna
 * @since 22/05/2019
 */
public class FieldFormatUtils {

    /**
     * Formata para permitir apenas números
     *
     * @param textField passa textField
     */
    public static void formatarDigitos(JTextField textField) {
        textField.addKeyListener(new KeyNumbers());
        textField.addFocusListener(new FocoAdapter());
    }

    public static void formatarMonetario(JTextField textField) {
        textField.setDocument(new MonetarioDocument());
        textField.addFocusListener(new FocoAdapter());
    }

    public static void formatarCep(JFormattedTextField textField) {
        try {
            MaskFormatter maskFormatter = new MaskFormatter(CEPUtils.MASK_CPF);
            maskFormatter.setValueContainsLiteralCharacters(false);
            maskFormatter.setPlaceholderCharacter(' ');
            textField.setFormatterFactory(new DefaultFormatterFactory(maskFormatter));
        } catch (ParseException ex) {
        }
        textField.addFocusListener(new FocoAdapter());
    }

    public static void formatarCpf(JFormattedTextField textField) {
        try {
            MaskFormatter maskFormatter = new MaskFormatter(CPFUtils.MASK_CPF);
            maskFormatter.setValueContainsLiteralCharacters(false);
            maskFormatter.setPlaceholderCharacter(' ');
            textField.setFormatterFactory(new DefaultFormatterFactory(maskFormatter));
        } catch (ParseException ex) {
        }
        textField.addFocusListener(new FocoAdapter());
    }

    public static void formatarCnpj(JFormattedTextField textField) {
        try {
            MaskFormatter maskFormatter = new MaskFormatter(CNPJUtils.MASK_CNPJ);
            maskFormatter.setValueContainsLiteralCharacters(false);
            maskFormatter.setPlaceholderCharacter(' ');
            textField.setFormatterFactory(new DefaultFormatterFactory(maskFormatter));
        } catch (ParseException ex) {
        }
        textField.addFocusListener(new FocoAdapter());
    }

    public static void formatarTelefone(JFormattedTextField textField) {
        if (textField == null) {
            return;
        }
        textField.addKeyListener(new MascaraTelefone(textField));
        textField.addFocusListener(new FocoAdapter());
    }

    public static void formatarSemEspaco(JTextField textField) {
        textField.addKeyListener(new KeySemEspaco());
    }

    private static class KeySemEspaco extends KeyAdapter {

        @Override
        public void keyTyped(KeyEvent e) {
            if (e.getKeyCode() == 32) {
                e.consume();
            }
        }
    }

    private static class FocoAdapter extends FocusAdapter {

        @Override
        public void focusGained(FocusEvent e) {
            if (e.getComponent() instanceof JTextComponent) {
                ((JTextComponent) e.getComponent()).selectAll();
            }
        }
    }

    private static class KeyNumbers extends KeyAdapter {

        @Override
        public void keyTyped(KeyEvent e) {
            if (!Character.isDigit(e.getKeyChar())) {
                e.consume();
            }
        }

    }

    private static class MonetarioDocument extends PlainDocument {

        private static final int MAX = 12;
        private static final int DIGITOS = 2;

        @Override
        public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
            String texto = getText(0, getLength());
            str = NumberUtils.desformatar(str);
            for (int i = 0; i < str.length(); i++) {
                char c = str.charAt(i);
                if (!Character.isDigit(c)) {
                    return;
                }
            }
            if (texto.length() < MAX) {
                super.remove(0, getLength());
                texto = texto.replace(".", "").replace(",", "");
                StringBuilder s = new StringBuilder(texto + str);
                //Remove zeros à esquerda
                if (s.length() > 0) {
                    for (int i = 0; i < s.length(); i++) {
                        if (s.charAt(0) == '0') {
                            s.deleteCharAt(0);
                        } else {
                            break;
                        }
                    }
                }

                //Impede que o campo fique vazio
                if (s.length() < 3) {
                    if (s.length() < 1) {
                        s.insert(0, "000");
                    } else if (s.length() < 2) {
                        s.insert(0, "00");
                    } else {
                        s.insert(0, "0");
                    }
                }
                //Adiciona vírgula nos decimais
                s.insert(s.length() - DIGITOS, ",");

                //Separadores
                if (s.length() > 6) {
                    s.insert(s.length() - 6, ".");
                }
                if (s.length() > 10) {
                    s.insert(s.length() - 10, ".");
                }
                super.insertString(0, s.toString(), a);
            }
        }

        @Override
        public void remove(int offset, int length) throws BadLocationException {
            super.remove(offset, length);
            String texto = getText(0, getLength()).replace(",", "").replace(".", "");
            super.remove(0, getLength());
            insertString(0, texto, null);
        }
    }

    private static class MascaraTelefone extends KeyAdapter {

        private static final int MAX = 11;
        private final JFormattedTextField textField;

        public MascaraTelefone(JFormattedTextField textField) {
            this.textField = textField;
        }

        private String resetarText(String numero) {
            return numero.replaceAll("\\D", "");
        }

        @Override
        public void keyReleased(KeyEvent e) {
            String texto = resetarText(textField.getText());
            if (texto.length() > MAX && Character.isLetterOrDigit(e.getKeyChar())) {
                e.consume();
                return;
            }
            textField.setText(TelefoneUtils.formatar(texto));
        }

        @Override
        public void keyTyped(KeyEvent e) {
            String texto = resetarText(textField.getText());
            if (!Character.isDigit(e.getKeyChar()) || texto.length() + 1 > MAX) {
                e.consume();
            }
        }

    }

}
