package base.utils;

import java.text.DecimalFormat;

/**
 *
 * @author Rodrigo Vianna
 * @since 13/03/2019
 */
public class NumberUtils {

    public static final String MASK_D0 = "0";
    public static final String MASK_D2 = "0.##";

    /**
     * Limpa a string, deixando apenas dígitos.
     *
     * @param string passa String formatada
     * @return String só com dígitos
     */
    public static String desformatar(String string) {
        if (string == null || string.isEmpty()) {
            return "";
        }
        return string.replaceAll("\\D", "");
    }

    public static String formatar(String string) {
        return formatar(string, MASK_D2);
    }

    public static String formatar(Number number) {
        return formatar(number, MASK_D2);
    }

    public static String formatar(Object number, String pattern) {
        if (number == null) {
            return "";
        }
        try {
            return new DecimalFormat(pattern).format(number);
        } catch (IllegalArgumentException e) {
            return String.valueOf(number);
        }
    }

}
