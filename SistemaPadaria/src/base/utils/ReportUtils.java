package base.utils;

import base.bd.BDException;
import base.log.Log;
import base.msg.JAlerta;
import java.awt.Dialog;
import java.awt.Window;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import javax.swing.JDialog;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author Rodrigo Vianna
 * @since 05/06/2019
 */
public class ReportUtils {

    public static void gerarRelatorio(Window parent, ResultSet rs, JasperReport relatorio, Map parametros) throws JRException, BDException {
        gerarRelatorio(parent, rs, relatorio, parametros, "Visualização do Relatório");
    }

    public static void gerarRelatorio(Window parent, ResultSet rs, JasperReport relatorio, Map parametros, String titulo) throws JRException, BDException {
        try {
            if (!rs.isBeforeFirst()) {
                JAlerta.getAlertaMsg(parent, "Nenhum registro encontrado.");
                return;
            }
        } catch (SQLException ex) {
            throw new BDException(ex);
        }
        JasperPrint impressao = JasperFillManager.fillReport(relatorio, parametros, new JRResultSetDataSource(rs));
        JasperViewer viewer = new JasperViewer(impressao, false);

        Log.log("Gerando relatório: " + titulo + " [" + parent.getClass().toString() + ']');
        JDialog tela = new JDialog(parent, titulo, Dialog.ModalityType.APPLICATION_MODAL);
        tela.setSize(950, 650);
        tela.setLocationRelativeTo(null);
        tela.getContentPane().add(viewer.getContentPane());
        tela.setVisible(true);
    }

}
