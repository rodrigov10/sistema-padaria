package base.utils;

import base.utils.calendario.JDataField;
import java.awt.AWTKeyStroke;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.KeyboardFocusManager;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.HashSet;
import java.util.Set;
import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.FocusManager;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.text.JTextComponent;

/**
 * Classe utilitária com métdos para manipulação de janelas Swing.
 *
 * @author Rodrigo Vianna
 * @since 24/04/2019
 */
public class SwingUtils {

    /**
     * Define o cursor padrão do sistema.
     */
    private static final Cursor CURSOR_DEFAULT = new Cursor(Cursor.DEFAULT_CURSOR);
    /**
     * Define o cursor de carregamento.
     */
    private static final Cursor CURSOR_PROCESSO = new Cursor(Cursor.WAIT_CURSOR);

    /**
     * Define a ação da tecla enter que aciona ações de botão.
     */
    private static final EnterButton ENTER_ACTION_ADAPTER = new EnterButton();

    /**
     * Desabilita os campos informados
     *
     * @param components passa os components que seram desabilitados
     */
    public static void desabilitarCampos(Component... components) {
        for (Component c : components) {
            c.setFocusable(false);
            c.setEnabled(false);
        }
    }

    /**
     * Habilita os campos informados
     *
     * @param components passa os components que seram habilitados
     */
    public static void habilitarCampos(Component... components) {
        for (Component c : components) {
            c.setFocusable(true);
            c.setEnabled(true);
        }
    }

    /**
     * Altera o cursor para o modo de processamento.
     *
     * @param window passa janela
     */
    public static void iniciarCursorProcessamento(Window window) {
        if (window == null) {
            return;
        }
        window.setCursor(CURSOR_PROCESSO);
    }

    /**
     * Altera o cursor para o modo padrão.
     *
     * @param window passa janela
     */
    public static void finalizarCursorProcessamento(Window window) {
        if (window == null) {
            return;
        }
        window.setCursor(CURSOR_DEFAULT);
    }

    /**
     * Retorna a janela ativa.
     *
     * @return janela ativa
     */
    public static Window getJanelaAtiva() {
        return FocusManager.getCurrentManager().getActiveWindow();
    }

    /**
     * Retorna a janela em foco.
     *
     * @return Janela em foco.
     */
    public static Window getJanelaFocada() {
        Window[] windows = Window.getWindows();
        if (windows != null) {
            for (Window window : windows) {
                if (window.isFocused()) {
                    return window;
                }
                if (window.isActive()) {
                    return window;
                }
            }
        }
        return getJanelaAtiva();
    }

    /**
     * Inicializa a tela com os atalhos gerais do sistema e layout.
     *
     * @param window passa janela
     */
    public static void inicializarTela(Window window) {
        if (window == null) {
            return;
        }
        if (window instanceof FecharEsc) {
            inicializarKeyEvent(window, new FecharEscAdapter(window));
        } else {
            inicializarKeyEvent(window, null);
        }
    }

    /**
     * Adiciona os eventos de tecla nos componentes da tela de forma recursiva.
     *
     * @param container
     * @param escAdapter
     */
    private static void inicializarKeyEvent(Container container, FecharEscAdapter escAdapter) {
        for (Component comp : container.getComponents()) {
            comp.addKeyListener(escAdapter);
            //Possui um tratamento especial
            if (comp instanceof JDataField) {
                for (Component compField : ((Container) comp).getComponents()) {
                    alterarFonte(compField);
                    if (compField instanceof JTextComponent) {
                        alterarComponente((JTextComponent) compField);
                    } else if (compField instanceof AbstractButton) {
                        alterarFocoDirecional(compField);
                    }
                }
                continue;
            }
            if (comp instanceof Container) {
                inicializarKeyEvent((Container) comp, escAdapter);
            }
            if (comp instanceof JTextComponent) {
                alterarComponente((JTextComponent) comp);
                alterarFonte(comp);
            } else if (comp instanceof AbstractButton) {
                alterarComponente((AbstractButton) comp);
            } else if (comp instanceof JTable) {
                alterarComponente((JTable) comp);
            } else if (comp instanceof JTextComponent || comp instanceof JLabel || comp instanceof JTabbedPane || comp instanceof JComboBox) {
                alterarFonte(comp);
            }
        }
    }

    /**
     * Configura o componente do tipo {@link JTable}.
     * <p>
     * Configura o sistema de foco e a fonte.</p>
     *
     * @param comp
     */
    private static void alterarComponente(JTable comp) {
        comp.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "Enter");
        comp.getActionMap().put("Enter", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                //Para impedir que pule uma linha da tabela com Enter
            }
        });
        comp.setAutoCreateRowSorter(true);
        comp.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        alterarFonte(comp);
    }

    /**
     * Configura o componente do tipo {@link JTextComponent}.
     * <p>
     * Configura o sistema de foco e a fonte.</p>
     *
     * @param comp
     */
    private static void alterarComponente(JTextComponent comp) {
        alterarFocoEnter(comp);
        alterarFonte(comp);
    }

    /**
     * Configura o componente do tipo {@link AbstractButton}.
     * <p>
     * Configura o sistema de foco, a ação com Enter e a fonte.</p>
     *
     * @param comp
     */
    private static void alterarComponente(AbstractButton comp) {
        alterarFocoDirecional(comp);
        if (comp instanceof JButton) {
            comp.addKeyListener(ENTER_ACTION_ADAPTER);
        }
        alterarFonte(comp);
    }

    /**
     * Configura as ações das teclas direcionais Cima, Baixo, Enter e Tab para
     * que seja possível trocar de foco nos componentes da tela.
     *
     * @param comp
     */
    private static void alterarFocoEnter(Component comp) {
        Set<AWTKeyStroke> newKeystrokes = new HashSet(comp.getFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS));
        newKeystrokes.add(AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_ENTER, 0));
        newKeystrokes.add(AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_DOWN, 0));
        newKeystrokes.add(AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_KP_DOWN, 0));
        comp.setFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS, newKeystrokes);

        newKeystrokes = new HashSet(comp.getFocusTraversalKeys(KeyboardFocusManager.BACKWARD_TRAVERSAL_KEYS));
        newKeystrokes.add(AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_ENTER, InputEvent.SHIFT_DOWN_MASK));
        newKeystrokes.add(AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_UP, 0));
        newKeystrokes.add(AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_KP_UP, 0));
        comp.setFocusTraversalKeys(KeyboardFocusManager.BACKWARD_TRAVERSAL_KEYS, newKeystrokes);
    }

    /**
     * Configura as ações das teclas direcionais Cima, Baixo, e Tab para que
     * seja possível trocar de foco nos componentes da tela.
     *
     * @param comp
     */
    private static void alterarFocoDirecional(Component comp) {
        Set<AWTKeyStroke> newKeystrokes = new HashSet(comp.getFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS));
        newKeystrokes.add(AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_DOWN, 0));
        newKeystrokes.add(AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_KP_DOWN, 0));
        comp.setFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS, newKeystrokes);

        newKeystrokes = new HashSet(comp.getFocusTraversalKeys(KeyboardFocusManager.BACKWARD_TRAVERSAL_KEYS));
        newKeystrokes.add(AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_UP, 0));
        newKeystrokes.add(AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_KP_UP, 0));
        comp.setFocusTraversalKeys(KeyboardFocusManager.BACKWARD_TRAVERSAL_KEYS, newKeystrokes);
    }

    /**
     * Configura as ações das teclas direcionais Cima, Baixo, e Tab para que
     * seja possível trocar de foco nos componentes da tela. Remove as ações
     * anteriores.
     *
     * @param comp passa componente
     */
    public static void alterarFocoSemEnter(Component comp) {
        Set<AWTKeyStroke> newKeystrokes = new HashSet();
        newKeystrokes.add(AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_DOWN, 0));
        newKeystrokes.add(AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_KP_DOWN, 0));
        comp.setFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS, newKeystrokes);

        newKeystrokes = new HashSet(comp.getFocusTraversalKeys(KeyboardFocusManager.BACKWARD_TRAVERSAL_KEYS));
        newKeystrokes.add(AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_UP, 0));
        newKeystrokes.add(AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_KP_UP, 0));
        comp.setFocusTraversalKeys(KeyboardFocusManager.BACKWARD_TRAVERSAL_KEYS, newKeystrokes);
    }

    /**
     * Altera a fonte do componente.
     *
     * @param comp
     */
    private static void alterarFonte(Component component) {
        Font font = component.getFont();
        if (font == null) {
            return;
        }
        component.setFont(new Font("Arial", font.getStyle(), font.getSize()));
    }

    /**
     * Define a ação de fechar a janela com a tecla Esc.
     */
    private static class FecharEscAdapter extends KeyAdapter {

        private final Window window;

        public FecharEscAdapter(Window window) {
            this.window = window;
        }

        @Override
        public void keyPressed(KeyEvent e) {
            if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
                ((FecharEsc) window).fechar();
            }
        }
    }

    /**
     * Define a ação de chamar os eventos de {@link ActionListener} de um botão.
     */
    private static class EnterButton extends KeyAdapter {

        @Override
        public void keyPressed(KeyEvent evt) {
            if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER) {
                Component component = (Component) evt.getSource();
                if (component instanceof JButton) {
                    JButton button = (JButton) component;
                    ActionListener[] actions = button.getActionListeners();
                    if (actions != null) {
                        for (ActionListener action : actions) {
                            action.actionPerformed(new ActionEvent(button, ActionEvent.ACTION_PERFORMED, ""));
                        }
                    }
                }
            }
        }
    }

}
