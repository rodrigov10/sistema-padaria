package base.utils;

import java.text.ParseException;
import javax.swing.text.MaskFormatter;

/**
 *
 * @author Rodrigo Vianna
 * @since 22/05/2019
 */
public class TelefoneUtils {

    public static final String MASK_8_PRE = "(##) ####-####";
    public static final String MASK_9_PRE = "(##) # ####-####";
    public static final String MASK_8 = "####-####";
    public static final String MASK_9 = "# ####-####";
    public static final String MASK_0800 = "#### ### ####";

    public static String desformatar(String string) {
        return NumberUtils.desformatar(string);
    }

    public static String formatar(String string) {
        if (string == null || string.isEmpty()) {
            return "";
        }
        if (string.length() == 8) { //3821 2345
            return formatar(string, MASK_8);
        }
        if (string.length() == 9) { //9 3821 2345
            return formatar(string, MASK_9);
        }
        if (string.length() == 10) { //(35) 3821 2345
            return formatar(string, MASK_8_PRE);
        }
        if (string.length() == 11) {
            if ("0".equalsIgnoreCase(string.substring(0, 1))) {//0800 702 4000
                return formatar(string, MASK_0800);
            } else { //(35) 9 8821 2345
                return formatar(string, MASK_9_PRE);
            }
        }
        return string;
    }

    public static String formatar(String string, String mascara) {
        if (string == null || string.isEmpty()) {
            return "";
        }
        if (mascara == null || mascara.isEmpty()) {
            return TelefoneUtils.formatar(string);
        }
        try {
            MaskFormatter maskFormatter = new MaskFormatter(mascara);
            maskFormatter.setValueContainsLiteralCharacters(false);
            maskFormatter.setPlaceholderCharacter(' ');
            return maskFormatter.valueToString(string);
        } catch (ParseException e) {
            return string;
        }
    }
}
