package base.utils.calendario;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Rodrigo Vianna
 * @since 01/05/2019
 */
class CalendarUtils {
    
    private static final String FORMATO_DATA = "dd/MM/yyyy";
    
    /**
     * Transfomra uma data em String a partir de um formato.
     *
     * @param formato Formato de data para transformar.
     * @param data Data a ser transformada
     * @return String da data
     */
    public static String dataToString(Date data) {
        if (data == null) {
            return "";
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat(FORMATO_DATA);
        return dateFormat.format(data);
    }
    
    /**
     * Transfomra uma string em data
     *
     * @param data String a ser transformada
     * @return data
     */
    public static Date stringToDate(String data) {
        if (data == null || data.isEmpty()) {
            return null;
        }
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat(FORMATO_DATA);
            return dateFormat.parse(data);
        } catch (ParseException e) {
            return null;
        }
    }

    private static boolean isAnoBissexto(int ano) {
        return (ano % 400 == 0 || ano % 4 == 0 && ano % 100 != 0);
    }

    static Date incrementaMes(Date data, int quantidade) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(data);
        calendar.add(java.util.Calendar.MONTH, quantidade);
        return calendar.getTime();
    }

    static Date decrementaMes(Date data, int quantidade) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(data);
        calendar.add(java.util.Calendar.MONTH, -quantidade);
        return calendar.getTime();
    }
    
    static int getPrimeiroDiaSemana(int semanaDoAno, Date data){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(data);
        calendar.setFirstDayOfWeek(Calendar.SUNDAY);
        calendar.set(Calendar.WEEK_OF_YEAR, semanaDoAno);
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
        return calendar.get(Calendar.DAY_OF_MONTH);
    }

    static String getSemanaNoAno(Date data) {
        return new SimpleDateFormat("w").format(data);
    }

    static String getDia(Date data) {
        return new SimpleDateFormat("dd").format(data);
    }

    static String getAno(Date data) {
        return new SimpleDateFormat("yyyy").format(data);
    }

    static String getMes(Date data) {
        return new SimpleDateFormat("MM").format(data);
    }

    static String getMesExtenso(Date data) {
        return new SimpleDateFormat("MMMMM").format(data);
    }

    static Date getData(int dia, int mes, int ano) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(ano, mes, dia);
        return calendar.getTime();
    }

    enum Mes {
        JANEIRO(Calendar.JANUARY, (byte) 31), FEVEREIRO(Calendar.FEBRUARY, (byte) 28), MARCO(Calendar.MARCH, (byte) 31),
        ABRIL(Calendar.APRIL, (byte) 30), MAIO(Calendar.MAY, (byte) 31), JUNHO(Calendar.JUNE, (byte) 30),
        JULHO(Calendar.JULY, (byte) 31), AGOSTO(Calendar.AUGUST, (byte) 31), SETEMBRO(Calendar.SEPTEMBER, (byte) 30),
        OUTUBRO(Calendar.OCTOBER, (byte) 31), NOVEMBRO(Calendar.NOVEMBER, (byte) 30), DEZEMBRO(Calendar.DECEMBER, (byte) 31);

        private final int valor;
        private final byte diasMes;
        final int posicao;
        final String nome;
        final char abreviatura;

        /**
         * Construtor do Enum de Meses do ano. Cria um calendário e define um
         * mês e realiza a formatação para retornar o nome do mês. Foi definido
         * dessa forma para que seja possível pegar o nome dos meses de acordo
         * com a Internacionalização
         *
         * @see java.util.Locale
         *
         * @param valor
         */
        private Mes(int valor, byte diasMes) {
            this.valor = valor;
            this.posicao = valor + 1;
            this.diasMes = diasMes;

            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.MONTH, valor);

            this.nome = new SimpleDateFormat("MMMM").format(calendar.getTime());
            this.abreviatura = this.nome.charAt(0);
        }

        public byte getNumeroDiasMesAno() {
            return getNumeroDiasMesAno(Integer.parseInt(new SimpleDateFormat("yyyy").format(new Date())));
        }

        public byte getNumeroDiasMesAno(int ano) {
            if (valor == Calendar.FEBRUARY && isAnoBissexto(ano)) {
                return (byte) (this.diasMes + 1);
            }
            return this.diasMes;
        }

        public static Mes buscar(int valor) {
            for (Mes mes : values()) {
                if (mes.valor == valor) {
                    return mes;
                }
            }
            return null;
        }

        @Override
        public String toString() {
            return nome;
        }

    }

    enum DiaSemana {
        DOMINGO(Calendar.SUNDAY), SEGUNDA(Calendar.MONDAY), TERCA(Calendar.TUESDAY), QUARTA(Calendar.WEDNESDAY),
        QUINTA(Calendar.THURSDAY), SEXTA(Calendar.FRIDAY), SABADO(Calendar.SATURDAY);

        private final int valor;
        final int posicao;
        final String nome;
        final char abreviatura;

        /**
         * Construtor do Enum de Meses do ano. Cria um calendário e define um
         * mês e realiza a formatação para retornar o nome do mês. Foi definido
         * dessa forma para que seja possível pegar o nome dos meses de acordo
         * com a Internacionalização
         *
         * @see java.util.Locale
         *
         * @param valor
         */
        private DiaSemana(int valor) {
            this.valor = valor;
            this.posicao = valor + 1;

            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.DAY_OF_WEEK, valor);

            this.nome = new SimpleDateFormat("EEEE").format(calendar.getTime());
            this.abreviatura = this.nome.charAt(0);
        }

        public static DiaSemana buscar(int valor) {
            for (DiaSemana diaSemana : values()) {
                if (diaSemana.valor == valor) {
                    return diaSemana;
                }
            }
            return null;
        }

        public static DiaSemana buscar(Date data) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(data);
            return DiaSemana.buscar(calendar.get(Calendar.DAY_OF_WEEK));
        }

        @Override
        public String toString() {
            return nome;
        }
    }
}
