package base.utils.calendario;

import base.utils.SwingUtils;
import java.awt.AWTKeyStroke;
import java.awt.Component;
import java.awt.Container;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.swing.JButton;
import javax.swing.JLabel;

/**
 *
 * @author Rodrigo Vianna
 * @since 01/05/2019
 */
public class JDataChooser extends javax.swing.JDialog {

    private static final int MAX_LINHAS = 5;
    private static final int MAX_COLUNAS = 7;

    private final KeyAdapterWindow keyAdapterWindow = new KeyAdapterWindow();
    private final MouseAdapterWindow mouseAdapterWindow = new MouseAdapterWindow();

    private boolean isOk = false;

    private Date dataSelecionada;

    public static JDataChooser getInstance() {
        return new JDataChooser();
    }

    private JDataChooser() {
        super((java.awt.Dialog) null, true);
        initComponents();
        inicializar();
    }

    private void inicializar() {
        inicializarDiasSemana();
        SwingUtils.inicializarTela(this);
        inicializarKeyEvent(this, keyAdapterWindow, mouseAdapterWindow);

        dataSelecionada = new Date();
        popular(CalendarUtils.getMes(dataSelecionada), CalendarUtils.getAno(dataSelecionada));
    }

    private static void inicializarKeyEvent(Container container, KeyListener keyListener, MouseWheelListener mouseListener) {
        for (Component comp : container.getComponents()) {
            comp.addKeyListener(keyListener);
            comp.addMouseWheelListener(mouseListener);
            if (comp instanceof Container) {
                inicializarKeyEvent((Container) comp, keyListener, mouseListener);
            }
        }
    }

    private void popular(String mes, String ano) {
        jPanel_Dias.removeAll();
        final Date data = CalendarUtils.getData(1, Integer.parseInt(mes) - 1, Integer.parseInt(ano));

        jLabel_MesAno.setText(CalendarUtils.getMesExtenso(data) + '/' + ano);

        final int semanaAno = Integer.parseInt(CalendarUtils.getSemanaNoAno(data));
        popularSemanaAno(semanaAno);

        final CalendarUtils.DiaSemana diaSemana = CalendarUtils.DiaSemana.buscar(data);
        final byte nrDiasMesAtual = CalendarUtils.Mes.buscar(Integer.parseInt(mes) - 1).getNumeroDiasMesAno(Integer.parseInt(ano));

        JCelulaButton buttonPrimeiro = null;
        int primeiroDiaSemana = CalendarUtils.getPrimeiroDiaSemana(semanaAno, data);
        boolean iniciou = false;
        int dia = 1;
        for (int i = 0; i < MAX_LINHAS * MAX_COLUNAS; i++) {
            if (dia > nrDiasMesAtual || (!iniciou && i + 1 < diaSemana.posicao - 1)) {
                jPanel_Dias.add(new JCelulaLabel(String.valueOf(primeiroDiaSemana++)));
                continue;
            } else {
                iniciou = true;
                primeiroDiaSemana = 1;
            }
            JCelulaButton jCelulaButton = new JCelulaButton(dia++, this);
            if (buttonPrimeiro == null) {
                buttonPrimeiro = jCelulaButton;
            }
            jPanel_Dias.add(jCelulaButton);

            Set<AWTKeyStroke> newKeystrokes = new HashSet(jCelulaButton.getFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS));
            newKeystrokes.add(AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_DOWN, 0));
            newKeystrokes.add(AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_KP_DOWN, 0));
            jCelulaButton.setFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS, newKeystrokes);

            newKeystrokes = new HashSet(jCelulaButton.getFocusTraversalKeys(KeyboardFocusManager.BACKWARD_TRAVERSAL_KEYS));
            newKeystrokes.add(AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_UP, 0));
            newKeystrokes.add(AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_KP_UP, 0));
            jCelulaButton.setFocusTraversalKeys(KeyboardFocusManager.BACKWARD_TRAVERSAL_KEYS, newKeystrokes);

            jCelulaButton.addKeyListener(keyAdapterWindow);
            jCelulaButton.addMouseWheelListener(mouseAdapterWindow);
        }
        if (buttonPrimeiro != null) {
            buttonPrimeiro.requestFocusInWindow();
        }
    }

    private void inicializarDiasSemana() {
        for (CalendarUtils.DiaSemana diaSemana : CalendarUtils.DiaSemana.values()) {
            jPanel_DiasSemana.add(new JCelulaLabel(String.valueOf(diaSemana.abreviatura)));
        }
    }

    private void popularSemanaAno(int semanaAno) {
        jPanel_SemanaAno.removeAll();
        for (int i = 0; i < MAX_LINHAS; i++) {
            jPanel_SemanaAno.add(new JCelulaLabel(String.valueOf(semanaAno++)));
        }
    }

    private void avancar() {
        dataSelecionada = CalendarUtils.incrementaMes(dataSelecionada, 1);
        popular(CalendarUtils.getMes(dataSelecionada), CalendarUtils.getAno(dataSelecionada));
    }

    private void recuar() {
        dataSelecionada = CalendarUtils.decrementaMes(dataSelecionada, 1);
        popular(CalendarUtils.getMes(dataSelecionada), CalendarUtils.getAno(dataSelecionada));
    }

    private void setDiaSelecionado(int dia) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dataSelecionada);
        calendar.set(Calendar.DAY_OF_MONTH, dia);
        dataSelecionada = calendar.getTime();
        isOk = true;
        fechar();
    }

    public void setData(Date data) {
        dataSelecionada = data;
        if (dataSelecionada != null) {
            popular(CalendarUtils.getMes(dataSelecionada), CalendarUtils.getAno(dataSelecionada));
        }
    }

    public Date getData() {
        if (!isOk) {
            return null;
        }
        return dataSelecionada;
    }

    public boolean isOk() {
        return isOk;
    }

    @Override
    public void setVisible(boolean b) {
        if (b) {
            isOk = false;
        }
        super.setVisible(b);
    }

    private void fechar() {
        setVisible(false);
        dispose();
    }

    public static void main(String[] args) {
        JDataChooser.getInstance().setVisible(true);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Calendário");
        setMaximumSize(new java.awt.Dimension(300, 220));
        setMinimumSize(new java.awt.Dimension(300, 220));
        setUndecorated(true);
        setPreferredSize(new java.awt.Dimension(300, 220));
        setResizable(false);

        jPanel_MesAno.setMaximumSize(new java.awt.Dimension(300, 30));
        jPanel_MesAno.setMinimumSize(new java.awt.Dimension(300, 30));
        jPanel_MesAno.setPreferredSize(new java.awt.Dimension(300, 30));
        jPanel_MesAno.setLayout(new java.awt.BorderLayout());

        jButton_Recuar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/base/utils/calendario/img/left-arrow16px.png"))); // NOI18N
        jButton_Recuar.setMaximumSize(new java.awt.Dimension(20, 23));
        jButton_Recuar.setMinimumSize(new java.awt.Dimension(20, 23));
        jButton_Recuar.setPreferredSize(new java.awt.Dimension(20, 23));
        jButton_Recuar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_RecuarActionPerformed(evt);
            }
        });
        jPanel_MesAno.add(jButton_Recuar, java.awt.BorderLayout.WEST);

        jButton_Avancar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/base/utils/calendario/img/right-arrow16px.png"))); // NOI18N
        jButton_Avancar.setMaximumSize(new java.awt.Dimension(20, 23));
        jButton_Avancar.setMinimumSize(new java.awt.Dimension(20, 23));
        jButton_Avancar.setPreferredSize(new java.awt.Dimension(20, 23));
        jButton_Avancar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_AvancarActionPerformed(evt);
            }
        });
        jPanel_MesAno.add(jButton_Avancar, java.awt.BorderLayout.EAST);

        jLabel_MesAno.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel_MesAno.setText("Mes/Ano");
        jPanel_MesAno.add(jLabel_MesAno, java.awt.BorderLayout.CENTER);

        getContentPane().add(jPanel_MesAno, java.awt.BorderLayout.NORTH);

        jPanel_Center.setMinimumSize(new java.awt.Dimension(300, 200));
        jPanel_Center.setLayout(new java.awt.BorderLayout());

        jPanel_NorthDias.setBackground(new java.awt.Color(204, 204, 204));
        jPanel_NorthDias.setMaximumSize(new java.awt.Dimension(300, 25));
        jPanel_NorthDias.setMinimumSize(new java.awt.Dimension(300, 25));
        jPanel_NorthDias.setPreferredSize(new java.awt.Dimension(300, 25));
        jPanel_NorthDias.setLayout(new java.awt.BorderLayout());

        jPanel_EspacoDiasSemana.setBackground(new java.awt.Color(204, 204, 204));
        jPanel_EspacoDiasSemana.setMaximumSize(new java.awt.Dimension(25, 300));
        jPanel_EspacoDiasSemana.setMinimumSize(new java.awt.Dimension(25, 10));
        jPanel_EspacoDiasSemana.setPreferredSize(new java.awt.Dimension(25, 10));
        jPanel_EspacoDiasSemana.setLayout(new java.awt.GridLayout(5, 1));
        jPanel_NorthDias.add(jPanel_EspacoDiasSemana, java.awt.BorderLayout.WEST);

        jPanel_DiasSemana.setBackground(new java.awt.Color(204, 204, 204));
        jPanel_DiasSemana.setMaximumSize(new java.awt.Dimension(300, 25));
        jPanel_DiasSemana.setMinimumSize(new java.awt.Dimension(300, 25));
        jPanel_DiasSemana.setPreferredSize(new java.awt.Dimension(300, 25));
        jPanel_DiasSemana.setLayout(new java.awt.GridLayout(1, 7));
        jPanel_NorthDias.add(jPanel_DiasSemana, java.awt.BorderLayout.CENTER);

        jPanel_Center.add(jPanel_NorthDias, java.awt.BorderLayout.NORTH);

        jPanel_SemanaAno.setBackground(new java.awt.Color(204, 204, 204));
        jPanel_SemanaAno.setMaximumSize(new java.awt.Dimension(25, 300));
        jPanel_SemanaAno.setMinimumSize(new java.awt.Dimension(25, 100));
        jPanel_SemanaAno.setPreferredSize(new java.awt.Dimension(25, 100));
        jPanel_SemanaAno.setLayout(new java.awt.GridLayout(5, 1));
        jPanel_Center.add(jPanel_SemanaAno, java.awt.BorderLayout.WEST);

        jPanel_Dias.setMinimumSize(new java.awt.Dimension(300, 200));
        jPanel_Dias.setLayout(new java.awt.GridLayout(5, 7));
        jPanel_Center.add(jPanel_Dias, java.awt.BorderLayout.CENTER);

        getContentPane().add(jPanel_Center, java.awt.BorderLayout.CENTER);

        setSize(new java.awt.Dimension(300, 220));
    }// </editor-fold>//GEN-END:initComponents

    private void jButton_AvancarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_AvancarActionPerformed
        avancar();
    }//GEN-LAST:event_jButton_AvancarActionPerformed

    private void jButton_RecuarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_RecuarActionPerformed
        recuar();
    }//GEN-LAST:event_jButton_RecuarActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private final javax.swing.JButton jButton_Avancar = new javax.swing.JButton();
    private final javax.swing.JButton jButton_Recuar = new javax.swing.JButton();
    private final javax.swing.JLabel jLabel_MesAno = new javax.swing.JLabel();
    private final javax.swing.JPanel jPanel_Center = new javax.swing.JPanel();
    private final javax.swing.JPanel jPanel_Dias = new javax.swing.JPanel();
    private final javax.swing.JPanel jPanel_DiasSemana = new javax.swing.JPanel();
    private final javax.swing.JPanel jPanel_EspacoDiasSemana = new javax.swing.JPanel();
    private final javax.swing.JPanel jPanel_MesAno = new javax.swing.JPanel();
    private final javax.swing.JPanel jPanel_NorthDias = new javax.swing.JPanel();
    private final javax.swing.JPanel jPanel_SemanaAno = new javax.swing.JPanel();
    // End of variables declaration//GEN-END:variables

    private class MouseAdapterWindow extends MouseAdapter {

        @Override
        public void mouseWheelMoved(MouseWheelEvent e) {
            if (e.getWheelRotation() > 0) {
                recuar();
            } else {
                avancar();
            }
        }

    }

    private class KeyAdapterWindow extends KeyAdapter {

        @Override
        public void keyPressed(java.awt.event.KeyEvent evt) {
            if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_ESCAPE) {
                JDataChooser.this.fechar();
            }
        }
    }

    private static class ActionCelulaButton implements ActionListener {

        private final JDataChooser jDataChooser;
        private final int dia;

        public ActionCelulaButton(int dia, JDataChooser jDataChooser) {
            this.jDataChooser = jDataChooser;
            this.dia = dia;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            jDataChooser.setDiaSelecionado(dia);
        }

    }

    private static class JCelulaButton extends JButton {

        private final JDataChooser jDataChooser;

        private final int dia;

        private JCelulaButton(int dia, JDataChooser jDataChooser) {
            this.jDataChooser = jDataChooser;
            this.dia = dia;
            inicializar();
        }

        private void inicializar() {
            setText(String.valueOf(dia));
            setMargin(new java.awt.Insets(2, 1, 2, 1));
            addActionListener(new ActionCelulaButton(dia, jDataChooser));

        }

    }

    private static class JCelulaLabel extends JLabel {

        public JCelulaLabel(String text) {
            super(text);
            inicializar();
        }

        private void inicializar() {
            setHorizontalAlignment(CENTER);
        }

    }

}
