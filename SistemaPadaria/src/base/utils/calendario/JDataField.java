package base.utils.calendario;

import java.awt.Point;
import java.awt.event.KeyEvent;
import java.util.Date;

/**
 *
 * @author Rodrigo Vianna
 * @since 01/05/2019
 */
public class JDataField extends javax.swing.JPanel {

    private final JDataChooser jDataChooser = JDataChooser.getInstance();
    private Date dataSelecionada;

    public JDataField() {
        initComponents();
    }

    private Date montarData() {
        return CalendarUtils.stringToDate(jFormattedTextField_Data.getText());
    }

    private void consultar() {
        Date data = montarData();
        if (data != null) {
            jDataChooser.setData(data);
        } else {
            jDataChooser.setData(new Date());
        }
        Point point = jFormattedTextField_Data.getLocationOnScreen();
        point.y += jFormattedTextField_Data.getHeight() + 1;
        jDataChooser.setLocation(point);
        jDataChooser.setVisible(true);
        if (jDataChooser.isOk()) {
            this.dataSelecionada = jDataChooser.getData();
        }
        jFormattedTextField_Data.setText(CalendarUtils.dataToString(this.dataSelecionada));
    }

    public void setDataSelecionada(Date dataSelecionada) {
        this.dataSelecionada = dataSelecionada;
        jFormattedTextField_Data.setText(CalendarUtils.dataToString(this.dataSelecionada));
        jDataChooser.setData(dataSelecionada);
    }

    public Date getDataSelecionada() {
        return dataSelecionada;
    }

    @Override
    public void setFocusable(boolean b) {
        jButton.setFocusable(b);
        jFormattedTextField_Data.setFocusable(b);
    }

    @Override
    public void setEnabled(boolean b) {
        jButton.setEnabled(b);
        jFormattedTextField_Data.setEnabled(b);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setMaximumSize(new java.awt.Dimension(120, 20));
        setMinimumSize(new java.awt.Dimension(120, 20));
        setPreferredSize(new java.awt.Dimension(120, 20));
        setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 5, 0));

        jFormattedTextField_Data.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.DateFormatter()));
        jFormattedTextField_Data.setMaximumSize(new java.awt.Dimension(80, 20));
        jFormattedTextField_Data.setMinimumSize(new java.awt.Dimension(80, 20));
        jFormattedTextField_Data.setPreferredSize(new java.awt.Dimension(80, 20));
        jFormattedTextField_Data.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jFormattedTextField_DataFocusGained(evt);
            }
        });
        add(jFormattedTextField_Data);

        jButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/base/utils/calendario/img/calendar16px.png"))); // NOI18N
        jButton.setMargin(new java.awt.Insets(1, 1, 1, 1));
        jButton.setMaximumSize(new java.awt.Dimension(23, 20));
        jButton.setMinimumSize(new java.awt.Dimension(23, 20));
        jButton.setPreferredSize(new java.awt.Dimension(23, 20));
        jButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonActionPerformed(evt);
            }
        });
        jButton.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jButtonKeyPressed(evt);
            }
        });
        add(jButton);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonActionPerformed
        consultar();
    }//GEN-LAST:event_jButtonActionPerformed

    private void jButtonKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButtonKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (jFormattedTextField_Data.getText().isEmpty()) {
                consultar();
            }
            if (evt.getModifiers() == KeyEvent.SHIFT_MASK) {
                transferFocus();
            } else {
                transferFocusBackward();
            }
        }
    }//GEN-LAST:event_jButtonKeyPressed

    private void jFormattedTextField_DataFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jFormattedTextField_DataFocusGained
        jFormattedTextField_Data.selectAll();
    }//GEN-LAST:event_jFormattedTextField_DataFocusGained

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private final javax.swing.JButton jButton = new javax.swing.JButton();
    private final javax.swing.JFormattedTextField jFormattedTextField_Data = new javax.swing.JFormattedTextField();
    // End of variables declaration//GEN-END:variables

}
