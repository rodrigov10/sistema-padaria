package com;

import base.Propriedades;
import base.notificacao.AlertaTable;
import base.TableModelObjeto;
import base.Valor;
import base.bd.BDException;
import base.login.Sessao;
import base.msg.JAlerta;
import base.rotina.RotinaManager;
import base.usuario.JUsuario;
import base.utils.FieldFormatUtils;
import base.utils.SwingUtils;
import com.empresa.JEmpresa;
import com.encomenda.JEncomenda;
import com.pessoa.JPessoa;
import com.pessoa.JPessoaConsulta;
import com.produto.JProduto;
import com.produto.JProdutoConsulta;
import com.produto.Produto;
import com.produto.ProdutoController;
import com.relatorio.encomenda.JRelEncomenda;
import com.relatorio.pessoa.JRelPessoa;
import com.relatorio.venda.JRelVenda;
import com.venda.ItemVendaProduto;
import com.venda.JPagamento;
import com.venda.JVendaCancelamento;
import com.venda.Venda;
import java.awt.Component;
import java.awt.Container;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.math.BigDecimal;

/**
 * Classe do Menu principal. 
 *
 * @author Éderson
 * @since 27/03/2019
 */
public class JMenu extends javax.swing.JFrame {

    private JProdutoConsulta jProdutoConsulta;
    private JPessoaConsulta jPessoaConsulta;

    private Venda venda;
    private Valor precoTotal;
    private Produto produto;

    public JMenu() {
        inicializar();
    }
    
    /**
     * Método de inicialização dos componentes da classe.
     */
    
    private void inicializar() {
        //Inicializa Tela
        SwingUtils.inicializarTela(this);
        initComponents();
        inicializarTabela();
        inicializarAtalhos(this, new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                switch (e.getKeyCode()) {
                    case KeyEvent.VK_F2:
                        jBConsultaProduto.doClick();
                        break;
                    case KeyEvent.VK_F3:
                        jBConsultaCliente.doClick();
                        break;
                    case KeyEvent.VK_F4:
                        jBFinalizaVenda.doClick();
                        break;
                    case KeyEvent.VK_F5:
                        jBCancelaVenda.doClick();
                        break;
                }
            }
        });

        SwingUtils.alterarFocoSemEnter(jTFCodigo);
        SwingUtils.alterarFocoSemEnter(jTFQuantidade);
        SwingUtils.alterarFocoSemEnter(jTFPrecoUnitario);

        FieldFormatUtils.formatarMonetario(jTFPrecoUnitario);
        FieldFormatUtils.formatarDigitos(jTFQuantidade);

        setExtendedState(MAXIMIZED_BOTH);

        //Inicializa tabela de alertas
        AlertaTable.getAlerta().setTabelaAlerta(jTableAlerta);

        //Inicia as rotinas em segundo plano
        RotinaManager.iniciar();

        try {
            jCheckBox_AutoLogin.setSelected(Integer.parseInt(Propriedades.getProperty("prop.login.autologin")) == 1);
        } catch (IOException e) {
        }
        try {
            jCheckBox_AdicionarAutomaticamente.setSelected("1".equals(Propriedades.getProperty("prop.menu.itemauto", "0")));
        } catch (IOException e) {
        }
        limparTela();
    }
    
    private void inicializarAtalhos(Container container, KeyAdapter keyAdapter) {
        for (Component c : container.getComponents()) {
            c.addKeyListener(keyAdapter);
            if (c instanceof Container) {
                inicializarAtalhos((Container) c, keyAdapter);
            }
        }
    }

    /**
     *  Método para verificação de opção selecionado do auto login.
     * 
     *  Se selecionado ativa  auto login, senão limpa o auto login. 
     */
    private void alterarAutoLogin() {
        final boolean autoLogin = jCheckBox_AutoLogin.isSelected();
        if (autoLogin) {
            Sessao.setAutoLogin();
        } else {
            Sessao.limparAutoLogin();
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    //Venda
    ////////////////////////////////////////////////////////////////////////////
    private void cancelarVenda() {
        limparTela();
    }

    /**
     * Método que finaliza a venda, abrindo a janela de pagameto.
     * 
     * Se venda for null ele não faz nada.
     */
    private void finalizaVenda() {
        if (venda == null) {
            return;
        }
        JPagamento jPagamento = new JPagamento(this, venda);
        jPagamento.setVisible(true);
        if (jPagamento.isVendido() || jPagamento.isCancelado()) {
            limparTela();
        }
    }

    /**
     * Método que adiciona um novo itemVendaProduto a venda
     */
    private void adicionarItem() {
        if (produto == null) {
            return;
        }
        ItemVendaProduto itemVendaProduto = new ItemVendaProduto();

        itemVendaProduto.setProduto(produto);

        itemVendaProduto.setQuantidade(new Valor(jTFQuantidade.getText()));
        itemVendaProduto.setValor(itemVendaProduto.getQuantidade().mult(new Valor(jTFPrecoUnitario.getText())));

        if (venda == null) {
            venda = new Venda();
        }
        venda.addItemVendaProduto(itemVendaProduto);
        produto = null;
        atualizarTabela();
        limparCampos();
        jTFCodigo.selectAll();
        jTFCodigo.requestFocusInWindow();

    }

    private void inicializarTabela() {
        TableModelObjeto<Produto> tm = new TableModelObjeto(jTable.getColumnModel());
        tm.addColumn("Código");
        tm.addColumn("Descrição");
        tm.addColumn("Quantidade");
        tm.addColumn("Valor Unitário");
        tm.addColumn("Total");
        tm.addColumn("Objeto");

        jTable.setModel(tm);

        tm.getColumn("Código").setPreferredWidth(50);
        tm.getColumn("Descrição").setPreferredWidth(200);
        tm.getColumn("Quantidade").setPreferredWidth(50);
        tm.getColumn("Valor Unitário").setPreferredWidth(50);
        tm.getColumn("Total").setPreferredWidth(50);
        jTable.removeColumn(tm.getColumn("Objeto"));

        jTable.setOpaque(false);
        jSPCupom.getViewport().setOpaque(false);
        jSPCupom.setViewportBorder(null);
    }

    /**
     * Método que atualiza a tabela.
     * 
     * Limpa a tabela e insere os itemVendaProduto de Venda. 
     */
    private void atualizarTabela() {
        TableModelObjeto<Produto> tm = ((TableModelObjeto<Produto>) jTable.getModel());
        tm.reset();

        if (venda == null || venda.getItemVendaProduto().isEmpty()) {
            return;
        }

        precoTotal = new Valor(BigDecimal.ZERO);
        Valor v;
        for (ItemVendaProduto item : venda.getItemVendaProduto()) {
            Object[] row = new Object[5];
            row[0] = item.getProduto().getCodigoBarra1();
            row[1] = item.getProduto().getNome();
            row[2] = item.getQuantidade().toString();
            row[3] = item.getProduto().getValor().toString();
            v = item.getProduto().getValor().mult(item.getQuantidade());
            row[4] = v.toString();
            precoTotal = precoTotal.mais(v);
            tm.addRow(row);
        }

        jLTotalPagar.setText(precoTotal.toString());
        if (jTable.getRowCount() > 0) {
            jTable.changeSelection(0, 0, false, false);
        }
        jTable.getSelectionModel().clearSelection();

    }

    ////////////////////////////////////////////////////////////////////////////
    //Produto
    ////////////////////////////////////////////////////////////////////////////
    /**
     * Método para verificação de Produto.
     */
    private void verificarProduto() {
        if (jTFCodigo.getText().trim().isEmpty()) {
            return;
        }
        try {
            produto = new Produto();
            produto.setCodigoBarra1(jTFCodigo.getText());
            produto = ProdutoController.buscar(produto);
            if (produto == null) {
                produto = new Produto();
                produto.setCodigo(Integer.parseInt(jTFCodigo.getText()));
                produto = ProdutoController.buscar(produto);
                if (produto == null) {
                    JAlerta.getAlertaMsg(this, "Produto não existente");
                    return;
                }
            }
            jTFPrecoUnitario.setText(produto.getValor().toString());
            jLDescricaoProduto.setText(produto.getNome());
            jTFQuantidade.setText("1");
            Valor precoTotalItem = produto.getValor().mult(new Valor(jTFQuantidade.getText()));
            jLPrecoTotal.setText(String.valueOf(precoTotalItem.toString()));
            calculaTotal();
            jTFQuantidade.requestFocusInWindow();
            if (jCheckBox_AdicionarAutomaticamente.isSelected()) {
                adicionarItem();
            }
        } catch (BDException e) {
        }
    }

    /**
     *  Método que calcula o valor total da venda
     */
    private void calculaTotal() {
        Valor quantidade = new Valor(jTFQuantidade.getText());
        Valor total = (quantidade.mult(new Valor(jTFPrecoUnitario.getText())));
        jLPrecoTotal.setText(String.valueOf(total.toString()));

    }

    ////////////////////////////////////////////////////////////////////////////
    //Consultas
    ////////////////////////////////////////////////////////////////////////////
    private void consultarProduto() {
        try {

            if (jProdutoConsulta == null) {
                jProdutoConsulta = JProdutoConsulta.getInstance(this);
                jProdutoConsulta.atualizarTabelaThread();
            }
            jProdutoConsulta.setVisible(true);

            if (jProdutoConsulta.isRetornar()) {
                produto = jProdutoConsulta.getProduto();
                jTFCodigo.setText(produto.getCodigoBarra1());
                if (jTFCodigo.getText().isEmpty()) {
                    jTFCodigo.setText(produto.getCodigoBarra2());
                }
                if (jTFCodigo.getText().isEmpty()) {
                    jTFCodigo.setText(produto.getCodigo().toString());
                }
                jTFPrecoUnitario.setText(produto.getValor().toString());
                jLDescricaoProduto.setText(produto.getNome());
                jTFQuantidade.setText("1");
            }
            jTFQuantidade.requestFocusInWindow();
            calculaTotal();
            if (jCheckBox_AdicionarAutomaticamente.isSelected()) {
                adicionarItem();
            }
        } catch (BDException e) {
            JAlerta.getAlertaErro(this, "Erro ao consultar Produto: ", e);
            produto = null;
        }
    }

    private void consultarCliente() {
        if (jPessoaConsulta == null) {
            jPessoaConsulta = JPessoaConsulta.getInstance(this);
            jPessoaConsulta.setModoCliente(true);
            jPessoaConsulta.atualizarTabelaThread();
        }
        if (!jPessoaConsulta.isModoCliente()) {
            jPessoaConsulta.setModoCliente(true);
            jPessoaConsulta.atualizarTabelaThread();
        }
        jPessoaConsulta.setVisible(true);
    }

    ////////////////////////////////////////////////////////////////////////////
    //Tela
    ////////////////////////////////////////////////////////////////////////////
    private void limparCampos() {
        jLDescricaoProduto.setText("Produto");
        jLPrecoTotal.setText(Valor.VALOR_ZEROD2.toString());
        jTFCodigo.setText("");
        jTFPrecoUnitario.setText(Valor.VALOR_ZEROD2.toString());
        jTFQuantidade.setText(Valor.VALOR_ZEROD0.toString());
    }

    private void limparTela() {
        venda = null;
        produto = null;
        precoTotal = Valor.VALOR_ZEROD2;
        jLTotalPagar.setText(Valor.VALOR_ZEROD2.toString());
        limparCampos();
        atualizarTabela();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPMItemTabela = new javax.swing.JPopupMenu();
        jMRemoveItem = new javax.swing.JMenuItem();
        jPanelImage = new base.JPanelImage();
        jPCabecalho = new javax.swing.JPanel();
        jPTitulo = new javax.swing.JPanel();
        jLTitulo = new javax.swing.JLabel();
        jPProduto = new javax.swing.JPanel();
        jPDescricaoProdutoFundo = new javax.swing.JPanel();
        jPDescricaoProduto = new javax.swing.JPanel();
        jLDescricaoProduto = new javax.swing.JLabel();
        jPColuna = new javax.swing.JPanel();
        jPDetalheCompra = new javax.swing.JPanel();
        jPCodigoQuantidade = new javax.swing.JPanel();
        jPCodigoFundo = new javax.swing.JPanel();
        jPCodigo = new javax.swing.JPanel();
        jTFCodigo = new javax.swing.JTextField();
        jPQuantidadeFundo = new javax.swing.JPanel();
        jPQuantidade = new javax.swing.JPanel();
        jTFQuantidade = new javax.swing.JTextField();
        jPPrecos = new javax.swing.JPanel();
        jPPrecoUnitFundo = new javax.swing.JPanel();
        jPPrecoUnit = new javax.swing.JPanel();
        jTFPrecoUnitario = new javax.swing.JTextField();
        jPPrecoTotalFundo = new javax.swing.JPanel();
        jPPrecoTotal = new javax.swing.JPanel();
        jLPrecoTotal = new javax.swing.JLabel();
        jPValorTotal = new javax.swing.JPanel();
        jPTotalPagarFundo = new javax.swing.JPanel();
        jPTotalPagar = new javax.swing.JPanel();
        jLTotalPagar = new javax.swing.JLabel();
        jPBotoes = new javax.swing.JPanel();
        jBConsultaProduto = new javax.swing.JButton();
        jBConsultaCliente = new javax.swing.JButton();
        jBFinalizaVenda = new javax.swing.JButton();
        jBCancelaVenda = new javax.swing.JButton();
        jSPAlerta = new javax.swing.JScrollPane();
        jTableAlerta = new javax.swing.JTable();
        jPCupomCliente = new javax.swing.JPanel();
        jSPCupom = new javax.swing.JScrollPane();
        jTable = new javax.swing.JTable();
        jMenu = new javax.swing.JMenuBar();
        jMCadastros = new javax.swing.JMenu();
        jMICadastroEncomenda = new javax.swing.JMenuItem();
        jMICancelar = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        jMICadastroPessoa = new javax.swing.JMenuItem();
        jMICadastroProduto = new javax.swing.JMenuItem();
        jMICadastroUsuario = new javax.swing.JMenuItem();
        jMRelatorios = new javax.swing.JMenu();
        jMIRelatorioPessoas = new javax.swing.JMenuItem();
        jMIRelatorioProduto = new javax.swing.JMenuItem();
        jMIRelatorioVenda = new javax.swing.JMenuItem();
        jMSobre = new javax.swing.JMenu();
        jMIEmpresa = new javax.swing.JMenuItem();
        jCheckBox_AutoLogin = new javax.swing.JCheckBoxMenuItem();
        jCheckBox_AdicionarAutomaticamente = new javax.swing.JCheckBoxMenuItem();
        jSeparator = new javax.swing.JPopupMenu.Separator();
        jMISobre = new javax.swing.JMenuItem();

        jMRemoveItem.setText("Remover");
        jMRemoveItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMRemoveItemActionPerformed(evt);
            }
        });
        jPMItemTabela.add(jMRemoveItem);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Sistema de Vendas");
        setMinimumSize(new java.awt.Dimension(933, 606));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jPanelImage.setLayout(new java.awt.BorderLayout());

        jPCabecalho.setMaximumSize(new java.awt.Dimension(100, 60));
        jPCabecalho.setOpaque(false);
        jPCabecalho.setPreferredSize(new java.awt.Dimension(100, 60));
        jPCabecalho.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 5, 2));

        jPTitulo.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createEmptyBorder(2, 2, 2, 2), javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        jPTitulo.setMaximumSize(new java.awt.Dimension(250, 55));
        jPTitulo.setMinimumSize(new java.awt.Dimension(250, 55));
        jPTitulo.setPreferredSize(new java.awt.Dimension(250, 55));
        jPTitulo.setLayout(new javax.swing.BoxLayout(jPTitulo, javax.swing.BoxLayout.X_AXIS));

        jLTitulo.setFont(new java.awt.Font("Arial", 1, 24)); // NOI18N
        jLTitulo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLTitulo.setText("FRENTE DE CAIXA");
        jLTitulo.setMaximumSize(new java.awt.Dimension(200, 80));
        jLTitulo.setMinimumSize(new java.awt.Dimension(200, 80));
        jLTitulo.setPreferredSize(new java.awt.Dimension(250, 80));
        jPTitulo.add(jLTitulo);

        jPCabecalho.add(jPTitulo);

        jPanelImage.add(jPCabecalho, java.awt.BorderLayout.NORTH);

        jPProduto.setBorder(javax.swing.BorderFactory.createEmptyBorder(5, 5, 5, 5));
        jPProduto.setMaximumSize(new java.awt.Dimension(500, 1550));
        jPProduto.setMinimumSize(new java.awt.Dimension(200, 1550));
        jPProduto.setOpaque(false);
        jPProduto.setPreferredSize(new java.awt.Dimension(500, 1550));
        jPProduto.setLayout(new java.awt.BorderLayout());

        jPDescricaoProdutoFundo.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Descrição do Produto", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 1, 14))); // NOI18N
        jPDescricaoProdutoFundo.setMaximumSize(new java.awt.Dimension(820, 90));
        jPDescricaoProdutoFundo.setMinimumSize(new java.awt.Dimension(820, 90));
        jPDescricaoProdutoFundo.setOpaque(false);
        jPDescricaoProdutoFundo.setPreferredSize(new java.awt.Dimension(820, 90));
        jPDescricaoProdutoFundo.setLayout(new java.awt.BorderLayout());

        jPDescricaoProduto.setBackground(new java.awt.Color(255, 255, 255));

        jLDescricaoProduto.setBackground(new java.awt.Color(255, 255, 255));
        jLDescricaoProduto.setFont(new java.awt.Font("Arial", 1, 40)); // NOI18N
        jLDescricaoProduto.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLDescricaoProduto.setMaximumSize(new java.awt.Dimension(800, 50));
        jLDescricaoProduto.setMinimumSize(new java.awt.Dimension(800, 50));
        jLDescricaoProduto.setPreferredSize(new java.awt.Dimension(800, 50));
        jPDescricaoProduto.add(jLDescricaoProduto);

        jPDescricaoProdutoFundo.add(jPDescricaoProduto, java.awt.BorderLayout.CENTER);

        jPProduto.add(jPDescricaoProdutoFundo, java.awt.BorderLayout.NORTH);

        jPColuna.setMaximumSize(new java.awt.Dimension(450, 100));
        jPColuna.setMinimumSize(new java.awt.Dimension(450, 100));
        jPColuna.setOpaque(false);
        jPColuna.setPreferredSize(new java.awt.Dimension(450, 100));
        jPColuna.setLayout(new java.awt.BorderLayout());

        jPDetalheCompra.setMaximumSize(new java.awt.Dimension(450, 100));
        jPDetalheCompra.setMinimumSize(new java.awt.Dimension(450, 100));
        jPDetalheCompra.setOpaque(false);
        jPDetalheCompra.setPreferredSize(new java.awt.Dimension(450, 100));
        jPDetalheCompra.setLayout(new javax.swing.BoxLayout(jPDetalheCompra, javax.swing.BoxLayout.Y_AXIS));

        jPCodigoQuantidade.setMaximumSize(new java.awt.Dimension(450, 80));
        jPCodigoQuantidade.setMinimumSize(new java.awt.Dimension(450, 80));
        jPCodigoQuantidade.setOpaque(false);
        jPCodigoQuantidade.setPreferredSize(new java.awt.Dimension(450, 80));

        jPCodigoFundo.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Código", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 1, 14))); // NOI18N
        jPCodigoFundo.setMaximumSize(new java.awt.Dimension(200, 70));
        jPCodigoFundo.setMinimumSize(new java.awt.Dimension(200, 70));
        jPCodigoFundo.setName(""); // NOI18N
        jPCodigoFundo.setOpaque(false);
        jPCodigoFundo.setPreferredSize(new java.awt.Dimension(200, 70));
        jPCodigoFundo.setLayout(new javax.swing.BoxLayout(jPCodigoFundo, javax.swing.BoxLayout.LINE_AXIS));

        jPCodigo.setBackground(new java.awt.Color(255, 255, 255));
        jPCodigo.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 5, 13));

        jTFCodigo.setFont(new java.awt.Font("Arial", 0, 24)); // NOI18N
        jTFCodigo.setMinimumSize(new java.awt.Dimension(20, 20));
        jTFCodigo.setName(""); // NOI18N
        jTFCodigo.setPreferredSize(new java.awt.Dimension(180, 29));
        jTFCodigo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTFCodigoKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTFCodigoKeyTyped(evt);
            }
        });
        jPCodigo.add(jTFCodigo);

        jPCodigoFundo.add(jPCodigo);

        jPCodigoQuantidade.add(jPCodigoFundo);

        jPQuantidadeFundo.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Quantidade", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 1, 14))); // NOI18N
        jPQuantidadeFundo.setMaximumSize(new java.awt.Dimension(200, 70));
        jPQuantidadeFundo.setMinimumSize(new java.awt.Dimension(200, 70));
        jPQuantidadeFundo.setName(""); // NOI18N
        jPQuantidadeFundo.setOpaque(false);
        jPQuantidadeFundo.setPreferredSize(new java.awt.Dimension(200, 70));
        jPQuantidadeFundo.setLayout(new javax.swing.BoxLayout(jPQuantidadeFundo, javax.swing.BoxLayout.LINE_AXIS));

        jPQuantidade.setBackground(new java.awt.Color(255, 255, 255));
        jPQuantidade.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 5, 8));

        jTFQuantidade.setFont(new java.awt.Font("Arial", 0, 24)); // NOI18N
        jTFQuantidade.setPreferredSize(new java.awt.Dimension(80, 29));
        jTFQuantidade.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTFQuantidadeKeyPressed(evt);
            }
        });
        jPQuantidade.add(jTFQuantidade);

        jPQuantidadeFundo.add(jPQuantidade);

        jPCodigoQuantidade.add(jPQuantidadeFundo);

        jPDetalheCompra.add(jPCodigoQuantidade);

        jPPrecos.setMaximumSize(new java.awt.Dimension(450, 80));
        jPPrecos.setMinimumSize(new java.awt.Dimension(450, 80));
        jPPrecos.setOpaque(false);
        jPPrecos.setPreferredSize(new java.awt.Dimension(450, 80));

        jPPrecoUnitFundo.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Preço Unitário", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 1, 14))); // NOI18N
        jPPrecoUnitFundo.setMaximumSize(new java.awt.Dimension(200, 70));
        jPPrecoUnitFundo.setMinimumSize(new java.awt.Dimension(200, 70));
        jPPrecoUnitFundo.setName(""); // NOI18N
        jPPrecoUnitFundo.setOpaque(false);
        jPPrecoUnitFundo.setPreferredSize(new java.awt.Dimension(200, 70));
        jPPrecoUnitFundo.setLayout(new javax.swing.BoxLayout(jPPrecoUnitFundo, javax.swing.BoxLayout.LINE_AXIS));

        jPPrecoUnit.setBackground(new java.awt.Color(255, 255, 255));
        jPPrecoUnit.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 5, 8));

        jTFPrecoUnitario.setFont(new java.awt.Font("Arial", 0, 24)); // NOI18N
        jTFPrecoUnitario.setMinimumSize(new java.awt.Dimension(20, 20));
        jTFPrecoUnitario.setPreferredSize(new java.awt.Dimension(80, 29));
        jTFPrecoUnitario.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTFPrecoUnitarioKeyPressed(evt);
            }
        });
        jPPrecoUnit.add(jTFPrecoUnitario);

        jPPrecoUnitFundo.add(jPPrecoUnit);

        jPPrecos.add(jPPrecoUnitFundo);

        jPPrecoTotalFundo.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Preço Total", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 1, 14))); // NOI18N
        jPPrecoTotalFundo.setMaximumSize(new java.awt.Dimension(200, 70));
        jPPrecoTotalFundo.setMinimumSize(new java.awt.Dimension(200, 70));
        jPPrecoTotalFundo.setName(""); // NOI18N
        jPPrecoTotalFundo.setOpaque(false);
        jPPrecoTotalFundo.setPreferredSize(new java.awt.Dimension(200, 70));
        jPPrecoTotalFundo.setLayout(new javax.swing.BoxLayout(jPPrecoTotalFundo, javax.swing.BoxLayout.LINE_AXIS));

        jPPrecoTotal.setBackground(new java.awt.Color(255, 255, 255));
        jPPrecoTotal.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 5, 8));

        jLPrecoTotal.setFont(new java.awt.Font("Arial", 0, 24)); // NOI18N
        jLPrecoTotal.setText("0,00");
        jPPrecoTotal.add(jLPrecoTotal);

        jPPrecoTotalFundo.add(jPPrecoTotal);

        jPPrecos.add(jPPrecoTotalFundo);

        jPDetalheCompra.add(jPPrecos);

        jPValorTotal.setMaximumSize(new java.awt.Dimension(450, 120));
        jPValorTotal.setMinimumSize(new java.awt.Dimension(450, 120));
        jPValorTotal.setOpaque(false);
        jPValorTotal.setPreferredSize(new java.awt.Dimension(450, 120));

        jPTotalPagarFundo.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Total a Pagar", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 1, 24))); // NOI18N
        jPTotalPagarFundo.setMaximumSize(new java.awt.Dimension(300, 100));
        jPTotalPagarFundo.setMinimumSize(new java.awt.Dimension(300, 100));
        jPTotalPagarFundo.setName(""); // NOI18N
        jPTotalPagarFundo.setOpaque(false);
        jPTotalPagarFundo.setPreferredSize(new java.awt.Dimension(300, 100));
        jPTotalPagarFundo.setLayout(new javax.swing.BoxLayout(jPTotalPagarFundo, javax.swing.BoxLayout.LINE_AXIS));

        jPTotalPagar.setBackground(new java.awt.Color(255, 255, 255));
        jPTotalPagar.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 5, 10));

        jLTotalPagar.setFont(new java.awt.Font("Arial", 1, 36)); // NOI18N
        jLTotalPagar.setText("0,00");
        jPTotalPagar.add(jLTotalPagar);

        jPTotalPagarFundo.add(jPTotalPagar);

        jPValorTotal.add(jPTotalPagarFundo);

        jPDetalheCompra.add(jPValorTotal);

        jPBotoes.setMaximumSize(new java.awt.Dimension(250, 150));
        jPBotoes.setMinimumSize(new java.awt.Dimension(450, 40));
        jPBotoes.setOpaque(false);
        jPBotoes.setPreferredSize(new java.awt.Dimension(450, 100));
        jPBotoes.setLayout(new java.awt.GridLayout(5, 1, 10, 5));

        jBConsultaProduto.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jBConsultaProduto.setText("F2 - Consultar Produto");
        jBConsultaProduto.setMaximumSize(new java.awt.Dimension(150, 40));
        jBConsultaProduto.setMinimumSize(new java.awt.Dimension(150, 40));
        jBConsultaProduto.setPreferredSize(new java.awt.Dimension(150, 40));
        jBConsultaProduto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBConsultaProdutoActionPerformed(evt);
            }
        });
        jPBotoes.add(jBConsultaProduto);

        jBConsultaCliente.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jBConsultaCliente.setText("F3 - Consultar Cliente");
        jBConsultaCliente.setMaximumSize(new java.awt.Dimension(150, 40));
        jBConsultaCliente.setMinimumSize(new java.awt.Dimension(150, 40));
        jBConsultaCliente.setPreferredSize(new java.awt.Dimension(150, 40));
        jBConsultaCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBConsultaClienteActionPerformed(evt);
            }
        });
        jPBotoes.add(jBConsultaCliente);

        jBFinalizaVenda.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jBFinalizaVenda.setText("F4 - Fechar Venda");
        jBFinalizaVenda.setMaximumSize(new java.awt.Dimension(150, 40));
        jBFinalizaVenda.setMinimumSize(new java.awt.Dimension(150, 40));
        jBFinalizaVenda.setPreferredSize(new java.awt.Dimension(150, 40));
        jBFinalizaVenda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBFinalizaVendaActionPerformed(evt);
            }
        });
        jPBotoes.add(jBFinalizaVenda);

        jBCancelaVenda.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jBCancelaVenda.setText("F5 - Cancelar Venda");
        jBCancelaVenda.setMaximumSize(new java.awt.Dimension(150, 40));
        jBCancelaVenda.setMinimumSize(new java.awt.Dimension(150, 40));
        jBCancelaVenda.setPreferredSize(new java.awt.Dimension(150, 40));
        jBCancelaVenda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBCancelaVendaActionPerformed(evt);
            }
        });
        jPBotoes.add(jBCancelaVenda);

        jPDetalheCompra.add(jPBotoes);

        jPColuna.add(jPDetalheCompra, java.awt.BorderLayout.CENTER);

        jSPAlerta.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Alerta", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 1, 14))); // NOI18N
        jSPAlerta.setMaximumSize(new java.awt.Dimension(1000, 250));
        jSPAlerta.setOpaque(false);
        jSPAlerta.setPreferredSize(new java.awt.Dimension(462, 150));

        jTableAlerta.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jSPAlerta.setViewportView(jTableAlerta);

        jPColuna.add(jSPAlerta, java.awt.BorderLayout.SOUTH);

        jPProduto.add(jPColuna, java.awt.BorderLayout.CENTER);

        jPanelImage.add(jPProduto, java.awt.BorderLayout.EAST);

        jPCupomCliente.setBorder(javax.swing.BorderFactory.createEmptyBorder(5, 5, 5, 5));
        jPCupomCliente.setMaximumSize(new java.awt.Dimension(500, 50));
        jPCupomCliente.setMinimumSize(new java.awt.Dimension(500, 50));
        jPCupomCliente.setOpaque(false);
        jPCupomCliente.setPreferredSize(new java.awt.Dimension(500, 50));
        jPCupomCliente.setLayout(new java.awt.BorderLayout());

        jSPCupom.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Itens", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 1, 14))); // NOI18N
        jSPCupom.setOpaque(false);

        jTable.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jTableMousePressed(evt);
            }
        });
        jSPCupom.setViewportView(jTable);

        jPCupomCliente.add(jSPCupom, java.awt.BorderLayout.CENTER);

        jPanelImage.add(jPCupomCliente, java.awt.BorderLayout.CENTER);

        getContentPane().add(jPanelImage, java.awt.BorderLayout.CENTER);

        jMCadastros.setText("Cadastros");

        jMICadastroEncomenda.setText("Cadastro de Encomenda");
        jMICadastroEncomenda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMICadastroEncomendaActionPerformed(evt);
            }
        });
        jMCadastros.add(jMICadastroEncomenda);

        jMICancelar.setText("Cancelamento de Venda");
        jMICancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMICancelarActionPerformed(evt);
            }
        });
        jMCadastros.add(jMICancelar);
        jMCadastros.add(jSeparator1);

        jMICadastroPessoa.setText("Cadastro de Pessoa");
        jMICadastroPessoa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMICadastroPessoaActionPerformed(evt);
            }
        });
        jMCadastros.add(jMICadastroPessoa);

        jMICadastroProduto.setText("Cadastro de Produto");
        jMICadastroProduto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMICadastroProdutoActionPerformed(evt);
            }
        });
        jMCadastros.add(jMICadastroProduto);

        jMICadastroUsuario.setText("Cadastro de Usuário");
        jMICadastroUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMICadastroUsuarioActionPerformed(evt);
            }
        });
        jMCadastros.add(jMICadastroUsuario);

        jMenu.add(jMCadastros);

        jMRelatorios.setText("Relatórios");

        jMIRelatorioPessoas.setText("Relatório de Pessoas");
        jMIRelatorioPessoas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMIRelatorioPessoasActionPerformed(evt);
            }
        });
        jMRelatorios.add(jMIRelatorioPessoas);

        jMIRelatorioProduto.setText("Relatório de Encomendas");
        jMIRelatorioProduto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMIRelatorioProdutoActionPerformed(evt);
            }
        });
        jMRelatorios.add(jMIRelatorioProduto);

        jMIRelatorioVenda.setText("Relatório de Vendas");
        jMIRelatorioVenda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMIRelatorioVendaActionPerformed(evt);
            }
        });
        jMRelatorios.add(jMIRelatorioVenda);

        jMenu.add(jMRelatorios);

        jMSobre.setText("Opções");

        jMIEmpresa.setText("Dados da Empresa");
        jMIEmpresa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMIEmpresaActionPerformed(evt);
            }
        });
        jMSobre.add(jMIEmpresa);

        jCheckBox_AutoLogin.setSelected(true);
        jCheckBox_AutoLogin.setText("Auto-Login");
        jCheckBox_AutoLogin.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jCheckBox_AutoLoginItemStateChanged(evt);
            }
        });
        jMSobre.add(jCheckBox_AutoLogin);

        jCheckBox_AdicionarAutomaticamente.setSelected(true);
        jCheckBox_AdicionarAutomaticamente.setText("Adicionar item automaticamente");
        jCheckBox_AdicionarAutomaticamente.setToolTipText("Após buscar o produto, irá adicioná-lo automaticamente no lista.");
        jCheckBox_AdicionarAutomaticamente.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jCheckBox_AdicionarAutomaticamenteItemStateChanged(evt);
            }
        });
        jMSobre.add(jCheckBox_AdicionarAutomaticamente);
        jMSobre.add(jSeparator);

        jMISobre.setText("Sobre");
        jMISobre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMISobreActionPerformed(evt);
            }
        });
        jMSobre.add(jMISobre);

        jMenu.add(jMSobre);

        setJMenuBar(jMenu);

        setSize(new java.awt.Dimension(1016, 739));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jMICadastroPessoaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMICadastroPessoaActionPerformed
        JPessoa jPessoa = JPessoa.getInstance();
        jPessoa.limparTela();
        jPessoa.setVisible(true);
    }//GEN-LAST:event_jMICadastroPessoaActionPerformed

    private void jMICadastroProdutoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMICadastroProdutoActionPerformed
        JProduto jProduto = JProduto.getInstance();
        jProduto.limparTela();
        jProduto.setVisible(true);
    }//GEN-LAST:event_jMICadastroProdutoActionPerformed

    private void jMICadastroUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMICadastroUsuarioActionPerformed
        JUsuario jUsuario = JUsuario.getInstance();
        jUsuario.limparTela();
        jUsuario.setVisible(true);
    }//GEN-LAST:event_jMICadastroUsuarioActionPerformed

    private void jBFinalizaVendaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBFinalizaVendaActionPerformed
        finalizaVenda();
    }//GEN-LAST:event_jBFinalizaVendaActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        Start.finalizarSistema();
    }//GEN-LAST:event_formWindowClosing

    private void jBConsultaProdutoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBConsultaProdutoActionPerformed
        consultarProduto();
    }//GEN-LAST:event_jBConsultaProdutoActionPerformed

    private void jBConsultaClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBConsultaClienteActionPerformed
        consultarCliente();
    }//GEN-LAST:event_jBConsultaClienteActionPerformed

    private void jTFCodigoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTFCodigoKeyPressed
        if (produto != null && evt.getKeyCode() == KeyEvent.VK_ENTER) {
            jTFQuantidade.requestFocusInWindow();
        } else if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            verificarProduto();
        }
    }//GEN-LAST:event_jTFCodigoKeyPressed

    private void jMISobreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMISobreActionPerformed
        JSobre.getInstance(this).setVisible(true);
    }//GEN-LAST:event_jMISobreActionPerformed

    private void jMIRelatorioPessoasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMIRelatorioPessoasActionPerformed
        JRelPessoa.getInstance(this).setVisible(true);
    }//GEN-LAST:event_jMIRelatorioPessoasActionPerformed

    private void jMIEmpresaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMIEmpresaActionPerformed
        JEmpresa.getInstance(this).setVisible(true);
    }//GEN-LAST:event_jMIEmpresaActionPerformed

    private void jMICadastroEncomendaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMICadastroEncomendaActionPerformed
        JEncomenda jEncomenda = JEncomenda.getInstance();
        jEncomenda.limparTela();
        jEncomenda.setVisible(true);
    }//GEN-LAST:event_jMICadastroEncomendaActionPerformed

    private void jMRemoveItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMRemoveItemActionPerformed
        venda.removeItemVendaProduto(jTable.getSelectedRow());
        limparCampos();
        atualizarTabela();
    }//GEN-LAST:event_jMRemoveItemActionPerformed

    private void jTableMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableMousePressed
        if (evt.getButton() == MouseEvent.BUTTON3) {
            jPMItemTabela.show(this, evt.getXOnScreen(), evt.getYOnScreen());
        }
    }//GEN-LAST:event_jTableMousePressed

    private void jBCancelaVendaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBCancelaVendaActionPerformed
        cancelarVenda();
    }//GEN-LAST:event_jBCancelaVendaActionPerformed

    private void jMIRelatorioProdutoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMIRelatorioProdutoActionPerformed
        JRelEncomenda.getInstance(this).setVisible(true);
    }//GEN-LAST:event_jMIRelatorioProdutoActionPerformed

    private void jTFCodigoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTFCodigoKeyTyped
        if (!Character.isDigit(evt.getKeyChar()) || jTFCodigo.getText().length() > 12) {
            evt.consume();
        }
    }//GEN-LAST:event_jTFCodigoKeyTyped

    private void jMIRelatorioVendaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMIRelatorioVendaActionPerformed
        JRelVenda.getInstance(this).setVisible(true);
    }//GEN-LAST:event_jMIRelatorioVendaActionPerformed

    private void jCheckBox_AutoLoginItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jCheckBox_AutoLoginItemStateChanged
        alterarAutoLogin();
    }//GEN-LAST:event_jCheckBox_AutoLoginItemStateChanged

    private void jMICancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMICancelarActionPerformed
        JVendaCancelamento.getInstance(this).setVisible(true);
    }//GEN-LAST:event_jMICancelarActionPerformed

    private void jTFPrecoUnitarioKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTFPrecoUnitarioKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            adicionarItem();
        }
    }//GEN-LAST:event_jTFPrecoUnitarioKeyPressed

    private void jTFQuantidadeKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTFQuantidadeKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            jTFPrecoUnitario.requestFocusInWindow();
        }
    }//GEN-LAST:event_jTFQuantidadeKeyPressed

    private void jCheckBox_AdicionarAutomaticamenteItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jCheckBox_AdicionarAutomaticamenteItemStateChanged
        Propriedades.setProperty("prop.menu.itemauto", jCheckBox_AdicionarAutomaticamente.isSelected() ? "1" : "0");
    }//GEN-LAST:event_jCheckBox_AdicionarAutomaticamenteItemStateChanged

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jBCancelaVenda;
    private javax.swing.JButton jBConsultaCliente;
    private javax.swing.JButton jBConsultaProduto;
    private javax.swing.JButton jBFinalizaVenda;
    private javax.swing.JCheckBoxMenuItem jCheckBox_AdicionarAutomaticamente;
    private javax.swing.JCheckBoxMenuItem jCheckBox_AutoLogin;
    private javax.swing.JLabel jLDescricaoProduto;
    private javax.swing.JLabel jLPrecoTotal;
    private javax.swing.JLabel jLTitulo;
    private javax.swing.JLabel jLTotalPagar;
    private javax.swing.JMenu jMCadastros;
    private javax.swing.JMenuItem jMICadastroEncomenda;
    private javax.swing.JMenuItem jMICadastroPessoa;
    private javax.swing.JMenuItem jMICadastroProduto;
    private javax.swing.JMenuItem jMICadastroUsuario;
    private javax.swing.JMenuItem jMICancelar;
    private javax.swing.JMenuItem jMIEmpresa;
    private javax.swing.JMenuItem jMIRelatorioPessoas;
    private javax.swing.JMenuItem jMIRelatorioProduto;
    private javax.swing.JMenuItem jMIRelatorioVenda;
    private javax.swing.JMenuItem jMISobre;
    private javax.swing.JMenu jMRelatorios;
    private javax.swing.JMenuItem jMRemoveItem;
    private javax.swing.JMenu jMSobre;
    private javax.swing.JMenuBar jMenu;
    private javax.swing.JPanel jPBotoes;
    private javax.swing.JPanel jPCabecalho;
    private javax.swing.JPanel jPCodigo;
    private javax.swing.JPanel jPCodigoFundo;
    private javax.swing.JPanel jPCodigoQuantidade;
    private javax.swing.JPanel jPColuna;
    private javax.swing.JPanel jPCupomCliente;
    private javax.swing.JPanel jPDescricaoProduto;
    private javax.swing.JPanel jPDescricaoProdutoFundo;
    private javax.swing.JPanel jPDetalheCompra;
    private javax.swing.JPopupMenu jPMItemTabela;
    private javax.swing.JPanel jPPrecoTotal;
    private javax.swing.JPanel jPPrecoTotalFundo;
    private javax.swing.JPanel jPPrecoUnit;
    private javax.swing.JPanel jPPrecoUnitFundo;
    private javax.swing.JPanel jPPrecos;
    private javax.swing.JPanel jPProduto;
    private javax.swing.JPanel jPQuantidade;
    private javax.swing.JPanel jPQuantidadeFundo;
    private javax.swing.JPanel jPTitulo;
    private javax.swing.JPanel jPTotalPagar;
    private javax.swing.JPanel jPTotalPagarFundo;
    private javax.swing.JPanel jPValorTotal;
    private base.JPanelImage jPanelImage;
    private javax.swing.JScrollPane jSPAlerta;
    private javax.swing.JScrollPane jSPCupom;
    private javax.swing.JPopupMenu.Separator jSeparator;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JTextField jTFCodigo;
    private javax.swing.JTextField jTFPrecoUnitario;
    private javax.swing.JTextField jTFQuantidade;
    private javax.swing.JTable jTable;
    private javax.swing.JTable jTableAlerta;
    // End of variables declaration//GEN-END:variables
}
