package com;

import base.bd.BD;
import base.utils.FecharEsc;
import base.utils.SwingUtils;
import java.awt.Window;

/**
 *
 * @author Rodrigo Vianna
 * @since 12/06/2019
 */
public class JSobre extends javax.swing.JDialog implements FecharEsc {

    private static JSobre instance;

    public static JSobre getInstance(Window parent) {
        if (instance == null) {
            instance = new JSobre(parent, false);
        }
        return instance;
    }

    private JSobre(Window parent, boolean modal) {
        super(parent, modal ? ModalityType.APPLICATION_MODAL : ModalityType.MODELESS);
        initComponents();
        inicializar();
    }

    private void inicializar() {
        SwingUtils.inicializarTela(this);
        if (BD.getBancoDados().isEmpty()) {
            BD.configurarBD();
        }
        String s
                = "<html>\n"
                + " <head>\n"
                + "     <meta charset=\"utf-8\">\n"
                + " </head>\n"
                + " <body>\n"
                + "     <h1 style = \"font-family: Arial; text-align: center;\">Sistema de Vendas para Padaria</h1>\n"
                + "     <br><h2 style = \"font-family: Arial; text-align: center;\">Elaboração do Projeto</h2>\n"
                + "     <p style = \"font-family: Arial; text-align: center;\">\n"
                + "         Maurício dos Santos\n"
                + "         <br>Matthias Trennepohl\n"
                + "         <br>Renan Oliveira\n"
                + "         <br>Rodrigo Vianna\n"
                + "     </p>\n"
                + "\n"
                + "     <br><h2 style = \"font-family: Arial; text-align: center;\">Desenvolvimento</h2>\n"
                + "     <p style = \"font-family: Arial; text-align: center;\">\n"
                + "         Carlos Valdiero\n"
                + "         <br>Éderson Siqueira\n"
                + "         <br>Jean Gomes\n"
                + "         <br>Rodrigo Vianna\n"
                + "     </p>\n"
                + "\n"
                + "     <br><h2 style = \"font-family: Arial; text-align: center;\">Sistema</h2>\n"
                + "     <p style = \"font-family: Arial; text-align: center;\">\n"
                + "         Banco de Dados: " + BD.getBancoDados() + "\n"
                + "         <br>Driver: " + BD.getDriverBancoDados() + "\n"
                + "         <br>Versão do Driver: " + BD.getDriverVersaoBancoDados() + "\n"
                + "         <br>Nome do Banco: " + BD.getNomeBancoDados() + "\n"
                + "         <br>URL: " + BD.getUrlBancoDados() + "\n"
                + "         <br>Usuário do Banco:" + BD.getUsuarioBancoDados() + "\n"
                + "         <br>Versão do Banco:" + BD.getVersaoBancoDados() + "\n"
                + "     </p>\n"
                + " </body>\n"
                + "</html>";
        jTextPane.setText(s);
    }

    @Override
    public void fechar() {
        setVisible(false);
        dispose();
    }

    public static void main(String[] args) {
        if (com.Start.iniciarSistema()) {
            getInstance((java.awt.Dialog) null).setVisible(true);
        }
        System.exit(0);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane2 = new javax.swing.JScrollPane();
        jTextPane = new javax.swing.JTextPane();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Sobre");
        setMinimumSize(new java.awt.Dimension(630, 670));
        setPreferredSize(new java.awt.Dimension(630, 670));
        setResizable(false);

        jPanel_Filtro.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Sobre", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 11))); // NOI18N
        jPanel_Filtro.setLayout(new javax.swing.BoxLayout(jPanel_Filtro, javax.swing.BoxLayout.Y_AXIS));

        jTextPane.setEditable(false);
        jTextPane.setContentType("text/html"); // NOI18N
        jScrollPane2.setViewportView(jTextPane);

        jPanel_Filtro.add(jScrollPane2);

        getContentPane().add(jPanel_Filtro, java.awt.BorderLayout.CENTER);

        setSize(new java.awt.Dimension(646, 709));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private final javax.swing.JPanel jPanel_Filtro = new javax.swing.JPanel();
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextPane jTextPane;
    // End of variables declaration//GEN-END:variables

}
