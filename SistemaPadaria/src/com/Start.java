package com;

import base.Propriedades;
import base.bd.BD;
import base.log.Log;
import base.login.JLogin;
import base.msg.JAlerta;
import java.awt.Dialog;
import java.io.IOException;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 * Classe que inicializa o sistema.
 *
 * @author Rodrigo
 * @since 13/03/2019
 */
public class Start {

    /**
     * Main que inicia o sistema.
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        if (iniciarSistema()) {
            new JMenu().setVisible(true);
        }
    }

    /**
     * Inicializa o sistema e configura.
     *
     * @return verdadeiro se inicializou
     */
    public static boolean iniciarSistema() {
        configurarSistema();
        Log.log("Inicializando o Sistema.");

        JLogin jLogin = JLogin.getInstance();
        jLogin.setVisible(true);
        return jLogin.isOk();
    }

    /**
     * Finaliza o sistema.
     */
    public static void finalizarSistema() {
        Log.log("Encerrando conexões...");
        BD.encerrarConexoes();
        Log.log("Sistema finalizado.\n");
    }

    /**
     * Realiza as configurações iniciais do sistema, como a inicialização do Log
     * e Properties. Deve ser chamado APENAS ao inicializar o sistema.
     */
    public static void configurarSistema() {
        //Configura o arquivo .properities
        try {
            Propriedades.configurarProperties();
        } catch (IOException e) {
            JAlerta.getAlertaErro((Dialog) null, "Erro ao configurar propriedades do sistema.\nContate o administrador:", e);
            System.exit(1);
        }

        //Configurando o log
        try {
            Log.configurarLog();
        } catch (IOException e) {
            JAlerta.getAlertaErro((Dialog) null, "Erro ao configurar log do sistema.\nContate o administrador:", e);
            System.exit(1);
        }

        //Configura o Look And Feel
        try {
            //Configura o Look And Feel do SO em execução
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            UIManager.put("Button.showMnemonics", true);
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
            Log.log("Erro ao configurar look and feel: ", e);
        }

        //Configurando o Banco de dados
        BD.configurarBD();

    }

}
