package com.empresa;

import base.msg.JAlerta;
import base.relatorio.AutoReport;
import base.utils.FecharEsc;
import base.utils.SwingUtils;
import java.awt.Component;
import java.awt.Window;

/**
 * Classe para cadastro de empresa
 * @author Rodrigo Vianna
 * @since 12/06/2019
 */
public class JEmpresa extends javax.swing.JDialog implements FecharEsc {

    private static JEmpresa instance;

    public static JEmpresa getInstance(Window parent) {
        if (instance == null) {
            instance = new JEmpresa(parent, false);
        }
        return instance;
    }

    private JEmpresa(Window parent, boolean modal) {
        super(parent, modal ? ModalityType.APPLICATION_MODAL : ModalityType.MODELESS);
        initComponents();
        inicializar();
    }

    private void inicializar() {
        SwingUtils.inicializarTela(this);

        jTFNome.setText(AutoReport.getNomeEmpresa());
        jTFEndereco.setText(AutoReport.getEnderecoEmpresa());
        jTFCidade.setText(AutoReport.getCidadeEmpresa());
        jTFContato.setText(AutoReport.getTelefoneEmpresa());

    }

    private void gravar() {
        new Thread(() -> {
            gravarRun();
        }).start();
    }

    private void gravarRun() {
        try {
            SwingUtils.iniciarCursorProcessamento(this);
            SwingUtils.desabilitarCampos(getCampos());

            AutoReport.setNomeEmpresa(jTFNome.getText());
            AutoReport.setEnderecoEmpresa(jTFEndereco.getText());
            AutoReport.setCidadeEmpresa(jTFCidade.getText());
            AutoReport.setTelefoneEmpresa(jTFContato.getText());

        } catch (Exception e) {
            JAlerta.getAlertaErro(this, "Erro ao realizar consutla:", e);
        } finally {
            SwingUtils.habilitarCampos(getCampos());
            SwingUtils.finalizarCursorProcessamento(this);
        }
    }

    private Component[] getCampos() {
        return new Component[]{jButton_Gerar};
    }

    @Override
    public void fechar() {
        setVisible(false);
        dispose();
    }

    public static void main(String[] args) {
        if (com.Start.iniciarSistema()) {
            getInstance((java.awt.Dialog) null).setVisible(true);
        }
        System.exit(0);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPNome = new javax.swing.JPanel();
        jLNome = new javax.swing.JLabel();
        jTFNome = new javax.swing.JTextField();
        jPNome1 = new javax.swing.JPanel();
        jLNome1 = new javax.swing.JLabel();
        jTFEndereco = new javax.swing.JTextField();
        jPNome2 = new javax.swing.JPanel();
        jLNome2 = new javax.swing.JLabel();
        jTFCidade = new javax.swing.JTextField();
        jPNome3 = new javax.swing.JPanel();
        jLNome3 = new javax.swing.JLabel();
        jTFContato = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Dados da Empresa");
        setMaximumSize(new java.awt.Dimension(435, 215));
        setMinimumSize(new java.awt.Dimension(435, 215));
        setPreferredSize(new java.awt.Dimension(435, 215));
        setResizable(false);

        jPanel_Filtro.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Dados", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 11))); // NOI18N
        jPanel_Filtro.setLayout(new javax.swing.BoxLayout(jPanel_Filtro, javax.swing.BoxLayout.Y_AXIS));

        jPNome.setMaximumSize(new java.awt.Dimension(32767, 32));
        jPNome.setMinimumSize(new java.awt.Dimension(10, 22));
        jPNome.setPreferredSize(new java.awt.Dimension(438, 32));
        jPNome.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 6));

        jLNome.setText("Nome:");
        jLNome.setMaximumSize(new java.awt.Dimension(60, 20));
        jLNome.setMinimumSize(new java.awt.Dimension(60, 20));
        jLNome.setPreferredSize(new java.awt.Dimension(60, 20));
        jPNome.add(jLNome);

        jTFNome.setMaximumSize(new java.awt.Dimension(300, 20));
        jTFNome.setMinimumSize(new java.awt.Dimension(300, 20));
        jTFNome.setPreferredSize(new java.awt.Dimension(300, 20));
        jPNome.add(jTFNome);

        jPanel_Filtro.add(jPNome);

        jPNome1.setMaximumSize(new java.awt.Dimension(32767, 32));
        jPNome1.setMinimumSize(new java.awt.Dimension(10, 22));
        jPNome1.setPreferredSize(new java.awt.Dimension(438, 32));
        jPNome1.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 6));

        jLNome1.setText("Endereço:");
        jLNome1.setMaximumSize(new java.awt.Dimension(60, 20));
        jLNome1.setMinimumSize(new java.awt.Dimension(60, 20));
        jLNome1.setPreferredSize(new java.awt.Dimension(60, 20));
        jPNome1.add(jLNome1);

        jTFEndereco.setMaximumSize(new java.awt.Dimension(300, 20));
        jTFEndereco.setMinimumSize(new java.awt.Dimension(300, 20));
        jTFEndereco.setPreferredSize(new java.awt.Dimension(300, 20));
        jPNome1.add(jTFEndereco);

        jPanel_Filtro.add(jPNome1);

        jPNome2.setMaximumSize(new java.awt.Dimension(32767, 32));
        jPNome2.setMinimumSize(new java.awt.Dimension(10, 22));
        jPNome2.setPreferredSize(new java.awt.Dimension(438, 32));
        jPNome2.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 6));

        jLNome2.setText("Cidade:");
        jLNome2.setMaximumSize(new java.awt.Dimension(60, 20));
        jLNome2.setMinimumSize(new java.awt.Dimension(60, 20));
        jLNome2.setPreferredSize(new java.awt.Dimension(60, 20));
        jPNome2.add(jLNome2);

        jTFCidade.setMaximumSize(new java.awt.Dimension(300, 20));
        jTFCidade.setMinimumSize(new java.awt.Dimension(300, 20));
        jTFCidade.setPreferredSize(new java.awt.Dimension(300, 20));
        jPNome2.add(jTFCidade);

        jPanel_Filtro.add(jPNome2);

        jPNome3.setMaximumSize(new java.awt.Dimension(32767, 32));
        jPNome3.setMinimumSize(new java.awt.Dimension(10, 22));
        jPNome3.setPreferredSize(new java.awt.Dimension(438, 32));
        jPNome3.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 6));

        jLNome3.setText("Contato:");
        jLNome3.setMaximumSize(new java.awt.Dimension(60, 20));
        jLNome3.setMinimumSize(new java.awt.Dimension(60, 20));
        jLNome3.setPreferredSize(new java.awt.Dimension(60, 20));
        jPNome3.add(jLNome3);

        jTFContato.setMaximumSize(new java.awt.Dimension(300, 20));
        jTFContato.setMinimumSize(new java.awt.Dimension(300, 20));
        jTFContato.setPreferredSize(new java.awt.Dimension(300, 20));
        jPNome3.add(jTFContato);

        jPanel_Filtro.add(jPNome3);

        getContentPane().add(jPanel_Filtro, java.awt.BorderLayout.CENTER);

        jPanel_Buttons.setPreferredSize(new java.awt.Dimension(400, 33));

        jButton_Gerar.setMnemonic('G');
        jButton_Gerar.setText("Gravar");
        jButton_Gerar.setToolTipText("");
        jButton_Gerar.setMaximumSize(new java.awt.Dimension(95, 23));
        jButton_Gerar.setMinimumSize(new java.awt.Dimension(95, 23));
        jButton_Gerar.setPreferredSize(new java.awt.Dimension(95, 23));
        jButton_Gerar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_GerarActionPerformed(evt);
            }
        });
        jPanel_Buttons.add(jButton_Gerar);

        getContentPane().add(jPanel_Buttons, java.awt.BorderLayout.SOUTH);

        setSize(new java.awt.Dimension(451, 254));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton_GerarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_GerarActionPerformed
        gravar();
    }//GEN-LAST:event_jButton_GerarActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private final javax.swing.JButton jButton_Gerar = new javax.swing.JButton();
    private javax.swing.JLabel jLNome;
    private javax.swing.JLabel jLNome1;
    private javax.swing.JLabel jLNome2;
    private javax.swing.JLabel jLNome3;
    private javax.swing.JPanel jPNome;
    private javax.swing.JPanel jPNome1;
    private javax.swing.JPanel jPNome2;
    private javax.swing.JPanel jPNome3;
    private final javax.swing.JPanel jPanel_Buttons = new javax.swing.JPanel();
    private final javax.swing.JPanel jPanel_Filtro = new javax.swing.JPanel();
    private javax.swing.JTextField jTFCidade;
    private javax.swing.JTextField jTFContato;
    private javax.swing.JTextField jTFEndereco;
    private javax.swing.JTextField jTFNome;
    // End of variables declaration//GEN-END:variables

}
