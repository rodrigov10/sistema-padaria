package com.encomenda;

import base.Valor;
import base.bd.ObjetoID;
import base.cache.CacheObjeto;
import base.usuario.Usuario;
import com.pessoa.Pessoa;
import java.util.Date;
import java.util.Objects;

/**
 * Classe de emcomenda.
 * @author Rodrigo Vianna
 * @since 13/03/2019
 */
public class Encomenda implements ObjetoID, CacheObjeto<Integer> {

    private Integer id;
    private Integer codigo;
    private Date dataPedido;
    private Date dataParaEntrega;
    private Valor valorTotal;
    private String observacao;
    private Pessoa cliente;
    private boolean finalizado = false;

    private Date dataInclusao;
    private Usuario usuarioInclusao;
    private Date dataAlteracao;
    private Usuario usuarioAlteracao;

    public Encomenda(Integer id) {
        this.id = id;
    }

    public Encomenda() {
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public Date getDataPedido() {
        return dataPedido;
    }

    public void setDataPedido(Date dataPedido) {
        this.dataPedido = dataPedido;
    }

    public Date getDataParaEntrega() {
        return dataParaEntrega;
    }

    public void setDataParaEntrega(Date dataParaEntrega) {
        this.dataParaEntrega = dataParaEntrega;
    }

    public Valor getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(Valor valorTotal) {
        this.valorTotal = valorTotal;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public Pessoa getCliente() {
        return cliente;
    }

    public void setCliente(Pessoa cliente) {
        this.cliente = cliente;
    }

    public boolean isFinalizado() {
        return finalizado;
    }

    public void setFinalizado(boolean finalizado) {
        this.finalizado = finalizado;
    }

    public Date getDataInclusao() {
        return dataInclusao;
    }

    public void setDataInclusao(Date dataInclusao) {
        this.dataInclusao = dataInclusao;
    }

    public Usuario getUsuarioInclusao() {
        return usuarioInclusao;
    }

    public void setUsuarioInclusao(Usuario usuarioInclusao) {
        this.usuarioInclusao = usuarioInclusao;
    }

    public Date getDataAlteracao() {
        return dataAlteracao;
    }

    public void setDataAlteracao(Date dataAlteracao) {
        this.dataAlteracao = dataAlteracao;
    }

    public Usuario getUsuarioAlteracao() {
        return usuarioAlteracao;
    }

    public void setUsuarioAlteracao(Usuario usuarioAlteracao) {
        this.usuarioAlteracao = usuarioAlteracao;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Encomenda other = (Encomenda) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

}
