package com.encomenda;

import base.Valor;
import base.bd.BDException;
import base.bd.Conexao;
import base.bd.SQL;
import base.cache.CacheManager;
import base.login.Sessao;
import base.usuario.Usuario;
import base.usuario.UsuarioBD;
import com.pessoa.Pessoa;
import com.pessoa.PessoaBD;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Carlos
 * @since 15/05/2019
 */
public class EncomendaBD {

    public static final String TBL = "tabencomenda";
    
    private static final CacheManager<Encomenda> CACHE = CacheManager.getInstance(Encomenda.class);

    private static Integer getNovoId() throws BDException {
        SQL sql = new SQL("SELECT nextval('tabencomenda_id_seq') seq");
        try (Conexao c = Conexao.getConexao()) {
            try (ResultSet rs = c.executeQuery(sql)) {
                if (rs.next()) {
                    return rs.getInt("seq");
                }
            } catch (SQLException ex) {
                throw new BDException(ex);
            }
        }
        return null;
    }

    private static Integer getNovoCodigo() throws BDException {
        SQL sql = new SQL("SELECT nextval('tabencomenda_codigo_seq') seq");
        try (Conexao c = Conexao.getConexao()) {
            try (ResultSet rs = c.executeQuery(sql)) {
                if (rs.next()) {
                    return rs.getInt("seq");
                }
            } catch (SQLException ex) {
                throw new BDException(ex);
            }
        }
        return null;
    }

    public static void salvar(Encomenda encomenda) throws BDException {
        encomenda.setId(getNovoId());
        encomenda.setCodigo(getNovoCodigo());
        Usuario usuario = Sessao.getUsuarioLogado();
        Date data = new Date();
        SQL sql = new SQL();
        sql.add("INSERT INTO " + TBL);
        sql.add("(id, id_pessoa, codigo, datapedido,dataparaentrega, finalizado, valor, observacao, dt_inclusao, dt_alteracao, id_usu_inclusao, id_usu_alteracao)");
        sql.add("VALUES(:id,:id_pessoa,:codigo,:datapedido,:dataparaentrega, :finalizado, :valor,:observacao,:dt_inclusao,:dt_alteracao,:id_usu_inclusao,:id_usu_alteracao)");
        sql.setParametro("id", encomenda);
        sql.setParametro("id_pessoa", encomenda.getCliente());
        sql.setParametro("codigo", encomenda.getCodigo());
        sql.setParametro("datapedido", encomenda.getDataPedido());
        sql.setParametro("dataparaentrega", encomenda.getDataParaEntrega());
        sql.setParametro("finalizado", encomenda.isFinalizado());
        sql.setParametro("valor", encomenda.getValorTotal());
        sql.setParametro("observacao", encomenda.getObservacao());
        sql.setParametro("dt_inclusao", data);
        sql.setParametro("dt_alteracao", data);
        sql.setParametro("id_usu_inclusao", usuario);
        sql.setParametro("id_usu_alteracao", usuario);
        Conexao.update(sql);
        encomenda.setUsuarioInclusao(usuario);
        encomenda.setDataInclusao(data);
        encomenda.setUsuarioAlteracao(usuario);
        encomenda.setDataAlteracao(data);
        CACHE.put(encomenda);
    }

    public static void alterar(Encomenda encomenda) throws BDException {
        Usuario usuario = Sessao.getUsuarioLogado();
        Date data = new Date();
        SQL sql = new SQL();
        sql.add("UPDATE " + TBL);
        sql.add("SET id_pessoa = :id_pessoa, codigo = :codigo, datapedido = :datapedido, dataparaentrega = :dataparaentrega, finalizado = :finalizado,");
        sql.add("valor = :valor, observacao = :observacao, dt_alteracao = :dt_alteracao, id_usu_alteracao = :id_usu_alteracao");
        sql.add("WHERE id = :id");
        sql.setParametro("id", encomenda);
        sql.setParametro("codigo", encomenda.getCodigo());
        sql.setParametro("id_pessoa", encomenda.getCliente());
        sql.setParametro("datapedido", encomenda.getDataPedido());
        sql.setParametro("dataparaentrega", encomenda.getDataParaEntrega());
        sql.setParametro("finalizado", encomenda.isFinalizado());
        sql.setParametro("valor", encomenda.getValorTotal());
        sql.setParametro("observacao", encomenda.getObservacao());
        sql.setParametro("dt_alteracao", data);
        sql.setParametro("id_usu_alteracao", usuario);
        Conexao.update(sql);
        encomenda.setUsuarioAlteracao(usuario);
        encomenda.setDataAlteracao(data);
        CACHE.put(encomenda);
    }

    public static void excluir(Integer id) throws BDException {
        SQL sql = new SQL("DELETE FROM " + TBL + " WHERE id = :id");
        sql.setParametro("id", id);
        Conexao.update(sql);
        CACHE.remove(id);
    }

    public static boolean existe(Encomenda encomenda) throws BDException {
        SQL sql = new SQL("SELECT id FROM " + TBL + " WHERE codigo = :codigo");
        sql.setParametro("codigo", encomenda.getCodigo());
        try (Conexao c = Conexao.getConexao()) {
            try (ResultSet rs = c.executeQuery(sql)) {
                if (rs.next()) {
                    encomenda.setId(rs.getInt("id"));
                    return true;
                }
                return false;
            } catch (SQLException ex) {
                throw new BDException(ex);
            }
        }
    }

    public static void carregar(Encomenda encomenda) throws BDException {
        Encomenda objCache = CACHE.get(encomenda.getId());
        if (objCache != null) {
            carregarObjeto(encomenda, objCache);
            return;
        }
        SQL sql = new SQL("SELECT * FROM " + TBL + " WHERE id = :id");
        sql.setParametro("id", encomenda);
        try (Conexao c = Conexao.getConexao()) {
            try (ResultSet rs = c.executeQuery(sql)) {
                while (rs.next()) {
                    carregarObjeto(encomenda, rs);
                }
            } catch (SQLException ex) {
                throw new BDException(ex);
            }
        }
    }
    
    private static void carregarObjeto(Encomenda encomenda, Encomenda cache) {
        encomenda.setId(cache.getId());
        encomenda.setCodigo(cache.getCodigo());
        encomenda.setDataPedido(cache.getDataPedido());
        encomenda.setDataParaEntrega(cache.getDataParaEntrega());
        encomenda.setObservacao(cache.getObservacao());
        encomenda.setValorTotal(cache.getValorTotal());
        encomenda.setCliente(cache.getCliente());
        encomenda.setFinalizado(cache.isFinalizado());
        encomenda.setDataInclusao(cache.getDataInclusao());
        encomenda.setUsuarioInclusao(cache.getUsuarioInclusao());
        encomenda.setDataAlteracao(cache.getDataAlteracao());
        encomenda.setUsuarioAlteracao(cache.getUsuarioAlteracao());
    }

    private static void carregarObjeto(Encomenda encomenda, ResultSet rs) throws SQLException, BDException {
        encomenda.setCodigo(rs.getInt("codigo"));
        encomenda.setDataPedido(rs.getDate("datapedido"));
        encomenda.setDataParaEntrega(rs.getDate("dataparaentrega"));
        encomenda.setObservacao(rs.getString("observacao"));
        encomenda.setValorTotal(new Valor(rs.getBigDecimal("valor")));
        encomenda.setCliente(new Pessoa(rs.getInt("id_pessoa")));
        PessoaBD.carregar(encomenda.getCliente());
        encomenda.setFinalizado(rs.getBoolean("finalizado"));
        encomenda.setUsuarioInclusao(new Usuario(rs.getInt("id_usu_inclusao")));
        UsuarioBD.carregar(encomenda.getUsuarioInclusao());
        encomenda.setDataInclusao(rs.getDate("dt_inclusao"));
        encomenda.setUsuarioAlteracao(new Usuario(rs.getInt("id_usu_alteracao")));
        UsuarioBD.carregar(encomenda.getUsuarioAlteracao());
        encomenda.setDataAlteracao(rs.getDate("dt_alteracao"));
    }

    public static List<Encomenda> listarConsulta(EncomendaSC filtro) throws BDException {
        SQL sql = new SQL();
        sql.add("SELECT e.id, e.codigo, e.datapedido, e.dataparaentrega, e.observacao, e.valor, e.id_pessoa, e.finalizado, ");
        sql.add("p.codigo cdPes, p.nome nmPes");
        sql.add("FROM " + TBL + " e");
        sql.add("INNER JOIN tabpessoa p ON p.id = e.id_pessoa");
        sql.add("WHERE TRUE");
        if (filtro.apenasAtrasados) {
            sql.add("AND DATE(e.dataparaentrega) < DATE(NOW())");
        } else if (filtro.dtInicialEntrega != null && filtro.dtFinalEntrega != null) {
            sql.add("AND DATE(e.dataparaentrega) BETWEEN DATE(:dtInicialEntrega) AND DATE(:dtFinalEntrega)");
            sql.setParametro("dtInicialEntrega", filtro.dtInicialEntrega);
            sql.setParametro("dtFinalEntrega", filtro.dtFinalEntrega);
        }
        if (!filtro.incluirFinalizados) {
            sql.add("AND e.finalizado = FALSE");
        }
        if (filtro.nomeCliente != null && !filtro.nomeCliente.isEmpty()) {
            sql.add("AND p.nome = :nome");
            sql.setParametro("nome", filtro.nomeCliente + "%");
        }
        sql.add("LIMIT 1000");
        
        try (Conexao c = Conexao.getConexao()) {
            try (ResultSet rs = c.executeQuery(sql)) {
                List<Encomenda> lista = new ArrayList();
                while (rs.next()) {
                    Encomenda encomenda = new Encomenda();
                    encomenda.setId(rs.getInt("id"));
                    encomenda.setCodigo(rs.getInt("codigo"));
                    encomenda.setDataPedido(rs.getDate("datapedido"));
                    encomenda.setDataParaEntrega(rs.getDate("dataparaentrega"));
                    encomenda.setObservacao(rs.getString("observacao"));
                    encomenda.setFinalizado(rs.getBoolean("finalizado"));
                    encomenda.setValorTotal(new Valor(rs.getBigDecimal("valor")));
                    encomenda.setCliente(new Pessoa(rs.getInt("id_pessoa")));
                    encomenda.getCliente().setCodigo(rs.getInt("cdPes"));
                    encomenda.getCliente().setNome(rs.getString("nmPes"));
                    lista.add(encomenda);
                }
                return lista;
            } catch (SQLException ex) {
                throw new BDException(ex);
            }
        }
    }

    public static void alterarStatusFinalizado(Integer id, boolean status) throws BDException {
        SQL sql = new SQL();
        sql.add("UPDATE " + TBL);
        sql.add("SET finalizado = :finalizado");
        sql.add("WHERE id = :id");
        sql.setParametro("id", id);
        sql.setParametro("finalizado", status);
        Conexao.update(sql);
    }
}
