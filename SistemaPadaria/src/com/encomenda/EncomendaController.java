package com.encomenda;

import base.CampoException;
import base.bd.BDException;
import com.encomenda.item.ItemEncomendaProdutoBD;

/**
 *
 * @author Carlos
 * @since 10/04/2019
 */
public class EncomendaController {

    public static void salvar(Encomenda encomenda) throws BDException, CampoException {
        if (encomenda.getId() == null || !EncomendaBD.existe(encomenda)) {
            EncomendaBD.salvar(encomenda);
        } else {
            if (encomenda.getCodigo() == null) {
                throw new CampoException("Código não informado");
            }
            EncomendaBD.alterar(encomenda);
        }
    }

    public static Encomenda buscar(Integer codigo) throws BDException {
        Encomenda encomenda = new Encomenda();
        encomenda.setCodigo(codigo);
        if (EncomendaBD.existe(encomenda)) {
            EncomendaBD.carregar(encomenda);
            return encomenda;
        }
        return null;
    }

    public static void excluir(Integer codigo) throws BDException {
        ItemEncomendaProdutoBD.excluirEncomenda(codigo);
        EncomendaBD.excluir(codigo);
    }

    public static boolean alterarStatusFinalizado(Encomenda encomenda, boolean status) throws BDException {
        if (encomenda == null || encomenda.getId() == null) {
            return false;
        }
        EncomendaBD.alterarStatusFinalizado(encomenda.getId(), status);
        encomenda.setFinalizado(status);
        return true;
    }

}
