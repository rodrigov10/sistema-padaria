package com.encomenda;

import java.util.Date;

/**
 *
 * @author Rodrigo Vianna
 * @since 14/06/2019
 */
public class EncomendaSC {

    public Date dtInicialEntrega;
    public Date dtFinalEntrega;
    public String nomeCliente;
    public boolean incluirFinalizados;
    public boolean apenasAtrasados;

}
