package com.encomenda;

import base.CampoException;
import com.encomenda.item.ItemEncomendaProduto;
import base.TableModelObjeto;
import base.Valor;
import base.bd.BDException;
import base.msg.JAlerta;
import base.utils.FieldFormatUtils;
import base.utils.SwingUtils;
import com.encomenda.item.ItemEncomendaProdutoBD;
import com.encomenda.item.ItemEncomendaProdutoController;
import com.encomenda.item.ItemEncomendaProdutoSC;
import com.pessoa.PessoaListener;
import com.produto.ProdutoListener;
import java.awt.Component;
import java.awt.event.ItemEvent;
import java.util.List;
import javax.swing.event.ListSelectionEvent;

/**
 *
 * @author Éderson
 * @since 27/03/2019
 */
public class JEncomenda extends javax.swing.JFrame {

    private static JEncomenda jEncomenda;

    public static JEncomenda getInstance() {
        if (jEncomenda == null) {
            jEncomenda = new JEncomenda();
        }

        return jEncomenda;
    }

    private Encomenda encomenda;
    private ItemEncomendaProduto item;

    private JEncomendaConsulta jEncomendaConsulta;

    private PessoaListener pessoaListener;
    private ProdutoListener produtoListener;

    private boolean processando = false;
    private boolean atualizarTabela = false;

    private JEncomenda() {
        initComponents();
        inicializar();
    }

    private void inicializar() {
        SwingUtils.inicializarTela(this);

        pessoaListener = new PessoaListener(this, jTFCliente_Codigo, jTFCliente_Nome, jBBuscarCliente, true, PessoaListener.Tipo.CLIENTE);
        produtoListener = new ProdutoListener(this, jTFProduto_Codigo, jTFProduto_Nome, jBBuscar_Produto, true);

        FieldFormatUtils.formatarMonetario(jTFValorTotal);
        FieldFormatUtils.formatarMonetario(jFTQuantidade);

        inicializarTabela();
        limparTela();
    }

    ////////////////////////////////////////////////////////////////////////////
    //Encomenda
    ////////////////////////////////////////////////////////////////////////////
    public void setEncomenda(Encomenda encomenda) {
        try {
            processando = true;
            this.encomenda = encomenda;

            jTFCodigo.setText(encomenda == null ? "" : String.valueOf(encomenda.getCodigo()));
            pessoaListener.setPessoa(encomenda == null ? null : encomenda.getCliente());
            jDataFieldDataPedido.setDataSelecionada(encomenda == null ? null : encomenda.getDataPedido());
            jDataFieldDataEntrega.setDataSelecionada(encomenda == null ? null : encomenda.getDataParaEntrega());
            jCheckBox_Finalizado.setSelected(encomenda == null || encomenda.isFinalizado());
            jTFValorTotal.setText(encomenda == null ? "0,00" : encomenda.getValorTotal().toString());
            jTFObservacao.setText(encomenda == null ? "" : encomenda.getObservacao());

            if (this.encomenda == null) {
                SwingUtils.desabilitarCampos(jBInserir, jBRemover, jBAlterar, jBLimparItem);
            } else {
                atualizarTabela();
                SwingUtils.habilitarCampos(jBInserir, jBRemover, jBAlterar, jBLimparItem);

            }
        } finally {
            processando = false;
        }
    }

    private void salvar() {
        try {
            SwingUtils.iniciarCursorProcessamento(this);
            SwingUtils.desabilitarCampos(getCampos());

            String codigo = jTFCodigo.getText().trim();

            if (encomenda == null) {
                encomenda = new Encomenda();
            }
            if (!codigo.isEmpty()) {
                encomenda.setCodigo(Integer.parseInt(codigo));
            }

            encomenda.setCliente(pessoaListener.getPessoa());
            encomenda.setDataParaEntrega(jDataFieldDataEntrega.getDataSelecionada());
            encomenda.setDataPedido(jDataFieldDataPedido.getDataSelecionada());
            encomenda.setValorTotal(new Valor(jTFValorTotal.getText()));
            encomenda.setObservacao(jTFObservacao.getText());

            try {
                EncomendaController.salvar(encomenda);
                atualizarTabela = true;
                if (encomenda.getCodigo() != null) {
                    jTFCodigo.setText(String.valueOf(encomenda.getCodigo()));
                }
                JAlerta.getAlertaMsg(this, "Salvo com sucesso.");
            } catch (BDException ex) {
                JAlerta.getAlertaErro(this, "Erro ao salvar encomenda: ", ex);
            } catch (CampoException ex) {
                JAlerta.getAlertaErro(this, ex.getMessage());
            }
        } finally {
            SwingUtils.finalizarCursorProcessamento(this);
            SwingUtils.habilitarCampos(getCampos());
        }
    }

    private void buscar() {
        String codigo = jTFCodigo.getText().trim();
        if (codigo.isEmpty()) {
            JAlerta.getAlertaMsg(this, "Código não informado.");
            jTFCodigo.requestFocusInWindow();
            return;
        }
        try {
            SwingUtils.iniciarCursorProcessamento(this);
            SwingUtils.desabilitarCampos(getCampos());

            Encomenda e = EncomendaController.buscar(Integer.parseInt(codigo));
            if (e == null) {
                JAlerta.getAlertaMsg(this, "Encomenda não encontrada");
            }
            setEncomenda(e);
        } catch (BDException ex) {
            JAlerta.getAlertaErro(this, "Erro ao buscar encomenda: ", ex);
        } finally {
            SwingUtils.finalizarCursorProcessamento(this);
            SwingUtils.habilitarCampos(getCampos());
        }
    }

    private void excluir() {
        if (encomenda == null) {
            JAlerta.getAlertaMsg(this, "Encomenda não informada.");
            return;
        }
        try {
            SwingUtils.iniciarCursorProcessamento(this);
            SwingUtils.desabilitarCampos(getCampos());

            EncomendaController.excluir(encomenda.getId());
            atualizarTabela = true;
            limparTela();
        } catch (BDException ex) {
            JAlerta.getAlertaErro(this, "Erro ao buscar pessoa: ", ex);
        } finally {
            SwingUtils.finalizarCursorProcessamento(this);
            SwingUtils.habilitarCampos(getCampos());
        }
    }

    private void consultar() {
        try {
            SwingUtils.iniciarCursorProcessamento(this);

            if (jEncomendaConsulta == null) {
                jEncomendaConsulta = JEncomendaConsulta.getInstance(this);
                jEncomendaConsulta.atualizarTabelaThread();
            } else if (atualizarTabela) {
                atualizarTabela = false;
                jEncomendaConsulta.atualizarTabelaThread();
            }
            jEncomendaConsulta.setVisible(true);
            if (jEncomendaConsulta.isRetornar()) {
                Encomenda encomenda = jEncomendaConsulta.getEncomenda();
                setEncomenda(encomenda);
            }
        } catch (BDException ex) {
            JAlerta.getAlertaErro(this, "Erro ao consultar encomenda: ", ex);
            setEncomenda(null);
        } finally {
            SwingUtils.finalizarCursorProcessamento(this);
        }
    }

    private void alterarFinalizado(ItemEvent evt) {
        try {
            SwingUtils.iniciarCursorProcessamento(this);
            SwingUtils.desabilitarCampos(jCheckBox_Finalizado);

            EncomendaController.alterarStatusFinalizado(encomenda, evt.getStateChange() == ItemEvent.SELECTED);
        } catch (BDException ex) {
            JAlerta.getAlertaErro(this, "Erro ao alterar status: ", ex);
        } finally {
            SwingUtils.finalizarCursorProcessamento(this);
            SwingUtils.habilitarCampos(jCheckBox_Finalizado);
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    //Item
    ////////////////////////////////////////////////////////////////////////////
    private void inicializarTabela() {
        TableModelObjeto<ItemEncomendaProduto> tm = new TableModelObjeto(jTable.getColumnModel());
        tm.addColumn("Produto");
        tm.addColumn("Quantidade");
        tm.addColumn("Objeto");

        jTable.setModel(tm);

        tm.getColumn("Produto").setPreferredWidth(200);
        tm.getColumn("Quantidade").setPreferredWidth(80);
        jTable.removeColumn(tm.getColumn("Objeto"));

        jTable.getSelectionModel().addListSelectionListener((ListSelectionEvent e) -> {
            if (e.getValueIsAdjusting() || jTable.getSelectedRow() == -1) {
                return;
            }
            setItemEncomendaProduto(((TableModelObjeto<ItemEncomendaProduto>) jTable.getModel()).getObjeto(jTable.getSelectedRow()));
        });
    }

    private void atualizarTabela() {
        try {
            TableModelObjeto<ItemEncomendaProduto> tm = ((TableModelObjeto<ItemEncomendaProduto>) jTable.getModel());
            tm.reset();

            if (encomenda == null) {
                return;
            }

            List<ItemEncomendaProduto> lista = ItemEncomendaProdutoBD.listar(new ItemEncomendaProdutoSC(encomenda));
            if (lista.isEmpty()) {
                return;
            }

            lista.stream().map((item) -> {
                Object[] row = new Object[3];
                row[0] = item.getProduto().getNome();
                row[1] = item.getQuantidade().toString();
                row[2] = item;
                return row;
            }).forEachOrdered((row) -> {
                tm.addRow(row);
            });

            if (jTable.getRowCount() > 0) {
                jTable.changeSelection(0, 0, false, false);
            }

        } catch (BDException ex) {
            JAlerta.getAlertaErro(this, "Erro ao listar itens: ", ex);
        }
    }

    private void salvarItem() {
        try {
            SwingUtils.iniciarCursorProcessamento(this);
            SwingUtils.desabilitarCampos(getCampos());

            item = new ItemEncomendaProduto();
            item.setEncomenda(encomenda);
            item.setProduto(produtoListener.getProduto());
            item.setQuantidade(new Valor(jFTQuantidade.getText()));
            item.setObservacao(jTFObservacaoItem.getText());

            try {
                ItemEncomendaProdutoController.salvar(item);
                atualizarTabela();
            } catch (BDException ex) {
                JAlerta.getAlertaErro(this, "Erro ao salvar encomenda: ", ex);
            } catch (CampoException ex) {
                JAlerta.getAlertaErro(this, ex.getMessage());
            }
        } finally {
            SwingUtils.finalizarCursorProcessamento(this);
            SwingUtils.habilitarCampos(getCampos());
        }
    }

    private void alterarItem() {
        try {
            SwingUtils.iniciarCursorProcessamento(this);
            SwingUtils.desabilitarCampos(getCampos());

            item.setProduto(produtoListener.getProduto());
            item.setQuantidade(new Valor(jFTQuantidade.getText()));
            item.setObservacao(jTFObservacaoItem.getText());

            try {
                ItemEncomendaProdutoController.salvar(item);
                atualizarTabela();
            } catch (BDException ex) {
                JAlerta.getAlertaErro(this, "Erro ao salvar encomenda: ", ex);
            } catch (CampoException ex) {
                JAlerta.getAlertaErro(this, ex.getMessage());
            }
        } finally {
            SwingUtils.finalizarCursorProcessamento(this);
            SwingUtils.habilitarCampos(getCampos());
        }
    }

    private void excluirItem() {
        if (item == null) {
            JAlerta.getAlertaMsg(this, "Nenhum item selecionado.");
            return;
        }
        try {
            SwingUtils.iniciarCursorProcessamento(this);
            SwingUtils.desabilitarCampos(getCampos());

            ItemEncomendaProdutoController.excluir(item.getId());
            atualizarTabela = true;
            limparTela();
        } catch (BDException ex) {
            JAlerta.getAlertaErro(this, "Erro ao buscar pessoa: ", ex);
        } finally {
            SwingUtils.finalizarCursorProcessamento(this);
            SwingUtils.habilitarCampos(getCampos());
        }
    }

    private void limparItem() {
        jTable.clearSelection();
        setItemEncomendaProduto(null);
    }

    private void setItemEncomendaProduto(ItemEncomendaProduto item) {
        this.item = item;

        produtoListener.setProduto(item == null ? null : item.getProduto());
        jFTQuantidade.setText(item == null ? "0,00" : item.getQuantidade().toString());
        jTFObservacaoItem.setText(item == null ? null : item.getObservacao());

        jBInserir.setVisible(item == null);
        jBRemover.setVisible(item != null);
        jBAlterar.setVisible(item != null);
    }

    public void limparTela() {
        setEncomenda(null);
        limparItem();
        TableModelObjeto<ItemEncomendaProduto> tm = ((TableModelObjeto<ItemEncomendaProduto>) jTable.getModel());
        tm.reset();
    }

    protected Component[] getCampos() {
        return new Component[]{jBGravar, jBConsultar, jBLimpar, jBExcluir, jBBuscar, jCheckBox_Finalizado, jBBuscarCliente, jBBuscar_Produto, jBInserir, jBRemover, jBAlterar, jBLimparItem};
    }

    public static void main(String[] args) {
        if (com.Start.iniciarSistema()) {
            new JEncomenda().setVisible(true);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPCliente = new javax.swing.JPanel();
        jLCliente = new javax.swing.JLabel();
        jTFCliente_Codigo = new javax.swing.JTextField();
        jTFCliente_Nome = new javax.swing.JTextField();
        jBBuscarCliente = new javax.swing.JButton();
        jDataFieldDataEntrega = new base.utils.calendario.JDataField();
        jDataFieldDataPedido = new base.utils.calendario.JDataField();
        jTFValorTotal = new javax.swing.JTextField();
        jPProduto = new javax.swing.JPanel();
        jLProduto = new javax.swing.JLabel();
        jTFProduto_Codigo = new javax.swing.JTextField();
        jTFProduto_Nome = new javax.swing.JTextField();
        jBBuscar_Produto = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Cadastro de Encomenda");
        setMinimumSize(new java.awt.Dimension(500, 705));
        setPreferredSize(new java.awt.Dimension(500, 705));

        jPFundo.setLayout(new java.awt.BorderLayout());

        jPDadosEncomenda.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Dados", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 1, 14))); // NOI18N
        jPDadosEncomenda.setPreferredSize(new java.awt.Dimension(450, 491));
        jPDadosEncomenda.setLayout(new javax.swing.BoxLayout(jPDadosEncomenda, javax.swing.BoxLayout.Y_AXIS));

        jPCodigo.setMaximumSize(new java.awt.Dimension(32767, 32));
        jPCodigo.setMinimumSize(new java.awt.Dimension(10, 22));
        jPCodigo.setPreferredSize(new java.awt.Dimension(438, 32));
        jPCodigo.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 6));

        jLCodigo.setText("Código:");
        jLCodigo.setMaximumSize(new java.awt.Dimension(102, 20));
        jLCodigo.setMinimumSize(new java.awt.Dimension(102, 20));
        jLCodigo.setPreferredSize(new java.awt.Dimension(102, 20));
        jPCodigo.add(jLCodigo);

        jTFCodigo.setMaximumSize(new java.awt.Dimension(250, 20));
        jTFCodigo.setMinimumSize(new java.awt.Dimension(250, 20));
        jTFCodigo.setPreferredSize(new java.awt.Dimension(100, 20));
        jPCodigo.add(jTFCodigo);

        jBBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/icones/buscar_16.png"))); // NOI18N
        jBBuscar.setToolTipText("Adicionar");
        jBBuscar.setBorder(null);
        jBBuscar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jBBuscar.setMaximumSize(new java.awt.Dimension(23, 20));
        jBBuscar.setMinimumSize(new java.awt.Dimension(23, 20));
        jBBuscar.setPreferredSize(new java.awt.Dimension(23, 20));
        jBBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBBuscarActionPerformed(evt);
            }
        });
        jPCodigo.add(jBBuscar);
        jBBuscar.getAccessibleContext().setAccessibleDescription("Buscar");

        jPDadosEncomenda.add(jPCodigo);

        jPCliente.setMaximumSize(new java.awt.Dimension(32767, 32));
        jPCliente.setMinimumSize(new java.awt.Dimension(10, 22));
        jPCliente.setPreferredSize(new java.awt.Dimension(438, 32));
        jPCliente.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 6));

        jLCliente.setText("Cliente:");
        jLCliente.setMaximumSize(new java.awt.Dimension(102, 20));
        jLCliente.setMinimumSize(new java.awt.Dimension(102, 20));
        jLCliente.setPreferredSize(new java.awt.Dimension(102, 20));
        jPCliente.add(jLCliente);

        jTFCliente_Codigo.setMaximumSize(new java.awt.Dimension(70, 20));
        jTFCliente_Codigo.setMinimumSize(new java.awt.Dimension(70, 20));
        jTFCliente_Codigo.setPreferredSize(new java.awt.Dimension(70, 20));
        jPCliente.add(jTFCliente_Codigo);

        jTFCliente_Nome.setMaximumSize(new java.awt.Dimension(250, 20));
        jTFCliente_Nome.setMinimumSize(new java.awt.Dimension(250, 20));
        jTFCliente_Nome.setPreferredSize(new java.awt.Dimension(220, 20));
        jPCliente.add(jTFCliente_Nome);

        jBBuscarCliente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/icones/buscar_16.png"))); // NOI18N
        jBBuscarCliente.setToolTipText("Adicionar");
        jBBuscarCliente.setBorder(null);
        jBBuscarCliente.setMaximumSize(new java.awt.Dimension(23, 20));
        jBBuscarCliente.setMinimumSize(new java.awt.Dimension(23, 20));
        jBBuscarCliente.setPreferredSize(new java.awt.Dimension(23, 20));
        jPCliente.add(jBBuscarCliente);

        jPDadosEncomenda.add(jPCliente);

        jPDataEntrega.setMaximumSize(new java.awt.Dimension(32767, 32));
        jPDataEntrega.setMinimumSize(new java.awt.Dimension(10, 22));
        jPDataEntrega.setPreferredSize(new java.awt.Dimension(438, 32));
        jPDataEntrega.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 6));

        jLDataEntrega.setText("Data de Entrega:");
        jLDataEntrega.setMaximumSize(new java.awt.Dimension(95, 20));
        jLDataEntrega.setMinimumSize(new java.awt.Dimension(95, 20));
        jLDataEntrega.setPreferredSize(new java.awt.Dimension(95, 20));
        jPDataEntrega.add(jLDataEntrega);
        jPDataEntrega.add(jDataFieldDataEntrega);

        jCheckBox_Finalizado.setText("Finalizado");
        jCheckBox_Finalizado.setMaximumSize(new java.awt.Dimension(80, 23));
        jCheckBox_Finalizado.setMinimumSize(new java.awt.Dimension(80, 23));
        jCheckBox_Finalizado.setPreferredSize(new java.awt.Dimension(80, 23));
        jCheckBox_Finalizado.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jCheckBox_FinalizadoItemStateChanged(evt);
            }
        });
        jPDataEntrega.add(jCheckBox_Finalizado);

        jPDadosEncomenda.add(jPDataEntrega);

        jPDataPedido.setMaximumSize(new java.awt.Dimension(32767, 32));
        jPDataPedido.setMinimumSize(new java.awt.Dimension(10, 22));
        jPDataPedido.setPreferredSize(new java.awt.Dimension(438, 32));
        jPDataPedido.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 6));

        jLDataPedido.setText("Data do Pedido:");
        jLDataPedido.setMaximumSize(new java.awt.Dimension(95, 20));
        jLDataPedido.setMinimumSize(new java.awt.Dimension(95, 20));
        jLDataPedido.setPreferredSize(new java.awt.Dimension(95, 20));
        jPDataPedido.add(jLDataPedido);
        jPDataPedido.add(jDataFieldDataPedido);

        jPDadosEncomenda.add(jPDataPedido);

        jPCPF2.setMaximumSize(new java.awt.Dimension(32767, 32));
        jPCPF2.setMinimumSize(new java.awt.Dimension(10, 22));
        jPCPF2.setPreferredSize(new java.awt.Dimension(438, 32));
        jPCPF2.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 6));

        jLCliente3.setText("Valor Total:");
        jLCliente3.setMaximumSize(new java.awt.Dimension(102, 20));
        jLCliente3.setMinimumSize(new java.awt.Dimension(102, 20));
        jLCliente3.setPreferredSize(new java.awt.Dimension(102, 20));
        jPCPF2.add(jLCliente3);

        jTFValorTotal.setMaximumSize(new java.awt.Dimension(70, 20));
        jTFValorTotal.setMinimumSize(new java.awt.Dimension(70, 20));
        jTFValorTotal.setPreferredSize(new java.awt.Dimension(70, 20));
        jPCPF2.add(jTFValorTotal);

        jPDadosEncomenda.add(jPCPF2);

        jPEmail.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jPEmail.setMaximumSize(new java.awt.Dimension(1000, 32));
        jPEmail.setMinimumSize(new java.awt.Dimension(438, 32));
        jPEmail.setPreferredSize(new java.awt.Dimension(438, 32));
        jPEmail.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 6));

        jLEmail.setText("Observação:");
        jLEmail.setMaximumSize(new java.awt.Dimension(102, 20));
        jLEmail.setMinimumSize(new java.awt.Dimension(102, 20));
        jLEmail.setPreferredSize(new java.awt.Dimension(102, 20));
        jPEmail.add(jLEmail);

        jTFObservacao.setMaximumSize(new java.awt.Dimension(250, 20));
        jTFObservacao.setMinimumSize(new java.awt.Dimension(250, 20));
        jTFObservacao.setPreferredSize(new java.awt.Dimension(250, 20));
        jPEmail.add(jTFObservacao);

        jPDadosEncomenda.add(jPEmail);

        jPListaItens.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Itens", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 1, 14))); // NOI18N
        jPListaItens.setPreferredSize(new java.awt.Dimension(450, 300));
        jPListaItens.setLayout(new javax.swing.BoxLayout(jPListaItens, javax.swing.BoxLayout.Y_AXIS));

        jPProduto.setMaximumSize(new java.awt.Dimension(32767, 32));
        jPProduto.setMinimumSize(new java.awt.Dimension(10, 22));
        jPProduto.setPreferredSize(new java.awt.Dimension(438, 32));
        jPProduto.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 6));

        jLProduto.setText("Produto:");
        jLProduto.setMaximumSize(new java.awt.Dimension(95, 20));
        jLProduto.setMinimumSize(new java.awt.Dimension(95, 20));
        jLProduto.setPreferredSize(new java.awt.Dimension(95, 20));
        jPProduto.add(jLProduto);

        jTFProduto_Codigo.setMaximumSize(new java.awt.Dimension(70, 20));
        jTFProduto_Codigo.setMinimumSize(new java.awt.Dimension(70, 20));
        jTFProduto_Codigo.setPreferredSize(new java.awt.Dimension(70, 20));
        jPProduto.add(jTFProduto_Codigo);

        jTFProduto_Nome.setMaximumSize(new java.awt.Dimension(250, 20));
        jTFProduto_Nome.setMinimumSize(new java.awt.Dimension(250, 20));
        jTFProduto_Nome.setPreferredSize(new java.awt.Dimension(220, 20));
        jPProduto.add(jTFProduto_Nome);

        jBBuscar_Produto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/icones/buscar_16.png"))); // NOI18N
        jBBuscar_Produto.setToolTipText("Adicionar");
        jBBuscar_Produto.setBorder(null);
        jBBuscar_Produto.setMaximumSize(new java.awt.Dimension(23, 20));
        jBBuscar_Produto.setMinimumSize(new java.awt.Dimension(23, 20));
        jBBuscar_Produto.setPreferredSize(new java.awt.Dimension(23, 20));
        jPProduto.add(jBBuscar_Produto);

        jPListaItens.add(jPProduto);

        jPQuantidade.setMaximumSize(new java.awt.Dimension(1000, 32));
        jPQuantidade.setMinimumSize(new java.awt.Dimension(438, 32));
        jPQuantidade.setPreferredSize(new java.awt.Dimension(438, 32));
        jPQuantidade.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 6));

        jLTelefone2.setText("Quantidade:");
        jLTelefone2.setMaximumSize(new java.awt.Dimension(95, 20));
        jLTelefone2.setMinimumSize(new java.awt.Dimension(95, 20));
        jLTelefone2.setPreferredSize(new java.awt.Dimension(95, 20));
        jPQuantidade.add(jLTelefone2);

        jFTQuantidade.setMaximumSize(new java.awt.Dimension(100, 20));
        jFTQuantidade.setMinimumSize(new java.awt.Dimension(100, 20));
        jFTQuantidade.setPreferredSize(new java.awt.Dimension(100, 20));
        jPQuantidade.add(jFTQuantidade);

        jPListaItens.add(jPQuantidade);

        jPObservacao.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jPObservacao.setMaximumSize(new java.awt.Dimension(1000, 32));
        jPObservacao.setMinimumSize(new java.awt.Dimension(438, 32));
        jPObservacao.setPreferredSize(new java.awt.Dimension(438, 32));
        jPObservacao.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 6));

        jLObservacaoItem.setText("Observação:");
        jLObservacaoItem.setMaximumSize(new java.awt.Dimension(95, 20));
        jLObservacaoItem.setMinimumSize(new java.awt.Dimension(95, 20));
        jLObservacaoItem.setPreferredSize(new java.awt.Dimension(95, 20));
        jPObservacao.add(jLObservacaoItem);

        jTFObservacaoItem.setMaximumSize(new java.awt.Dimension(250, 20));
        jTFObservacaoItem.setMinimumSize(new java.awt.Dimension(250, 20));
        jTFObservacaoItem.setPreferredSize(new java.awt.Dimension(250, 20));
        jPObservacao.add(jTFObservacaoItem);

        jPListaItens.add(jPObservacao);

        jBInserir.setText("Inserir");
        jBInserir.setMaximumSize(new java.awt.Dimension(95, 23));
        jBInserir.setMinimumSize(new java.awt.Dimension(95, 23));
        jBInserir.setPreferredSize(new java.awt.Dimension(95, 23));
        jBInserir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBInserirActionPerformed(evt);
            }
        });
        jPBotoesItem.add(jBInserir);

        jBRemover.setText("Remover");
        jBRemover.setMaximumSize(new java.awt.Dimension(95, 23));
        jBRemover.setMinimumSize(new java.awt.Dimension(95, 23));
        jBRemover.setPreferredSize(new java.awt.Dimension(95, 23));
        jBRemover.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBRemoverActionPerformed(evt);
            }
        });
        jPBotoesItem.add(jBRemover);

        jBAlterar.setText("Alterar");
        jBAlterar.setMaximumSize(new java.awt.Dimension(95, 23));
        jBAlterar.setMinimumSize(new java.awt.Dimension(95, 23));
        jBAlterar.setPreferredSize(new java.awt.Dimension(95, 23));
        jBAlterar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBAlterarActionPerformed(evt);
            }
        });
        jPBotoesItem.add(jBAlterar);

        jBLimparItem.setText("Limpar");
        jBLimparItem.setMaximumSize(new java.awt.Dimension(95, 23));
        jBLimparItem.setMinimumSize(new java.awt.Dimension(95, 23));
        jBLimparItem.setPreferredSize(new java.awt.Dimension(95, 23));
        jBLimparItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBLimparItemActionPerformed(evt);
            }
        });
        jPBotoesItem.add(jBLimparItem);

        jPListaItens.add(jPBotoesItem);

        jScrollPane.setViewportView(jTable);

        jPListaItens.add(jScrollPane);

        jPDadosEncomenda.add(jPListaItens);

        jPFundo.add(jPDadosEncomenda, java.awt.BorderLayout.CENTER);

        getContentPane().add(jPFundo, java.awt.BorderLayout.CENTER);

        jPTitulo.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createEmptyBorder(10, 10, 10, 10), javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED)));

        jLTitulo.setFont(new java.awt.Font("Arial", 1, 24)); // NOI18N
        jLTitulo.setText("Cadastro de Encomenda");
        jPTitulo.add(jLTitulo);

        getContentPane().add(jPTitulo, java.awt.BorderLayout.NORTH);

        jBGravar.setMnemonic('G');
        jBGravar.setText("Gravar");
        jBGravar.setMaximumSize(new java.awt.Dimension(95, 23));
        jBGravar.setMinimumSize(new java.awt.Dimension(95, 23));
        jBGravar.setPreferredSize(new java.awt.Dimension(95, 23));
        jBGravar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBGravarActionPerformed(evt);
            }
        });
        jPBotoes.add(jBGravar);

        jBConsultar.setMnemonic('C');
        jBConsultar.setText("Consultar");
        jBConsultar.setMaximumSize(new java.awt.Dimension(95, 23));
        jBConsultar.setMinimumSize(new java.awt.Dimension(95, 23));
        jBConsultar.setPreferredSize(new java.awt.Dimension(95, 23));
        jBConsultar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBConsultarActionPerformed(evt);
            }
        });
        jPBotoes.add(jBConsultar);

        jBLimpar.setMnemonic('L');
        jBLimpar.setText("Limpar");
        jBLimpar.setMaximumSize(new java.awt.Dimension(95, 23));
        jBLimpar.setMinimumSize(new java.awt.Dimension(95, 23));
        jBLimpar.setPreferredSize(new java.awt.Dimension(95, 23));
        jBLimpar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBLimparActionPerformed(evt);
            }
        });
        jPBotoes.add(jBLimpar);

        jBExcluir.setMnemonic('E');
        jBExcluir.setText("Excluir");
        jBExcluir.setMaximumSize(new java.awt.Dimension(95, 23));
        jBExcluir.setMinimumSize(new java.awt.Dimension(95, 23));
        jBExcluir.setPreferredSize(new java.awt.Dimension(95, 23));
        jBExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBExcluirActionPerformed(evt);
            }
        });
        jPBotoes.add(jBExcluir);

        getContentPane().add(jPBotoes, java.awt.BorderLayout.SOUTH);

        setSize(new java.awt.Dimension(516, 744));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jBLimparActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBLimparActionPerformed
        limparTela();
    }//GEN-LAST:event_jBLimparActionPerformed

    private void jBExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBExcluirActionPerformed
        new Thread(() -> {
            excluir();
        }, "Excluir Encomenda").start();
    }//GEN-LAST:event_jBExcluirActionPerformed

    private void jBGravarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBGravarActionPerformed
        new Thread(() -> {
            salvar();
        }, "Gravar Encomenda").start();
    }//GEN-LAST:event_jBGravarActionPerformed

    private void jBBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBBuscarActionPerformed
        new Thread(() -> {
            buscar();
        }, "Busca Encomenda").start();
    }//GEN-LAST:event_jBBuscarActionPerformed

    private void jBConsultarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBConsultarActionPerformed
        consultar();
    }//GEN-LAST:event_jBConsultarActionPerformed

    private void jBInserirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBInserirActionPerformed
        new Thread(() -> {
            salvarItem();
        }, "SalvarItem de Encomenda").start();
    }//GEN-LAST:event_jBInserirActionPerformed

    private void jBRemoverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBRemoverActionPerformed
        new Thread(() -> {
            excluirItem();
        }, "Excluir Item de Encomenda").start();
    }//GEN-LAST:event_jBRemoverActionPerformed

    private void jBAlterarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBAlterarActionPerformed
        new Thread(() -> {
            alterarItem();
        }, "Alterar Item de Encomenda").start();
    }//GEN-LAST:event_jBAlterarActionPerformed

    private void jBLimparItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBLimparItemActionPerformed
        limparItem();
    }//GEN-LAST:event_jBLimparItemActionPerformed

    private void jCheckBox_FinalizadoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jCheckBox_FinalizadoItemStateChanged
        if (processando) {
            return;
        }
        new Thread(() -> {
            alterarFinalizado(evt);
        }, "Alterar Status Ativo").start();
    }//GEN-LAST:event_jCheckBox_FinalizadoItemStateChanged

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private final javax.swing.JButton jBAlterar = new javax.swing.JButton();
    private final javax.swing.JButton jBBuscar = new javax.swing.JButton();
    private javax.swing.JButton jBBuscarCliente;
    private javax.swing.JButton jBBuscar_Produto;
    private final javax.swing.JButton jBConsultar = new javax.swing.JButton();
    private final javax.swing.JButton jBExcluir = new javax.swing.JButton();
    private final javax.swing.JButton jBGravar = new javax.swing.JButton();
    private final javax.swing.JButton jBInserir = new javax.swing.JButton();
    private final javax.swing.JButton jBLimpar = new javax.swing.JButton();
    private final javax.swing.JButton jBLimparItem = new javax.swing.JButton();
    private final javax.swing.JButton jBRemover = new javax.swing.JButton();
    private final javax.swing.JCheckBox jCheckBox_Finalizado = new javax.swing.JCheckBox();
    private base.utils.calendario.JDataField jDataFieldDataEntrega;
    private base.utils.calendario.JDataField jDataFieldDataPedido;
    private final javax.swing.JFormattedTextField jFTQuantidade = new javax.swing.JFormattedTextField();
    private javax.swing.JLabel jLCliente;
    private final javax.swing.JLabel jLCliente3 = new javax.swing.JLabel();
    private final javax.swing.JLabel jLCodigo = new javax.swing.JLabel();
    private final javax.swing.JLabel jLDataEntrega = new javax.swing.JLabel();
    private final javax.swing.JLabel jLDataPedido = new javax.swing.JLabel();
    private final javax.swing.JLabel jLEmail = new javax.swing.JLabel();
    private final javax.swing.JLabel jLObservacaoItem = new javax.swing.JLabel();
    private javax.swing.JLabel jLProduto;
    private final javax.swing.JLabel jLTelefone2 = new javax.swing.JLabel();
    private final javax.swing.JLabel jLTitulo = new javax.swing.JLabel();
    private final javax.swing.JPanel jPBotoes = new javax.swing.JPanel();
    private final javax.swing.JPanel jPBotoesItem = new javax.swing.JPanel();
    private final javax.swing.JPanel jPCPF2 = new javax.swing.JPanel();
    private javax.swing.JPanel jPCliente;
    private final javax.swing.JPanel jPCodigo = new javax.swing.JPanel();
    private final javax.swing.JPanel jPDadosEncomenda = new javax.swing.JPanel();
    private final javax.swing.JPanel jPDataEntrega = new javax.swing.JPanel();
    private final javax.swing.JPanel jPDataPedido = new javax.swing.JPanel();
    private final javax.swing.JPanel jPEmail = new javax.swing.JPanel();
    private final javax.swing.JPanel jPFundo = new javax.swing.JPanel();
    private final javax.swing.JPanel jPListaItens = new javax.swing.JPanel();
    private final javax.swing.JPanel jPObservacao = new javax.swing.JPanel();
    private javax.swing.JPanel jPProduto;
    private final javax.swing.JPanel jPQuantidade = new javax.swing.JPanel();
    private final javax.swing.JPanel jPTitulo = new javax.swing.JPanel();
    private final javax.swing.JScrollPane jScrollPane = new javax.swing.JScrollPane();
    private javax.swing.JTextField jTFCliente_Codigo;
    private javax.swing.JTextField jTFCliente_Nome;
    private final javax.swing.JTextField jTFCodigo = new javax.swing.JTextField();
    private final javax.swing.JTextField jTFObservacao = new javax.swing.JTextField();
    private final javax.swing.JTextField jTFObservacaoItem = new javax.swing.JTextField();
    private javax.swing.JTextField jTFProduto_Codigo;
    private javax.swing.JTextField jTFProduto_Nome;
    private javax.swing.JTextField jTFValorTotal;
    private final javax.swing.JTable jTable = new javax.swing.JTable();
    // End of variables declaration//GEN-END:variables
}
