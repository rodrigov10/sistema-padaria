package com.encomenda;

import base.bd.BDException;
import base.bd.Conexao;
import base.bd.SQL;
import base.log.Log;
import base.notificacao.AlertaTable;
import base.notificacao.Registro;
import base.rotina.RotinaInterface;
import base.utils.DataUtils;
import base.utils.SwingUtils;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Rodrigo Vianna
 * @since 14/06/2019
 */
public class RotinaEncomenda implements RotinaInterface {

    private static final int TEMPO_ATUALIZACAO = 900000;

    private int encomendasDia = 0;
    private int encomendasSemana = 0;
    private int encomendasAtrasada = 0;

    private final ArrayList<Registro> registrosAtivos = new ArrayList(1);

    @Override
    public void run() {
        while (true) {
            try {
                encomendasDia = 0;
                encomendasSemana = 0;
                Log.log("Verificando encomendas...");
                getRegistrosBanco();

                AlertaTable.getAlerta().removerRegistro(getRegistros());

                registrosAtivos.clear();

                registrosAtivos.add(gerarDia());
                registrosAtivos.add(gerarSemana());
                registrosAtivos.add(gerarAtrasada());

                AlertaTable.getAlerta().adicionarRegistro(getRegistros());
            } catch (BDException e) {
                Log.log("Erro ao validar encomendas do dia: ", e);
            }
            try {
                Thread.sleep(TEMPO_ATUALIZACAO);
            } catch (InterruptedException e) {
            }
        }
    }

    private Registro[] getRegistros() {
        Registro[] lista = new Registro[registrosAtivos.size()];
        for (int i = 0; i < registrosAtivos.size(); i++) {
            lista[i] = registrosAtivos.get(i);
        }
        return lista;
    }

    private Registro gerarDia() {
        if (encomendasDia <= 0) {
            return null;
        }
        String descricao;
        if (encomendasDia == 1) {
            descricao = "Existe uma encomenda para hoje.";
        } else {
            descricao = "Existem " + encomendasDia + " encomendas para hoje.";
        }

        Registro.AcaoAoSelecionar acao = new Registro.AcaoAoSelecionar() {
            @Override
            public void run() {
                EncomendaSC filtro = new EncomendaSC();
                filtro.dtInicialEntrega = new Date();
                filtro.dtFinalEntrega = filtro.dtInicialEntrega;
                filtro.incluirFinalizados = false;
                JEncomenda jEncomenda = JEncomenda.getInstance();
                JEncomendaConsulta jEncomendaConsulta = JEncomendaConsulta.getInstance(jEncomenda);
                jEncomendaConsulta.atualizarTabela(filtro);
                jEncomendaConsulta.setVisible(true);
                if (jEncomendaConsulta.isRetornar()) {
                    new Thread(() -> {
                        try {
                            jEncomenda.setVisible(true);
                            SwingUtils.iniciarCursorProcessamento(jEncomenda);
                            SwingUtils.desabilitarCampos(jEncomenda.getCampos());
                            
                            jEncomenda.setEncomenda(jEncomendaConsulta.getEncomenda());
                            
                            SwingUtils.habilitarCampos(jEncomenda.getCampos());
                            SwingUtils.finalizarCursorProcessamento(jEncomenda);
                        } catch (BDException e) {
                            Log.log("Erro ao abrir tela de encomenda pelo alerta: ", e);
                        }
                    }).start();
                }
            }
        };

        return new Registro(Registro.Prioridade.ALTA, acao, descricao);
    }

    private Registro gerarSemana() {
        if (encomendasSemana <= 0) {
            return null;
        }
        String descricao;
        if (encomendasSemana == 1) {
            descricao = "Existe uma encomenda para essa semana.";
        } else {
            descricao = "Existem " + encomendasSemana + " encomendas essa semana.";
        }

        Registro.AcaoAoSelecionar acao = new Registro.AcaoAoSelecionar() {
            @Override
            public void run() {
                EncomendaSC filtro = new EncomendaSC();
                filtro.dtInicialEntrega = new Date();
                filtro.dtFinalEntrega = DataUtils.incrementaDia(filtro.dtInicialEntrega, 7);
                filtro.incluirFinalizados = false;
                JEncomenda jEncomenda = JEncomenda.getInstance();
                JEncomendaConsulta jEncomendaConsulta = JEncomendaConsulta.getInstance(jEncomenda);
                jEncomendaConsulta.atualizarTabela(filtro);
                jEncomendaConsulta.setVisible(true);
                if (jEncomendaConsulta.isRetornar()) {
                    new Thread(() -> {
                        try {
                            jEncomenda.setVisible(true);
                            SwingUtils.iniciarCursorProcessamento(jEncomenda);
                            SwingUtils.desabilitarCampos(jEncomenda.getCampos());
                            
                            jEncomenda.setEncomenda(jEncomendaConsulta.getEncomenda());
                            
                            SwingUtils.habilitarCampos(jEncomenda.getCampos());
                            SwingUtils.finalizarCursorProcessamento(jEncomenda);
                        } catch (BDException e) {
                            Log.log("Erro ao abrir tela de encomenda pelo alerta: ", e);
                        }
                    }).start();
                }
            }
        };

        return new Registro(Registro.Prioridade.MEDIA, acao, descricao);
    }

    private Registro gerarAtrasada() {
        if (encomendasAtrasada <= 0) {
            return null;
        }
        String descricao;
        if (encomendasAtrasada == 1) {
            descricao = "Existe uma encomenda atrasada.";
        } else {
            descricao = "Existem " + encomendasAtrasada + " encomendas atrasadas.";
        }

        Registro.AcaoAoSelecionar acao = new Registro.AcaoAoSelecionar() {
            @Override
            public void run() {
                EncomendaSC filtro = new EncomendaSC();
                filtro.apenasAtrasados = true;
                filtro.incluirFinalizados = false;
                JEncomenda jEncomenda = JEncomenda.getInstance();
                JEncomendaConsulta jEncomendaConsulta = JEncomendaConsulta.getInstance(jEncomenda);
                jEncomendaConsulta.atualizarTabela(filtro);
                jEncomendaConsulta.setVisible(true);
                if (jEncomendaConsulta.isRetornar()) {
                    new Thread(() -> {
                        try {
                            jEncomenda.setVisible(true);
                            SwingUtils.iniciarCursorProcessamento(jEncomenda);
                            SwingUtils.desabilitarCampos(jEncomenda.getCampos());
                            
                            jEncomenda.setEncomenda(jEncomendaConsulta.getEncomenda());
                            
                            SwingUtils.habilitarCampos(jEncomenda.getCampos());
                            SwingUtils.finalizarCursorProcessamento(jEncomenda);
                        } catch (BDException e) {
                            Log.log("Erro ao abrir tela de encomenda pelo alerta: ", e);
                        }
                    }).start();
                }
            }
        };

        return new Registro(Registro.Prioridade.ALTA, acao, descricao);
    }

    private void getRegistrosBanco() throws BDException {
        SQL sql = new SQL();
        sql.add("SELECT COUNT(id) c,");
        sql.add("CASE WHEN DATE(dataparaentrega) = DATE(NOW()) THEN 0 ");
        sql.add("WHEN DATE(dataparaentrega) > DATE(NOW()) THEN 1 ELSE 2 END dataEntrega");
        sql.add("FROM " + EncomendaBD.TBL + " e");
        sql.add("WHERE (dataparaentrega BETWEEN DATE(NOW()) AND (DATE(NOW()) + 7) OR dataparaentrega < DATE(NOW()))");
        sql.add("AND finalizado = false");
        sql.add("GROUP BY dataEntrega");

        try (Conexao c = Conexao.getConexao()) {
            try (ResultSet rs = c.executeQuery(sql)) {
                while (rs.next()) {
                    switch (rs.getInt("dataEntrega")) {
                        case 0:
                            encomendasDia = rs.getInt("c");
                            break;
                        case 1:
                            encomendasSemana = rs.getInt("c");
                            break;
                        default:
                            encomendasAtrasada = rs.getInt("c");
                            break;
                    }
                }
            } catch (SQLException ex) {
                throw new BDException(ex);
            }
        }
    }

}
