package com.encomenda.item;

import base.Valor;
import base.bd.ObjetoID;
import base.cache.CacheObjeto;
import base.usuario.Usuario;
import com.encomenda.Encomenda;
import com.produto.Produto;
import java.util.Date;
import java.util.Objects;

/**
 *
 * @author Carlos
 * @since 10/04/2019
 */
public class ItemEncomendaProduto implements ObjetoID, CacheObjeto<Integer> {

    private Integer id;
    private Valor quantidade;
    private String observacao;
    private Encomenda encomenda;
    private Produto produto;

    private Date dataInclusao;
    private Usuario usuarioInclusao;
    private Date dataAlteracao;
    private Usuario usuarioAlteracao;

    public ItemEncomendaProduto() {
    }

    public ItemEncomendaProduto(Integer id) {
        this.id = id;
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public Valor getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Valor quantidade) {
        this.quantidade = quantidade;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public Encomenda getEncomenda() {
        return encomenda;
    }

    public void setEncomenda(Encomenda encomenda) {
        this.encomenda = encomenda;
    }

    public Date getDataInclusao() {
        return dataInclusao;
    }

    public void setDataInclusao(Date dataInclusao) {
        this.dataInclusao = dataInclusao;
    }

    public Usuario getUsuarioInclusao() {
        return usuarioInclusao;
    }

    public void setUsuarioInclusao(Usuario usuarioInclusao) {
        this.usuarioInclusao = usuarioInclusao;
    }

    public Date getDataAlteracao() {
        return dataAlteracao;
    }

    public void setDataAlteracao(Date dataAlteracao) {
        this.dataAlteracao = dataAlteracao;
    }

    public Usuario getUsuarioAlteracao() {
        return usuarioAlteracao;
    }

    public void setUsuarioAlteracao(Usuario usuarioAlteracao) {
        this.usuarioAlteracao = usuarioAlteracao;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ItemEncomendaProduto other = (ItemEncomendaProduto) obj;
        return Objects.equals(this.id, other.id);
    }

}
