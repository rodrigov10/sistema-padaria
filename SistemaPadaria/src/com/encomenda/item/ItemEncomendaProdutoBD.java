package com.encomenda.item;

import base.Valor;
import base.bd.BDException;
import base.bd.Conexao;
import base.bd.SQL;
import base.cache.CacheManager;
import base.login.Sessao;
import base.usuario.Usuario;
import base.usuario.UsuarioBD;
import com.encomenda.Encomenda;
import com.produto.Produto;
import com.produto.ProdutoBD;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Carlos
 * @since 15/05/2019
 */
public class ItemEncomendaProdutoBD {

    public static final String TBL = "tabenc_produto";
    
    private static final CacheManager<ItemEncomendaProduto> CACHE = CacheManager.getInstance(ItemEncomendaProduto.class);

    private static Integer getNovoId() throws BDException {
        SQL sql = new SQL("SELECT nextval('tabenc_produto_id_seq') seq");
        try (Conexao c = Conexao.getConexao()) {
            try (ResultSet rs = c.executeQuery(sql)) {
                if (rs.next()) {
                    return rs.getInt("seq");
                }
            } catch (SQLException ex) {
                throw new BDException(ex);
            }
        }
        return null;
    }

    public static void salvar(ItemEncomendaProduto encomendaProduto) throws BDException {
        encomendaProduto.setId(getNovoId());
        Usuario usuario = Sessao.getUsuarioLogado();
        Date data = new Date();
        SQL sql = new SQL();
        sql.add("INSERT INTO " + TBL);
        sql.add("(id, id_entrega, id_produto, quantidade,observacao, dt_inclusao, dt_alteracao, id_usu_inclusao, id_usu_alteracao)");
        sql.add("VALUES(:id,:id_entrega,:id_produto,:quantidade,:observacao,:dt_inclusao,:dt_alteracao,:id_usu_inclusao,:id_usu_alteracao)");
        sql.setParametro("id", encomendaProduto.getId());
        sql.setParametro("id_entrega", encomendaProduto.getEncomenda());
        sql.setParametro("id_produto", encomendaProduto.getProduto());
        sql.setParametro("quantidade", encomendaProduto.getQuantidade());
        sql.setParametro("observacao", encomendaProduto.getObservacao());
        sql.setParametro("dt_inclusao", data);
        sql.setParametro("dt_alteracao", data);
        sql.setParametro("id_usu_inclusao", usuario);
        sql.setParametro("id_usu_alteracao", usuario);
        Conexao.update(sql);
        encomendaProduto.setUsuarioInclusao(usuario);
        encomendaProduto.setDataInclusao(data);
        encomendaProduto.setUsuarioAlteracao(usuario);
        encomendaProduto.setDataAlteracao(data);
        CACHE.put(encomendaProduto);

    }

    public static void alterar(ItemEncomendaProduto encomendaProduto) throws BDException {
        Usuario usuario = Sessao.getUsuarioLogado();
        Date data = new Date();
        SQL sql = new SQL();
        sql.add("UPDATE " + TBL);
        sql.add("SET id_entrega = :id_entrega, id_produto = :id_produto, quantidade = :quantidade, observacao = :observacao,");
        sql.add("dt_alteracao = :dt_alteracao, id_usu_alteracao = :id_usu_alteracao");
        sql.add("WHERE id = :id");
        sql.setParametro("id", encomendaProduto);
        sql.setParametro("id_entrega", encomendaProduto.getEncomenda());
        sql.setParametro("id_produto", encomendaProduto.getProduto());
        sql.setParametro("quantidade", encomendaProduto.getQuantidade());
        sql.setParametro("observacao", encomendaProduto.getObservacao());
        sql.setParametro("dt_alteracao", data);
        sql.setParametro("id_usu_alteracao", usuario);
        Conexao.update(sql);
        encomendaProduto.setUsuarioAlteracao(usuario);
        encomendaProduto.setDataAlteracao(data);
        CACHE.put(encomendaProduto);
    }

    public static void excluir(Integer id) throws BDException {
        SQL sql = new SQL("DELETE FROM " + TBL + " WHERE id = :id");
        sql.setParametro("id", id);
        Conexao.update(sql);
        CACHE.remove(id);
    }

    public static void excluirEncomenda(Integer id) throws BDException {
        SQL sql = new SQL("DELETE FROM " + TBL + " WHERE id_entrega = :id_entrega");
        sql.setParametro("id_entrega", id);
        Conexao.update(sql);
        CACHE.getObjetos().forEach((item) -> {
            CACHE.remove(item.getId());
        });
    }

    public static void carregar(ItemEncomendaProduto encomendaProduto) throws BDException {
        ItemEncomendaProduto objCache = CACHE.get(encomendaProduto.getId());
        if (objCache != null) {
            carregarObjeto(encomendaProduto, objCache);
            return;
        }
        SQL sql = new SQL("SELECT * FROM " + TBL + " WHERE id = :id");
        sql.setParametro("id", encomendaProduto);
        try (Conexao c = Conexao.getConexao()) {
            try (ResultSet rs = c.executeQuery(sql)) {
                while (rs.next()) {
                    carregarObjeto(encomendaProduto, rs);
                }
            } catch (SQLException ex) {
                throw new BDException(ex);
            }
        }
    }
    
    private static void carregarObjeto(ItemEncomendaProduto encomendaProduto, ItemEncomendaProduto cache) {
        encomendaProduto.setId(cache.getId());
        encomendaProduto.setProduto(cache.getProduto());
        encomendaProduto.setEncomenda(cache.getEncomenda());
        encomendaProduto.setQuantidade(cache.getQuantidade());
        encomendaProduto.setObservacao(cache.getObservacao());
        encomendaProduto.setDataInclusao(cache.getDataInclusao());
        encomendaProduto.setUsuarioInclusao(cache.getUsuarioInclusao());
        encomendaProduto.setDataAlteracao(cache.getDataAlteracao());
        encomendaProduto.setUsuarioAlteracao(cache.getUsuarioAlteracao());
    }

    private static void carregarObjeto(ItemEncomendaProduto encomendaProduto, ResultSet rs) throws SQLException, BDException {
        encomendaProduto.setProduto(new Produto(rs.getInt("id_produto")));
        ProdutoBD.carregar(encomendaProduto.getProduto());
        encomendaProduto.setEncomenda(new Encomenda(rs.getInt("id_entrega")));
        ProdutoBD.carregar(encomendaProduto.getProduto());
        encomendaProduto.setQuantidade(new Valor(rs.getBigDecimal("quantidade")));
        encomendaProduto.setObservacao(rs.getString("observacao"));
        encomendaProduto.setUsuarioInclusao(new Usuario(rs.getInt("id_usu_inclusao")));
        UsuarioBD.carregar(encomendaProduto.getUsuarioInclusao());
        encomendaProduto.setDataInclusao(rs.getDate("dt_inclusao"));
        encomendaProduto.setUsuarioAlteracao(new Usuario(rs.getInt("id_usu_alteracao")));
        UsuarioBD.carregar(encomendaProduto.getUsuarioAlteracao());
        encomendaProduto.setDataAlteracao(rs.getDate("dt_alteracao"));
    }

    public static List<ItemEncomendaProduto> listar(ItemEncomendaProdutoSC filtro) throws BDException {
        SQL sql = new SQL("SELECT * FROM " + TBL);
        sql.add("WHERE TRUE");
        if (filtro.encomenda != null) {
            sql.add("AND id_entrega = :id_entrega");
            sql.setParametro("id_entrega", filtro.encomenda);
        }
        try (Conexao c = Conexao.getConexao()) {
            try (ResultSet rs = c.executeQuery(sql)) {
                List<ItemEncomendaProduto> lista = new ArrayList();
                while (rs.next()) {
                    ItemEncomendaProduto encomendaProduto = new ItemEncomendaProduto();
                    encomendaProduto.setId(rs.getInt("id"));
                    carregarObjeto(encomendaProduto, rs);
                    lista.add(encomendaProduto);
                }
                return lista;
            } catch (SQLException ex) {
                throw new BDException(ex);
            }
        }
    }
}
