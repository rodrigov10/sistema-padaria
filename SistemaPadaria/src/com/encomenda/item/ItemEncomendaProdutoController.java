package com.encomenda.item;

import base.CampoException;
import base.bd.BDException;

/**
 *
 * @author Carlos
 * @since 10/04/2019
 */
public class ItemEncomendaProdutoController {

    public static void salvar(ItemEncomendaProduto itemEncomendaProduto) throws BDException, CampoException {
        if (itemEncomendaProduto.getId() == null) {
            ItemEncomendaProdutoBD.salvar(itemEncomendaProduto);
        } else {
            ItemEncomendaProdutoBD.alterar(itemEncomendaProduto);
        }
    }

    public static void excluir(Integer id) throws BDException {
        ItemEncomendaProdutoBD.excluir(id);
    }
 
}
