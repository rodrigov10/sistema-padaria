package com.encomenda.item;

import com.encomenda.Encomenda;

/**
 *
 * @author Rodrigo Vianna
 * @since 14/06/2019
 */
public class ItemEncomendaProdutoSC {
    
    public Encomenda encomenda;

    public ItemEncomendaProdutoSC() {
    }

    public ItemEncomendaProdutoSC(Encomenda encomenda) {
        this.encomenda = encomenda;
    }

}
