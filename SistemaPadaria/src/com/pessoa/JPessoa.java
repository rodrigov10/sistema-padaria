package com.pessoa;

import base.CampoException;
import base.bd.BDException;
import base.msg.JAlerta;
import base.utils.FieldFormatUtils;
import base.utils.SwingUtils;
import java.awt.Component;
import java.awt.event.ItemEvent;

/**
 *
 * @author Éderson
 * @since 27/03/2019
 */
public class JPessoa extends javax.swing.JFrame {

    private static JPessoa jPessoa;

    public static JPessoa getInstance() {
        if (jPessoa == null) {
            jPessoa = new JPessoa();
        }
        jPessoa.limparTela();
        return jPessoa;
    }

    private Pessoa pessoa;

    private JPessoaConsulta jPessoaConsulta;

    private boolean atualizarTabela = false;

    private boolean processando = false;

    private JPessoa() {
        initComponents();
        inicializar();
    }

    private void inicializar() {
        SwingUtils.inicializarTela(this);

        FieldFormatUtils.formatarTelefone(jFTFTelefone1);
        FieldFormatUtils.formatarTelefone(jFTFTelefone2);
        FieldFormatUtils.formatarCep(jFTFCEP);
        FieldFormatUtils.formatarDigitos(jTFCodigo);
        FieldFormatUtils.formatarDigitos(jTFNumero);

        alterarTipo(TipoPessoa.FISICA);
        jCBTipoPessoa.addItem(TipoPessoa.FISICA);
        jCBTipoPessoa.addItem(TipoPessoa.JURIDICA);
        jCBTipoPessoa.setSelectedItem(TipoPessoa.FISICA);

        limparTela();
    }

    ////////////////////////////////////////////////////////////////////////////
    //Pessoa
    ////////////////////////////////////////////////////////////////////////////
    private void setPessoa(Pessoa pessoa) {
        try {
            processando = true;

            this.pessoa = pessoa;
            jTFCodigo.setText(pessoa == null ? "" : String.valueOf(pessoa.getCodigo()));
            jTFNome.setText(pessoa == null ? "" : pessoa.getNome());
            jCBTipoPessoa.setSelectedItem(pessoa == null ? TipoPessoa.FISICA : pessoa.getTipoPessoa());
            if (pessoa == null || pessoa.getTipoPessoa() == TipoPessoa.FISICA) {
                jFTFDocumento.setText(pessoa == null ? "" : pessoa.getCpf());
            } else {
                jFTFDocumento.setText(pessoa.getCnpj());
            }
            jCheckBox_Ativo.setSelected(pessoa == null || pessoa.isAtivo());

            jTFLogradouro.setText(pessoa == null ? "" : pessoa.getLogradouro());
            jTFNumero.setText(pessoa == null ? "" : pessoa.getNumero());
            jTFBairro.setText(pessoa == null ? "" : pessoa.getBairro());
            jFTFCEP.setText(pessoa == null ? "" : pessoa.getCep());
            jTFCidade.setText(pessoa == null ? "" : pessoa.getCidade());
            jTFComplemento.setText(pessoa == null ? "" : pessoa.getComplemeto());

            jFTFTelefone1.setText(pessoa == null ? "" : pessoa.getTelefone1());
            jFTFTelefone2.setText(pessoa == null ? "" : pessoa.getTelefone2());
            jTFEmail.setText(pessoa == null ? "" : pessoa.getEmail());

            jCheckBox_Fornecedor.setSelected(pessoa == null || pessoa.isFornecedor());
            jCheckBox_Cliente.setSelected(pessoa == null || pessoa.isCliente());
            jCheckBox_Fornecedor.setEnabled(pessoa != null);
            jCheckBox_Cliente.setEnabled(pessoa != null);
        } finally {
            processando = false;
        }
    }

    private void salvar() {
        try {
            SwingUtils.iniciarCursorProcessamento(this);
            SwingUtils.desabilitarCampos(getCampos());

            String codigo = jTFCodigo.getText().trim();

            if (pessoa == null) {
                pessoa = new Pessoa();
            }
            if (!codigo.isEmpty()) {
                pessoa.setCodigo(Integer.parseInt(codigo));
            }
            pessoa.setNome(jTFNome.getText().trim());
            pessoa.setTipoPessoa(jCBTipoPessoa.getItemAt(jCBTipoPessoa.getSelectedIndex()));
            if (pessoa.getTipoPessoa() == TipoPessoa.FISICA) {
                pessoa.setCpf(jFTFDocumento.getText().trim());
                pessoa.setCnpj("");
            } else {
                pessoa.setCpf("");
                pessoa.setCnpj(jFTFDocumento.getText().trim());
            }
            pessoa.setAtivo(jCheckBox_Ativo.isSelected());

            pessoa.setLogradouro(jTFLogradouro.getText());
            pessoa.setNumero(jTFNumero.getText());
            pessoa.setBairro(jTFBairro.getText());
            pessoa.setCep(jFTFCEP.getText());
            pessoa.setCidade(jTFCidade.getText());
            pessoa.setComplemeto(jTFComplemento.getText());
            pessoa.setFornecedor(jCheckBox_Fornecedor.isSelected());
            pessoa.setCliente(jCheckBox_Cliente.isSelected());
            pessoa.setAtivo(jCheckBox_Ativo.isSelected());

            pessoa.setTelefone1(jFTFTelefone1.getText());
            pessoa.setTelefone2(jFTFTelefone2.getText());
            pessoa.setEmail(jTFEmail.getText());

            try {
                PessoaController.salvar(pessoa);
                atualizarTabela = true;
                if (pessoa.getCodigo() != null) {
                    jTFCodigo.setText(String.valueOf(pessoa.getCodigo()));
                }
                JAlerta.getAlertaMsg(this, "Salvo com sucesso.");
            } catch (BDException ex) {
                JAlerta.getAlertaErro(this, "Erro ao salvar pessoa: ", ex);
            } catch (CampoException ex) {
                JAlerta.getAlertaErro(this, ex.getMessage());
            } finally {
                jCheckBox_Fornecedor.setEnabled(pessoa != null);
                jCheckBox_Cliente.setEnabled(pessoa != null);
            }
        } finally {
            SwingUtils.finalizarCursorProcessamento(this);
            SwingUtils.habilitarCampos(getCampos());
        }
    }

    private void buscar() {
        String codigo = jTFCodigo.getText().trim();
        if (codigo.isEmpty()) {
            JAlerta.getAlertaMsg(this, "Código não informado.");
            jTFCodigo.requestFocusInWindow();
            return;
        }
        try {
            SwingUtils.iniciarCursorProcessamento(this);
            SwingUtils.desabilitarCampos(getCampos());

            Pessoa p = PessoaController.buscar(Integer.parseInt(codigo));
            if (p == null) {
                JAlerta.getAlertaMsg(this, "Pessoa não encontrada");
            }
            setPessoa(p);
        } catch (BDException ex) {
            JAlerta.getAlertaErro(this, "Erro ao buscar pessoa: ", ex);
        } finally {
            SwingUtils.finalizarCursorProcessamento(this);
            SwingUtils.habilitarCampos(getCampos());
        }
    }

    private void excluir() {
        if (pessoa == null) {
            JAlerta.getAlertaMsg(this, "Pessoa não informada.");
            return;
        }
        try {
            SwingUtils.iniciarCursorProcessamento(this);
            SwingUtils.desabilitarCampos(getCampos());

            PessoaController.excluir(pessoa.getId());
            atualizarTabela = true;
            limparTela();
        } catch (BDException ex) {
            JAlerta.getAlertaErro(this, "Erro ao buscar pessoa: ", ex);
        } finally {
            SwingUtils.finalizarCursorProcessamento(this);
            SwingUtils.habilitarCampos(getCampos());
        }
    }

    private void consultar() {
        try {
            SwingUtils.iniciarCursorProcessamento(this);

            if (jPessoaConsulta == null) {
                jPessoaConsulta = JPessoaConsulta.getInstance(this);
                jPessoaConsulta.setModoCliente(false);
                jPessoaConsulta.atualizarTabelaThread();
            } else if (atualizarTabela) {
                atualizarTabela = false;
                jPessoaConsulta.setModoCliente(false);
                jPessoaConsulta.atualizarTabelaThread();
            }
            jPessoaConsulta.setVisible(true);
            if (jPessoaConsulta.isRetornar()) {
                Pessoa pessoa = jPessoaConsulta.getPessoa();
                setPessoa(pessoa);
            }
        } catch (BDException ex) {
            JAlerta.getAlertaErro(this, "Erro ao consultar pessoa: ", ex);
            setPessoa(null);
        } finally {
            SwingUtils.finalizarCursorProcessamento(this);
        }
    }

    private void alterarFornecedor(ItemEvent evt) {
        try {
            SwingUtils.iniciarCursorProcessamento(this);
            SwingUtils.desabilitarCampos(jCheckBox_Fornecedor);

            PessoaController.alterarStatusFornecedor(pessoa, evt.getStateChange() == ItemEvent.SELECTED);
        } catch (BDException ex) {
            JAlerta.getAlertaErro(this, "Erro ao alterar status: ", ex);
        } finally {
            SwingUtils.finalizarCursorProcessamento(this);
            SwingUtils.habilitarCampos(jCheckBox_Fornecedor);
        }
    }

    private void alterarCliente(ItemEvent evt) {
        try {
            SwingUtils.iniciarCursorProcessamento(this);
            SwingUtils.desabilitarCampos(jCheckBox_Cliente);

            PessoaController.alterarStatusCliente(pessoa, evt.getStateChange() == ItemEvent.SELECTED);
        } catch (BDException ex) {
            JAlerta.getAlertaErro(this, "Erro ao alterar status: ", ex);
        } finally {
            SwingUtils.finalizarCursorProcessamento(this);
            SwingUtils.habilitarCampos(jCheckBox_Cliente);
        }
    }

    private void alterarAtivo(ItemEvent evt) {
        try {
            SwingUtils.iniciarCursorProcessamento(this);
            SwingUtils.desabilitarCampos(jCheckBox_Ativo);

            PessoaController.alterarStatusAtivo(pessoa, evt.getStateChange() == ItemEvent.SELECTED);
        } catch (BDException ex) {
            JAlerta.getAlertaErro(this, "Erro ao alterar status: ", ex);
        } finally {
            SwingUtils.finalizarCursorProcessamento(this);
            SwingUtils.habilitarCampos(jCheckBox_Ativo);
        }
    }

    private void alterarTipo(TipoPessoa tipoPessoa) {
        if (tipoPessoa == TipoPessoa.FISICA) {
            FieldFormatUtils.formatarCpf(jFTFDocumento);
        } else {
            FieldFormatUtils.formatarCnpj(jFTFDocumento);
        }
    }

    private Component[] getCampos() {
        return new Component[]{jBGravar, jBConsultar, jBLimpar, jBExcluir, jBBuscar, jCheckBox_Ativo, jCheckBox_Fornecedor, jCheckBox_Cliente};
    }

    public void limparTela() {
        setPessoa(null);
    }

    public static void main(String[] args) {
        if (com.Start.iniciarSistema()) {
            new JPessoa().setVisible(true);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Cadastro de Pessoa");
        setMaximumSize(new java.awt.Dimension(470, 670));
        setMinimumSize(new java.awt.Dimension(470, 670));
        setPreferredSize(new java.awt.Dimension(470, 670));

        jPFundo.setLayout(new java.awt.BorderLayout());

        jPPessoal.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Pessoal", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 1, 14))); // NOI18N
        jPPessoal.setPreferredSize(new java.awt.Dimension(450, 491));
        jPPessoal.setLayout(new javax.swing.BoxLayout(jPPessoal, javax.swing.BoxLayout.Y_AXIS));

        jPCodigo.setMaximumSize(new java.awt.Dimension(32767, 32));
        jPCodigo.setMinimumSize(new java.awt.Dimension(10, 22));
        jPCodigo.setPreferredSize(new java.awt.Dimension(438, 32));
        jPCodigo.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 6));

        jLCodigo.setText("Código:");
        jLCodigo.setMaximumSize(new java.awt.Dimension(80, 20));
        jLCodigo.setMinimumSize(new java.awt.Dimension(80, 20));
        jLCodigo.setPreferredSize(new java.awt.Dimension(80, 20));
        jPCodigo.add(jLCodigo);

        jTFCodigo.setMaximumSize(new java.awt.Dimension(70, 20));
        jTFCodigo.setMinimumSize(new java.awt.Dimension(250, 20));
        jTFCodigo.setPreferredSize(new java.awt.Dimension(50, 20));
        jPCodigo.add(jTFCodigo);

        jBBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/icones/buscar_16.png"))); // NOI18N
        jBBuscar.setToolTipText("Adicionar");
        jBBuscar.setBorder(null);
        jBBuscar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jBBuscar.setMaximumSize(new java.awt.Dimension(23, 20));
        jBBuscar.setMinimumSize(new java.awt.Dimension(23, 20));
        jBBuscar.setPreferredSize(new java.awt.Dimension(23, 20));
        jBBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBBuscarActionPerformed(evt);
            }
        });
        jPCodigo.add(jBBuscar);
        jBBuscar.getAccessibleContext().setAccessibleDescription("Buscar");

        jCheckBox_Ativo.setText("Ativo");
        jCheckBox_Ativo.setMaximumSize(new java.awt.Dimension(80, 23));
        jCheckBox_Ativo.setMinimumSize(new java.awt.Dimension(80, 23));
        jCheckBox_Ativo.setPreferredSize(new java.awt.Dimension(80, 23));
        jCheckBox_Ativo.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jCheckBox_AtivoItemStateChanged(evt);
            }
        });
        jPCodigo.add(jCheckBox_Ativo);

        jPPessoal.add(jPCodigo);

        jPNome.setMaximumSize(new java.awt.Dimension(32767, 32));
        jPNome.setMinimumSize(new java.awt.Dimension(10, 22));
        jPNome.setPreferredSize(new java.awt.Dimension(438, 32));
        jPNome.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 6));

        jLNome.setText("Nome:");
        jLNome.setMaximumSize(new java.awt.Dimension(80, 20));
        jLNome.setMinimumSize(new java.awt.Dimension(80, 20));
        jLNome.setPreferredSize(new java.awt.Dimension(80, 20));
        jPNome.add(jLNome);

        jTFNome.setMaximumSize(new java.awt.Dimension(250, 20));
        jTFNome.setMinimumSize(new java.awt.Dimension(250, 20));
        jTFNome.setPreferredSize(new java.awt.Dimension(250, 20));
        jPNome.add(jTFNome);

        jPPessoal.add(jPNome);

        jPCPF.setMaximumSize(new java.awt.Dimension(32767, 32));
        jPCPF.setMinimumSize(new java.awt.Dimension(10, 22));
        jPCPF.setPreferredSize(new java.awt.Dimension(438, 32));
        jPCPF.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 6));

        jLCPF.setText("CPF/CNPJ:");
        jLCPF.setMaximumSize(new java.awt.Dimension(80, 20));
        jLCPF.setMinimumSize(new java.awt.Dimension(80, 20));
        jLCPF.setPreferredSize(new java.awt.Dimension(80, 20));
        jPCPF.add(jLCPF);

        jFTFDocumento.setMaximumSize(new java.awt.Dimension(150, 20));
        jFTFDocumento.setMinimumSize(new java.awt.Dimension(150, 20));
        jFTFDocumento.setPreferredSize(new java.awt.Dimension(150, 20));
        jPCPF.add(jFTFDocumento);

        jCBTipoPessoa.setMaximumSize(new java.awt.Dimension(80, 20));
        jCBTipoPessoa.setMinimumSize(new java.awt.Dimension(80, 20));
        jCBTipoPessoa.setPreferredSize(new java.awt.Dimension(100, 20));
        jCBTipoPessoa.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jCBTipoPessoaItemStateChanged(evt);
            }
        });
        jPCPF.add(jCBTipoPessoa);

        jPPessoal.add(jPCPF);

        jPCPF1.setMaximumSize(new java.awt.Dimension(32767, 32));
        jPCPF1.setMinimumSize(new java.awt.Dimension(10, 22));
        jPCPF1.setPreferredSize(new java.awt.Dimension(438, 32));
        jPCPF1.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 6));

        jCheckBox_Fornecedor.setText("Fornecedor");
        jCheckBox_Fornecedor.setMaximumSize(new java.awt.Dimension(90, 23));
        jCheckBox_Fornecedor.setMinimumSize(new java.awt.Dimension(90, 23));
        jCheckBox_Fornecedor.setPreferredSize(new java.awt.Dimension(100, 23));
        jCheckBox_Fornecedor.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jCheckBox_FornecedorItemStateChanged(evt);
            }
        });
        jPCPF1.add(jCheckBox_Fornecedor);

        jCheckBox_Cliente.setText("Cliente");
        jCheckBox_Cliente.setMaximumSize(new java.awt.Dimension(80, 23));
        jCheckBox_Cliente.setMinimumSize(new java.awt.Dimension(80, 23));
        jCheckBox_Cliente.setPreferredSize(new java.awt.Dimension(80, 23));
        jCheckBox_Cliente.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jCheckBox_ClienteItemStateChanged(evt);
            }
        });
        jPCPF1.add(jCheckBox_Cliente);

        jPPessoal.add(jPCPF1);

        jPEndereco.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Endereço", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 1, 14))); // NOI18N
        jPEndereco.setLayout(new javax.swing.BoxLayout(jPEndereco, javax.swing.BoxLayout.Y_AXIS));

        jPLogradouro.setMaximumSize(new java.awt.Dimension(32767, 32));
        jPLogradouro.setMinimumSize(new java.awt.Dimension(10, 22));
        jPLogradouro.setPreferredSize(new java.awt.Dimension(438, 32));
        jPLogradouro.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 6));

        jLLogradouro.setText("Logradouro:");
        jLLogradouro.setMaximumSize(new java.awt.Dimension(95, 23));
        jLLogradouro.setMinimumSize(new java.awt.Dimension(95, 23));
        jLLogradouro.setPreferredSize(new java.awt.Dimension(95, 23));
        jPLogradouro.add(jLLogradouro);

        jTFLogradouro.setMaximumSize(new java.awt.Dimension(250, 20));
        jTFLogradouro.setMinimumSize(new java.awt.Dimension(250, 20));
        jTFLogradouro.setPreferredSize(new java.awt.Dimension(250, 20));
        jPLogradouro.add(jTFLogradouro);

        jPEndereco.add(jPLogradouro);

        jPNumero.setMaximumSize(new java.awt.Dimension(32767, 32));
        jPNumero.setMinimumSize(new java.awt.Dimension(10, 22));
        jPNumero.setPreferredSize(new java.awt.Dimension(438, 32));
        jPNumero.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 6));

        jLabel6.setText("Número:");
        jLabel6.setMaximumSize(new java.awt.Dimension(95, 23));
        jLabel6.setMinimumSize(new java.awt.Dimension(95, 23));
        jLabel6.setPreferredSize(new java.awt.Dimension(95, 23));
        jPNumero.add(jLabel6);

        jTFNumero.setMaximumSize(new java.awt.Dimension(250, 20));
        jTFNumero.setMinimumSize(new java.awt.Dimension(250, 20));
        jTFNumero.setPreferredSize(new java.awt.Dimension(50, 20));
        jPNumero.add(jTFNumero);

        jPEndereco.add(jPNumero);

        jPBairro.setMaximumSize(new java.awt.Dimension(32767, 32));
        jPBairro.setMinimumSize(new java.awt.Dimension(10, 22));
        jPBairro.setPreferredSize(new java.awt.Dimension(438, 32));
        jPBairro.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 6));

        jLBairro.setText("Bairro:");
        jLBairro.setMaximumSize(new java.awt.Dimension(95, 23));
        jLBairro.setMinimumSize(new java.awt.Dimension(95, 23));
        jLBairro.setPreferredSize(new java.awt.Dimension(95, 23));
        jPBairro.add(jLBairro);

        jTFBairro.setMaximumSize(new java.awt.Dimension(250, 20));
        jTFBairro.setMinimumSize(new java.awt.Dimension(250, 20));
        jTFBairro.setPreferredSize(new java.awt.Dimension(120, 20));
        jPBairro.add(jTFBairro);

        jPEndereco.add(jPBairro);

        jPCEP.setMaximumSize(new java.awt.Dimension(32767, 32));
        jPCEP.setMinimumSize(new java.awt.Dimension(10, 22));
        jPCEP.setPreferredSize(new java.awt.Dimension(438, 32));
        jPCEP.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 6));

        jLCEP.setText("CEP:");
        jLCEP.setMaximumSize(new java.awt.Dimension(95, 23));
        jLCEP.setMinimumSize(new java.awt.Dimension(95, 23));
        jLCEP.setPreferredSize(new java.awt.Dimension(95, 23));
        jPCEP.add(jLCEP);

        jFTFCEP.setMaximumSize(new java.awt.Dimension(80, 20));
        jFTFCEP.setMinimumSize(new java.awt.Dimension(80, 20));
        jFTFCEP.setPreferredSize(new java.awt.Dimension(80, 20));
        jPCEP.add(jFTFCEP);

        jPEndereco.add(jPCEP);

        jPCidade.setMaximumSize(new java.awt.Dimension(32767, 32));
        jPCidade.setMinimumSize(new java.awt.Dimension(10, 22));
        jPCidade.setPreferredSize(new java.awt.Dimension(438, 32));
        jPCidade.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 6));

        jLCidade.setText("Cidade:");
        jLCidade.setMaximumSize(new java.awt.Dimension(95, 23));
        jLCidade.setMinimumSize(new java.awt.Dimension(95, 23));
        jLCidade.setPreferredSize(new java.awt.Dimension(95, 23));
        jPCidade.add(jLCidade);

        jTFCidade.setMaximumSize(new java.awt.Dimension(250, 20));
        jTFCidade.setMinimumSize(new java.awt.Dimension(250, 20));
        jTFCidade.setPreferredSize(new java.awt.Dimension(180, 20));
        jPCidade.add(jTFCidade);

        jPEndereco.add(jPCidade);

        jPComplemento.setMaximumSize(new java.awt.Dimension(32767, 32));
        jPComplemento.setMinimumSize(new java.awt.Dimension(10, 22));
        jPComplemento.setPreferredSize(new java.awt.Dimension(438, 32));
        jPComplemento.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 6));

        jLComplemento.setText("Complemento:");
        jLComplemento.setMaximumSize(new java.awt.Dimension(95, 23));
        jLComplemento.setMinimumSize(new java.awt.Dimension(95, 23));
        jLComplemento.setPreferredSize(new java.awt.Dimension(95, 23));
        jPComplemento.add(jLComplemento);

        jTFComplemento.setMaximumSize(new java.awt.Dimension(250, 20));
        jTFComplemento.setMinimumSize(new java.awt.Dimension(250, 20));
        jTFComplemento.setPreferredSize(new java.awt.Dimension(180, 20));
        jPComplemento.add(jTFComplemento);

        jPEndereco.add(jPComplemento);

        jPPessoal.add(jPEndereco);

        jPContato.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Contato", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 1, 14))); // NOI18N
        jPContato.setMaximumSize(new java.awt.Dimension(1000, 122));
        jPContato.setLayout(new javax.swing.BoxLayout(jPContato, javax.swing.BoxLayout.Y_AXIS));

        jPTelefone.setMaximumSize(new java.awt.Dimension(1000, 32));
        jPTelefone.setMinimumSize(new java.awt.Dimension(438, 32));
        jPTelefone.setPreferredSize(new java.awt.Dimension(438, 32));
        jPTelefone.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 6));

        jLTelefone1.setText("Telefone 1:");
        jLTelefone1.setMaximumSize(new java.awt.Dimension(95, 23));
        jLTelefone1.setMinimumSize(new java.awt.Dimension(95, 23));
        jLTelefone1.setPreferredSize(new java.awt.Dimension(95, 23));
        jPTelefone.add(jLTelefone1);

        jFTFTelefone1.setMaximumSize(new java.awt.Dimension(100, 20));
        jFTFTelefone1.setMinimumSize(new java.awt.Dimension(100, 20));
        jFTFTelefone1.setPreferredSize(new java.awt.Dimension(100, 20));
        jPTelefone.add(jFTFTelefone1);

        jPContato.add(jPTelefone);

        jPTelefone2.setMaximumSize(new java.awt.Dimension(1000, 32));
        jPTelefone2.setMinimumSize(new java.awt.Dimension(438, 32));
        jPTelefone2.setPreferredSize(new java.awt.Dimension(438, 32));
        jPTelefone2.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 6));

        jLTelefone2.setText("Telefone 2:");
        jLTelefone2.setMaximumSize(new java.awt.Dimension(95, 23));
        jLTelefone2.setMinimumSize(new java.awt.Dimension(95, 23));
        jLTelefone2.setPreferredSize(new java.awt.Dimension(95, 23));
        jPTelefone2.add(jLTelefone2);

        jFTFTelefone2.setMaximumSize(new java.awt.Dimension(100, 20));
        jFTFTelefone2.setMinimumSize(new java.awt.Dimension(100, 20));
        jFTFTelefone2.setPreferredSize(new java.awt.Dimension(100, 20));
        jPTelefone2.add(jFTFTelefone2);

        jPContato.add(jPTelefone2);

        jPEmail.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jPEmail.setMaximumSize(new java.awt.Dimension(1000, 32));
        jPEmail.setMinimumSize(new java.awt.Dimension(438, 32));
        jPEmail.setPreferredSize(new java.awt.Dimension(438, 32));
        jPEmail.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 6));

        jLEmail.setText("Email:");
        jLEmail.setMaximumSize(new java.awt.Dimension(95, 23));
        jLEmail.setMinimumSize(new java.awt.Dimension(95, 23));
        jLEmail.setPreferredSize(new java.awt.Dimension(95, 23));
        jPEmail.add(jLEmail);

        jTFEmail.setMaximumSize(new java.awt.Dimension(250, 20));
        jTFEmail.setMinimumSize(new java.awt.Dimension(250, 20));
        jTFEmail.setPreferredSize(new java.awt.Dimension(250, 20));
        jPEmail.add(jTFEmail);

        jPContato.add(jPEmail);

        jPPessoal.add(jPContato);

        jPFundo.add(jPPessoal, java.awt.BorderLayout.CENTER);

        getContentPane().add(jPFundo, java.awt.BorderLayout.CENTER);

        jPTitulo.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createEmptyBorder(10, 10, 10, 10), javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED)));

        jLTitulo.setFont(new java.awt.Font("Arial", 1, 24)); // NOI18N
        jLTitulo.setText("Cadastro de Pessoa");
        jPTitulo.add(jLTitulo);

        getContentPane().add(jPTitulo, java.awt.BorderLayout.NORTH);

        jBGravar.setMnemonic('G');
        jBGravar.setText("Gravar");
        jBGravar.setMaximumSize(new java.awt.Dimension(95, 23));
        jBGravar.setMinimumSize(new java.awt.Dimension(95, 23));
        jBGravar.setPreferredSize(new java.awt.Dimension(95, 23));
        jBGravar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBGravarActionPerformed(evt);
            }
        });
        jPBotoes.add(jBGravar);

        jBConsultar.setMnemonic('C');
        jBConsultar.setText("Consultar");
        jBConsultar.setMaximumSize(new java.awt.Dimension(95, 23));
        jBConsultar.setMinimumSize(new java.awt.Dimension(95, 23));
        jBConsultar.setPreferredSize(new java.awt.Dimension(95, 23));
        jBConsultar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBConsultarActionPerformed(evt);
            }
        });
        jPBotoes.add(jBConsultar);

        jBLimpar.setMnemonic('L');
        jBLimpar.setText("Limpar");
        jBLimpar.setMaximumSize(new java.awt.Dimension(95, 23));
        jBLimpar.setMinimumSize(new java.awt.Dimension(95, 23));
        jBLimpar.setPreferredSize(new java.awt.Dimension(95, 23));
        jBLimpar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBLimparActionPerformed(evt);
            }
        });
        jPBotoes.add(jBLimpar);

        jBExcluir.setMnemonic('E');
        jBExcluir.setText("Excluir");
        jBExcluir.setMaximumSize(new java.awt.Dimension(95, 23));
        jBExcluir.setMinimumSize(new java.awt.Dimension(95, 23));
        jBExcluir.setPreferredSize(new java.awt.Dimension(95, 23));
        jBExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBExcluirActionPerformed(evt);
            }
        });
        jPBotoes.add(jBExcluir);

        getContentPane().add(jPBotoes, java.awt.BorderLayout.SOUTH);

        setSize(new java.awt.Dimension(469, 683));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jBLimparActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBLimparActionPerformed
        limparTela();
    }//GEN-LAST:event_jBLimparActionPerformed

    private void jBExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBExcluirActionPerformed
        new Thread(() -> {
            excluir();
        }, "Excluir Pessoa").start();
    }//GEN-LAST:event_jBExcluirActionPerformed

    private void jBGravarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBGravarActionPerformed
        new Thread(() -> {
            salvar();
        }, "Gravar Pessoa").start();
    }//GEN-LAST:event_jBGravarActionPerformed

    private void jBBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBBuscarActionPerformed
        new Thread(() -> {
            buscar();
        }, "Busca Pessoa").start();
    }//GEN-LAST:event_jBBuscarActionPerformed

    private void jCheckBox_FornecedorItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jCheckBox_FornecedorItemStateChanged
        if (processando) {
            return;
        }
        new Thread(() -> {
            alterarFornecedor(evt);
        }, "Alterar Status Fornecedor").start();
    }//GEN-LAST:event_jCheckBox_FornecedorItemStateChanged

    private void jCheckBox_ClienteItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jCheckBox_ClienteItemStateChanged
        if (processando) {
            return;
        }
        new Thread(() -> {
            alterarCliente(evt);
        }, "Alterar Status Cliente").start();
    }//GEN-LAST:event_jCheckBox_ClienteItemStateChanged

    private void jCheckBox_AtivoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jCheckBox_AtivoItemStateChanged
        if (processando) {
            return;
        }
        new Thread(() -> {
            alterarAtivo(evt);
        }, "Alterar Status Ativo").start();
    }//GEN-LAST:event_jCheckBox_AtivoItemStateChanged

    private void jCBTipoPessoaItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jCBTipoPessoaItemStateChanged
        alterarTipo((TipoPessoa) evt.getItem());
    }//GEN-LAST:event_jCBTipoPessoaItemStateChanged

    private void jBConsultarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBConsultarActionPerformed
        consultar();
    }//GEN-LAST:event_jBConsultarActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private final javax.swing.JButton jBBuscar = new javax.swing.JButton();
    private final javax.swing.JButton jBConsultar = new javax.swing.JButton();
    private final javax.swing.JButton jBExcluir = new javax.swing.JButton();
    private final javax.swing.JButton jBGravar = new javax.swing.JButton();
    private final javax.swing.JButton jBLimpar = new javax.swing.JButton();
    private final javax.swing.JComboBox<TipoPessoa> jCBTipoPessoa = new javax.swing.JComboBox<>();
    private final javax.swing.JCheckBox jCheckBox_Ativo = new javax.swing.JCheckBox();
    private final javax.swing.JCheckBox jCheckBox_Cliente = new javax.swing.JCheckBox();
    private final javax.swing.JCheckBox jCheckBox_Fornecedor = new javax.swing.JCheckBox();
    private final javax.swing.JFormattedTextField jFTFCEP = new javax.swing.JFormattedTextField();
    private final javax.swing.JFormattedTextField jFTFDocumento = new javax.swing.JFormattedTextField();
    private final javax.swing.JFormattedTextField jFTFTelefone1 = new javax.swing.JFormattedTextField();
    private final javax.swing.JFormattedTextField jFTFTelefone2 = new javax.swing.JFormattedTextField();
    private final javax.swing.JLabel jLBairro = new javax.swing.JLabel();
    private final javax.swing.JLabel jLCEP = new javax.swing.JLabel();
    private final javax.swing.JLabel jLCPF = new javax.swing.JLabel();
    private final javax.swing.JLabel jLCidade = new javax.swing.JLabel();
    private final javax.swing.JLabel jLCodigo = new javax.swing.JLabel();
    private final javax.swing.JLabel jLComplemento = new javax.swing.JLabel();
    private final javax.swing.JLabel jLEmail = new javax.swing.JLabel();
    private final javax.swing.JLabel jLLogradouro = new javax.swing.JLabel();
    private final javax.swing.JLabel jLNome = new javax.swing.JLabel();
    private final javax.swing.JLabel jLTelefone1 = new javax.swing.JLabel();
    private final javax.swing.JLabel jLTelefone2 = new javax.swing.JLabel();
    private final javax.swing.JLabel jLTitulo = new javax.swing.JLabel();
    private final javax.swing.JLabel jLabel6 = new javax.swing.JLabel();
    private final javax.swing.JPanel jPBairro = new javax.swing.JPanel();
    private final javax.swing.JPanel jPBotoes = new javax.swing.JPanel();
    private final javax.swing.JPanel jPCEP = new javax.swing.JPanel();
    private final javax.swing.JPanel jPCPF = new javax.swing.JPanel();
    private final javax.swing.JPanel jPCPF1 = new javax.swing.JPanel();
    private final javax.swing.JPanel jPCidade = new javax.swing.JPanel();
    private final javax.swing.JPanel jPCodigo = new javax.swing.JPanel();
    private final javax.swing.JPanel jPComplemento = new javax.swing.JPanel();
    private final javax.swing.JPanel jPContato = new javax.swing.JPanel();
    private final javax.swing.JPanel jPEmail = new javax.swing.JPanel();
    private final javax.swing.JPanel jPEndereco = new javax.swing.JPanel();
    private final javax.swing.JPanel jPFundo = new javax.swing.JPanel();
    private final javax.swing.JPanel jPLogradouro = new javax.swing.JPanel();
    private final javax.swing.JPanel jPNome = new javax.swing.JPanel();
    private final javax.swing.JPanel jPNumero = new javax.swing.JPanel();
    private final javax.swing.JPanel jPPessoal = new javax.swing.JPanel();
    private final javax.swing.JPanel jPTelefone = new javax.swing.JPanel();
    private final javax.swing.JPanel jPTelefone2 = new javax.swing.JPanel();
    private final javax.swing.JPanel jPTitulo = new javax.swing.JPanel();
    private final javax.swing.JTextField jTFBairro = new javax.swing.JTextField();
    private final javax.swing.JTextField jTFCidade = new javax.swing.JTextField();
    private final javax.swing.JTextField jTFCodigo = new javax.swing.JTextField();
    private final javax.swing.JTextField jTFComplemento = new javax.swing.JTextField();
    private final javax.swing.JTextField jTFEmail = new javax.swing.JTextField();
    private final javax.swing.JTextField jTFLogradouro = new javax.swing.JTextField();
    private final javax.swing.JTextField jTFNome = new javax.swing.JTextField();
    private final javax.swing.JTextField jTFNumero = new javax.swing.JTextField();
    // End of variables declaration//GEN-END:variables
}
