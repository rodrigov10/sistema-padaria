package com.pessoa;

import base.TableModelObjeto;
import base.bd.BDException;
import base.msg.JAlerta;
import base.utils.FecharEsc;
import base.utils.SwingUtils;
import java.awt.Component;
import java.awt.Window;
import java.util.List;

/**
 *
 * @author Rodrigo Vianna
 * @since 24/05/2019
 */
public class JPessoaConsulta extends javax.swing.JDialog implements FecharEsc {

    private static JPessoaConsulta instance;

    public static JPessoaConsulta getInstance(Window parent) {
        if (instance == null) {
            instance = new JPessoaConsulta(parent, true);
        }
        instance.setModoCliente(false);
        return instance;
    }

    private boolean retornar = false;

    private boolean apenasCliente = false;

    private JPessoaConsulta(Window parent, boolean modal) {
        super(parent, modal ? ModalityType.APPLICATION_MODAL : ModalityType.MODELESS);
        initComponents();
        inicializar();
    }

    private void inicializar() {
        SwingUtils.inicializarTela(this);

        inicializarTabela();
    }

    private void inicializarTabela() {
        TableModelObjeto<Pessoa> tm = new TableModelObjeto(jTable.getColumnModel());
        tm.addColumn("Código");
        tm.addColumn("Nome");
        tm.addColumn("Tipo");
        tm.addColumn("Cliente");
        tm.addColumn("Fornecedor");
        tm.addColumn("Objeto");

        jTable.setModel(tm);

        tm.getColumn("Código").setPreferredWidth(100);
        tm.getColumn("Nome").setPreferredWidth(200);
        tm.getColumn("Tipo").setPreferredWidth(50);
        tm.getColumn("Cliente").setPreferredWidth(50);
        tm.getColumn("Fornecedor").setPreferredWidth(50);
        jTable.removeColumn(tm.getColumn("Objeto"));
    }

    public void atualizarTabelaThread() {
        new Thread(() -> {
            atualizarTabela();
        }, "Buscando Lista de Pessoas").start();
    }

    public void atualizarTabela() {
        try {
            SwingUtils.desabilitarCampos(getCampos());
            SwingUtils.iniciarCursorProcessamento(this);

            TableModelObjeto<Pessoa> tm = ((TableModelObjeto<Pessoa>) jTable.getModel());
            tm.reset();
            
            retornar = false;

            PessoaSC filtro = new PessoaSC();
            filtro.nome = jTextField_Nome.getText().trim();
            if (apenasCliente) {
                filtro.cliente = true;
            } else {
                filtro.cliente = jCheckBox_Cliente.isSelected() ? true : null;
                filtro.fornecedor = jCheckBox_Fornecedor.isSelected() ? true : null;
            }
            filtro.ativo = jCheckBox_Ativo.isSelected() ? true : null;
            List<Pessoa> lista = PessoaBD.listarConsulta(filtro);
            if (lista.isEmpty()) {
                JAlerta.getAlertaMsg(this, "Nenhum registro encontrado.");
                return;
            }

            lista.stream().map((pessoa) -> {
                Object[] row = new Object[6];
                row[0] = pessoa.getCodigo();
                row[1] = pessoa.getNome();
                row[2] = pessoa.getTipoPessoa().toString();
                row[3] = pessoa.isCliente() ? "Sim" : "Não";
                row[4] = pessoa.isFornecedor() ? "Sim" : "Não";
                row[5] = pessoa;
                return row;
            }).forEachOrdered((row) -> {
                tm.addRow(row);
            });

            if (jTable.getRowCount() > 0) {
                jTable.changeSelection(0, 0, false, false);
            }

        } catch (BDException ex) {
            JAlerta.getAlertaErro(this, "Erro ao listar pessoas: ", ex);
        } finally {
            SwingUtils.habilitarCampos(getCampos());
            SwingUtils.finalizarCursorProcessamento(this);
        }
    }

    public boolean isRetornar() {
        return this.retornar;
    }

    public Pessoa getPessoa() throws BDException {
        if (jTable.getSelectedRow() == -1) {
            return null;
        }
        Pessoa pessoa = ((TableModelObjeto<Pessoa>) jTable.getModel()).getObjeto(jTable.getSelectedRow());
        PessoaBD.carregar(pessoa);
        return pessoa;
    }

    private Component[] getCampos() {
        return new Component[]{jButton_Retornar, jButton_Buscar, jTextField_Nome, jCheckBox_Ativo, jCheckBox_Cliente, jCheckBox_Fornecedor};
    }

    public void setModoCliente(boolean apenasCliente) {
        this.apenasCliente = apenasCliente;
        if (apenasCliente) {
            jCheckBox_Fornecedor.setVisible(false);
            jCheckBox_Cliente.setVisible(false);
        } else {
            jCheckBox_Fornecedor.setVisible(true);
            jCheckBox_Cliente.setVisible(true);
        }
    }

    public boolean isModoCliente() {
        return this.apenasCliente;
    }

    @Override
    public void fechar() {
        fechar(false);
    }

    private void fechar(boolean retornar) {
        this.retornar = retornar;
        setVisible(false);
        dispose();
    }

    public static void main(String[] args) {
        if (com.Start.iniciarSistema()) {
            getInstance((java.awt.Dialog) null).setVisible(true);
        }
        System.exit(0);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton_Buscar = new javax.swing.JButton();
        jCheckBox_Fornecedor = new javax.swing.JCheckBox();
        jCheckBox_Cliente = new javax.swing.JCheckBox();
        jCheckBox_Ativo = new javax.swing.JCheckBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Consulta de Pessoas");
        setMaximumSize(new java.awt.Dimension(1000, 700));
        setMinimumSize(new java.awt.Dimension(750, 370));
        setPreferredSize(new java.awt.Dimension(750, 370));

        jPanel_Filtro.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Filtro", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 11))); // NOI18N
        jPanel_Filtro.setLayout(new javax.swing.BoxLayout(jPanel_Filtro, javax.swing.BoxLayout.Y_AXIS));

        jPanel_Nome.setMaximumSize(new java.awt.Dimension(32767, 32));
        jPanel_Nome.setMinimumSize(new java.awt.Dimension(10, 22));
        jPanel_Nome.setPreferredSize(new java.awt.Dimension(438, 22));
        jPanel_Nome.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 1));

        jLabel_Nome.setText("Nome:");
        jLabel_Nome.setMaximumSize(new java.awt.Dimension(60, 20));
        jLabel_Nome.setMinimumSize(new java.awt.Dimension(60, 20));
        jLabel_Nome.setPreferredSize(new java.awt.Dimension(60, 20));
        jPanel_Nome.add(jLabel_Nome);

        jTextField_Nome.setMaximumSize(new java.awt.Dimension(250, 20));
        jTextField_Nome.setMinimumSize(new java.awt.Dimension(250, 20));
        jTextField_Nome.setPreferredSize(new java.awt.Dimension(250, 20));
        jPanel_Nome.add(jTextField_Nome);

        jButton_Buscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/icones/buscar_16.png"))); // NOI18N
        jButton_Buscar.setToolTipText("Adicionar");
        jButton_Buscar.setBorder(null);
        jButton_Buscar.setMaximumSize(new java.awt.Dimension(23, 20));
        jButton_Buscar.setMinimumSize(new java.awt.Dimension(23, 20));
        jButton_Buscar.setPreferredSize(new java.awt.Dimension(23, 20));
        jButton_Buscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_BuscarActionPerformed(evt);
            }
        });
        jPanel_Nome.add(jButton_Buscar);

        jCheckBox_Fornecedor.setText("Fornecedor");
        jCheckBox_Fornecedor.setMaximumSize(new java.awt.Dimension(100, 20));
        jCheckBox_Fornecedor.setMinimumSize(new java.awt.Dimension(100, 20));
        jCheckBox_Fornecedor.setPreferredSize(new java.awt.Dimension(100, 20));
        jPanel_Nome.add(jCheckBox_Fornecedor);

        jCheckBox_Cliente.setText("Cliente");
        jCheckBox_Cliente.setMaximumSize(new java.awt.Dimension(80, 20));
        jCheckBox_Cliente.setMinimumSize(new java.awt.Dimension(80, 20));
        jCheckBox_Cliente.setPreferredSize(new java.awt.Dimension(80, 20));
        jPanel_Nome.add(jCheckBox_Cliente);

        jCheckBox_Ativo.setSelected(true);
        jCheckBox_Ativo.setText("Somente Ativos");
        jCheckBox_Ativo.setMaximumSize(new java.awt.Dimension(150, 20));
        jCheckBox_Ativo.setMinimumSize(new java.awt.Dimension(150, 20));
        jCheckBox_Ativo.setPreferredSize(new java.awt.Dimension(150, 20));
        jPanel_Nome.add(jCheckBox_Ativo);

        jPanel_Filtro.add(jPanel_Nome);

        getContentPane().add(jPanel_Filtro, java.awt.BorderLayout.NORTH);

        jTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTableMouseClicked(evt);
            }
        });
        jScrollPane.setViewportView(jTable);

        getContentPane().add(jScrollPane, java.awt.BorderLayout.CENTER);

        jPanel_Buttons.setPreferredSize(new java.awt.Dimension(400, 33));

        jButton_Retornar.setMnemonic('R');
        jButton_Retornar.setText("Retornar");
        jButton_Retornar.setMaximumSize(new java.awt.Dimension(95, 23));
        jButton_Retornar.setMinimumSize(new java.awt.Dimension(95, 23));
        jButton_Retornar.setPreferredSize(new java.awt.Dimension(95, 23));
        jButton_Retornar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_RetornarActionPerformed(evt);
            }
        });
        jPanel_Buttons.add(jButton_Retornar);

        getContentPane().add(jPanel_Buttons, java.awt.BorderLayout.SOUTH);

        setSize(new java.awt.Dimension(766, 409));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton_RetornarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_RetornarActionPerformed
        fechar(true);
    }//GEN-LAST:event_jButton_RetornarActionPerformed

    private void jTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableMouseClicked
        if (evt.getClickCount() > 1 && jTable.getSelectedRow() != -1) {
            fechar(true);
        }
    }//GEN-LAST:event_jTableMouseClicked

    private void jButton_BuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_BuscarActionPerformed
        atualizarTabelaThread();
    }//GEN-LAST:event_jButton_BuscarActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton_Buscar;
    private final javax.swing.JButton jButton_Retornar = new javax.swing.JButton();
    private javax.swing.JCheckBox jCheckBox_Ativo;
    private javax.swing.JCheckBox jCheckBox_Cliente;
    private javax.swing.JCheckBox jCheckBox_Fornecedor;
    private final javax.swing.JLabel jLabel_Nome = new javax.swing.JLabel();
    private final javax.swing.JPanel jPanel_Buttons = new javax.swing.JPanel();
    private final javax.swing.JPanel jPanel_Filtro = new javax.swing.JPanel();
    private final javax.swing.JPanel jPanel_Nome = new javax.swing.JPanel();
    private final javax.swing.JScrollPane jScrollPane = new javax.swing.JScrollPane();
    private final javax.swing.JTable jTable = new javax.swing.JTable();
    private final javax.swing.JTextField jTextField_Nome = new javax.swing.JTextField();
    // End of variables declaration//GEN-END:variables
}
