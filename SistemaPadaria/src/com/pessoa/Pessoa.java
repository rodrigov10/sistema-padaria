package com.pessoa;

import base.bd.ObjetoID;
import base.cache.CacheObjeto;
import base.usuario.Usuario;
import base.utils.CEPUtils;
import base.utils.CPFUtils;
import base.utils.TelefoneUtils;
import java.util.Comparator;
import java.util.Date;
import java.util.Objects;

/**
 *
 * @author Rodrigo Vianna
 * @since 13/03/2019
 */
public class Pessoa implements ObjetoID, CacheObjeto<Integer>, Comparator<Pessoa> {

    private Integer id;
    private Integer codigo;
    private String nome;
    private String cpf;
    private String cnpj;
    private TipoPessoa tipoPessoa;

    private String numero;
    private String logradouro;
    private String bairro;
    private String cidade;
    private String complemeto;
    private String cep;

    private String telefone1;
    private String telefone2;
    private String email;

    private boolean ativo;
    private boolean fornecedor;
    private boolean cliente;

    private Date dataInclusao;
    private Usuario usuarioInclusao;
    private Date dataAlteracao;
    private Usuario usuarioAlteracao;

    public Pessoa() {
    }

    public Pessoa(Integer id) {
        this.id = id;
    }

    /**
     * Método que retorna o CPF
     *
     * @return CPF
     */
    public String getCpf() {
        return cpf;
    }

    /**
     * Método que modifica CPF
     *
     * @param cpf Número do CPF
     */
    public void setCpf(String cpf) {
        this.cpf = CPFUtils.desformatar(cpf);
        if (this.cpf.isEmpty()) {
            this.cpf = null;
        }
    }

    /**
     * Método que retorna o CNPJ
     *
     * @return CNPJ
     */
    public String getCnpj() {
        return cnpj;
    }

    /**
     * Método que modifica CNPJ
     *
     * @param cnpj passa o cnpj
     */
    public void setCnpj(String cnpj) {
        this.cnpj = CPFUtils.desformatar(cnpj);
        if (this.cnpj.isEmpty()) {
            this.cnpj = null;
        }
    }

    /**
     * Método que retorna o Id
     *
     * @return O id de Pessoa
     */
    @Override
    public Integer getId() {
        return id;
    }

    /**
     * Método que retorna o Codigo
     *
     * @return O código de Pessoa
     */
    public Integer getCodigo() {
        return codigo;
    }

    /**
     * Método que retorna o Nome
     *
     * @return O nome de Pessoa
     */
    public String getNome() {
        return nome;
    }

    /**
     * Método que retorna true se estiver ativo
     *
     * @return O estado de Pessoa
     */
    public boolean isAtivo() {
        return ativo;
    }

    /**
     * Método que modifica estado de pessoa
     *
     * @param ativo para ativo passar true
     */
    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public boolean isFornecedor() {
        return fornecedor;
    }

    public void setFornecedor(boolean fornecedor) {
        this.fornecedor = fornecedor;
    }

    public boolean isCliente() {
        return cliente;
    }

    public void setCliente(boolean cliente) {
        this.cliente = cliente;
    }

    /**
     * Método que modifica Id
     *
     * @param id passa o id de Pessoa
     */
    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Método que modifica Codigo
     *
     * @param codigo passa o código de Pessoa
     */
    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    /**
     * Método que modifica Nome
     *
     * @param nome Passa o nome de Pessoa
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    public TipoPessoa getTipoPessoa() {
        return tipoPessoa;
    }

    public void setTipoPessoa(TipoPessoa tipoPessoa) {
        this.tipoPessoa = tipoPessoa;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getComplemeto() {
        return complemeto;
    }

    public void setComplemeto(String complemeto) {
        this.complemeto = complemeto;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = CEPUtils.desformatar(cep);
    }

    public String getTelefone1() {
        return telefone1;
    }

    public void setTelefone1(String telefone1) {
        this.telefone1 = TelefoneUtils.desformatar(telefone1);
    }

    public String getTelefone2() {
        return telefone2;
    }

    public void setTelefone2(String telefone2) {
        this.telefone2 = TelefoneUtils.desformatar(telefone2);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getDataInclusao() {
        return dataInclusao;
    }

    public void setDataInclusao(Date dataInclusao) {
        this.dataInclusao = dataInclusao;
    }

    public Usuario getUsuarioInclusao() {
        return usuarioInclusao;
    }

    public void setUsuarioInclusao(Usuario usuarioInclusao) {
        this.usuarioInclusao = usuarioInclusao;
    }

    public Date getDataAlteracao() {
        return dataAlteracao;
    }

    public void setDataAlteracao(Date dataAlteracao) {
        this.dataAlteracao = dataAlteracao;
    }

    public Usuario getUsuarioAlteracao() {
        return usuarioAlteracao;
    }

    public void setUsuarioAlteracao(Usuario usuarioAlteracao) {
        this.usuarioAlteracao = usuarioAlteracao;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 19 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Pessoa o2 = (Pessoa) obj;
        return Objects.equals(this.id, o2.id);
    }

    @Override
    public String toString() {
        return nome;
    }

    @Override
    public int compare(Pessoa o1, Pessoa o2) {
        if (o1.nome == null && o2.nome != null) {
            return -1;
        } else if (o1.nome != null && o2.nome == null) {
            return 1;
        }
        return o1.nome.compareToIgnoreCase(o2.nome);
    }

}
