package com.pessoa;

import base.bd.BDException;
import base.bd.Conexao;
import base.bd.SQL;
import base.cache.CacheManager;
import base.login.Sessao;
import base.usuario.Usuario;
import base.usuario.UsuarioBD;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Rodrigo Vianna
 * @since 10/04/2019
 */
public class PessoaBD {

    public static final String TBL = "tabpessoa";
    
    private static final CacheManager<Pessoa> CACHE = CacheManager.getInstance(Pessoa.class);

    private static Integer getNovoId() throws BDException {
        SQL sql = new SQL("SELECT nextval('tabpessoa_id_seq') seq");
        try (Conexao c = Conexao.getConexao()) {
            try (ResultSet rs = c.executeQuery(sql)) {
                if (rs.next()) {
                    return rs.getInt("seq");
                }
            } catch (SQLException ex) {
                throw new BDException(ex);
            }
        }
        return null;
    }

    private static Integer getNovoCodigo() throws BDException {
        SQL sql = new SQL("SELECT nextval('tabpessoa_codigo_seq') seq");
        try (Conexao c = Conexao.getConexao()) {
            try (ResultSet rs = c.executeQuery(sql)) {
                if (rs.next()) {
                    return rs.getInt("seq");
                }
            } catch (SQLException ex) {
                throw new BDException(ex);
            }
        }
        return null;
    }

    public static void salvar(Pessoa pessoa) throws BDException {
        pessoa.setId(getNovoId());
        pessoa.setCodigo(getNovoCodigo());
        Usuario usuario = Sessao.getUsuarioLogado();
        Date data = new Date();
        SQL sql = new SQL();
        sql.add("INSERT INTO " + TBL);
        sql.add("(id, codigo, tipo_pessoa, nome, cpf, cnpj, ");
        sql.add("ativo, cliente, fornecedor,");
        sql.add("numero, logradouro, bairro, cidade, cep, complemento,");
        sql.add("telefone1, telefone2, email,");
        sql.add("dt_inclusao, dt_alteracao, id_usu_inclusao, id_usu_alteracao)");
        sql.add("VALUES");
        sql.add("(:id, :codigo, :tipo_pessoa, :nome, :cpf, :cnpj,");
        sql.add(":ativo, :cliente, :fornecedor,");
        sql.add(":numero, :logradouro, :bairro, :cidade, :cep, :complemento,");
        sql.add(":telefone1, :telefone2, :email,");
        sql.add(":dt_inclusao, :dt_alteracao, :id_usu_inclusao, :id_usu_alteracao)");
        sql.setParametro("id", pessoa);
        sql.setParametro("codigo", pessoa.getCodigo());
        sql.setParametro("tipo_pessoa", pessoa.getTipoPessoa());
        sql.setParametro("nome", pessoa.getNome());
        sql.setParametro("cpf", pessoa.getCpf());
        sql.setParametro("cnpj", pessoa.getCnpj());
        sql.setParametro("ativo", pessoa.isAtivo());
        sql.setParametro("cliente", pessoa.isCliente());
        sql.setParametro("fornecedor", pessoa.isFornecedor());
        sql.setParametro("numero", pessoa.getNumero());
        sql.setParametro("logradouro", pessoa.getLogradouro());
        sql.setParametro("bairro", pessoa.getBairro());
        sql.setParametro("cidade", pessoa.getCidade());
        sql.setParametro("cep", pessoa.getCep());
        sql.setParametro("complemento", pessoa.getComplemeto());
        sql.setParametro("telefone1", pessoa.getTelefone1());
        sql.setParametro("telefone2", pessoa.getTelefone2());
        sql.setParametro("email", pessoa.getEmail());
        sql.setParametro("dt_inclusao", data);
        sql.setParametro("dt_alteracao", data);
        sql.setParametro("id_usu_inclusao", usuario);
        sql.setParametro("id_usu_alteracao", usuario);
        Conexao.update(sql);
        pessoa.setUsuarioInclusao(usuario);
        pessoa.setDataInclusao(data);
        pessoa.setUsuarioAlteracao(usuario);
        pessoa.setDataAlteracao(data);
        CACHE.put(pessoa);
    }

    public static void alterar(Pessoa pessoa) throws BDException {
        Usuario usuario = Sessao.getUsuarioLogado();
        Date data = new Date();
        SQL sql = new SQL();
        sql.add("UPDATE " + TBL);
        sql.add("SET codigo = :codigo, nome = :nome, cpf = :cpf, cnpj = :cnpj, ativo = :ativo, tipo_pessoa = :tipo_pessoa,");
        sql.add("cliente = :cliente, fornecedor = :fornecedor, numero = :numero, logradouro = :logradouro, bairro = :bairro, cidade = :cidade, cep = :cep, complemento = :complemento,");
        sql.add("telefone1 = :telefone1, telefone2= :telefone2, email = :email,");
        sql.add("dt_alteracao = :dt_alteracao, id_usu_alteracao = :id_usu_alteracao");
        sql.add("WHERE id = :id");
        sql.setParametro("id", pessoa);
        sql.setParametro("codigo", pessoa.getCodigo());
        sql.setParametro("tipo_pessoa", pessoa.getTipoPessoa());
        sql.setParametro("nome", pessoa.getNome());
        sql.setParametro("cpf", pessoa.getCpf());
        sql.setParametro("cnpj", pessoa.getCnpj());
        sql.setParametro("ativo", pessoa.isAtivo());
        sql.setParametro("cliente", pessoa.isCliente());
        sql.setParametro("fornecedor", pessoa.isFornecedor());
        sql.setParametro("numero", pessoa.getNumero());
        sql.setParametro("logradouro", pessoa.getLogradouro());
        sql.setParametro("bairro", pessoa.getBairro());
        sql.setParametro("cidade", pessoa.getCidade());
        sql.setParametro("cep", pessoa.getCep());
        sql.setParametro("complemento", pessoa.getComplemeto());
        sql.setParametro("telefone1", pessoa.getTelefone1());
        sql.setParametro("telefone2", pessoa.getTelefone2());
        sql.setParametro("email", pessoa.getEmail());
        sql.setParametro("dt_alteracao", data);
        sql.setParametro("id_usu_alteracao", usuario);
        Conexao.update(sql);
        pessoa.setUsuarioAlteracao(usuario);
        pessoa.setDataAlteracao(data);
        CACHE.put(pessoa);
    }

    public static void excluir(Integer id) throws BDException {
        SQL sql = new SQL("DELETE FROM " + TBL + " WHERE id = :id");
        sql.setParametro("id", id);
        Conexao.update(sql);
        CACHE.remove(id);
    }

    public static boolean existe(Pessoa pessoa) throws BDException {
        SQL sql = new SQL("SELECT id FROM " + TBL + " WHERE codigo = :codigo");
        sql.setParametro("codigo", pessoa.getCodigo());
        try (Conexao c = Conexao.getConexao()) {
            try (ResultSet rs = c.executeQuery(sql)) {
                if (rs.next()) {
                    pessoa.setId(rs.getInt("id"));
                    return true;
                }
                return false;
            } catch (SQLException ex) {
                throw new BDException(ex);
            }
        }
    }

    public static void carregar(Pessoa pessoa) throws BDException {
        Pessoa objCache = CACHE.get(pessoa.getId());
        if (objCache != null) {
            carregarObjeto(pessoa, objCache);
            return;
        }
        SQL sql = new SQL("SELECT * FROM " + TBL + " WHERE id = :id");
        sql.setParametro("id", pessoa);
        try (Conexao c = Conexao.getConexao()) {
            try (ResultSet rs = c.executeQuery(sql)) {
                while (rs.next()) {
                    carregarObjeto(pessoa, rs);
                }
            } catch (SQLException ex) {
                throw new BDException(ex);
            }
        }
    }
    
    private static void carregarObjeto(Pessoa pessoa, Pessoa cache) {
        pessoa.setId(cache.getId());
        pessoa.setCodigo(cache.getCodigo());
        pessoa.setNome(cache.getNome());
        pessoa.setCpf(cache.getCpf());
        pessoa.setCnpj(cache.getCnpj());
        pessoa.setTipoPessoa(cache.getTipoPessoa());
        pessoa.setAtivo(cache.isAtivo());
        pessoa.setCliente(cache.isCliente());
        pessoa.setFornecedor(cache.isFornecedor());
        pessoa.setNumero(cache.getNumero());
        pessoa.setLogradouro(cache.getLogradouro());
        pessoa.setBairro(cache.getBairro());
        pessoa.setCidade(cache.getCidade());
        pessoa.setCep(cache.getCep());
        pessoa.setComplemeto(cache.getComplemeto());
        pessoa.setTelefone1(cache.getTelefone1());
        pessoa.setTelefone2(cache.getTelefone2());
        pessoa.setEmail(cache.getEmail());
        pessoa.setDataInclusao(cache.getDataInclusao());
        pessoa.setUsuarioInclusao(cache.getUsuarioInclusao());
        pessoa.setDataAlteracao(cache.getDataAlteracao());
        pessoa.setUsuarioAlteracao(cache.getUsuarioAlteracao());
    }

    private static void carregarObjeto(Pessoa pessoa, ResultSet rs) throws SQLException, BDException {
        pessoa.setCodigo(rs.getInt("codigo"));
        pessoa.setNome(rs.getString("nome"));
        pessoa.setCpf(rs.getString("cpf"));
        pessoa.setCnpj(rs.getString("cnpj"));
        pessoa.setTipoPessoa(TipoPessoa.getTipoPessoa(rs.getInt("tipo_pessoa")));
        pessoa.setAtivo(rs.getBoolean("ativo"));
        pessoa.setCliente(rs.getBoolean("cliente"));
        pessoa.setFornecedor(rs.getBoolean("fornecedor"));
        pessoa.setNumero(rs.getString("numero"));
        pessoa.setLogradouro(rs.getString("logradouro"));
        pessoa.setBairro(rs.getString("bairro"));
        pessoa.setCidade(rs.getString("cidade"));
        pessoa.setCep(rs.getString("cep"));
        pessoa.setComplemeto(rs.getString("complemento"));
        pessoa.setTelefone1(rs.getString("telefone1"));
        pessoa.setTelefone2(rs.getString("telefone2"));
        pessoa.setEmail(rs.getString("email"));
        pessoa.setDataInclusao(rs.getTimestamp("dt_inclusao"));
        pessoa.setUsuarioInclusao(new Usuario(rs.getInt("id_usu_alteracao")));
        UsuarioBD.carregar(pessoa.getUsuarioInclusao());
        pessoa.setDataAlteracao(rs.getTimestamp("dt_alteracao"));
        pessoa.setUsuarioAlteracao(new Usuario(rs.getInt("id_usu_alteracao")));
        UsuarioBD.carregar(pessoa.getUsuarioAlteracao());
        CACHE.put(pessoa);
    }

    public static List<Pessoa> listarConsulta(PessoaSC filtro) throws BDException {
        SQL sql = new SQL();
        sql.add("SELECT id, codigo, nome, tipo_pessoa, ativo, cliente, fornecedor");
        sql.add("FROM " + TBL);
        sql.add("WHERE TRUE");
        if (filtro.nome != null && !filtro.nome.isEmpty()) {
            sql.add("AND nome LIKE :nome");
            sql.setParametro("nome", filtro.nome + "%");
        }
        if (filtro.cliente != null && filtro.cliente) {
            sql.add("AND cliente = true");
        }
        if (filtro.fornecedor != null && filtro.fornecedor) {
            sql.add("AND fornecedor = true");
        }
        if (filtro.ativo != null && filtro.ativo) {
            sql.add("AND ativo = true");
        }
        sql.add("ORDER BY nome");
        sql.add("LIMIT 1000");

        try (Conexao c = Conexao.getConexao()) {
            try (ResultSet rs = c.executeQuery(sql)) {
                List<Pessoa> lista = new ArrayList();
                while (rs.next()) {
                    Pessoa pessoa = new Pessoa();
                    pessoa.setId(rs.getInt("id"));
                    pessoa.setCodigo(rs.getInt("codigo"));
                    pessoa.setNome(rs.getString("nome"));
                    pessoa.setTipoPessoa(TipoPessoa.getTipoPessoa(rs.getInt("tipo_pessoa")));
                    pessoa.setAtivo(rs.getBoolean("ativo"));
                    pessoa.setCliente(rs.getBoolean("cliente"));
                    pessoa.setFornecedor(rs.getBoolean("fornecedor"));
                    lista.add(pessoa);
                }
                return lista;
            } catch (SQLException ex) {
                throw new BDException(ex);
            }
        }
    }

    public static void alterarStatusCliente(Pessoa pessoa, boolean status) throws BDException {
        SQL sql = new SQL();
        sql.add("UPDATE " + TBL);
        sql.add("SET cliente = :cliente");
        sql.add("WHERE id = :id");
        sql.setParametro("id", pessoa);
        sql.setParametro("cliente", status);
        Conexao.update(sql);
        pessoa.setCliente(status);
        CACHE.put(pessoa);
    }

    public static void alterarStatusFornecedor(Pessoa pessoa, boolean status) throws BDException {
        SQL sql = new SQL();
        sql.add("UPDATE " + TBL);
        sql.add("SET fornecedor = :fornecedor");
        sql.add("WHERE id = :id");
        sql.setParametro("id", pessoa);
        sql.setParametro("fornecedor", status);
        Conexao.update(sql);
        pessoa.setFornecedor(status);
        CACHE.put(pessoa);
    }

    public static void alterarStatusAtivo(Pessoa pessoa, boolean status) throws BDException {
        SQL sql = new SQL();
        sql.add("UPDATE " + TBL);
        sql.add("SET ativo = :ativo");
        sql.add("WHERE id = :id");
        sql.setParametro("id", pessoa);
        sql.setParametro("ativo", status);
        Conexao.update(sql);
        pessoa.setAtivo(status);
        CACHE.put(pessoa);
    }
}
