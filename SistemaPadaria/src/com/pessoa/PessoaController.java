package com.pessoa;

import base.CampoException;
import base.bd.BDException;

/**
 *
 * @author Carlos
 * @since 10/04/2019
 */
public class PessoaController {

    public static void salvar(Pessoa pessoa) throws BDException, CampoException {
        if (pessoa.getNome() == null || pessoa.getNome().isEmpty()) {
            throw new CampoException("Nome não informado");
        }
        if (pessoa.getId() == null || !PessoaBD.existe(pessoa)) {
            PessoaBD.salvar(pessoa);
        } else {
            if (pessoa.getCodigo() == null) {
                throw new CampoException("Código não informado");
            }
            PessoaBD.alterar(pessoa);
        }
    }

    public static Pessoa buscar(Integer codigo) throws BDException {
        Pessoa pessoa = new Pessoa();
        pessoa.setCodigo(codigo);
        if (PessoaBD.existe(pessoa)) {
            PessoaBD.carregar(pessoa);
            return pessoa;
        }
        return null;
    }

    public static void excluir(Integer id) throws BDException {
        PessoaBD.excluir(id);
    }

    public static boolean alterarStatusCliente(Pessoa pessoa, boolean status) throws BDException {
        if (pessoa == null || pessoa.getId() == null) {
            return false;
        }
        PessoaBD.alterarStatusCliente(pessoa, status);
        return true;
    }

    public static boolean alterarStatusFornecedor(Pessoa pessoa, boolean status) throws BDException {
        if (pessoa == null || pessoa.getId() == null) {
            return false;
        }
        PessoaBD.alterarStatusFornecedor(pessoa, status);
        return true;
    }
    
    public static boolean alterarStatusAtivo(Pessoa pessoa, boolean status) throws BDException {
        if (pessoa == null || pessoa.getId() == null) {
            return false;
        }
        PessoaBD.alterarStatusAtivo(pessoa, status);
        return true;
    }
}
