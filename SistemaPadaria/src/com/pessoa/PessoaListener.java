package com.pessoa;

import base.bd.BDException;
import base.log.Log;
import base.msg.JAlerta;
import base.utils.SwingUtils;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JButton;
import javax.swing.JTextField;

/**
 *
 * @author Rodrigo Vianna
 * @since 12/05/2019
 */
public class PessoaListener {

    private final Window parent;
    private final JTextField jTextField_Cod;
    private final JTextField jTextField_Nome;
    private final JButton jButton;
    private final boolean obrigatorio;

    private final Tipo tipo;

    private Pessoa pessoa;

    public PessoaListener(Window parent, JTextField jTextField_Cod, JTextField jTextField_Nome, JButton jButton, boolean obrigatorio) {
        this(parent, jTextField_Cod, jTextField_Nome, jButton, obrigatorio, Tipo.TODOS);
    }

    public PessoaListener(Window parent, JTextField jTextField_Cod, JTextField jTextField_Nome, JButton jButton, boolean obrigatorio, Tipo tipo) {
        this.parent = parent;
        this.jTextField_Cod = jTextField_Cod;
        this.jTextField_Nome = jTextField_Nome;
        this.jButton = jButton;
        this.obrigatorio = obrigatorio;
        this.tipo = tipo;

        SwingUtils.alterarFocoSemEnter(this.jTextField_Cod);
        SwingUtils.desabilitarCampos(this.jTextField_Nome);

        this.jTextField_Cod.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    consultarCodigo();
                    jTextField_Cod.transferFocus();
                }
            }

        });
        this.jButton.addActionListener((ActionEvent e) -> {
            consultar();
        });
    }

    private void consultarCodigo() {
        String codigo = jTextField_Cod.getText();
        Pessoa pessoa = null;
        try {
            if (codigo.isEmpty()) {
                if (obrigatorio) {
                    JPessoaConsulta jPessoaConsulta = JPessoaConsulta.getInstance(parent);
                    jPessoaConsulta.setVisible(true);
                    if (jPessoaConsulta.isRetornar()) {
                        pessoa = jPessoaConsulta.getPessoa();
                    }
                }
                return;
            }
            pessoa = PessoaController.buscar(Integer.valueOf(codigo));
        } catch (BDException ex) {
            Log.log("Erro ao buscar pessoa pelo campo.", ex);
        } finally {
            setPessoa(pessoa);
        }
    }

    private void consultar() {
        Pessoa pessoa = null;
        try {
            JPessoaConsulta jPessoaConsulta = JPessoaConsulta.getInstance(parent);
            jPessoaConsulta.setVisible(true);
            if (jPessoaConsulta.isRetornar()) {
                pessoa = jPessoaConsulta.getPessoa();
            }
        } catch (BDException ex) {
            Log.log("Erro ao buscar pessoa pelo campo.", ex);
        } finally {
            setPessoa(pessoa);
        }
    }

    public void setPessoa(Pessoa pessoa) {
        if (pessoa != null && ((tipo == Tipo.CLIENTE && !pessoa.isCliente()) || (tipo == Tipo.FORNECEDOR && !pessoa.isFornecedor()))) {
            JAlerta.getAlertaErro((java.awt.Dialog) null, "Pessoa inválida");
            return;
        }
        this.pessoa = pessoa;
        jTextField_Nome.setText(pessoa == null ? "" : pessoa.getNome());
        jTextField_Cod.setText(pessoa == null ? "" : pessoa.getCodigo().toString());
    }

    public void bloquear(boolean b) {
        if (b) {
            SwingUtils.desabilitarCampos(jTextField_Cod, jButton);
        } else {
            SwingUtils.habilitarCampos(jTextField_Cod, jButton);
        }
    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    public enum Tipo {
        TODOS, FORNECEDOR, CLIENTE;
    }

}
