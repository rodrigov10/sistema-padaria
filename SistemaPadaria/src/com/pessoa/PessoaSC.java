package com.pessoa;

/**
 *
 * Classe representa o filtro para listagem de pessoas no banco de dados.
 *
 * @author Rodrigo Vianna
 * @since 25/05/2019
 */
public class PessoaSC {

    public String nome;
    public Boolean cliente;
    public Boolean fornecedor;
    public Boolean ativo;

}
