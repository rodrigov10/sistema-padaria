package com.pessoa;

import base.bd.EnumID;

/**
 *
 * @author Rodrigo
 * @since 12/05/2019
 */
public enum TipoPessoa implements EnumID {
    FISICA(0, "Fisica"),
    JURIDICA(1, "Juridica");

    public final Integer codigo;
    public final String nome;

    private TipoPessoa(Integer codigo, String nome) {
        this.nome = nome;
        this.codigo = codigo;
    }

    public static TipoPessoa getTipoPessoa(Integer codigo) {
        for (TipoPessoa tipo : values()) {
            if (tipo.codigo.equals(codigo)) {
                return tipo;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return nome;
    }

    @Override
    public Integer getCodigo() {
        return this.codigo;
    }

}
