package com.produto;

import base.CampoException;
import base.Valor;
import base.bd.BDException;
import base.msg.JAlerta;
import base.utils.FieldFormatUtils;
import base.utils.SwingUtils;
import com.pessoa.PessoaListener;
import java.awt.Component;

/**
 * Classe para cadastro de Produto
 * @author Éderson
 * @since 02/04/2019
 */
public class JProduto extends javax.swing.JFrame {

    private static JProduto jProduto;

    public static JProduto getInstance() {
        if (jProduto == null) {
            jProduto = new JProduto();
        }
        jProduto.limparTela();
        return jProduto;
    }

    private Produto produto;

    private JProdutoConsulta jProdutoConsulta;

    private PessoaListener pessoaListener;

    private boolean atualizarTabela = false;

    private JProduto() {
        initComponents();
        inicializar();
    }

    private void inicializar() {
        SwingUtils.inicializarTela(this);

        pessoaListener = new PessoaListener(this, jTFFornecedor_Codigo, jTFFornecedor_Nome, jBBuscarFornecedor, true, PessoaListener.Tipo.FORNECEDOR);

        FieldFormatUtils.formatarMonetario(jTFValor);
        FieldFormatUtils.formatarDigitos(jTFCodigo);
        FieldFormatUtils.formatarDigitos(jTFFornecedor_Codigo);
        FieldFormatUtils.formatarDigitos(jTFCodBarra1);
        FieldFormatUtils.formatarDigitos(jTFCodBarra2);

        jCBUnit.addItem(UnidadeMedida.UNIDADE);
        jCBUnit.addItem(UnidadeMedida.KILO);
        jCBUnit.addItem(UnidadeMedida.LITRO);
        jCBUnit.addItem(UnidadeMedida.GRAMA);
        jCBUnit.setSelectedItem(UnidadeMedida.UNIDADE);
    }

    ////////////////////////////////////////////////////////////////////////////
    //Produto
    ////////////////////////////////////////////////////////////////////////////
    public void setProduto(Produto produto) {
        this.produto = produto;
        jTFCodigo.setText(produto == null ? "" : String.valueOf(produto.getCodigo()));
        jTFNome.setText(produto == null ? "" : produto.getNome());
        jTFMarca.setText(produto == null ? "" : produto.getMarca());
        jTFValor.setText(produto == null ? "0,00" : String.valueOf(produto.getValor()));
        jCBUnit.setSelectedItem(produto == null ? UnidadeMedida.UNIDADE : produto.getUnidadeMedida());
        jTFCodBarra1.setText(produto == null ? "" : produto.getCodigoBarra1());
        jTFCodBarra2.setText(produto == null ? "" : produto.getCodigoBarra2());
        pessoaListener.setPessoa(produto == null ? null : produto.getFornecedor());
    }

    private void salvar() {
        try {
            SwingUtils.iniciarCursorProcessamento(this);
            SwingUtils.desabilitarCampos(getCampos());
            pessoaListener.bloquear(true);

            String codigo = jTFCodigo.getText().trim();
            String valor = jTFValor.getText().trim();

            if (produto == null) {
                produto = new Produto();
            }
            if (!codigo.isEmpty()) {
                produto.setCodigo(Integer.parseInt(codigo));
            }
            produto.setNome(jTFNome.getText().trim());
            produto.setMarca(jTFMarca.getText().trim());
            produto.setCodigoBarra1(jTFCodBarra1.getText().trim());
            produto.setCodigoBarra2(jTFCodBarra2.getText().trim());
            produto.setUnidadeMedida(jCBUnit.getItemAt(jCBUnit.getSelectedIndex()));
            produto.setFornecedor(pessoaListener.getPessoa());
            if (!valor.isEmpty()) {
                produto.setValor(new Valor(valor));
            } else {
                produto.setValor(Valor.VALOR_ZEROD2);
            }

            try {
                ProdutoController.salvar(produto);
                atualizarTabela = true;
                if (produto.getCodigo() != null) {
                    jTFCodigo.setText(String.valueOf(produto.getCodigo()));
                }
                JAlerta.getAlertaMsg(this, "Salvo com sucesso.");
            } catch (BDException ex) {
                JAlerta.getAlertaErro(this, "Erro ao salvar produto: ", ex);
            } catch (CampoException ex) {
                JAlerta.getAlertaErro(this, ex.getMessage());
            }
        } finally {
            pessoaListener.bloquear(false);
            SwingUtils.habilitarCampos(getCampos());
            SwingUtils.finalizarCursorProcessamento(this);
        }
    }

    private void buscar() {
        String codigo = jTFCodigo.getText().trim();
        if (codigo.isEmpty()) {
            JAlerta.getAlertaMsg(this, "Código não informado.");
            jTFCodigo.requestFocusInWindow();
            return;
        }
        try {
            SwingUtils.iniciarCursorProcessamento(this);
            SwingUtils.desabilitarCampos(getCampos());
            pessoaListener.bloquear(true);

            Produto produto = ProdutoController.buscar(Integer.parseInt(codigo));
            if (produto == null) {
                JAlerta.getAlertaMsg(this, "Produto não encontrado");
            }
            setProduto(produto);
        } catch (BDException ex) {
            JAlerta.getAlertaErro(this, "Erro ao buscar produto: ", ex);
        } finally {
            pessoaListener.bloquear(false);
            SwingUtils.habilitarCampos(getCampos());
            SwingUtils.finalizarCursorProcessamento(this);
        }
    }

    private void excluir() {
        if (produto == null) {
            JAlerta.getAlertaMsg(this, "Produto não informado.");
            return;
        }
        try {
            SwingUtils.iniciarCursorProcessamento(this);
            SwingUtils.desabilitarCampos(getCampos());
            pessoaListener.bloquear(true);

            ProdutoController.excluir(produto.getId());
            atualizarTabela = true;
            limparTela();
        } catch (BDException ex) {
            JAlerta.getAlertaErro(this, "Erro ao buscar produto: ", ex);
        } finally {
            pessoaListener.bloquear(false);
            SwingUtils.habilitarCampos(getCampos());
            SwingUtils.finalizarCursorProcessamento(this);
        }
    }

    private void consultar() {
        try {
            SwingUtils.iniciarCursorProcessamento(this);
            if (jProdutoConsulta == null) {
                jProdutoConsulta = JProdutoConsulta.getInstance(this);
                jProdutoConsulta.atualizarTabelaThread();
            } else if (atualizarTabela) {
                atualizarTabela = false;
                jProdutoConsulta.atualizarTabelaThread();
            }
            jProdutoConsulta.setVisible(true);
            if (jProdutoConsulta.isRetornar()) {
                Produto produto = jProdutoConsulta.getProduto();
                setProduto(produto);
            }
        } catch (BDException ex) {
            JAlerta.getAlertaErro(this, "Erro ao consultar pessoa: ", ex);
            setProduto(null);
        } finally {
            SwingUtils.finalizarCursorProcessamento(this);
        }
    }

    public void limparTela() {
        setProduto(null);
    }

    private Component[] getCampos() {
        return new Component[]{jBGravar, jBConsultar, jBLimpar, jBExcluir, jBBuscar};
    }

    public static void main(String[] args) {
        if (com.Start.iniciarSistema()) {
            new JProduto().setVisible(true);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPFundo = new javax.swing.JPanel();
        jPPessoal = new javax.swing.JPanel();
        jPCodigo = new javax.swing.JPanel();
        jLCodigo = new javax.swing.JLabel();
        jTFCodigo = new javax.swing.JTextField();
        jBBuscar = new javax.swing.JButton();
        jPNome = new javax.swing.JPanel();
        jLNome = new javax.swing.JLabel();
        jTFNome = new javax.swing.JTextField();
        jPMarca = new javax.swing.JPanel();
        jLMarca = new javax.swing.JLabel();
        jTFMarca = new javax.swing.JTextField();
        jPValor = new javax.swing.JPanel();
        jLValor = new javax.swing.JLabel();
        jTFValor = new javax.swing.JTextField();
        jLUnit = new javax.swing.JLabel();
        jCBUnit = new javax.swing.JComboBox<>();
        jPFornecedor = new javax.swing.JPanel();
        jLFornecedor = new javax.swing.JLabel();
        jTFFornecedor_Codigo = new javax.swing.JTextField();
        jTFFornecedor_Nome = new javax.swing.JTextField();
        jBBuscarFornecedor = new javax.swing.JButton();
        jPCodBarra = new javax.swing.JPanel();
        jLCodBarra = new javax.swing.JLabel();
        jTFCodBarra1 = new javax.swing.JTextField();
        jPCodBarra2 = new javax.swing.JPanel();
        jLCodBarra2 = new javax.swing.JLabel();
        jTFCodBarra2 = new javax.swing.JTextField();
        jPTitulo = new javax.swing.JPanel();
        jLTitulo = new javax.swing.JLabel();
        jPBotoes = new javax.swing.JPanel();
        jBGravar = new javax.swing.JButton();
        jBConsultar = new javax.swing.JButton();
        jBLimpar = new javax.swing.JButton();
        jBExcluir = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Cadastro de Produto");
        setMaximumSize(new java.awt.Dimension(430, 395));
        setMinimumSize(new java.awt.Dimension(430, 395));
        setPreferredSize(new java.awt.Dimension(430, 395));

        jPFundo.setMaximumSize(new java.awt.Dimension(450, 200));
        jPFundo.setMinimumSize(new java.awt.Dimension(450, 200));
        jPFundo.setPreferredSize(new java.awt.Dimension(450, 200));
        jPFundo.setLayout(new java.awt.BorderLayout());

        jPPessoal.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Dados", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 1, 14))); // NOI18N
        jPPessoal.setMaximumSize(new java.awt.Dimension(1000, 180));
        jPPessoal.setMinimumSize(new java.awt.Dimension(450, 180));
        jPPessoal.setPreferredSize(new java.awt.Dimension(450, 180));
        jPPessoal.setLayout(new javax.swing.BoxLayout(jPPessoal, javax.swing.BoxLayout.Y_AXIS));

        jPCodigo.setMaximumSize(new java.awt.Dimension(32767, 32));
        jPCodigo.setMinimumSize(new java.awt.Dimension(10, 22));
        jPCodigo.setPreferredSize(new java.awt.Dimension(438, 32));
        jPCodigo.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 6));

        jLCodigo.setText("Código:");
        jLCodigo.setMaximumSize(new java.awt.Dimension(90, 23));
        jLCodigo.setMinimumSize(new java.awt.Dimension(90, 23));
        jLCodigo.setPreferredSize(new java.awt.Dimension(90, 23));
        jPCodigo.add(jLCodigo);

        jTFCodigo.setMaximumSize(new java.awt.Dimension(70, 20));
        jTFCodigo.setMinimumSize(new java.awt.Dimension(250, 20));
        jTFCodigo.setPreferredSize(new java.awt.Dimension(50, 20));
        jPCodigo.add(jTFCodigo);

        jBBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/icones/buscar_16.png"))); // NOI18N
        jBBuscar.setToolTipText("Adicionar");
        jBBuscar.setBorder(null);
        jBBuscar.setMaximumSize(new java.awt.Dimension(23, 20));
        jBBuscar.setMinimumSize(new java.awt.Dimension(23, 20));
        jBBuscar.setPreferredSize(new java.awt.Dimension(23, 20));
        jBBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBBuscarActionPerformed(evt);
            }
        });
        jPCodigo.add(jBBuscar);

        jPPessoal.add(jPCodigo);

        jPNome.setMaximumSize(new java.awt.Dimension(32767, 32));
        jPNome.setMinimumSize(new java.awt.Dimension(10, 22));
        jPNome.setPreferredSize(new java.awt.Dimension(438, 32));
        jPNome.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 6));

        jLNome.setText("Nome:");
        jLNome.setMaximumSize(new java.awt.Dimension(90, 23));
        jLNome.setMinimumSize(new java.awt.Dimension(90, 23));
        jLNome.setPreferredSize(new java.awt.Dimension(90, 23));
        jPNome.add(jLNome);

        jTFNome.setMaximumSize(new java.awt.Dimension(250, 20));
        jTFNome.setMinimumSize(new java.awt.Dimension(250, 20));
        jTFNome.setPreferredSize(new java.awt.Dimension(250, 20));
        jPNome.add(jTFNome);

        jPPessoal.add(jPNome);

        jPMarca.setMaximumSize(new java.awt.Dimension(32767, 32));
        jPMarca.setMinimumSize(new java.awt.Dimension(10, 22));
        jPMarca.setPreferredSize(new java.awt.Dimension(438, 32));
        jPMarca.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 6));

        jLMarca.setText("Marca:");
        jLMarca.setMaximumSize(new java.awt.Dimension(90, 23));
        jLMarca.setMinimumSize(new java.awt.Dimension(90, 23));
        jLMarca.setPreferredSize(new java.awt.Dimension(90, 23));
        jPMarca.add(jLMarca);

        jTFMarca.setMaximumSize(new java.awt.Dimension(250, 20));
        jTFMarca.setMinimumSize(new java.awt.Dimension(250, 20));
        jTFMarca.setPreferredSize(new java.awt.Dimension(120, 20));
        jPMarca.add(jTFMarca);

        jPPessoal.add(jPMarca);

        jPValor.setMaximumSize(new java.awt.Dimension(32767, 32));
        jPValor.setMinimumSize(new java.awt.Dimension(10, 22));
        jPValor.setPreferredSize(new java.awt.Dimension(438, 32));
        jPValor.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 6));

        jLValor.setText("Valor:");
        jLValor.setMaximumSize(new java.awt.Dimension(90, 23));
        jLValor.setMinimumSize(new java.awt.Dimension(90, 23));
        jLValor.setPreferredSize(new java.awt.Dimension(90, 23));
        jPValor.add(jLValor);

        jTFValor.setMaximumSize(new java.awt.Dimension(250, 20));
        jTFValor.setMinimumSize(new java.awt.Dimension(250, 20));
        jTFValor.setPreferredSize(new java.awt.Dimension(80, 20));
        jPValor.add(jTFValor);

        jLUnit.setText("Unidade Medida:");
        jLUnit.setMaximumSize(new java.awt.Dimension(60, 20));
        jLUnit.setMinimumSize(new java.awt.Dimension(60, 20));
        jLUnit.setPreferredSize(new java.awt.Dimension(80, 20));
        jPValor.add(jLUnit);

        jCBUnit.setMaximumSize(new java.awt.Dimension(80, 20));
        jCBUnit.setMinimumSize(new java.awt.Dimension(80, 20));
        jCBUnit.setPreferredSize(new java.awt.Dimension(80, 20));
        jPValor.add(jCBUnit);

        jPPessoal.add(jPValor);

        jPFornecedor.setMaximumSize(new java.awt.Dimension(32767, 32));
        jPFornecedor.setMinimumSize(new java.awt.Dimension(10, 22));
        jPFornecedor.setPreferredSize(new java.awt.Dimension(438, 32));
        jPFornecedor.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 6));

        jLFornecedor.setText("Fornecedor:");
        jLFornecedor.setMaximumSize(new java.awt.Dimension(90, 23));
        jLFornecedor.setMinimumSize(new java.awt.Dimension(90, 23));
        jLFornecedor.setPreferredSize(new java.awt.Dimension(90, 23));
        jPFornecedor.add(jLFornecedor);

        jTFFornecedor_Codigo.setMaximumSize(new java.awt.Dimension(70, 20));
        jTFFornecedor_Codigo.setMinimumSize(new java.awt.Dimension(250, 20));
        jTFFornecedor_Codigo.setPreferredSize(new java.awt.Dimension(50, 20));
        jPFornecedor.add(jTFFornecedor_Codigo);

        jTFFornecedor_Nome.setMaximumSize(new java.awt.Dimension(250, 20));
        jTFFornecedor_Nome.setMinimumSize(new java.awt.Dimension(250, 20));
        jTFFornecedor_Nome.setPreferredSize(new java.awt.Dimension(220, 20));
        jPFornecedor.add(jTFFornecedor_Nome);

        jBBuscarFornecedor.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/icones/buscar_16.png"))); // NOI18N
        jBBuscarFornecedor.setToolTipText("Adicionar");
        jBBuscarFornecedor.setBorder(null);
        jBBuscarFornecedor.setMaximumSize(new java.awt.Dimension(23, 20));
        jBBuscarFornecedor.setMinimumSize(new java.awt.Dimension(23, 20));
        jBBuscarFornecedor.setPreferredSize(new java.awt.Dimension(23, 20));
        jPFornecedor.add(jBBuscarFornecedor);

        jPPessoal.add(jPFornecedor);

        jPCodBarra.setMaximumSize(new java.awt.Dimension(1000, 32));
        jPCodBarra.setMinimumSize(new java.awt.Dimension(438, 32));
        jPCodBarra.setPreferredSize(new java.awt.Dimension(438, 32));
        jPCodBarra.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 6));

        jLCodBarra.setText("Cod. Barra 1:");
        jLCodBarra.setMaximumSize(new java.awt.Dimension(90, 23));
        jLCodBarra.setMinimumSize(new java.awt.Dimension(90, 23));
        jLCodBarra.setPreferredSize(new java.awt.Dimension(90, 23));
        jPCodBarra.add(jLCodBarra);

        jTFCodBarra1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTFCodBarra1.setMaximumSize(new java.awt.Dimension(190, 20));
        jTFCodBarra1.setMinimumSize(new java.awt.Dimension(190, 20));
        jTFCodBarra1.setPreferredSize(new java.awt.Dimension(190, 20));
        jTFCodBarra1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTFCodBarra1KeyTyped(evt);
            }
        });
        jPCodBarra.add(jTFCodBarra1);

        jPPessoal.add(jPCodBarra);

        jPCodBarra2.setMaximumSize(new java.awt.Dimension(1000, 32));
        jPCodBarra2.setMinimumSize(new java.awt.Dimension(438, 32));
        jPCodBarra2.setPreferredSize(new java.awt.Dimension(438, 32));
        jPCodBarra2.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 6));

        jLCodBarra2.setText("Cod. Barra 2:");
        jLCodBarra2.setMaximumSize(new java.awt.Dimension(90, 23));
        jLCodBarra2.setMinimumSize(new java.awt.Dimension(90, 23));
        jLCodBarra2.setPreferredSize(new java.awt.Dimension(90, 23));
        jPCodBarra2.add(jLCodBarra2);

        jTFCodBarra2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTFCodBarra2.setMaximumSize(new java.awt.Dimension(190, 20));
        jTFCodBarra2.setMinimumSize(new java.awt.Dimension(190, 20));
        jTFCodBarra2.setPreferredSize(new java.awt.Dimension(190, 20));
        jTFCodBarra2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTFCodBarra2KeyTyped(evt);
            }
        });
        jPCodBarra2.add(jTFCodBarra2);

        jPPessoal.add(jPCodBarra2);

        jPFundo.add(jPPessoal, java.awt.BorderLayout.CENTER);

        getContentPane().add(jPFundo, java.awt.BorderLayout.CENTER);

        jPTitulo.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createEmptyBorder(10, 10, 10, 10), javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED)));

        jLTitulo.setFont(new java.awt.Font("Arial", 1, 24)); // NOI18N
        jLTitulo.setText("Cadastro de Produto");
        jPTitulo.add(jLTitulo);

        getContentPane().add(jPTitulo, java.awt.BorderLayout.NORTH);

        jBGravar.setMnemonic('G');
        jBGravar.setText("Gravar");
        jBGravar.setMaximumSize(new java.awt.Dimension(95, 23));
        jBGravar.setMinimumSize(new java.awt.Dimension(95, 23));
        jBGravar.setPreferredSize(new java.awt.Dimension(95, 23));
        jBGravar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBGravarActionPerformed(evt);
            }
        });
        jPBotoes.add(jBGravar);

        jBConsultar.setMnemonic('C');
        jBConsultar.setText("Consultar");
        jBConsultar.setMaximumSize(new java.awt.Dimension(95, 23));
        jBConsultar.setMinimumSize(new java.awt.Dimension(95, 23));
        jBConsultar.setPreferredSize(new java.awt.Dimension(95, 23));
        jBConsultar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBConsultarActionPerformed(evt);
            }
        });
        jPBotoes.add(jBConsultar);

        jBLimpar.setMnemonic('L');
        jBLimpar.setText("Limpar");
        jBLimpar.setMaximumSize(new java.awt.Dimension(95, 23));
        jBLimpar.setMinimumSize(new java.awt.Dimension(95, 23));
        jBLimpar.setPreferredSize(new java.awt.Dimension(95, 23));
        jBLimpar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBLimparActionPerformed(evt);
            }
        });
        jPBotoes.add(jBLimpar);

        jBExcluir.setMnemonic('E');
        jBExcluir.setText("Excluir");
        jBExcluir.setMaximumSize(new java.awt.Dimension(95, 23));
        jBExcluir.setMinimumSize(new java.awt.Dimension(95, 23));
        jBExcluir.setPreferredSize(new java.awt.Dimension(95, 23));
        jBExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBExcluirActionPerformed(evt);
            }
        });
        jPBotoes.add(jBExcluir);

        getContentPane().add(jPBotoes, java.awt.BorderLayout.SOUTH);

        setSize(new java.awt.Dimension(529, 418));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jBLimparActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBLimparActionPerformed
        limparTela();
    }//GEN-LAST:event_jBLimparActionPerformed

    private void jBExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBExcluirActionPerformed
        new Thread(() -> {
            excluir();
        }, "Excluir Produto").start();
    }//GEN-LAST:event_jBExcluirActionPerformed

    private void jBGravarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBGravarActionPerformed
        new Thread(() -> {
            salvar();
        }, "Gravar Produto").start();
    }//GEN-LAST:event_jBGravarActionPerformed

    private void jBConsultarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBConsultarActionPerformed
        consultar();
    }//GEN-LAST:event_jBConsultarActionPerformed

    private void jBBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBBuscarActionPerformed
        new Thread(() -> {
            buscar();
        }, "Busca Pessoa").start();
    }//GEN-LAST:event_jBBuscarActionPerformed

    private void jTFCodBarra1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTFCodBarra1KeyTyped
        if (!Character.isDigit(evt.getKeyChar()) || jTFCodBarra1.getText().length() > 12) {
            evt.consume();
        }
    }//GEN-LAST:event_jTFCodBarra1KeyTyped

    private void jTFCodBarra2KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTFCodBarra2KeyTyped
        if (!Character.isDigit(evt.getKeyChar()) || jTFCodBarra2.getText().length() > 12) {
            evt.consume();
        }
    }//GEN-LAST:event_jTFCodBarra2KeyTyped

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jBBuscar;
    private javax.swing.JButton jBBuscarFornecedor;
    private javax.swing.JButton jBConsultar;
    private javax.swing.JButton jBExcluir;
    private javax.swing.JButton jBGravar;
    private javax.swing.JButton jBLimpar;
    private javax.swing.JComboBox<UnidadeMedida> jCBUnit;
    private javax.swing.JLabel jLCodBarra;
    private javax.swing.JLabel jLCodBarra2;
    private javax.swing.JLabel jLCodigo;
    private javax.swing.JLabel jLFornecedor;
    private javax.swing.JLabel jLMarca;
    private javax.swing.JLabel jLNome;
    private javax.swing.JLabel jLTitulo;
    private javax.swing.JLabel jLUnit;
    private javax.swing.JLabel jLValor;
    private javax.swing.JPanel jPBotoes;
    private javax.swing.JPanel jPCodBarra;
    private javax.swing.JPanel jPCodBarra2;
    private javax.swing.JPanel jPCodigo;
    private javax.swing.JPanel jPFornecedor;
    private javax.swing.JPanel jPFundo;
    private javax.swing.JPanel jPMarca;
    private javax.swing.JPanel jPNome;
    private javax.swing.JPanel jPPessoal;
    private javax.swing.JPanel jPTitulo;
    private javax.swing.JPanel jPValor;
    private javax.swing.JTextField jTFCodBarra1;
    private javax.swing.JTextField jTFCodBarra2;
    private javax.swing.JTextField jTFCodigo;
    private javax.swing.JTextField jTFFornecedor_Codigo;
    private javax.swing.JTextField jTFFornecedor_Nome;
    private javax.swing.JTextField jTFMarca;
    private javax.swing.JTextField jTFNome;
    private javax.swing.JTextField jTFValor;
    // End of variables declaration//GEN-END:variables
}
