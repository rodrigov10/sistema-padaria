package com.produto;

import base.Valor;
import base.bd.ObjetoID;
import base.cache.CacheObjeto;
import base.usuario.Usuario;
import com.pessoa.Pessoa;
import java.util.Comparator;
import java.util.Date;
import java.util.Objects;

/**
 * 
 * @author Rodrigo Vianna
 * @since 13/03/2019
 */
public class Produto implements ObjetoID, CacheObjeto<Integer>, Comparator<Produto> {

    private Integer id;
    private Integer codigo;
    private String nome;
    private String marca;
    private Valor valor;
    private Pessoa fornecedor;
    private UnidadeMedida unidadeMedida;
    private String codigoBarra1;
    private String codigoBarra2;

    private Date dataInclusao;
    private Usuario usuarioInclusao;
    private Date dataAlteracao;
    private Usuario usuarioAlteracao;

    public Produto(Integer id) {
        this.id = id;
    }

    public Produto() {
    }

    public Pessoa getFornecedor() {
        return fornecedor;
    }

    public void setFornecedor(Pessoa fornecedor) {
        this.fornecedor = fornecedor;
    }

    public UnidadeMedida getUnidadeMedida() {
        return unidadeMedida;
    }

    public void setUnidadeMedida(UnidadeMedida unidadeMedida) {
        this.unidadeMedida = unidadeMedida;
    }

    @Override
    public Integer getId() {
        return id;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public String getNome() {
        return nome;
    }

    public String getMarca() {
        return marca;
    }

    public Valor getValor() {
        return valor;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public void setValor(Valor valor) {
        this.valor = valor;
        if (this.valor == null) {
            this.valor = Valor.VALOR_ZEROD2;
        }
    }

    public String getCodigoBarra1() {
        return codigoBarra1;
    }

    public void setCodigoBarra1(String codigoBarra1) {
        this.codigoBarra1 = codigoBarra1;
    }

    public String getCodigoBarra2() {
        return codigoBarra2;
    }

    public void setCodigoBarra2(String codigoBarra2) {
        this.codigoBarra2 = codigoBarra2;
    }

    public Date getDataInclusao() {
        return dataInclusao;
    }

    public void setDataInclusao(Date dataInclusao) {
        this.dataInclusao = dataInclusao;
    }

    public Usuario getUsuarioInclusao() {
        return usuarioInclusao;
    }

    public void setUsuarioInclusao(Usuario usuarioInclusao) {
        this.usuarioInclusao = usuarioInclusao;
    }

    public Date getDataAlteracao() {
        return dataAlteracao;
    }

    public void setDataAlteracao(Date dataAlteracao) {
        this.dataAlteracao = dataAlteracao;
    }

    public Usuario getUsuarioAlteracao() {
        return usuarioAlteracao;
    }

    public void setUsuarioAlteracao(Usuario usuarioAlteracao) {
        this.usuarioAlteracao = usuarioAlteracao;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 73 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Produto other = (Produto) obj;
        return Objects.equals(this.id, other.id);
    }

    @Override
    public String toString() {
        return nome;
    }

    @Override
    public int compare(Produto o1, Produto o2) {
        if (o1.nome == null && o2.nome != null) {
            return -1;
        } else if (o1.nome != null && o2.nome == null) {
            return 1;
        }
        return o1.nome.compareToIgnoreCase(o2.nome);
    }

}
