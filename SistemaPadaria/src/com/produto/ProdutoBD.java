package com.produto;

import base.Valor;
import base.bd.BDException;
import base.bd.Conexao;
import base.bd.SQL;
import base.cache.CacheManager;
import base.login.Sessao;
import base.usuario.Usuario;
import base.usuario.UsuarioBD;
import com.pessoa.Pessoa;
import com.pessoa.PessoaBD;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Rodrigo Vianna
 * @since 10/04/2019
 */
public class ProdutoBD {

    public static final String TBL = "tabproduto";
    
    private static final CacheManager<Produto> CACHE = CacheManager.getInstance(Produto.class);

    private static Integer getNovoId() throws BDException {
        SQL sql = new SQL("SELECT nextval('tabproduto_id_seq') seq");
        try (Conexao c = Conexao.getConexao()) {
            try (ResultSet rs = c.executeQuery(sql)) {
                if (rs.next()) {
                    return rs.getInt("seq");
                }
            } catch (SQLException ex) {
                throw new BDException(ex);
            }
        }
        return null;
    }

    private static Integer getNovoCodigo() throws BDException {
        SQL sql = new SQL("SELECT nextval('tabproduto_codigo_seq') seq");
        try (Conexao c = Conexao.getConexao()) {
            try (ResultSet rs = c.executeQuery(sql)) {
                if (rs.next()) {
                    return rs.getInt("seq");
                }
            } catch (SQLException ex) {
                throw new BDException(ex);
            }
        }
        return null;
    }

    public static void salvar(Produto produto) throws BDException {
        produto.setId(getNovoId());
        produto.setCodigo(getNovoCodigo());
        Usuario usuario = Sessao.getUsuarioLogado();
        Date data = new Date();
        SQL sql = new SQL();
        sql.add("INSERT INTO " + TBL);
        sql.add("(id, codigo, nome, marca, valor, id_pessoa, unidade_medida,");
        sql.add("cod_barra1, cod_barra2, dt_inclusao, dt_alteracao, id_usu_inclusao, id_usu_alteracao)");
        sql.add("VALUES");
        sql.add("(:id, :codigo, :nome, :marca, :valor, :id_pessoa, :unidade_medida,");
        sql.add(":cod_barra1, :cod_barra2, :dt_inclusao, :dt_alteracao, :id_usu_inclusao, :id_usu_alteracao)");
        sql.setParametro("id", produto);
        sql.setParametro("codigo", produto.getCodigo());
        sql.setParametro("nome", produto.getNome());
        sql.setParametro("marca", produto.getMarca());
        sql.setParametro("valor", produto.getValor());
        sql.setParametro("id_pessoa", produto.getFornecedor() == null ? null : produto.getFornecedor());
        sql.setParametro("unidade_medida", produto.getUnidadeMedida());
        sql.setParametro("cod_barra1", produto.getCodigoBarra1());
        sql.setParametro("cod_barra2", produto.getCodigoBarra2());
        sql.setParametro("dt_inclusao", data);
        sql.setParametro("dt_alteracao", data);
        sql.setParametro("id_usu_inclusao", usuario);
        sql.setParametro("id_usu_alteracao", usuario);
        Conexao.update(sql);
        produto.setUsuarioInclusao(usuario);
        produto.setDataInclusao(data);
        produto.setUsuarioAlteracao(usuario);
        produto.setDataAlteracao(data);
        CACHE.put(produto);
    }

    public static void alterar(Produto produto) throws BDException {
        Usuario usuario = Sessao.getUsuarioLogado();
        Date data = new Date();
        SQL sql = new SQL();
        sql.add("UPDATE " + TBL);
        sql.add("SET codigo = :codigo, nome = :nome, marca = :marca, valor = :valor, id_pessoa = :id_pessoa, unidade_medida = :unidade_medida,");
        sql.add("cod_barra1 = :cod_barra1, cod_barra2 = :cod_barra2,");
        sql.add("dt_alteracao = :dt_alteracao, id_usu_alteracao = :id_usu_alteracao");
        sql.add("WHERE id = :id");
        sql.setParametro("id", produto);
        sql.setParametro("codigo", produto.getCodigo());
        sql.setParametro("nome", produto.getNome());
        sql.setParametro("marca", produto.getMarca());
        sql.setParametro("valor", produto.getValor());
        sql.setParametro("id_pessoa", produto.getFornecedor() == null ? null : produto.getFornecedor());
        sql.setParametro("unidade_medida", produto.getUnidadeMedida());
        sql.setParametro("cod_barra1", produto.getCodigoBarra1());
        sql.setParametro("cod_barra2", produto.getCodigoBarra2());
        sql.setParametro("dt_alteracao", data);
        sql.setParametro("id_usu_alteracao", usuario);
        Conexao.update(sql);
        produto.setUsuarioAlteracao(usuario);
        produto.setDataAlteracao(data);
        CACHE.put(produto);
    }

    public static void excluir(Integer id) throws BDException {
        SQL sql = new SQL("DELETE FROM " + TBL + " WHERE id = :id");
        sql.setParametro("id", id);
        Conexao.update(sql);
    }

    public static void carregar(Produto produto) throws BDException {
        Produto objCache = CACHE.get(produto.getId());
        if (objCache != null) {
            carregarObjeto(produto, objCache);
            return;
        }
        SQL sql = new SQL("SELECT * FROM " + TBL + " WHERE id = :id");
        sql.setParametro("id", produto);
        try (Conexao c = Conexao.getConexao()) {
            try (ResultSet rs = c.executeQuery(sql)) {
                while (rs.next()) {
                    carregarObjeto(produto, rs);
                }
            } catch (SQLException ex) {
                throw new BDException(ex);
            }
        }
    }
    
    private static void carregarObjeto(Produto produto, Produto cache) {
        produto.setId(cache.getId());
        produto.setCodigo(cache.getCodigo());
        produto.setNome(cache.getNome());
        produto.setMarca(cache.getMarca());
        produto.setValor(cache.getValor());
        produto.setUnidadeMedida(cache.getUnidadeMedida());
        produto.setFornecedor(cache.getFornecedor());
        produto.setCodigoBarra1(cache.getCodigoBarra1());
        produto.setCodigoBarra2(cache.getCodigoBarra2());
        produto.setDataInclusao(cache.getDataInclusao());
        produto.setUsuarioInclusao(cache.getUsuarioInclusao());
        produto.setDataAlteracao(cache.getDataAlteracao());
        produto.setUsuarioAlteracao(cache.getUsuarioAlteracao());
    }

    private static void carregarObjeto(Produto produto, ResultSet rs) throws SQLException, BDException {
        produto.setCodigo(rs.getInt("codigo"));
        produto.setNome(rs.getString("nome"));
        produto.setMarca(rs.getString("marca"));
        produto.setValor(new Valor(rs.getBigDecimal("valor")));
        produto.setUnidadeMedida(UnidadeMedida.getUnidadeMedida(rs.getInt("unidade_medida")));
        produto.setFornecedor(new Pessoa(rs.getInt("id_pessoa")));
        if (rs.wasNull()) {
            produto.setFornecedor(null);
        } else {
            PessoaBD.carregar(produto.getFornecedor());
        }
        produto.setCodigoBarra1(rs.getString("cod_barra1"));
        produto.setCodigoBarra2(rs.getString("cod_barra2"));
        produto.setUsuarioInclusao(new Usuario(rs.getInt("id_usu_inclusao")));
        UsuarioBD.carregar(produto.getUsuarioInclusao());
        produto.setDataInclusao(rs.getDate("dt_inclusao"));
        produto.setUsuarioAlteracao(new Usuario(rs.getInt("id_usu_alteracao")));
        UsuarioBD.carregar(produto.getUsuarioAlteracao());
        produto.setDataAlteracao(rs.getDate("dt_alteracao"));
    }

    public static List<Produto> listarConsulta(ProdutoSC filtro) throws BDException {
        SQL sql = new SQL();
        sql.add("SELECT id, codigo, nome, marca, valor, unidade_medida, id_pessoa, cod_barra1, cod_barra2");
        sql.add("FROM " + TBL);
        sql.add("WHERE TRUE");
        if (filtro.nome != null && !filtro.nome.isEmpty()) {
            sql.add("AND nome LIKE :nome");
            sql.setParametro("nome", filtro.nome + "%");
        }
        try (Conexao c = Conexao.getConexao()) {
            try (ResultSet rs = c.executeQuery(sql)) {
                List<Produto> lista = new ArrayList();
                while (rs.next()) {
                    Produto produto = new Produto();
                    produto.setId(rs.getInt("id"));
                    produto.setCodigo(rs.getInt("codigo"));
                    produto.setNome(rs.getString("nome"));
                    produto.setMarca(rs.getString("marca"));
                    produto.setValor(new Valor(rs.getBigDecimal("valor")));
                    produto.setUnidadeMedida(UnidadeMedida.getUnidadeMedida(rs.getInt("unidade_medida")));
                    produto.setFornecedor(new Pessoa(rs.getInt("id_pessoa")));
                    if (rs.wasNull()) {
                        produto.setFornecedor(null);
                    }
                    produto.setCodigoBarra1(rs.getString("cod_barra1"));
                    produto.setCodigoBarra2(rs.getString("cod_barra2"));
                    lista.add(produto);
                }
                return lista;
            } catch (SQLException ex) {
                throw new BDException(ex);
            }
        }
    }

    static boolean existe(Produto produto) throws BDException {
        SQL sql = new SQL("SELECT id FROM " + TBL);
        sql.add("WHERE TRUE");
        if (produto.getCodigo() != null) {
            sql.add(" AND codigo = :codigo");
            sql.setParametro("codigo", produto.getCodigo());
        } else if (!produto.getCodigoBarra1().isEmpty()) {
            sql.add(" AND cod_barra1 LIKE :cod_barra OR cod_barra2 LIKE :cod_barra");
            sql.setParametro("cod_barra", produto.getCodigoBarra1());
        }

        try (Conexao c = Conexao.getConexao()) {
            try (ResultSet rs = c.executeQuery(sql)) {
                if (rs.next()) {
                    produto.setId(rs.getInt("id"));
                    return true;
                }
                return false;
            } catch (SQLException ex) {
                throw new BDException(ex);
            }
        }
    }
}
