package com.produto;

import base.CampoException;
import base.bd.BDException;

/**
 *
 * @author Rodrigo Vianna
 * @since 02/05/2019
 */
public class ProdutoController {

    public static void salvar(Produto produto) throws BDException, CampoException {
        if (produto.getNome() == null || produto.getNome().isEmpty()) {
            throw new CampoException("Nome não informado");
        }
        if (produto.getId() == null || !ProdutoBD.existe(produto)) {
            ProdutoBD.salvar(produto);
        } else {
            ProdutoBD.alterar(produto);
        }
    }

    public static Produto buscar(Integer codigo) throws BDException {
        Produto produto = new Produto();
        produto.setCodigo(codigo);
        if (ProdutoBD.existe(produto)) {
            ProdutoBD.carregar(produto);
            return produto;
        }
        return null;
    }
    
    public static Produto buscar(Produto produto) throws BDException {
        if (ProdutoBD.existe(produto)) {
            ProdutoBD.carregar(produto);
            return produto;
        }
        return null;
    }

    public static void excluir(Integer id) throws BDException {
        ProdutoBD.excluir(id);
    }

}
