package com.produto;

import base.bd.BDException;
import base.log.Log;
import base.utils.SwingUtils;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JButton;
import javax.swing.JTextField;

/**
 *
 * @author Rodrigo Vianna
 * @since 14/06/2019
 */
public class ProdutoListener {

    private final Window parent;
    private final JTextField jTextField_Cod;
    private final JTextField jTextField_Nome;
    private final JButton jButton;
    private final boolean obrigatorio;

    private Produto produto;

    public ProdutoListener(Window parent, JTextField jTextField_Cod, JTextField jTextField_Nome, JButton jButton, boolean obrigatorio) {
        this.parent = parent;
        this.jTextField_Cod = jTextField_Cod;
        this.jTextField_Nome = jTextField_Nome;
        this.jButton = jButton;
        this.obrigatorio = obrigatorio;
        
        SwingUtils.alterarFocoSemEnter(this.jTextField_Cod);
        SwingUtils.desabilitarCampos(this.jTextField_Nome);

        this.jTextField_Cod.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    consultarCodigo();
                    jTextField_Cod.transferFocus();
                }
            }

        });
        this.jButton.addActionListener((ActionEvent e) -> {
            consultar();
        });
    }

    private void consultarCodigo() {
        String codigo = jTextField_Cod.getText();
        Produto produto = null;
        try {
            if (codigo.isEmpty()) {
                if (obrigatorio) {
                    JProdutoConsulta jProdutoConsulta = JProdutoConsulta.getInstance(parent);
                    jProdutoConsulta.setVisible(true);
                    if (jProdutoConsulta.isRetornar()) {
                        produto = jProdutoConsulta.getProduto();
                    }
                }
                return;
            }
            produto = ProdutoController.buscar(Integer.valueOf(codigo));
        } catch (BDException ex) {
            Log.log("Erro ao buscar produto pelo campo.", ex);
        } finally {
            setProduto(produto);
        }
    }

    private void consultar() {
        Produto produto = null;
        try {
            JProdutoConsulta jProdutoConsulta = JProdutoConsulta.getInstance(parent);
            jProdutoConsulta.setVisible(true);
            if (jProdutoConsulta.isRetornar()) {
                produto = jProdutoConsulta.getProduto();
            }
        } catch (BDException ex) {
            Log.log("Erro ao buscar produto pelo campo.", ex);
        } finally {
            setProduto(produto);
        }
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
        jTextField_Nome.setText(produto == null ? "" : produto.getNome());
        jTextField_Cod.setText(produto == null ? "" : produto.getCodigo().toString());
    }

    public void bloquear(boolean b) {
        if (b) {
            SwingUtils.desabilitarCampos(jTextField_Cod, jButton);
        } else {
            SwingUtils.habilitarCampos(jTextField_Cod, jButton);
        }
    }

    public Produto getProduto() {
        return produto;
    }

    public enum Tipo {
        TODOS, FORNECEDOR, CLIENTE;
    }

}
