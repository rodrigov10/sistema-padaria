package com.produto;

import base.bd.EnumID;

/**
 *
 * @author Carlos
 * @since 25/03/2019
 */
public enum UnidadeMedida implements EnumID {
    UNIDADE(0, "Unidade"),
    KILO(1, "Kilo"),
    GRAMA(2, "Grama"),
    LITRO(3, "Litro");

    public final Integer codigo;
    public final String nome;

    private UnidadeMedida(Integer codigo, String nome) {
        this.nome = nome;
        this.codigo = codigo;
    }

    public static UnidadeMedida getUnidadeMedida(Integer codigo) {
        for (UnidadeMedida unidade : values()) {
            if (unidade.codigo.equals(codigo)) {
                return unidade;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return nome;
    }

    @Override
    public Integer getCodigo() {
        return this.codigo;
    }

}
