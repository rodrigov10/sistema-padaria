package com.relatorio.encomenda;

import base.bd.BDException;
import base.bd.Conexao;
import base.bd.SQL;
import base.msg.JAlerta;
import base.utils.FecharEsc;
import base.utils.ReportUtils;
import base.utils.SwingUtils;
import com.encomenda.EncomendaBD;
import com.encomenda.item.ItemEncomendaProdutoBD;
import com.pessoa.PessoaBD;
import com.pessoa.PessoaListener;
import com.produto.ProdutoBD;
import java.awt.Component;
import java.awt.Window;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperReport;

/**
 *
 * @author Rodrigo Vianna
 * @since 22/06/2019
 */
public class JRelEncomenda extends javax.swing.JDialog implements FecharEsc {

    private static JRelEncomenda instance;

    public static JRelEncomenda getInstance(Window parent) {
        if (instance == null) {
            instance = new JRelEncomenda(parent, false);
        }
        return instance;
    }

    private PessoaListener pessoaListener;

    private JRelEncomenda(Window parent, boolean modal) {
        super(parent, modal ? ModalityType.APPLICATION_MODAL : ModalityType.MODELESS);
        initComponents();
        inicializar();
    }

    private void inicializar() {
        SwingUtils.inicializarTela(this);

        pessoaListener = new PessoaListener(this, jTextField_CodPessoa, jTextField_NomePessoa, jButton_Pessoa, true, PessoaListener.Tipo.TODOS);

        jComboBox_TipoPessoa.addItem(TipoPessoa.TODAS);
        jComboBox_TipoPessoa.addItem(TipoPessoa.FISICA);
        jComboBox_TipoPessoa.addItem(TipoPessoa.JURIDICA);

        jComboBox_ClienteFornecedor.addItem(ClienteFornecedor.TODOS);
        jComboBox_ClienteFornecedor.addItem(ClienteFornecedor.CLIENTE);
        jComboBox_ClienteFornecedor.addItem(ClienteFornecedor.FORNECEDOR);

        jComboBox_Finalizado.addItem(Finalizado.AMBOS);
        jComboBox_Finalizado.addItem(Finalizado.SIM);
        jComboBox_Finalizado.addItem(Finalizado.NAO);

    }

    private void gerar() {
        new Thread(() -> {
            gerarRun();
        }, "Geração de Relatórios de Encomendas").start();
    }

    private void gerarRun() {
        try {
            SwingUtils.iniciarCursorProcessamento(this);
            SwingUtils.desabilitarCampos(getCampos());

            //Cria o SQL de busca
            SQL sql = new SQL();
            sql.add("SELECT p.codigo cdPes, p.nome nmPes, e.codigo cdEnc, e.datapedido dtPed, e.dataparaentrega dtEnc,");
            sql.add("e.observacao obsEnc, e.valor vlrEnc, pr.codigo cdProd, pr.nome nmProd, i.quantidade qtd, i.observacao obs");
            sql.add("FROM " + EncomendaBD.TBL + " e");
            sql.add("INNER JOIN " + PessoaBD.TBL + " p ON e.id_pessoa = p.id");
            sql.add("LEFT JOIN " + ItemEncomendaProdutoBD.TBL + " i ON i.id_entrega = e.id");
            sql.add("LEFT JOIN " + ProdutoBD.TBL + " pr ON pr.id = i.id_produto");
            sql.add("WHERE TRUE");
            switch (jComboBox_TipoPessoa.getItemAt(jComboBox_TipoPessoa.getSelectedIndex())) {
                case FISICA:
                    sql.add("AND p.tipo_pessoa = :tipo_pessoa");
                    sql.setParametro("tipo_pessoa", com.pessoa.TipoPessoa.FISICA);
                    break;
                case JURIDICA:
                    sql.add("AND p.tipo_pessoa = :tipo_pessoa");
                    sql.setParametro("tipo_pessoa", com.pessoa.TipoPessoa.JURIDICA);
                    break;
                default:
            }
            switch (jComboBox_ClienteFornecedor.getItemAt(jComboBox_ClienteFornecedor.getSelectedIndex())) {
                case CLIENTE:
                    sql.add("AND p.cliente = TRUE");
                    break;
                case FORNECEDOR:
                    sql.add("AND p.fornecedor = TRUE");
                    break;
                default:
            }
            switch (jComboBox_Finalizado.getItemAt(jComboBox_Finalizado.getSelectedIndex())) {
                case SIM:
                    sql.add("AND e.finalizado = TRUE");
                    break;
                case NAO:
                    sql.add("AND e.finalizado = FALSE");
                    break;
                default:
            }
            if (pessoaListener.getPessoa() != null) {
                sql.add("AND p.id = :id_pessoa");
                sql.setParametro("id_pessoa", pessoaListener.getPessoa());
            }
            if (jDataField_DtEntregaInicial.getDataSelecionada() != null && jDataField_DtEntregaFinal.getDataSelecionada() != null) {
                sql.add("AND DATE(e.dataparaentrega) BETWEEN DATE(:dtEntregaInicial) AND DATE(:dtEntregaFinal)");
                sql.setParametro("dtEntregaInicial", jDataField_DtEntregaInicial.getDataSelecionada());
                sql.setParametro("dtEntregaFinal", jDataField_DtEntregaFinal.getDataSelecionada());
            } else if (jDataField_DtEntregaInicial.getDataSelecionada() == null && jDataField_DtEntregaFinal.getDataSelecionada() != null) {
                sql.add("AND DATE(e.dataparaentrega) <= DATE(:dtEntregaFinal)");
                sql.setParametro("dtEntregaFinal", jDataField_DtEntregaFinal.getDataSelecionada());
            } else if (jDataField_DtEntregaInicial.getDataSelecionada() != null && jDataField_DtEntregaFinal.getDataSelecionada() == null) {
                sql.add("AND DATE(e.dataparaentrega) >= DATE(:dtEntregaInicial)");
                sql.setParametro("dtEntregaInicial", jDataField_DtEntregaInicial.getDataSelecionada());
            }
            if (jDataField_DtPedidoInicial.getDataSelecionada() != null && jDataField_DtPedidoFinal.getDataSelecionada() != null) {
                sql.add("AND DATE(e.datapedido) BETWEEN DATE(:dtPedidoInicial) AND DATE(:dtPedidoFinal)");
                sql.setParametro("dtPedidoInicial", jDataField_DtPedidoInicial.getDataSelecionada());
                sql.setParametro("dtPedidoFinal", jDataField_DtPedidoFinal.getDataSelecionada());
            } else if (jDataField_DtPedidoInicial.getDataSelecionada() == null && jDataField_DtPedidoFinal.getDataSelecionada() != null) {
                sql.add("AND DATE(e.datapedido) <= DATE(:dtPedidoFinal)");
                sql.setParametro("dtPedidoFinal", jDataField_DtPedidoFinal.getDataSelecionada());
            } else if (jDataField_DtPedidoInicial.getDataSelecionada() != null && jDataField_DtPedidoFinal.getDataSelecionada() == null) {
                sql.add("AND DATE(e.datapedido) >= DATE(:dtPedidoInicial)");
                sql.setParametro("dtPedidoInicial", jDataField_DtPedidoInicial.getDataSelecionada());
            }
            sql.add("ORDER BY p.nome, e.codigo, pr.nome");

            try (Conexao c = Conexao.getConexao()) {
                try (ResultSet rs = c.executeQuery(sql)) {
                    //Cria os parâmetros
                    Map parametros = new HashMap();
                    parametros.put("TITULO", "Relação de Encomendas");

                    //Executa o relatório
                    JasperReport relatorio = JasperCompileManager.compileReport(getClass().getResourceAsStream("/com/relatorio/encomenda/RelEncomenda.jrxml"));
                    ReportUtils.gerarRelatorio(this, rs, relatorio, parametros, "Relação de Encomendas");
                } catch (SQLException ex) {
                    throw new BDException(ex);
                }
            }

        } catch (BDException e) {
            JAlerta.getAlertaErro(this, "Erro ao realizar consutla:", e);
        } catch (JRException e) {
            JAlerta.getAlertaErro(this, "Erro ao gerar relatório:", e);
        } finally {
            SwingUtils.habilitarCampos(getCampos());
            SwingUtils.finalizarCursorProcessamento(this);
        }
    }

    private Component[] getCampos() {
        return new Component[]{jButton_Gerar, jComboBox_Finalizado, jComboBox_TipoPessoa, jTextField_CodPessoa, jDataField_DtEntregaInicial, jDataField_DtEntregaFinal, jDataField_DtPedidoInicial, jDataField_DtPedidoFinal};
    }

    @Override
    public void fechar() {
        setVisible(false);
        dispose();
    }

    public static void main(String[] args) {
        if (com.Start.iniciarSistema()) {
            getInstance((java.awt.Dialog) null).setVisible(true);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Consulta de Encomendas");
        setMinimumSize(new java.awt.Dimension(520, 270));
        setPreferredSize(new java.awt.Dimension(520, 270));

        jPanel_Filtro.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Filtro", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 11))); // NOI18N
        jPanel_Filtro.setLayout(new javax.swing.BoxLayout(jPanel_Filtro, javax.swing.BoxLayout.Y_AXIS));

        jPanel_Filtros.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jPanel_Filtros.setMaximumSize(new java.awt.Dimension(1000, 500));
        jPanel_Filtros.setMinimumSize(new java.awt.Dimension(200, 32));
        jPanel_Filtros.setPreferredSize(new java.awt.Dimension(400, 44));
        jPanel_Filtros.setLayout(new javax.swing.BoxLayout(jPanel_Filtros, javax.swing.BoxLayout.Y_AXIS));

        jPanel_ClienteFornecedor.setMaximumSize(new java.awt.Dimension(32767, 32));
        jPanel_ClienteFornecedor.setMinimumSize(new java.awt.Dimension(10, 22));
        jPanel_ClienteFornecedor.setPreferredSize(new java.awt.Dimension(438, 32));
        jPanel_ClienteFornecedor.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 6));

        jLabel_ClienteFornecedor.setText("Cliente/Fornecedor:");
        jLabel_ClienteFornecedor.setMaximumSize(new java.awt.Dimension(120, 20));
        jLabel_ClienteFornecedor.setMinimumSize(new java.awt.Dimension(120, 20));
        jLabel_ClienteFornecedor.setPreferredSize(new java.awt.Dimension(120, 20));
        jPanel_ClienteFornecedor.add(jLabel_ClienteFornecedor);

        jComboBox_ClienteFornecedor.setMaximumSize(new java.awt.Dimension(80, 20));
        jComboBox_ClienteFornecedor.setMinimumSize(new java.awt.Dimension(80, 20));
        jComboBox_ClienteFornecedor.setPreferredSize(new java.awt.Dimension(80, 20));
        jPanel_ClienteFornecedor.add(jComboBox_ClienteFornecedor);

        jPanel_Filtros.add(jPanel_ClienteFornecedor);

        jPanel_TipoPessoa.setMaximumSize(new java.awt.Dimension(32767, 32));
        jPanel_TipoPessoa.setMinimumSize(new java.awt.Dimension(10, 22));
        jPanel_TipoPessoa.setPreferredSize(new java.awt.Dimension(438, 32));
        jPanel_TipoPessoa.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 6));

        jLabel_TipoPessoa.setText("Tipo de Pessoa:");
        jLabel_TipoPessoa.setMaximumSize(new java.awt.Dimension(120, 20));
        jLabel_TipoPessoa.setMinimumSize(new java.awt.Dimension(120, 20));
        jLabel_TipoPessoa.setPreferredSize(new java.awt.Dimension(120, 20));
        jPanel_TipoPessoa.add(jLabel_TipoPessoa);

        jComboBox_TipoPessoa.setMaximumSize(new java.awt.Dimension(80, 20));
        jComboBox_TipoPessoa.setMinimumSize(new java.awt.Dimension(80, 20));
        jComboBox_TipoPessoa.setPreferredSize(new java.awt.Dimension(80, 20));
        jPanel_TipoPessoa.add(jComboBox_TipoPessoa);

        jPanel_Filtros.add(jPanel_TipoPessoa);

        jPanel_Finalizado.setMaximumSize(new java.awt.Dimension(32767, 32));
        jPanel_Finalizado.setMinimumSize(new java.awt.Dimension(10, 22));
        jPanel_Finalizado.setPreferredSize(new java.awt.Dimension(438, 32));
        jPanel_Finalizado.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 6));

        jLabel_Finalizado.setText("Finalizado");
        jLabel_Finalizado.setMaximumSize(new java.awt.Dimension(120, 20));
        jLabel_Finalizado.setMinimumSize(new java.awt.Dimension(120, 20));
        jLabel_Finalizado.setPreferredSize(new java.awt.Dimension(120, 20));
        jPanel_Finalizado.add(jLabel_Finalizado);

        jComboBox_Finalizado.setMaximumSize(new java.awt.Dimension(80, 20));
        jComboBox_Finalizado.setMinimumSize(new java.awt.Dimension(80, 20));
        jComboBox_Finalizado.setPreferredSize(new java.awt.Dimension(80, 20));
        jPanel_Finalizado.add(jComboBox_Finalizado);

        jPanel_Filtros.add(jPanel_Finalizado);

        jPanel_Pessoa.setMaximumSize(new java.awt.Dimension(32767, 32));
        jPanel_Pessoa.setMinimumSize(new java.awt.Dimension(10, 22));
        jPanel_Pessoa.setPreferredSize(new java.awt.Dimension(438, 32));
        jPanel_Pessoa.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 6));

        jLabel_Pessoa.setText("Pessoa:");
        jLabel_Pessoa.setMaximumSize(new java.awt.Dimension(120, 20));
        jLabel_Pessoa.setMinimumSize(new java.awt.Dimension(120, 20));
        jLabel_Pessoa.setPreferredSize(new java.awt.Dimension(120, 20));
        jPanel_Pessoa.add(jLabel_Pessoa);

        jTextField_CodPessoa.setMaximumSize(new java.awt.Dimension(70, 20));
        jTextField_CodPessoa.setMinimumSize(new java.awt.Dimension(250, 20));
        jTextField_CodPessoa.setPreferredSize(new java.awt.Dimension(50, 20));
        jPanel_Pessoa.add(jTextField_CodPessoa);

        jTextField_NomePessoa.setMaximumSize(new java.awt.Dimension(250, 20));
        jTextField_NomePessoa.setMinimumSize(new java.awt.Dimension(250, 20));
        jTextField_NomePessoa.setPreferredSize(new java.awt.Dimension(220, 20));
        jPanel_Pessoa.add(jTextField_NomePessoa);

        jButton_Pessoa.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/icones/buscar_16.png"))); // NOI18N
        jButton_Pessoa.setToolTipText("Adicionar");
        jButton_Pessoa.setBorder(null);
        jButton_Pessoa.setMaximumSize(new java.awt.Dimension(23, 20));
        jButton_Pessoa.setMinimumSize(new java.awt.Dimension(23, 20));
        jButton_Pessoa.setPreferredSize(new java.awt.Dimension(23, 20));
        jPanel_Pessoa.add(jButton_Pessoa);

        jPanel_Filtros.add(jPanel_Pessoa);

        jPanel_DataEntrega.setMaximumSize(new java.awt.Dimension(32767, 32));
        jPanel_DataEntrega.setMinimumSize(new java.awt.Dimension(10, 22));
        jPanel_DataEntrega.setPreferredSize(new java.awt.Dimension(438, 32));
        jPanel_DataEntrega.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 6));

        jLabel_DtEntrega.setText("Data de Entrega:");
        jLabel_DtEntrega.setMaximumSize(new java.awt.Dimension(115, 20));
        jLabel_DtEntrega.setMinimumSize(new java.awt.Dimension(115, 20));
        jLabel_DtEntrega.setPreferredSize(new java.awt.Dimension(115, 20));
        jPanel_DataEntrega.add(jLabel_DtEntrega);
        jPanel_DataEntrega.add(jDataField_DtEntregaInicial);

        jLabel_A.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel_A.setText("a");
        jLabel_A.setMaximumSize(new java.awt.Dimension(120, 20));
        jLabel_A.setMinimumSize(new java.awt.Dimension(80, 20));
        jLabel_A.setPreferredSize(new java.awt.Dimension(10, 20));
        jPanel_DataEntrega.add(jLabel_A);
        jPanel_DataEntrega.add(jDataField_DtEntregaFinal);

        jPanel_Filtros.add(jPanel_DataEntrega);

        jPanel_DataPedido.setMaximumSize(new java.awt.Dimension(32767, 32));
        jPanel_DataPedido.setMinimumSize(new java.awt.Dimension(10, 22));
        jPanel_DataPedido.setPreferredSize(new java.awt.Dimension(438, 32));
        jPanel_DataPedido.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 6));

        jLabel_DtPedido.setText("Data de Pedido:");
        jLabel_DtPedido.setMaximumSize(new java.awt.Dimension(115, 20));
        jLabel_DtPedido.setMinimumSize(new java.awt.Dimension(115, 20));
        jLabel_DtPedido.setPreferredSize(new java.awt.Dimension(115, 20));
        jPanel_DataPedido.add(jLabel_DtPedido);
        jPanel_DataPedido.add(jDataField_DtPedidoInicial);

        jLabel_A1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel_A1.setText("a");
        jLabel_A1.setMaximumSize(new java.awt.Dimension(120, 20));
        jLabel_A1.setMinimumSize(new java.awt.Dimension(80, 20));
        jLabel_A1.setPreferredSize(new java.awt.Dimension(10, 20));
        jPanel_DataPedido.add(jLabel_A1);
        jPanel_DataPedido.add(jDataField_DtPedidoFinal);

        jPanel_Filtros.add(jPanel_DataPedido);

        jPanel_Filtro.add(jPanel_Filtros);

        getContentPane().add(jPanel_Filtro, java.awt.BorderLayout.CENTER);

        jPanel_Buttons.setPreferredSize(new java.awt.Dimension(400, 33));

        jButton_Gerar.setMnemonic('G');
        jButton_Gerar.setText("Gerar");
        jButton_Gerar.setToolTipText("");
        jButton_Gerar.setMaximumSize(new java.awt.Dimension(80, 23));
        jButton_Gerar.setMinimumSize(new java.awt.Dimension(80, 23));
        jButton_Gerar.setPreferredSize(new java.awt.Dimension(80, 23));
        jButton_Gerar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_GerarActionPerformed(evt);
            }
        });
        jPanel_Buttons.add(jButton_Gerar);

        getContentPane().add(jPanel_Buttons, java.awt.BorderLayout.SOUTH);

        setSize(new java.awt.Dimension(536, 309));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton_GerarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_GerarActionPerformed
        gerar();
    }//GEN-LAST:event_jButton_GerarActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private final javax.swing.JButton jButton_Gerar = new javax.swing.JButton();
    private final javax.swing.JButton jButton_Pessoa = new javax.swing.JButton();
    private final javax.swing.JComboBox<ClienteFornecedor> jComboBox_ClienteFornecedor = new javax.swing.JComboBox<>();
    private final javax.swing.JComboBox<Finalizado> jComboBox_Finalizado = new javax.swing.JComboBox<>();
    private final javax.swing.JComboBox<TipoPessoa> jComboBox_TipoPessoa = new javax.swing.JComboBox<>();
    private final base.utils.calendario.JDataField jDataField_DtEntregaFinal = new base.utils.calendario.JDataField();
    private final base.utils.calendario.JDataField jDataField_DtEntregaInicial = new base.utils.calendario.JDataField();
    private final base.utils.calendario.JDataField jDataField_DtPedidoFinal = new base.utils.calendario.JDataField();
    private final base.utils.calendario.JDataField jDataField_DtPedidoInicial = new base.utils.calendario.JDataField();
    private final javax.swing.JLabel jLabel_A = new javax.swing.JLabel();
    private final javax.swing.JLabel jLabel_A1 = new javax.swing.JLabel();
    private final javax.swing.JLabel jLabel_ClienteFornecedor = new javax.swing.JLabel();
    private final javax.swing.JLabel jLabel_DtEntrega = new javax.swing.JLabel();
    private final javax.swing.JLabel jLabel_DtPedido = new javax.swing.JLabel();
    private final javax.swing.JLabel jLabel_Finalizado = new javax.swing.JLabel();
    private final javax.swing.JLabel jLabel_Pessoa = new javax.swing.JLabel();
    private final javax.swing.JLabel jLabel_TipoPessoa = new javax.swing.JLabel();
    private final javax.swing.JPanel jPanel_Buttons = new javax.swing.JPanel();
    private final javax.swing.JPanel jPanel_ClienteFornecedor = new javax.swing.JPanel();
    private final javax.swing.JPanel jPanel_DataEntrega = new javax.swing.JPanel();
    private final javax.swing.JPanel jPanel_DataPedido = new javax.swing.JPanel();
    private final javax.swing.JPanel jPanel_Filtro = new javax.swing.JPanel();
    private final javax.swing.JPanel jPanel_Filtros = new javax.swing.JPanel();
    private final javax.swing.JPanel jPanel_Finalizado = new javax.swing.JPanel();
    private final javax.swing.JPanel jPanel_Pessoa = new javax.swing.JPanel();
    private final javax.swing.JPanel jPanel_TipoPessoa = new javax.swing.JPanel();
    private final javax.swing.JTextField jTextField_CodPessoa = new javax.swing.JTextField();
    private final javax.swing.JTextField jTextField_NomePessoa = new javax.swing.JTextField();
    // End of variables declaration//GEN-END:variables

    private enum TipoPessoa {
        TODAS("Todas"), FISICA("Física"), JURIDICA("Jurídica");

        private final String nome;

        private TipoPessoa(String nome) {
            this.nome = nome;
        }

        @Override
        public String toString() {
            return nome;
        }

    }

    private enum ClienteFornecedor {
        TODOS("Todos"), CLIENTE("Cliente"), FORNECEDOR("Fornecedor");

        private final String nome;

        private ClienteFornecedor(String nome) {
            this.nome = nome;
        }

        @Override
        public String toString() {
            return nome;
        }

    }

    private enum Finalizado {
        AMBOS("Ambos"), SIM("Sim"), NAO("Não");

        private final String nome;

        private Finalizado(String nome) {
            this.nome = nome;
        }

        @Override
        public String toString() {
            return nome;
        }

    }
}
