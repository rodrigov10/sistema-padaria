package com.relatorio.pessoa;

import base.bd.BDException;
import base.bd.Conexao;
import base.bd.SQL;
import base.msg.JAlerta;
import base.utils.FecharEsc;
import base.utils.ReportUtils;
import base.utils.SwingUtils;
import com.pessoa.PessoaBD;
import java.awt.Component;
import java.awt.Window;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperReport;

/**
 *
 * @author Rodrigo Vianna
 * @since 24/05/2019
 */
public class JRelPessoa extends javax.swing.JDialog implements FecharEsc {

    private static JRelPessoa instance;

    public static JRelPessoa getInstance(Window parent) {
        if (instance == null) {
            instance = new JRelPessoa(parent, false);
        }
        return instance;
    }

    private JRelPessoa(Window parent, boolean modal) {
        super(parent, modal ? ModalityType.APPLICATION_MODAL : ModalityType.MODELESS);
        initComponents();
        inicializar();
    }

    private void inicializar() {
        SwingUtils.inicializarTela(this);

        jComboBox_TipoPessoa.addItem(TipoPessoa.TODAS);
        jComboBox_TipoPessoa.addItem(TipoPessoa.FISICA);
        jComboBox_TipoPessoa.addItem(TipoPessoa.JURIDICA);

    }

    private void gerar() {
        new Thread(() -> {
            gerarRun();
        }, "Geração de Relatórios de Pessoas").start();
    }

    private void gerarRun() {
        try {
            SwingUtils.iniciarCursorProcessamento(this);
            SwingUtils.desabilitarCampos(getCampos());

            final boolean incluirEndereco = jCheckBox_ExibirEndereco.isSelected();
            
            //Cria o SQL de busca
            SQL sql = new SQL();
            sql.add("SELECT codigo cdPes, nome nmPes, tipo_pessoa tipo, ativo, cliente, fornecedor,");
            sql.add("telefone1 tel1, telefone2 tel2, email, numero, logradouro, bairro, cidade, cep, complemento");
            sql.add("FROM " + PessoaBD.TBL);
            sql.add("WHERE TRUE");
            if (jCheckBox_Ativo.isSelected()) {
                sql.add("AND ativo = true");
            }
            if (jCheckBox_Fornecedor.isSelected()) {
                sql.add("AND fornecedor = true");
            }
            if (jCheckBox_Cliente.isSelected()) {
                sql.add("AND cliente = true");
            }
            switch (jComboBox_TipoPessoa.getItemAt(jComboBox_TipoPessoa.getSelectedIndex())) {
                case FISICA:
                    sql.add("AND tipo_pessoa = :tipo_pessoa");
                    sql.setParametro("tipo_pessoa", com.pessoa.TipoPessoa.FISICA);
                    break;
                case JURIDICA:
                    sql.add("AND tipo_pessoa = :tipo_pessoa");
                    sql.setParametro("tipo_pessoa", com.pessoa.TipoPessoa.JURIDICA);
                    break;
                default:
            }
            sql.add("ORDER BY nome");

            try (Conexao c = Conexao.getConexao()) {
                try (ResultSet rs = c.executeQuery(sql)) {
                    //Cria os parâmetros
                    Map parametros = new HashMap();
                    parametros.put("incluirEndereco", incluirEndereco);
                    parametros.put("TITULO", "Relação de Pessoas");

                    //Executa o relatório
                    JasperReport relatorio = JasperCompileManager.compileReport(getClass().getResourceAsStream("/com/relatorio/pessoa/RelPessoa.jrxml"));
                    ReportUtils.gerarRelatorio(this, rs, relatorio, parametros, "Relação de Pessoas");
                } catch (SQLException ex) {
                    throw new BDException(ex);
                }
            }

        } catch (BDException e) {
            JAlerta.getAlertaErro(this, "Erro ao realizar consutla:", e);
        } catch (JRException e) {
            JAlerta.getAlertaErro(this, "Erro ao gerar relatório:", e);
        } finally {
            SwingUtils.habilitarCampos(getCampos());
            SwingUtils.finalizarCursorProcessamento(this);
        }
    }

    private Component[] getCampos() {
        return new Component[]{jButton_Gerar, jCheckBox_Ativo, jCheckBox_Cliente, jCheckBox_Fornecedor, jCheckBox_ExibirEndereco};
    }

    @Override
    public void fechar() {
        setVisible(false);
        dispose();
    }

    public static void main(String[] args) {
        if (com.Start.iniciarSistema()) {
            getInstance((java.awt.Dialog) null).setVisible(true);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Consulta de Pessoas");
        setMinimumSize(new java.awt.Dimension(440, 160));
        setPreferredSize(new java.awt.Dimension(440, 160));

        jPanel_Filtro.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Filtro", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 11))); // NOI18N
        jPanel_Filtro.setLayout(new javax.swing.BoxLayout(jPanel_Filtro, javax.swing.BoxLayout.Y_AXIS));

        jPanel_Filtros.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jPanel_Filtros.setMaximumSize(new java.awt.Dimension(1000, 500));
        jPanel_Filtros.setMinimumSize(new java.awt.Dimension(200, 32));
        jPanel_Filtros.setPreferredSize(new java.awt.Dimension(400, 44));
        jPanel_Filtros.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        jLabel_TipoPessoa.setText("Tipo de Pessoa:");
        jLabel_TipoPessoa.setMaximumSize(new java.awt.Dimension(95, 20));
        jLabel_TipoPessoa.setMinimumSize(new java.awt.Dimension(95, 20));
        jLabel_TipoPessoa.setPreferredSize(new java.awt.Dimension(120, 20));
        jPanel_Filtros.add(jLabel_TipoPessoa);

        jComboBox_TipoPessoa.setMaximumSize(new java.awt.Dimension(90, 20));
        jComboBox_TipoPessoa.setMinimumSize(new java.awt.Dimension(90, 20));
        jComboBox_TipoPessoa.setPreferredSize(new java.awt.Dimension(90, 20));
        jPanel_Filtros.add(jComboBox_TipoPessoa);

        jPanel_Filtro.add(jPanel_Filtros);

        jPanel_Params.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Parâmetros", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 11))); // NOI18N
        jPanel_Params.setMaximumSize(new java.awt.Dimension(1000, 66));
        jPanel_Params.setMinimumSize(new java.awt.Dimension(200, 66));
        jPanel_Params.setPreferredSize(new java.awt.Dimension(400, 66));
        jPanel_Params.setLayout(new java.awt.GridLayout(2, 3));

        jCheckBox_Fornecedor.setText("Somente Fornecedores");
        jCheckBox_Fornecedor.setMaximumSize(new java.awt.Dimension(90, 20));
        jCheckBox_Fornecedor.setMinimumSize(new java.awt.Dimension(90, 20));
        jCheckBox_Fornecedor.setPreferredSize(new java.awt.Dimension(90, 20));
        jPanel_Params.add(jCheckBox_Fornecedor);

        jCheckBox_Cliente.setText("Somente Clientes");
        jCheckBox_Cliente.setMaximumSize(new java.awt.Dimension(60, 20));
        jCheckBox_Cliente.setMinimumSize(new java.awt.Dimension(60, 20));
        jCheckBox_Cliente.setPreferredSize(new java.awt.Dimension(60, 20));
        jPanel_Params.add(jCheckBox_Cliente);

        jCheckBox_Ativo.setSelected(true);
        jCheckBox_Ativo.setText("Somente Ativos");
        jCheckBox_Ativo.setMaximumSize(new java.awt.Dimension(115, 20));
        jCheckBox_Ativo.setMinimumSize(new java.awt.Dimension(115, 20));
        jCheckBox_Ativo.setPreferredSize(new java.awt.Dimension(115, 20));
        jPanel_Params.add(jCheckBox_Ativo);

        jCheckBox_ExibirEndereco.setSelected(true);
        jCheckBox_ExibirEndereco.setText("Exibir Endereço");
        jCheckBox_ExibirEndereco.setMaximumSize(new java.awt.Dimension(115, 20));
        jCheckBox_ExibirEndereco.setMinimumSize(new java.awt.Dimension(115, 20));
        jCheckBox_ExibirEndereco.setPreferredSize(new java.awt.Dimension(115, 20));
        jPanel_Params.add(jCheckBox_ExibirEndereco);

        jPanel_Filtro.add(jPanel_Params);

        getContentPane().add(jPanel_Filtro, java.awt.BorderLayout.CENTER);

        jPanel_Buttons.setPreferredSize(new java.awt.Dimension(400, 33));

        jButton_Gerar.setMnemonic('G');
        jButton_Gerar.setText("Gerar");
        jButton_Gerar.setToolTipText("");
        jButton_Gerar.setMaximumSize(new java.awt.Dimension(80, 23));
        jButton_Gerar.setMinimumSize(new java.awt.Dimension(80, 23));
        jButton_Gerar.setPreferredSize(new java.awt.Dimension(80, 23));
        jButton_Gerar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_GerarActionPerformed(evt);
            }
        });
        jPanel_Buttons.add(jButton_Gerar);

        getContentPane().add(jPanel_Buttons, java.awt.BorderLayout.SOUTH);

        setSize(new java.awt.Dimension(456, 199));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton_GerarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_GerarActionPerformed
        gerar();
    }//GEN-LAST:event_jButton_GerarActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private final javax.swing.JButton jButton_Gerar = new javax.swing.JButton();
    private final javax.swing.JCheckBox jCheckBox_Ativo = new javax.swing.JCheckBox();
    private final javax.swing.JCheckBox jCheckBox_Cliente = new javax.swing.JCheckBox();
    private final javax.swing.JCheckBox jCheckBox_ExibirEndereco = new javax.swing.JCheckBox();
    private final javax.swing.JCheckBox jCheckBox_Fornecedor = new javax.swing.JCheckBox();
    private final javax.swing.JComboBox<TipoPessoa> jComboBox_TipoPessoa = new javax.swing.JComboBox<>();
    private final javax.swing.JLabel jLabel_TipoPessoa = new javax.swing.JLabel();
    private final javax.swing.JPanel jPanel_Buttons = new javax.swing.JPanel();
    private final javax.swing.JPanel jPanel_Filtro = new javax.swing.JPanel();
    private final javax.swing.JPanel jPanel_Filtros = new javax.swing.JPanel();
    private final javax.swing.JPanel jPanel_Params = new javax.swing.JPanel();
    // End of variables declaration//GEN-END:variables

    private enum TipoPessoa {
        TODAS("Todas"), FISICA("Física"), JURIDICA("Jurídica");

        private final String nome;

        private TipoPessoa(String nome) {
            this.nome = nome;
        }

        @Override
        public String toString() {
            return nome;
        }

    }
}
