package com.relatorio.venda;

import base.bd.BDException;
import base.bd.Conexao;
import base.bd.SQL;
import base.msg.JAlerta;
import base.usuario.UsuarioBD;
import base.utils.FecharEsc;
import base.utils.ReportUtils;
import base.utils.SwingUtils;
import com.pessoa.PessoaBD;
import com.pessoa.PessoaListener;
import com.produto.ProdutoBD;
import com.venda.ItemVendaProdutoBD;
import com.venda.VendaBD;
import java.awt.Component;
import java.awt.Window;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperReport;

/**
 *
 * @author Rodrigo Vianna
 * @since 23/06/2019
 */
public class JRelVenda extends javax.swing.JDialog implements FecharEsc {

    private static JRelVenda instance;

    public static JRelVenda getInstance(Window parent) {
        if (instance == null) {
            instance = new JRelVenda(parent, false);
        }
        return instance;
    }

    private PessoaListener pessoaListener;

    private JRelVenda(Window parent, boolean modal) {
        super(parent, modal ? ModalityType.APPLICATION_MODAL : ModalityType.MODELESS);
        initComponents();
        inicializar();
    }

    private void inicializar() {
        SwingUtils.inicializarTela(this);

        pessoaListener = new PessoaListener(this, jTextField_CodPessoa, jTextField_NomePessoa, jButton_Pessoa, true, PessoaListener.Tipo.TODOS);

        jComboBox_TipoPessoa.addItem(TipoPessoa.TODAS);
        jComboBox_TipoPessoa.addItem(TipoPessoa.FISICA);
        jComboBox_TipoPessoa.addItem(TipoPessoa.JURIDICA);

        jComboBox_Status.addItem(Status.TODOS);
        jComboBox_Status.addItem(Status.FINALIZADO);
        jComboBox_Status.addItem(Status.CANCELADO);

    }

    private void gerar() {
        new Thread(() -> {
            gerarRun();
        }, "Geração de Relatórios de Vendas").start();
    }

    private void gerarRun() {
        try {
            SwingUtils.iniciarCursorProcessamento(this);
            SwingUtils.desabilitarCampos(getCampos());

            //Cria o SQL de busca
            SQL sql = new SQL();
            sql.add("SELECT p.codigo cdPes, p.nome nmPes, v.id cdVenda, v.datavenda, v.valordesconto, v.valoracrescimo,");
            sql.add("v.cancelado, v.finalizado, i.quantidade, i.valor, v.forma_pagamento, pr.codigo cdProd, pr.nome nmProd,");
            sql.add("v.dt_cancelamento, u.nomeusuario");
            sql.add("FROM " + VendaBD.TBL + " v");
            sql.add("INNER JOIN " + PessoaBD.TBL + " p ON v.id_pessoa = p.id");
            sql.add("LEFT JOIN " + ItemVendaProdutoBD.TBL + " i ON i.id_venda = v.id");
            sql.add("LEFT JOIN " + ProdutoBD.TBL + " pr ON pr.id = i.id_produto");
            sql.add("LEFT JOIN " + UsuarioBD.TBL + " u ON u.id = v.id_usu_cancelamento");
            sql.add("WHERE TRUE");
            switch (jComboBox_TipoPessoa.getItemAt(jComboBox_TipoPessoa.getSelectedIndex())) {
                case FISICA:
                    sql.add("AND p.tipo_pessoa = :tipo_pessoa");
                    sql.setParametro("tipo_pessoa", com.pessoa.TipoPessoa.FISICA);
                    break;
                case JURIDICA:
                    sql.add("AND p.tipo_pessoa = :tipo_pessoa");
                    sql.setParametro("tipo_pessoa", com.pessoa.TipoPessoa.JURIDICA);
                    break;
                default:
            }
            switch (jComboBox_Status.getItemAt(jComboBox_Status.getSelectedIndex())) {
                case FINALIZADO:
                    sql.add("AND v.finalizado = TRUE");
                    sql.add("AND v.cancelado = FALSE");
                    break;
                case CANCELADO:
                    sql.add("AND v.cancelado = TRUE");
                    break;
                default:
            }
            if (pessoaListener.getPessoa() != null) {
                sql.add("AND p.id = :id_pessoa");
                sql.setParametro("id_pessoa", pessoaListener.getPessoa());
            }
            if (jDataField_DtVendaInicial.getDataSelecionada() != null && jDataField_DtVendaFinal.getDataSelecionada() != null) {
                sql.add("AND DATE(v.dataparaentrega) BETWEEN DATE(:dtEntregaInicial) AND DATE(:dtEntregaFinal)");
                sql.setParametro("dtEntregaInicial", jDataField_DtVendaInicial.getDataSelecionada());
                sql.setParametro("dtEntregaFinal", jDataField_DtVendaFinal.getDataSelecionada());
            } else if (jDataField_DtVendaInicial.getDataSelecionada() == null && jDataField_DtVendaFinal.getDataSelecionada() != null) {
                sql.add("AND DATE(v.dataparaentrega) <= DATE(:dtEntregaFinal)");
                sql.setParametro("dtEntregaFinal", jDataField_DtVendaFinal.getDataSelecionada());
            } else if (jDataField_DtVendaInicial.getDataSelecionada() != null && jDataField_DtVendaFinal.getDataSelecionada() == null) {
                sql.add("AND DATE(v.dataparaentrega) >= DATE(:dtEntregaInicial)");
                sql.setParametro("dtEntregaInicial", jDataField_DtVendaInicial.getDataSelecionada());
            }
            sql.add("ORDER BY p.nome, v.id, pr.nome");

            try (Conexao c = Conexao.getConexao()) {
                try (ResultSet rs = c.executeQuery(sql)) {
                    //Cria os parâmetros
                    Map parametros = new HashMap();
                    parametros.put("TITULO", "Relação de Vendas");

                    //Executa o relatório
                    JasperReport relatorio = JasperCompileManager.compileReport(getClass().getResourceAsStream("/com/relatorio/venda/RelVenda.jrxml"));
                    ReportUtils.gerarRelatorio(this, rs, relatorio, parametros, "Relação de Vendas");
                } catch (SQLException ex) {
                    throw new BDException(ex);
                }
            }

        } catch (BDException v) {
            JAlerta.getAlertaErro(this, "Erro ao realizar consutla:", v);
        } catch (JRException v) {
            JAlerta.getAlertaErro(this, "Erro ao gerar relatório:", v);
        } finally {
            SwingUtils.habilitarCampos(getCampos());
            SwingUtils.finalizarCursorProcessamento(this);
        }
    }

    private Component[] getCampos() {
        return new Component[]{jButton_Gerar, jComboBox_TipoPessoa, jTextField_CodPessoa, jDataField_DtVendaInicial, jDataField_DtVendaFinal};
    }

    @Override
    public void fechar() {
        setVisible(false);
        dispose();
    }

    public static void main(String[] args) {
        if (com.Start.iniciarSistema()) {
            getInstance((java.awt.Dialog) null).setVisible(true);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Consulta de Vendas");
        setMinimumSize(new java.awt.Dimension(520, 200));
        setPreferredSize(new java.awt.Dimension(520, 200));

        jPanel_Filtro.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Filtro", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 11))); // NOI18N
        jPanel_Filtro.setLayout(new javax.swing.BoxLayout(jPanel_Filtro, javax.swing.BoxLayout.Y_AXIS));

        jPanel_Filtros.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jPanel_Filtros.setMaximumSize(new java.awt.Dimension(1000, 500));
        jPanel_Filtros.setMinimumSize(new java.awt.Dimension(200, 32));
        jPanel_Filtros.setPreferredSize(new java.awt.Dimension(400, 44));
        jPanel_Filtros.setLayout(new javax.swing.BoxLayout(jPanel_Filtros, javax.swing.BoxLayout.Y_AXIS));

        jPanel_Status.setMaximumSize(new java.awt.Dimension(32767, 32));
        jPanel_Status.setMinimumSize(new java.awt.Dimension(10, 22));
        jPanel_Status.setPreferredSize(new java.awt.Dimension(438, 32));
        jPanel_Status.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 6));

        jLabel_Status.setText("Status:");
        jLabel_Status.setMaximumSize(new java.awt.Dimension(120, 20));
        jLabel_Status.setMinimumSize(new java.awt.Dimension(120, 20));
        jLabel_Status.setPreferredSize(new java.awt.Dimension(120, 20));
        jPanel_Status.add(jLabel_Status);

        jComboBox_Status.setMaximumSize(new java.awt.Dimension(80, 20));
        jComboBox_Status.setMinimumSize(new java.awt.Dimension(80, 20));
        jComboBox_Status.setPreferredSize(new java.awt.Dimension(80, 20));
        jPanel_Status.add(jComboBox_Status);

        jPanel_Filtros.add(jPanel_Status);

        jPanel_TipoPessoa.setMaximumSize(new java.awt.Dimension(32767, 32));
        jPanel_TipoPessoa.setMinimumSize(new java.awt.Dimension(10, 22));
        jPanel_TipoPessoa.setPreferredSize(new java.awt.Dimension(438, 32));
        jPanel_TipoPessoa.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 6));

        jLabel_TipoPessoa.setText("Tipo de Pessoa:");
        jLabel_TipoPessoa.setMaximumSize(new java.awt.Dimension(120, 20));
        jLabel_TipoPessoa.setMinimumSize(new java.awt.Dimension(120, 20));
        jLabel_TipoPessoa.setPreferredSize(new java.awt.Dimension(120, 20));
        jPanel_TipoPessoa.add(jLabel_TipoPessoa);

        jComboBox_TipoPessoa.setMaximumSize(new java.awt.Dimension(80, 20));
        jComboBox_TipoPessoa.setMinimumSize(new java.awt.Dimension(80, 20));
        jComboBox_TipoPessoa.setPreferredSize(new java.awt.Dimension(80, 20));
        jPanel_TipoPessoa.add(jComboBox_TipoPessoa);

        jPanel_Filtros.add(jPanel_TipoPessoa);

        jPanel_Pessoa.setMaximumSize(new java.awt.Dimension(32767, 32));
        jPanel_Pessoa.setMinimumSize(new java.awt.Dimension(10, 22));
        jPanel_Pessoa.setPreferredSize(new java.awt.Dimension(438, 32));
        jPanel_Pessoa.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 6));

        jLabel_Pessoa.setText("Pessoa:");
        jLabel_Pessoa.setMaximumSize(new java.awt.Dimension(120, 20));
        jLabel_Pessoa.setMinimumSize(new java.awt.Dimension(120, 20));
        jLabel_Pessoa.setPreferredSize(new java.awt.Dimension(120, 20));
        jPanel_Pessoa.add(jLabel_Pessoa);

        jTextField_CodPessoa.setMaximumSize(new java.awt.Dimension(70, 20));
        jTextField_CodPessoa.setMinimumSize(new java.awt.Dimension(250, 20));
        jTextField_CodPessoa.setPreferredSize(new java.awt.Dimension(50, 20));
        jPanel_Pessoa.add(jTextField_CodPessoa);

        jTextField_NomePessoa.setMaximumSize(new java.awt.Dimension(250, 20));
        jTextField_NomePessoa.setMinimumSize(new java.awt.Dimension(250, 20));
        jTextField_NomePessoa.setPreferredSize(new java.awt.Dimension(220, 20));
        jPanel_Pessoa.add(jTextField_NomePessoa);

        jButton_Pessoa.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/icones/buscar_16.png"))); // NOI18N
        jButton_Pessoa.setToolTipText("Adicionar");
        jButton_Pessoa.setBorder(null);
        jButton_Pessoa.setMaximumSize(new java.awt.Dimension(23, 20));
        jButton_Pessoa.setMinimumSize(new java.awt.Dimension(23, 20));
        jButton_Pessoa.setPreferredSize(new java.awt.Dimension(23, 20));
        jPanel_Pessoa.add(jButton_Pessoa);

        jPanel_Filtros.add(jPanel_Pessoa);

        jPanel_DataVenda.setMaximumSize(new java.awt.Dimension(32767, 32));
        jPanel_DataVenda.setMinimumSize(new java.awt.Dimension(10, 22));
        jPanel_DataVenda.setPreferredSize(new java.awt.Dimension(438, 32));
        jPanel_DataVenda.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 6));

        jLabel_DtVenda.setText("Data de Entrega:");
        jLabel_DtVenda.setMaximumSize(new java.awt.Dimension(95, 20));
        jLabel_DtVenda.setMinimumSize(new java.awt.Dimension(95, 20));
        jLabel_DtVenda.setPreferredSize(new java.awt.Dimension(115, 20));
        jPanel_DataVenda.add(jLabel_DtVenda);
        jPanel_DataVenda.add(jDataField_DtVendaInicial);

        jLabel_A.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel_A.setText("a");
        jLabel_A.setMaximumSize(new java.awt.Dimension(120, 20));
        jLabel_A.setMinimumSize(new java.awt.Dimension(80, 20));
        jLabel_A.setPreferredSize(new java.awt.Dimension(10, 20));
        jPanel_DataVenda.add(jLabel_A);
        jPanel_DataVenda.add(jDataField_DtVendaFinal);

        jPanel_Filtros.add(jPanel_DataVenda);

        jPanel_Filtro.add(jPanel_Filtros);

        getContentPane().add(jPanel_Filtro, java.awt.BorderLayout.CENTER);

        jPanel_Buttons.setPreferredSize(new java.awt.Dimension(400, 33));

        jButton_Gerar.setMnemonic('G');
        jButton_Gerar.setText("Gerar");
        jButton_Gerar.setToolTipText("");
        jButton_Gerar.setMaximumSize(new java.awt.Dimension(80, 23));
        jButton_Gerar.setMinimumSize(new java.awt.Dimension(80, 23));
        jButton_Gerar.setPreferredSize(new java.awt.Dimension(80, 23));
        jButton_Gerar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_GerarActionPerformed(evt);
            }
        });
        jPanel_Buttons.add(jButton_Gerar);

        getContentPane().add(jPanel_Buttons, java.awt.BorderLayout.SOUTH);

        setSize(new java.awt.Dimension(536, 239));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton_GerarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_GerarActionPerformed
        gerar();
    }//GEN-LAST:event_jButton_GerarActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private final javax.swing.JButton jButton_Gerar = new javax.swing.JButton();
    private final javax.swing.JButton jButton_Pessoa = new javax.swing.JButton();
    private final javax.swing.JComboBox<Status> jComboBox_Status = new javax.swing.JComboBox<>();
    private final javax.swing.JComboBox<TipoPessoa> jComboBox_TipoPessoa = new javax.swing.JComboBox<>();
    private final base.utils.calendario.JDataField jDataField_DtVendaFinal = new base.utils.calendario.JDataField();
    private final base.utils.calendario.JDataField jDataField_DtVendaInicial = new base.utils.calendario.JDataField();
    private final javax.swing.JLabel jLabel_A = new javax.swing.JLabel();
    private final javax.swing.JLabel jLabel_DtVenda = new javax.swing.JLabel();
    private final javax.swing.JLabel jLabel_Pessoa = new javax.swing.JLabel();
    private final javax.swing.JLabel jLabel_Status = new javax.swing.JLabel();
    private final javax.swing.JLabel jLabel_TipoPessoa = new javax.swing.JLabel();
    private final javax.swing.JPanel jPanel_Buttons = new javax.swing.JPanel();
    private final javax.swing.JPanel jPanel_DataVenda = new javax.swing.JPanel();
    private final javax.swing.JPanel jPanel_Filtro = new javax.swing.JPanel();
    private final javax.swing.JPanel jPanel_Filtros = new javax.swing.JPanel();
    private final javax.swing.JPanel jPanel_Pessoa = new javax.swing.JPanel();
    private final javax.swing.JPanel jPanel_Status = new javax.swing.JPanel();
    private final javax.swing.JPanel jPanel_TipoPessoa = new javax.swing.JPanel();
    private final javax.swing.JTextField jTextField_CodPessoa = new javax.swing.JTextField();
    private final javax.swing.JTextField jTextField_NomePessoa = new javax.swing.JTextField();
    // End of variables declaration//GEN-END:variables

    private enum TipoPessoa {
        TODAS("Todas"), FISICA("Física"), JURIDICA("Jurídica");

        private final String nome;

        private TipoPessoa(String nome) {
            this.nome = nome;
        }

        @Override
        public String toString() {
            return nome;
        }

    }

    private enum Status {
        TODOS("Todos"), FINALIZADO("Finalizado"), CANCELADO("Cancelado");

        private final String nome;

        private Status(String nome) {
            this.nome = nome;
        }

        @Override
        public String toString() {
            return nome;
        }

    }
}
