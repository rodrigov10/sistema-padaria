/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.venda;

import base.bd.EnumID;

/**
 *
* @author Éderson
* @since 02/04/2019
 */
public enum FormaPagamento implements EnumID {
    DINHEIRO(0, "Fisica"),
    CARTAO_CREDITO(1, "Cartão Crédito"),
    CARTAO_DEBITO(2, "Cartão Débito"),
    CHEQUE(3, "Cheque");
    

    public final Integer codigo;
    public final String nome;

    private FormaPagamento(Integer codigo, String nome) {
        this.nome = nome;
        this.codigo = codigo;
    }

    public static FormaPagamento getFormaPagamento(Integer codigo) {
        for (FormaPagamento tipo : values()) {
            if (tipo.codigo.equals(codigo)) {
                return tipo;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return nome;
    }

    @Override
    public Integer getCodigo() {
        return this.codigo;
    }
}
