package com.venda;

import base.Valor;
import base.bd.ObjetoID;
import base.cache.CacheObjeto;
import base.usuario.Usuario;
import com.produto.Produto;
import java.util.Date;
import java.util.Objects;

/**
 *
 * @author Carlos
 * @since 10/04/2019
 */
public class ItemVendaProduto implements ObjetoID,CacheObjeto<Integer>{
    private Integer id;
    private Valor quantidade;
    private Produto produto;
    private Valor valor;
    
    private Date dataInclusao;
    private Usuario usuarioInclusao;
    private Date dataAlteracao;
    private Usuario usuarioAlteracao;


    public ItemVendaProduto() {
    }

    public ItemVendaProduto(Valor quantidade, Produto produto, Valor valor) {
        this.quantidade = quantidade;
        this.produto = produto;
        this.valor=valor;
        
    }

    public Valor getValor() {
        return valor;
    }

    public void setValor(Valor valor) {
        this.valor = valor;
    }
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Valor getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Valor quantidade) {
        this.quantidade = quantidade;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public Date getDataAlteracao() {
        return dataAlteracao;
    }

    public void setDataAlteracao(Date dataAlteracao) {
        this.dataAlteracao = dataAlteracao;
    }
        
        public Date getDataInclusao() {
        return dataInclusao;
    }

    public void setDataInclusao(Date dataInclusao) {
        this.dataInclusao = dataInclusao;
    }

    public Usuario getUsuarioInclusao() {
        return usuarioInclusao;
    }

    public void setUsuarioInclusao(Usuario usuarioInclusao) {
        this.usuarioInclusao = usuarioInclusao;
    }
 
    public Usuario getUsuarioAlteracao() {
        return usuarioAlteracao;
    }

    public void setUsuarioAlteracao(Usuario usuarioAlteracao) {
        this.usuarioAlteracao = usuarioAlteracao;
    }

    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + Objects.hashCode(this.id);
        return hash;
    }
    
  
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ItemVendaProduto other = (ItemVendaProduto) obj;
        return Objects.equals(this.id, other.id);
    }

    
}
