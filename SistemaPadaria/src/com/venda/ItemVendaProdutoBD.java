package com.venda;

import base.Valor;
import base.bd.BDException;
import base.bd.Conexao;
import base.bd.SQL;
import base.cache.CacheManager;
import base.login.Sessao;
import base.usuario.Usuario;
import base.usuario.UsuarioBD;
import com.pessoa.Pessoa;
import com.produto.Produto;
import com.produto.ProdutoController;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Éderson
 * @since 02/04/2019
 */
public class ItemVendaProdutoBD {

    public static final String TBL = "tabvenda_produto";

    
    private static Integer getNovoId() throws BDException {
        SQL sql = new SQL("SELECT nextval('tabvenda_produto_id_seq') seq");
        try (Conexao c = Conexao.getConexao()) {
            try (ResultSet rs = c.executeQuery(sql)) {
                if (rs.next()) {
                    return rs.getInt("seq");
                }
            } catch (SQLException ex) {
                throw new BDException(ex);
            }
        }
        return null;
    }

    public static void salvar(ItemVendaProduto itemVendaProduto, Integer id_venda) throws BDException {
                
        itemVendaProduto.setId(getNovoId());
        Usuario usuario = Sessao.getUsuarioLogado();
        Date data = new Date();
        SQL sql = new SQL();
        sql.add("INSERT INTO " + TBL);
        sql.add("(id, id_produto, id_venda, quantidade,valor,");
        sql.add(" dt_inclusao, id_usu_inclusao,dt_alteracao,id_usu_alteracao)");
        sql.add("VALUES");
        sql.add("(:id, :id_produto, :id_venda, :quantidade, :valor,");
        sql.add(":dt_inclusao, :id_usu_inclusao,:dt_alteracao,:id_usu_alteracao)");
        sql.setParametro("id", itemVendaProduto.getId());
        sql.setParametro("id_produto", itemVendaProduto.getProduto().getId());
        sql.setParametro("id_venda", id_venda);
        sql.setParametro("quantidade", itemVendaProduto.getQuantidade());
        sql.setParametro("valor", itemVendaProduto.getValor());
        sql.setParametro("dt_inclusao", data);
        sql.setParametro("id_usu_inclusao", usuario);
        sql.setParametro("dt_alteracao", data);
        sql.setParametro("id_usu_alteracao", usuario);
        Conexao.update(sql);
    }

    public static void alterar(ItemVendaProduto itemVendaProduto) throws BDException {
        
    }

    private static void carregarObjeto(ItemVendaProduto itemVendaProduto, ItemVendaProduto cache) {
        itemVendaProduto.setId(cache.getId());
        itemVendaProduto.setProduto(cache.getProduto());
        itemVendaProduto.setQuantidade(cache.getQuantidade());
        itemVendaProduto.setValor(cache.getValor());
        itemVendaProduto.setUsuarioInclusao(cache.getUsuarioInclusao());
        itemVendaProduto.setDataInclusao(cache.getDataInclusao());
        itemVendaProduto.setUsuarioAlteracao(cache.getUsuarioAlteracao());
        itemVendaProduto.setDataAlteracao(cache.getDataAlteracao());
    }

    private static void carregarObjeto(ItemVendaProduto itemVendaProduto, ResultSet rs) throws SQLException, BDException {
        itemVendaProduto.setId(rs.getInt("id"));
        Produto p = new Produto();
        p.setCodigo(rs.getInt("id_produto"));
        itemVendaProduto.setProduto(ProdutoController.buscar(p));
        itemVendaProduto.setValor(new Valor(rs.getBigDecimal("valor")));
        itemVendaProduto.setQuantidade(new Valor(rs.getBigDecimal("quantidade")));
        itemVendaProduto.setUsuarioInclusao(new Usuario(rs.getInt("id_usu_inclusao")));
        UsuarioBD.carregar(itemVendaProduto.getUsuarioInclusao());
        itemVendaProduto.setDataInclusao(rs.getDate("dt_inclusao"));
        itemVendaProduto.setUsuarioAlteracao(new Usuario(rs.getInt("id_usu_alteracao")));
        UsuarioBD.carregar(itemVendaProduto.getUsuarioAlteracao());
        itemVendaProduto.setDataInclusao(rs.getDate("dt_alteracao"));
        
    }
}
