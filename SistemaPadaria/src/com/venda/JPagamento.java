package com.venda;

import base.Valor;
import base.bd.BDException;
import base.msg.JAlerta;
import base.utils.FecharEsc;
import base.utils.FieldFormatUtils;
import base.utils.SwingUtils;
import com.pessoa.PessoaListener;
import java.awt.Component;
import java.awt.Window;

/**
 *
 * @author Éderson
 * @since 02/04/2019
 */
public class JPagamento extends javax.swing.JDialog implements FecharEsc {

    private final Venda venda;
    private boolean vendido = false;
    private boolean cancelado = false;
    private PessoaListener pessoaListener;

    public JPagamento(Window parent, Venda venda) {
        super(parent, ModalityType.APPLICATION_MODAL);
        this.venda = venda;
        initComponents();
        inicializar();
    }

    private void inicializar() {
        SwingUtils.inicializarTela(this);

        pessoaListener = new PessoaListener(this, jTFCliente_Codigo, jTFCliente_Nome, jBBuscarCliente, true, PessoaListener.Tipo.CLIENTE);

        FieldFormatUtils.formatarDigitos(jTFDesconto);
        FieldFormatUtils.formatarDigitos(jTFAcrescimo);
        FieldFormatUtils.formatarMonetario(jTFValorDesconto);
        FieldFormatUtils.formatarMonetario(jTFValorAcrescimo);
        FieldFormatUtils.formatarMonetario(jTFSubtotal);
        FieldFormatUtils.formatarMonetario(jTFTotal);
        FieldFormatUtils.formatarMonetario(jTFPago);
        FieldFormatUtils.formatarMonetario(jTFTroco);

        SwingUtils.desabilitarCampos(jTFValorDesconto);
        SwingUtils.desabilitarCampos(jTFValorAcrescimo);
        SwingUtils.desabilitarCampos(jTFSubtotal);
        SwingUtils.desabilitarCampos(jTFTotal);
        SwingUtils.desabilitarCampos(jTFTroco);

        jTFDesconto.setText(Valor.VALOR_ZEROD0.toString());
        jTFAcrescimo.setText(Valor.VALOR_ZEROD0.toString());
        jTFValorDesconto.setText(Valor.VALOR_ZEROD2.toString());
        jTFValorAcrescimo.setText(Valor.VALOR_ZEROD2.toString());
        jTFTotal.setText(Valor.VALOR_ZEROD2.toString());
        jTFPago.setText(Valor.VALOR_ZEROD2.toString());
        jTFTroco.setText(Valor.VALOR_ZEROD2.toString());

        jTFSubtotal.setText(venda.getValorTotal().toString());

    }

    ////////////////////////////////////////////////////////////////////////////
    //Venda
    ////////////////////////////////////////////////////////////////////////////
    private void salvar() {
        if (pessoaListener.getPessoa() == null) {
            JAlerta.getAlertaErro(this, "Cliente não informado.");
            return;
        }
        
        if (jTFAcrescimo.getText().isEmpty()) {
            jTFAcrescimo.setText(Valor.VALOR_ZEROD2.toString());
            jTFDesconto.setText(Valor.VALOR_ZEROD2.toString());
        }
        if (jRBDinheiro.isSelected()) {
            venda.setFormaPagamento(FormaPagamento.DINHEIRO);
        } else if (jRBCartaoCredito.isSelected()) {
            venda.setFormaPagamento(FormaPagamento.CARTAO_CREDITO);
        } else if (jRBCartaoDebito.isSelected()) {
            venda.setFormaPagamento(FormaPagamento.CARTAO_DEBITO);
        } else {
            venda.setFormaPagamento(FormaPagamento.CHEQUE);
        }

        try {
            SwingUtils.iniciarCursorProcessamento(this);
            SwingUtils.desabilitarCampos(getCampos());
            venda.setCliente(pessoaListener.getPessoa());
            VendaController.salvar(venda);
            vendido = true;
            JAlerta.getAlertaMsg(this, "Salvo com sucesso.");
            fechar();
        } catch (BDException ex) {
            JAlerta.getAlertaErro(this, "Erro ao finalizar venda: ", ex);
        } finally {
            SwingUtils.finalizarCursorProcessamento(this);
            SwingUtils.habilitarCampos(getCampos());
        }

    }

    private void calcularTotal() {
        Valor total = (venda.getValorTotal().menos(new Valor(jTFValorDesconto.getText())).mais(new Valor(jTFValorAcrescimo.getText())));
        jTFTotal.setText(total.toString());
    }

    private void calcularDesconto() {
        if ("".equals(jTFValorDesconto.getText().trim())) {
            jTFValorDesconto.setText(Valor.VALOR_ZEROD2.toString());
        }
        Valor desconto = (venda.getValorTotal().mult(new Valor(jTFDesconto.getText())).div(Valor.VALOR_CEMD2));
        jTFValorDesconto.setText(desconto.toString());
        calcularTotal();
    }

    private void calcularAcrescimo() {
        if ("".equals(jTFValorAcrescimo.getText().trim())) {
            jTFValorAcrescimo.setText(Valor.VALOR_ZEROD2.toString());
        }
        Valor acrescimo = (venda.getValorTotal().mult(new Valor(jTFAcrescimo.getText())).div(Valor.VALOR_CEMD2));
        jTFValorAcrescimo.setText(acrescimo.toString());
        calcularTotal();
    }
    
    private void calcularPago(){
        if ("".equals(jTFPago.getText().trim())) {
            jTFPago.setText(jTFTotal.getText());
        }
        Valor troco = (new Valor(jTFPago.getText()).menos(new Valor(jTFTotal.getText())));
        jTFTroco.setText(troco.toString());
        calcularTotal();
    }

    public boolean isVendido() {
        return vendido;
    }

    public boolean isCancelado() {
        return cancelado;
    }

    private Component[] getCampos() {
        return new Component[]{jBConfirmar, jBCancelar};
    }

    @Override
    public void fechar() {
        dispose();
        setVisible(false);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnGroupPagamento = new javax.swing.ButtonGroup();
        jPFundo = new javax.swing.JPanel();
        jPFormaPagamento = new javax.swing.JPanel();
        jPDesconto = new javax.swing.JPanel();
        jLDesconto = new javax.swing.JLabel();
        jTFDesconto = new javax.swing.JTextField();
        jLTexto = new javax.swing.JLabel();
        cifrão = new javax.swing.JLabel();
        jTFValorDesconto = new javax.swing.JTextField();
        jPAcrescimo = new javax.swing.JPanel();
        jLAcrescimo = new javax.swing.JLabel();
        jTFAcrescimo = new javax.swing.JTextField();
        jLTexto2 = new javax.swing.JLabel();
        cifrão2 = new javax.swing.JLabel();
        jTFValorAcrescimo = new javax.swing.JTextField();
        jPPagamento = new javax.swing.JPanel();
        jRBDinheiro = new javax.swing.JRadioButton();
        jRBCartaoCredito = new javax.swing.JRadioButton();
        jRBCartaoDebito = new javax.swing.JRadioButton();
        jRBCheque = new javax.swing.JRadioButton();
        jPSubtotal = new javax.swing.JPanel();
        jLSubtotal = new javax.swing.JLabel();
        cifrão3 = new javax.swing.JLabel();
        jTFSubtotal = new javax.swing.JTextField();
        jPTotal = new javax.swing.JPanel();
        jLTotal = new javax.swing.JLabel();
        cifrão4 = new javax.swing.JLabel();
        jTFTotal = new javax.swing.JTextField();
        jPPago = new javax.swing.JPanel();
        jLPago = new javax.swing.JLabel();
        cifrão5 = new javax.swing.JLabel();
        jTFPago = new javax.swing.JTextField();
        jPTroco = new javax.swing.JPanel();
        jLTroco = new javax.swing.JLabel();
        cifrão6 = new javax.swing.JLabel();
        jTFTroco = new javax.swing.JTextField();
        jPCliente = new javax.swing.JPanel();
        jLCliente = new javax.swing.JLabel();
        jTFCliente_Codigo = new javax.swing.JTextField();
        jTFCliente_Nome = new javax.swing.JTextField();
        jBBuscarCliente = new javax.swing.JButton();
        jPTitulo = new javax.swing.JPanel();
        jLTitulo = new javax.swing.JLabel();
        jPBotoes = new javax.swing.JPanel();
        jBConfirmar = new javax.swing.JButton();
        jBCancelar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Pagamento");
        setMaximumSize(new java.awt.Dimension(525, 525));
        setMinimumSize(new java.awt.Dimension(525, 525));
        setPreferredSize(new java.awt.Dimension(525, 525));

        jPFundo.setMaximumSize(new java.awt.Dimension(450, 200));
        jPFundo.setMinimumSize(new java.awt.Dimension(450, 200));
        jPFundo.setPreferredSize(new java.awt.Dimension(450, 200));
        jPFundo.setLayout(new java.awt.BorderLayout());

        jPFormaPagamento.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Forma de Pagamento", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 1, 18))); // NOI18N
        jPFormaPagamento.setMaximumSize(new java.awt.Dimension(1000, 180));
        jPFormaPagamento.setMinimumSize(new java.awt.Dimension(450, 180));
        jPFormaPagamento.setPreferredSize(new java.awt.Dimension(450, 180));
        jPFormaPagamento.setLayout(new javax.swing.BoxLayout(jPFormaPagamento, javax.swing.BoxLayout.Y_AXIS));

        jPDesconto.setMaximumSize(new java.awt.Dimension(1000, 40));
        jPDesconto.setMinimumSize(new java.awt.Dimension(438, 40));
        jPDesconto.setPreferredSize(new java.awt.Dimension(438, 40));
        jPDesconto.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 5, 6));

        jLDesconto.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        jLDesconto.setText("Desconto: ");
        jPDesconto.add(jLDesconto);

        jTFDesconto.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        jTFDesconto.setMaximumSize(new java.awt.Dimension(60, 30));
        jTFDesconto.setMinimumSize(new java.awt.Dimension(60, 30));
        jTFDesconto.setPreferredSize(new java.awt.Dimension(60, 30));
        jTFDesconto.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTFDescontoFocusLost(evt);
            }
        });
        jPDesconto.add(jTFDesconto);

        jLTexto.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        jLTexto.setText("%");
        jLTexto.setPreferredSize(new java.awt.Dimension(120, 22));
        jPDesconto.add(jLTexto);

        cifrão.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        cifrão.setText("R$");
        jPDesconto.add(cifrão);

        jTFValorDesconto.setBackground(new java.awt.Color(240, 240, 240));
        jTFValorDesconto.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jTFValorDesconto.setMaximumSize(new java.awt.Dimension(95, 30));
        jTFValorDesconto.setMinimumSize(new java.awt.Dimension(95, 30));
        jTFValorDesconto.setPreferredSize(new java.awt.Dimension(95, 30));
        jPDesconto.add(jTFValorDesconto);

        jPFormaPagamento.add(jPDesconto);

        jPAcrescimo.setMaximumSize(new java.awt.Dimension(1000, 40));
        jPAcrescimo.setMinimumSize(new java.awt.Dimension(438, 40));
        jPAcrescimo.setPreferredSize(new java.awt.Dimension(438, 40));
        jPAcrescimo.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 5, 6));

        jLAcrescimo.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        jLAcrescimo.setText("Acrescimo:");
        jPAcrescimo.add(jLAcrescimo);

        jTFAcrescimo.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        jTFAcrescimo.setMaximumSize(new java.awt.Dimension(60, 30));
        jTFAcrescimo.setMinimumSize(new java.awt.Dimension(60, 30));
        jTFAcrescimo.setPreferredSize(new java.awt.Dimension(60, 30));
        jTFAcrescimo.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTFAcrescimoFocusLost(evt);
            }
        });
        jPAcrescimo.add(jTFAcrescimo);

        jLTexto2.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        jLTexto2.setText("%");
        jLTexto2.setPreferredSize(new java.awt.Dimension(120, 22));
        jPAcrescimo.add(jLTexto2);

        cifrão2.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        cifrão2.setText("R$");
        jPAcrescimo.add(cifrão2);

        jTFValorAcrescimo.setBackground(new java.awt.Color(240, 240, 240));
        jTFValorAcrescimo.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jTFValorAcrescimo.setMaximumSize(new java.awt.Dimension(95, 30));
        jTFValorAcrescimo.setMinimumSize(new java.awt.Dimension(95, 30));
        jTFValorAcrescimo.setPreferredSize(new java.awt.Dimension(95, 30));
        jPAcrescimo.add(jTFValorAcrescimo);

        jPFormaPagamento.add(jPAcrescimo);

        jPPagamento.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Pagamento", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 1, 14))); // NOI18N
        jPPagamento.setMaximumSize(new java.awt.Dimension(1000, 60));
        jPPagamento.setMinimumSize(new java.awt.Dimension(450, 60));
        jPPagamento.setPreferredSize(new java.awt.Dimension(450, 60));
        jPPagamento.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 5, 6));

        btnGroupPagamento.add(jRBDinheiro);
        jRBDinheiro.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jRBDinheiro.setSelected(true);
        jRBDinheiro.setText("Dinheiro");
        jPPagamento.add(jRBDinheiro);

        btnGroupPagamento.add(jRBCartaoCredito);
        jRBCartaoCredito.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jRBCartaoCredito.setText("Cartão/Crédito");
        jPPagamento.add(jRBCartaoCredito);

        btnGroupPagamento.add(jRBCartaoDebito);
        jRBCartaoDebito.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jRBCartaoDebito.setText("Cartão/Débito");
        jPPagamento.add(jRBCartaoDebito);

        btnGroupPagamento.add(jRBCheque);
        jRBCheque.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jRBCheque.setText("Cheque");
        jRBCheque.setPreferredSize(new java.awt.Dimension(85, 25));
        jPPagamento.add(jRBCheque);

        jPFormaPagamento.add(jPPagamento);

        jPSubtotal.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jPSubtotal.setMaximumSize(new java.awt.Dimension(1000, 50));
        jPSubtotal.setMinimumSize(new java.awt.Dimension(438, 50));
        jPSubtotal.setPreferredSize(new java.awt.Dimension(438, 50));
        jPSubtotal.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 5, 6));

        jLSubtotal.setFont(new java.awt.Font("Arial", 1, 24)); // NOI18N
        jLSubtotal.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLSubtotal.setText("Subtotal:");
        jLSubtotal.setMaximumSize(new java.awt.Dimension(170, 35));
        jLSubtotal.setMinimumSize(new java.awt.Dimension(170, 35));
        jLSubtotal.setPreferredSize(new java.awt.Dimension(135, 35));
        jPSubtotal.add(jLSubtotal);

        cifrão3.setFont(new java.awt.Font("Arial", 1, 24)); // NOI18N
        cifrão3.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        cifrão3.setText("R$");
        cifrão3.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        cifrão3.setPreferredSize(new java.awt.Dimension(45, 30));
        jPSubtotal.add(cifrão3);

        jTFSubtotal.setBackground(new java.awt.Color(240, 240, 240));
        jTFSubtotal.setFont(new java.awt.Font("Arial", 1, 24)); // NOI18N
        jTFSubtotal.setMaximumSize(new java.awt.Dimension(120, 40));
        jTFSubtotal.setMinimumSize(new java.awt.Dimension(120, 40));
        jTFSubtotal.setPreferredSize(new java.awt.Dimension(120, 40));
        jPSubtotal.add(jTFSubtotal);

        jPFormaPagamento.add(jPSubtotal);

        jPTotal.setMaximumSize(new java.awt.Dimension(1000, 50));
        jPTotal.setMinimumSize(new java.awt.Dimension(438, 50));
        jPTotal.setPreferredSize(new java.awt.Dimension(438, 50));
        jPTotal.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 5, 6));

        jLTotal.setFont(new java.awt.Font("Arial", 1, 24)); // NOI18N
        jLTotal.setForeground(new java.awt.Color(0, 0, 153));
        jLTotal.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLTotal.setText("Total:");
        jLTotal.setMaximumSize(new java.awt.Dimension(170, 35));
        jLTotal.setMinimumSize(new java.awt.Dimension(170, 35));
        jLTotal.setPreferredSize(new java.awt.Dimension(135, 35));
        jPTotal.add(jLTotal);

        cifrão4.setFont(new java.awt.Font("Arial", 1, 24)); // NOI18N
        cifrão4.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        cifrão4.setText("R$");
        cifrão4.setMaximumSize(new java.awt.Dimension(45, 30));
        cifrão4.setMinimumSize(new java.awt.Dimension(45, 30));
        cifrão4.setPreferredSize(new java.awt.Dimension(45, 30));
        jPTotal.add(cifrão4);

        jTFTotal.setBackground(new java.awt.Color(240, 240, 240));
        jTFTotal.setFont(new java.awt.Font("Arial", 1, 24)); // NOI18N
        jTFTotal.setMaximumSize(new java.awt.Dimension(120, 40));
        jTFTotal.setMinimumSize(new java.awt.Dimension(120, 40));
        jTFTotal.setPreferredSize(new java.awt.Dimension(120, 40));
        jPTotal.add(jTFTotal);

        jPFormaPagamento.add(jPTotal);

        jPPago.setMaximumSize(new java.awt.Dimension(1000, 50));
        jPPago.setMinimumSize(new java.awt.Dimension(438, 50));
        jPPago.setPreferredSize(new java.awt.Dimension(438, 50));
        jPPago.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 5, 6));

        jLPago.setFont(new java.awt.Font("Arial", 1, 24)); // NOI18N
        jLPago.setForeground(new java.awt.Color(0, 153, 0));
        jLPago.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLPago.setText("Pago:");
        jLPago.setMaximumSize(new java.awt.Dimension(170, 35));
        jLPago.setMinimumSize(new java.awt.Dimension(170, 35));
        jLPago.setPreferredSize(new java.awt.Dimension(135, 35));
        jPPago.add(jLPago);

        cifrão5.setFont(new java.awt.Font("Arial", 1, 24)); // NOI18N
        cifrão5.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        cifrão5.setText("R$");
        cifrão5.setMaximumSize(new java.awt.Dimension(45, 30));
        cifrão5.setMinimumSize(new java.awt.Dimension(45, 30));
        cifrão5.setPreferredSize(new java.awt.Dimension(45, 30));
        jPPago.add(cifrão5);

        jTFPago.setFont(new java.awt.Font("Arial", 1, 24)); // NOI18N
        jTFPago.setMaximumSize(new java.awt.Dimension(120, 40));
        jTFPago.setMinimumSize(new java.awt.Dimension(120, 40));
        jTFPago.setPreferredSize(new java.awt.Dimension(120, 40));
        jTFPago.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTFPagoFocusLost(evt);
            }
        });
        jPPago.add(jTFPago);

        jPFormaPagamento.add(jPPago);

        jPTroco.setMaximumSize(new java.awt.Dimension(1000, 50));
        jPTroco.setMinimumSize(new java.awt.Dimension(438, 50));
        jPTroco.setPreferredSize(new java.awt.Dimension(438, 50));
        jPTroco.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 5, 6));

        jLTroco.setFont(new java.awt.Font("Arial", 1, 24)); // NOI18N
        jLTroco.setForeground(new java.awt.Color(204, 0, 0));
        jLTroco.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLTroco.setText("Troco:");
        jLTroco.setMaximumSize(new java.awt.Dimension(170, 35));
        jLTroco.setMinimumSize(new java.awt.Dimension(170, 35));
        jLTroco.setPreferredSize(new java.awt.Dimension(135, 35));
        jPTroco.add(jLTroco);

        cifrão6.setFont(new java.awt.Font("Arial", 1, 24)); // NOI18N
        cifrão6.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        cifrão6.setText("R$");
        cifrão6.setMaximumSize(new java.awt.Dimension(45, 30));
        cifrão6.setMinimumSize(new java.awt.Dimension(45, 30));
        cifrão6.setPreferredSize(new java.awt.Dimension(45, 30));
        jPTroco.add(cifrão6);

        jTFTroco.setBackground(new java.awt.Color(240, 240, 240));
        jTFTroco.setFont(new java.awt.Font("Arial", 1, 24)); // NOI18N
        jTFTroco.setMaximumSize(new java.awt.Dimension(120, 40));
        jTFTroco.setMinimumSize(new java.awt.Dimension(120, 40));
        jTFTroco.setPreferredSize(new java.awt.Dimension(120, 40));
        jPTroco.add(jTFTroco);

        jPFormaPagamento.add(jPTroco);

        jPCliente.setMaximumSize(new java.awt.Dimension(32767, 32));
        jPCliente.setMinimumSize(new java.awt.Dimension(10, 22));
        jPCliente.setPreferredSize(new java.awt.Dimension(438, 32));
        jPCliente.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 5, 6));

        jLCliente.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLCliente.setText("Cliente:");
        jLCliente.setMaximumSize(new java.awt.Dimension(102, 20));
        jLCliente.setMinimumSize(new java.awt.Dimension(102, 20));
        jLCliente.setPreferredSize(new java.awt.Dimension(60, 20));
        jPCliente.add(jLCliente);

        jTFCliente_Codigo.setMaximumSize(new java.awt.Dimension(70, 20));
        jTFCliente_Codigo.setMinimumSize(new java.awt.Dimension(70, 20));
        jTFCliente_Codigo.setPreferredSize(new java.awt.Dimension(70, 20));
        jPCliente.add(jTFCliente_Codigo);

        jTFCliente_Nome.setMaximumSize(new java.awt.Dimension(250, 20));
        jTFCliente_Nome.setMinimumSize(new java.awt.Dimension(250, 20));
        jTFCliente_Nome.setPreferredSize(new java.awt.Dimension(220, 20));
        jPCliente.add(jTFCliente_Nome);

        jBBuscarCliente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/icones/buscar_16.png"))); // NOI18N
        jBBuscarCliente.setToolTipText("Adicionar");
        jBBuscarCliente.setBorder(null);
        jBBuscarCliente.setMaximumSize(new java.awt.Dimension(23, 20));
        jBBuscarCliente.setMinimumSize(new java.awt.Dimension(23, 20));
        jBBuscarCliente.setPreferredSize(new java.awt.Dimension(23, 20));
        jPCliente.add(jBBuscarCliente);

        jPFormaPagamento.add(jPCliente);

        jPFundo.add(jPFormaPagamento, java.awt.BorderLayout.CENTER);

        getContentPane().add(jPFundo, java.awt.BorderLayout.CENTER);

        jPTitulo.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createEmptyBorder(10, 10, 10, 10), javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED)));

        jLTitulo.setFont(new java.awt.Font("Arial", 1, 24)); // NOI18N
        jLTitulo.setText("Finalizar Venda");
        jPTitulo.add(jLTitulo);

        getContentPane().add(jPTitulo, java.awt.BorderLayout.NORTH);

        jPBotoes.setMaximumSize(new java.awt.Dimension(175, 40));
        jPBotoes.setMinimumSize(new java.awt.Dimension(175, 40));
        jPBotoes.setPreferredSize(new java.awt.Dimension(175, 40));

        jBConfirmar.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jBConfirmar.setMnemonic('o');
        jBConfirmar.setText("Confirmar");
        jBConfirmar.setMaximumSize(new java.awt.Dimension(130, 30));
        jBConfirmar.setMinimumSize(new java.awt.Dimension(130, 30));
        jBConfirmar.setPreferredSize(new java.awt.Dimension(130, 30));
        jBConfirmar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBConfirmarActionPerformed(evt);
            }
        });
        jPBotoes.add(jBConfirmar);

        jBCancelar.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jBCancelar.setMnemonic('a');
        jBCancelar.setText("Cancelar");
        jBCancelar.setMaximumSize(new java.awt.Dimension(130, 30));
        jBCancelar.setMinimumSize(new java.awt.Dimension(130, 30));
        jBCancelar.setPreferredSize(new java.awt.Dimension(130, 30));
        jBCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBCancelarActionPerformed(evt);
            }
        });
        jPBotoes.add(jBCancelar);

        getContentPane().add(jPBotoes, java.awt.BorderLayout.SOUTH);

        setSize(new java.awt.Dimension(541, 564));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jBCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBCancelarActionPerformed
        cancelado = true;
        fechar();
    }//GEN-LAST:event_jBCancelarActionPerformed

    private void jBConfirmarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBConfirmarActionPerformed
        new Thread(() -> {
            salvar();
        }, "Realizar Venda").start();
    }//GEN-LAST:event_jBConfirmarActionPerformed

    private void jTFDescontoFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTFDescontoFocusLost
        calcularDesconto();
    }//GEN-LAST:event_jTFDescontoFocusLost

    private void jTFAcrescimoFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTFAcrescimoFocusLost
        calcularAcrescimo();
    }//GEN-LAST:event_jTFAcrescimoFocusLost

    private void jTFPagoFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTFPagoFocusLost
        calcularPago();
    }//GEN-LAST:event_jTFPagoFocusLost

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup btnGroupPagamento;
    private javax.swing.JLabel cifrão;
    private javax.swing.JLabel cifrão2;
    private javax.swing.JLabel cifrão3;
    private javax.swing.JLabel cifrão4;
    private javax.swing.JLabel cifrão5;
    private javax.swing.JLabel cifrão6;
    private javax.swing.JButton jBBuscarCliente;
    private javax.swing.JButton jBCancelar;
    private javax.swing.JButton jBConfirmar;
    private javax.swing.JLabel jLAcrescimo;
    private javax.swing.JLabel jLCliente;
    private javax.swing.JLabel jLDesconto;
    private javax.swing.JLabel jLPago;
    private javax.swing.JLabel jLSubtotal;
    private javax.swing.JLabel jLTexto;
    private javax.swing.JLabel jLTexto2;
    private javax.swing.JLabel jLTitulo;
    private javax.swing.JLabel jLTotal;
    private javax.swing.JLabel jLTroco;
    private javax.swing.JPanel jPAcrescimo;
    private javax.swing.JPanel jPBotoes;
    private javax.swing.JPanel jPCliente;
    private javax.swing.JPanel jPDesconto;
    private javax.swing.JPanel jPFormaPagamento;
    private javax.swing.JPanel jPFundo;
    private javax.swing.JPanel jPPagamento;
    private javax.swing.JPanel jPPago;
    private javax.swing.JPanel jPSubtotal;
    private javax.swing.JPanel jPTitulo;
    private javax.swing.JPanel jPTotal;
    private javax.swing.JPanel jPTroco;
    private javax.swing.JRadioButton jRBCartaoCredito;
    private javax.swing.JRadioButton jRBCartaoDebito;
    private javax.swing.JRadioButton jRBCheque;
    private javax.swing.JRadioButton jRBDinheiro;
    private javax.swing.JTextField jTFAcrescimo;
    private javax.swing.JTextField jTFCliente_Codigo;
    private javax.swing.JTextField jTFCliente_Nome;
    private javax.swing.JTextField jTFDesconto;
    private javax.swing.JTextField jTFPago;
    private javax.swing.JTextField jTFSubtotal;
    private javax.swing.JTextField jTFTotal;
    private javax.swing.JTextField jTFTroco;
    private javax.swing.JTextField jTFValorAcrescimo;
    private javax.swing.JTextField jTFValorDesconto;
    // End of variables declaration//GEN-END:variables
}
