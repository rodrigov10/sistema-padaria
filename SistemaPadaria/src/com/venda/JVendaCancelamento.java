package com.venda;

import base.TableModelObjeto;
import base.bd.BDException;
import base.msg.JAlerta;
import base.utils.DataUtils;
import base.utils.FecharEsc;
import base.utils.SwingUtils;
import java.awt.Component;
import java.awt.Window;
import java.util.List;

/**
 *
 * @author Rodrigo Vianna
 * @since 26/06/2019
 */
public class JVendaCancelamento extends javax.swing.JDialog implements FecharEsc {

    private static JVendaCancelamento instance;

    public static JVendaCancelamento getInstance(Window parent) {
        if (instance == null) {
            instance = new JVendaCancelamento(parent, false);
        }
        return instance;
    }

    private JVendaCancelamento(Window parent, boolean modal) {
        super(parent, modal ? ModalityType.APPLICATION_MODAL : ModalityType.MODELESS);
        initComponents();
        inicializar();
    }

    private void inicializar() {
        SwingUtils.inicializarTela(this);

        inicializarTabela();
    }

    private void inicializarTabela() {
        TableModelObjeto<Venda> tm = new TableModelObjeto(jTable.getColumnModel());
        tm.addColumn("Data");
        tm.addColumn("Cliente");
        tm.addColumn("Valor");
        tm.addColumn("Objeto");

        jTable.setModel(tm);

        tm.getColumn("Data").setPreferredWidth(70);
        tm.getColumn("Cliente").setPreferredWidth(200);
        tm.getColumn("Valor").setPreferredWidth(50);
        jTable.removeColumn(tm.getColumn("Objeto"));
    }

    public void atualizarTabelaThread() {
        new Thread(() -> {
            atualizarTabela();
        }, "Buscando Lista de Vendas").start();
    }

    private void atualizarTabela() {
        try {
            SwingUtils.desabilitarCampos(getCampos());
            SwingUtils.iniciarCursorProcessamento(this);

            TableModelObjeto<Venda> tm = ((TableModelObjeto<Venda>) jTable.getModel());
            tm.reset();

            VendaSC filtro = new VendaSC();
            filtro.dtInicialVenda = jDataFieldDataEntrega_Inicial.getDataSelecionada();
            filtro.dtFinalVenda = jDataFieldDataEntrega_Final.getDataSelecionada();

            List<Venda> lista = VendaBD.listarNaoCancelados(filtro);
            if (lista.isEmpty()) {
                JAlerta.getAlertaMsg(this, "Nenhum registro encontrado.");
                return;
            }

            lista.stream().map((venda) -> {
                Object[] row = new Object[4];
                row[0] = DataUtils.dataToString(venda.getDataVenda());
                row[1] = venda.getCliente().getNome();
                row[2] = venda.getValorTotal().toString();
                row[3] = venda;
                return row;
            }).forEachOrdered((row) -> {
                tm.addRow(row);
            });

            if (jTable.getRowCount() > 0) {
                jTable.changeSelection(0, 0, false, false);
            }

        } catch (BDException ex) {
            JAlerta.getAlertaErro(this, "Erro ao listar vendas: ", ex);
        } finally {
            SwingUtils.habilitarCampos(getCampos());
            SwingUtils.finalizarCursorProcessamento(this);
        }
    }

    private void cancelar(Venda venda) {
        if (JAlerta.getAlertaConfirmacao(this, "Deseja cancelar esta venda?\nEssa operção não pode ser desfeita.")) {
            try {
                if (VendaController.cancelar(venda)) {
                    atualizarTabela();
                }
            } catch (BDException e) {
                JAlerta.getAlertaErro(this, "Erro ao cancelar Venda: ", e);
            }
        }
    }

    private Component[] getCampos() {
        return new Component[]{jButton_Retornar, jButton_Buscar, jDataFieldDataEntrega_Final, jDataFieldDataEntrega_Inicial};
    }

    @Override
    public void fechar() {
        setVisible(false);
        dispose();
    }

    public static void main(String[] args) {
        if (com.Start.iniciarSistema()) {
            getInstance((java.awt.Dialog) null).setVisible(true);
        }
        System.exit(0);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDataFieldDataEntrega_Inicial = new base.utils.calendario.JDataField();
        jDataFieldDataEntrega_Final = new base.utils.calendario.JDataField();
        jButton_Buscar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Cancelamento");
        setMinimumSize(new java.awt.Dimension(650, 370));

        jPanel_Filtro.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Filtro", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 11))); // NOI18N
        jPanel_Filtro.setLayout(new javax.swing.BoxLayout(jPanel_Filtro, javax.swing.BoxLayout.Y_AXIS));

        jPCPF.setMaximumSize(new java.awt.Dimension(32767, 32));
        jPCPF.setMinimumSize(new java.awt.Dimension(10, 22));
        jPCPF.setPreferredSize(new java.awt.Dimension(438, 32));
        jPCPF.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 6));

        jLCliente1.setText("Data:");
        jLCliente1.setMaximumSize(new java.awt.Dimension(50, 20));
        jLCliente1.setMinimumSize(new java.awt.Dimension(50, 20));
        jLCliente1.setPreferredSize(new java.awt.Dimension(50, 20));
        jPCPF.add(jLCliente1);
        jPCPF.add(jDataFieldDataEntrega_Inicial);

        jLCliente2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLCliente2.setText("a");
        jLCliente2.setMaximumSize(new java.awt.Dimension(120, 20));
        jLCliente2.setMinimumSize(new java.awt.Dimension(80, 20));
        jLCliente2.setPreferredSize(new java.awt.Dimension(10, 20));
        jPCPF.add(jLCliente2);
        jPCPF.add(jDataFieldDataEntrega_Final);

        jButton_Buscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/icones/buscar_16.png"))); // NOI18N
        jButton_Buscar.setToolTipText("Adicionar");
        jButton_Buscar.setBorder(null);
        jButton_Buscar.setMaximumSize(new java.awt.Dimension(23, 20));
        jButton_Buscar.setMinimumSize(new java.awt.Dimension(23, 20));
        jButton_Buscar.setPreferredSize(new java.awt.Dimension(23, 20));
        jButton_Buscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_BuscarActionPerformed(evt);
            }
        });
        jPCPF.add(jButton_Buscar);

        jPanel_Filtro.add(jPCPF);

        getContentPane().add(jPanel_Filtro, java.awt.BorderLayout.NORTH);

        jTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTableMouseClicked(evt);
            }
        });
        jScrollPane.setViewportView(jTable);

        getContentPane().add(jScrollPane, java.awt.BorderLayout.CENTER);

        jPanel_Buttons.setPreferredSize(new java.awt.Dimension(400, 33));

        jButton_Retornar.setMnemonic('R');
        jButton_Retornar.setText("Retornar");
        jButton_Retornar.setMaximumSize(new java.awt.Dimension(95, 23));
        jButton_Retornar.setMinimumSize(new java.awt.Dimension(95, 23));
        jButton_Retornar.setPreferredSize(new java.awt.Dimension(95, 23));
        jButton_Retornar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_RetornarActionPerformed(evt);
            }
        });
        jPanel_Buttons.add(jButton_Retornar);

        getContentPane().add(jPanel_Buttons, java.awt.BorderLayout.SOUTH);

        setSize(new java.awt.Dimension(666, 409));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton_RetornarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_RetornarActionPerformed
        fechar();
    }//GEN-LAST:event_jButton_RetornarActionPerformed

    private void jTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableMouseClicked
        if (evt.getClickCount() > 1 && jTable.getSelectedRow() != -1) {
            cancelar(((TableModelObjeto<Venda>) jTable.getModel()).getObjeto(jTable.getSelectedRow()));
        }
    }//GEN-LAST:event_jTableMouseClicked

    private void jButton_BuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_BuscarActionPerformed
        atualizarTabelaThread();
    }//GEN-LAST:event_jButton_BuscarActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton_Buscar;
    private final javax.swing.JButton jButton_Retornar = new javax.swing.JButton();
    private base.utils.calendario.JDataField jDataFieldDataEntrega_Final;
    private base.utils.calendario.JDataField jDataFieldDataEntrega_Inicial;
    private final javax.swing.JLabel jLCliente1 = new javax.swing.JLabel();
    private final javax.swing.JLabel jLCliente2 = new javax.swing.JLabel();
    private final javax.swing.JPanel jPCPF = new javax.swing.JPanel();
    private final javax.swing.JPanel jPanel_Buttons = new javax.swing.JPanel();
    private final javax.swing.JPanel jPanel_Filtro = new javax.swing.JPanel();
    private final javax.swing.JScrollPane jScrollPane = new javax.swing.JScrollPane();
    private final javax.swing.JTable jTable = new javax.swing.JTable();
    // End of variables declaration//GEN-END:variables
}
