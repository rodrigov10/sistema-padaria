package com.venda;

import base.Valor;
import base.bd.ObjetoID;
import base.cache.CacheObjeto;
import base.msg.JAlerta;
import base.usuario.Usuario;
import com.pessoa.Pessoa;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;

/**
 * Classe de venda. Contém todos os dados da venda.
 * @author Rodrigo Vianna
 * @since 13/03/2019
 */
public class Venda implements ObjetoID, CacheObjeto<Integer> {

    private Integer id;
    private Date dataVenda;
    private Valor valorTotal;
    private Valor valorDesconto;
    private Valor valorAcrescimo;
    private boolean finalizada;
    private Pessoa cliente;
    private FormaPagamento formaPagamento;
    private final ArrayList<ItemVendaProduto> itensVendaProduto;

    private boolean cancelada;
    private Date dataCancelamento;
    private Usuario usuarioCancelamento;

    private Date dataInclusao;
    private Usuario usuarioInclusao;

    public Venda() {
        valorTotal = Valor.VALOR_ZEROD2;
        dataVenda = new Date();
        cancelada = false;
        itensVendaProduto = new ArrayList<>();
    }

    /**
     * Método que retorna uma ArrayList com os itemVendaProduto.
     *
     * @return Lista de itens da venda
     */
    public ArrayList<ItemVendaProduto> getItemVendaProduto() {
        return itensVendaProduto;
    }

    /**
     * Método para adicionar novo itemVendaProduto
     *
     * @param itemVendaProduto passa item de venda
     * @return true or false
     */
    public boolean addItemVendaProduto(ItemVendaProduto itemVendaProduto) {
        valorTotal = valorTotal.mais(itemVendaProduto.getProduto().getValor().mult(itemVendaProduto.getQuantidade()));
        return this.itensVendaProduto.add(itemVendaProduto);
    }

    /**
     * Método que remove telefone pelo itemVendaProduto
     *
     * @param itemVendaProduto passa item de venda
     * @return true or false
     */
    public boolean removeItemVendaProduto(ItemVendaProduto itemVendaProduto) {
        valorTotal = valorTotal.menos(itemVendaProduto.getProduto().getValor().mult(itemVendaProduto.getQuantidade()));
        return this.itensVendaProduto.remove(itemVendaProduto);
    }

    /**
     * Método que remove itemVendaProduto pelo index na lista
     *
     * @param index passa index de item de venda
     * @return item de venda removido
     */
    public ItemVendaProduto removeItemVendaProduto(int index) {
        ItemVendaProduto itemVendaProduto = itensVendaProduto.remove(index);
        if (itemVendaProduto == null) {
            JAlerta.getAlertaMsg(null, "Item não existe!");
            return null;
        }
        valorTotal = valorTotal.menos(itemVendaProduto.getProduto().getValor().mult(itemVendaProduto.getQuantidade()));
        return itemVendaProduto;
    }

    public Pessoa getCliente() {
        return cliente;
    }

    public void setCliente(Pessoa cliente) {
        this.cliente = cliente;
    }

    public FormaPagamento getFormaPagamento() {
        return formaPagamento;
    }

    public void setFormaPagamento(FormaPagamento formaPagamento) {
        this.formaPagamento = formaPagamento;
    }

    @Override
    public Integer getId() {
        return id;
    }

    public Date getDataVenda() {
        return dataVenda;
    }

    public Valor getValorTotal() {
        return valorTotal;
    }

    public Valor getValorDesconto() {
        return valorDesconto;
    }

    public Valor getValorAcrescimo() {
        return valorAcrescimo;
    }

    public boolean isCancelada() {
        return cancelada;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public void setDataVenda(Date dataVenda) {
        this.dataVenda = dataVenda;
    }

    public void setValorTotal(Valor valorTotal) {
        this.valorTotal = valorTotal;
    }

    public void setValorDesconto(Valor valorDesconto) {
        this.valorDesconto = valorDesconto;
    }

    public void setValorAcrescimo(Valor valorAcrescimo) {
        this.valorAcrescimo = valorAcrescimo;
    }

    public void setCancelada(boolean cancelada) {
        this.cancelada = cancelada;
    }

    public boolean isFinalizada() {
        return finalizada;
    }

    public void setFinalizada(boolean finalizada) {
        this.finalizada = finalizada;
    }

    public Date getDataCancelamento() {
        return dataCancelamento;
    }

    public void setDataCancelamento(Date dataCancelamento) {
        this.dataCancelamento = dataCancelamento;
    }

    public Usuario getUsuarioCancelamento() {
        return usuarioCancelamento;
    }

    public void setUsuarioCancelamento(Usuario usuarioCancelamento) {
        this.usuarioCancelamento = usuarioCancelamento;
    }

    public Date getDataInclusao() {
        return dataInclusao;
    }

    public void setDataInclusao(Date dataInclusao) {
        this.dataInclusao = dataInclusao;
    }

    public Usuario getUsuarioInclusao() {
        return usuarioInclusao;
    }

    public void setUsuarioInclusao(Usuario usuarioInclusao) {
        this.usuarioInclusao = usuarioInclusao;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 83 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Venda other = (Venda) obj;
        return Objects.equals(this.id, other.id);
    }

}
