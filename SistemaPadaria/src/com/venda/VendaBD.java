package com.venda;

import base.Valor;
import base.bd.BDException;
import base.bd.Conexao;
import base.bd.SQL;
import base.cache.CacheManager;
import base.login.Sessao;
import base.usuario.Usuario;
import base.usuario.UsuarioBD;
import com.pessoa.Pessoa;
import com.pessoa.PessoaBD;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Éderson
 * @since 02/04/2019
 */
public class VendaBD {

    public static final String TBL = "tabvenda";

    private static final CacheManager<Venda> CACHE = CacheManager.getInstance(Venda.class);

    private static Integer getNovoId() throws BDException {
        SQL sql = new SQL("SELECT nextval('tabvenda_id_seq') seq");
        try (Conexao c = Conexao.getConexao()) {
            try (ResultSet rs = c.executeQuery(sql)) {
                if (rs.next()) {
                    return rs.getInt("seq");
                }
            } catch (SQLException ex) {
                throw new BDException(ex);
            }
        }
        return null;
    }

    public static void salvar(Venda venda) throws BDException {
        venda.setId(getNovoId());
        Usuario usuario = Sessao.getUsuarioLogado();
        Date data = new Date();
        SQL sql = new SQL();
        sql.add("INSERT INTO " + TBL);
        sql.add("(id, id_pessoa, datavenda, valortotal, valordesconto, valoracrescimo, forma_pagamento,");
        sql.add("finalizado, dt_inclusao, id_usu_inclusao)");
        sql.add("VALUES");
        sql.add("(:id, :id_pessoa, :datavenda, :valortotal, :valordesconto, :valoracrescimo, :forma_pagamento,");
        sql.add(":finalizado, :dt_inclusao, :id_usu_inclusao)");
        sql.setParametro("id", venda.getId());
        Pessoa p = new Pessoa();
        p.setId(0);
        PessoaBD.carregar(p);
        venda.setCliente(p);
        sql.setParametro("id_pessoa", venda.getCliente());
        sql.setParametro("datavenda", venda.getDataVenda());
        sql.setParametro("valortotal", venda.getValorTotal());
        sql.setParametro("valordesconto", venda.getValorDesconto());
        sql.setParametro("valoracrescimo", venda.getValorAcrescimo());
        sql.setParametro("forma_pagamento", venda.getFormaPagamento());
        sql.setParametro("finalizado", venda.isCancelada());
        sql.setParametro("dt_inclusao", data);
        sql.setParametro("id_usu_inclusao", usuario.getId());
        Conexao.update(sql);
        venda.setUsuarioInclusao(usuario);
        venda.setDataInclusao(data);

        for (ItemVendaProduto item : venda.getItemVendaProduto()) {
            ItemVendaProdutoBD.salvar(item, venda.getId());
        }
        CACHE.put(venda);
    }

    public static void alterar(Venda venda) throws BDException {
        SQL sql = new SQL();
        sql.add("UPDATE " + TBL);
        sql.add("SET id_pessoa = :id_pessoa, datavenda = :datavenda, valortotal = :valortotal, valordesconto = :valordesconto,");
        sql.add("forma_pagamento = :forma_pagamento, valoracrescimo = :valoracrescimo, finalizado = :finalizado");
        sql.add("WHERE id = :id");
        sql.setParametro("id", venda);
        sql.setParametro("id_pessoa", venda.getCliente());
        sql.setParametro("datavenda", venda.getDataVenda());
        sql.setParametro("valortotal", venda.getValorTotal());
        sql.setParametro("valordesconto", venda.getValorDesconto());
        sql.setParametro("valoracrescimo", venda.getValorAcrescimo());
        sql.setParametro("forma_pagamento", venda.getFormaPagamento());
        sql.setParametro("finalizado", venda.isCancelada());
        Conexao.update(sql);
        CACHE.put(venda);
    }

    /**
     * Uma venda não é excluída, e sim cancelada. Uma vez cancelada, não é
     * possível mais alterar.
     *
     * @param venda passa a venda
     * @throws BDException lança throws
     */
    public static void cancelar(Venda venda) throws BDException {
        venda.setCancelada(true);
        venda.setDataCancelamento(new Date());
        venda.setUsuarioCancelamento(Sessao.getUsuarioLogado());
        SQL sql = new SQL();
        sql.add("UPDATE " + TBL);
        sql.add("SET cancelado = :cancelado, dt_cancelamento = :dt_cancelamento, id_usu_cancelamento = :id_usu_cancelamento");
        sql.add("WHERE id = :id");
        sql.setParametro("id", venda);
        sql.setParametro("cancelado", venda.isCancelada());
        sql.setParametro("dt_cancelamento", venda.getDataCancelamento());
        sql.setParametro("id_usu_cancelamento", venda.getUsuarioCancelamento());
        Conexao.update(sql);
        CACHE.put(venda);
    }

    public static void carregar(Venda venda) throws BDException {
        Venda objCache = CACHE.get(venda.getId());
        if (objCache != null) {
            carregarObjeto(venda, objCache);
            return;
        }
        SQL sql = new SQL("SELECT * FROM " + TBL + " WHERE id = :id");
        sql.setParametro("id", venda);
        try (Conexao c = Conexao.getConexao()) {
            try (ResultSet rs = c.executeQuery(sql)) {
                while (rs.next()) {
                    carregarObjeto(venda, rs);
                }
            } catch (SQLException ex) {
                throw new BDException(ex);
            }
        }
    }

    private static void carregarObjeto(Venda venda, Venda cache) {
        venda.setId(cache.getId());
        venda.setCliente(cache.getCliente());
        venda.setDataVenda(cache.getDataVenda());
        venda.setValorTotal(cache.getValorTotal());
        venda.setValorAcrescimo(cache.getValorAcrescimo());
        venda.setValorDesconto(cache.getValorDesconto());
        venda.setCancelada(cache.isCancelada());
        venda.setFinalizada(cache.isFinalizada());
        venda.setFormaPagamento(cache.getFormaPagamento());
        venda.setUsuarioCancelamento(cache.getUsuarioCancelamento());
        venda.setDataCancelamento(cache.getDataCancelamento());
        venda.setUsuarioInclusao(cache.getUsuarioInclusao());
        venda.setDataInclusao(cache.getDataInclusao());
    }

    private static void carregarObjeto(Venda venda, ResultSet rs) throws SQLException, BDException {
        venda.setCliente(new Pessoa(rs.getInt("id_pessoa")));
        venda.setDataVenda(rs.getDate("datavenda"));
        venda.setValorTotal(new Valor(rs.getBigDecimal("valortotal")));
        venda.setValorAcrescimo(new Valor(rs.getBigDecimal("valoracrescimo")));
        venda.setValorDesconto(new Valor(rs.getBigDecimal("valordesconto")));
        venda.setCancelada(rs.getBoolean("cancelado"));
        venda.setFinalizada(rs.getBoolean("finalizado"));
        venda.setFormaPagamento(FormaPagamento.getFormaPagamento(rs.getInt("forma_pagamento")));
        venda.setUsuarioCancelamento(new Usuario(rs.getInt("id_usu_cancelamento")));
        if (rs.wasNull()) {
            venda.setUsuarioCancelamento(null);
        } else {
            UsuarioBD.carregar(venda.getUsuarioCancelamento());
        }
        venda.setDataCancelamento(rs.getDate("dt_cancelamento"));
        venda.setUsuarioInclusao(new Usuario(rs.getInt("id_usu_inclusao")));
        venda.setDataInclusao(rs.getDate("dt_inclusao"));
        CACHE.put(venda);
    }

    public static List<Venda> listarNaoCancelados(VendaSC filtro) throws BDException {
        SQL sql = new SQL();
        sql.add("SELECT v.*");
        sql.add("FROM " + TBL + " v");
        sql.add("WHERE cancelado = FALSE");
        if (filtro.dtInicialVenda != null && filtro.dtFinalVenda != null) {
            sql.add("AND DATE(e.datavenda) BETWEEN DATE(:dtInicialVenda) AND DATE(:dtFinalVenda)");
            sql.setParametro("dtInicialVenda", filtro.dtInicialVenda);
            sql.setParametro("dtFinalVenda", filtro.dtFinalVenda);
        }
        try (Conexao c = Conexao.getConexao()) {
            try (ResultSet rs = c.executeQuery(sql)) {
                List<Venda> lista = new ArrayList();
                while (rs.next()) {
                    Venda venda = new Venda();
                    carregarObjeto(venda, rs);
                    lista.add(venda);
                }
                return lista;
            } catch (SQLException ex) {
                throw new BDException(ex);
            }
        }
    }

}
