package com.venda;

import base.bd.BDException;

/**
 *
 * @author Rodrigo Vianna
 * @since 02/05/2019
 */
public class VendaController {

    public static void salvar(Venda venda) throws BDException {
        if (venda.getId() == null) {
            VendaBD.salvar(venda);
        } else {
            VendaBD.alterar(venda);
        }
    }

    public static boolean cancelar(Venda venda) throws BDException {
        if (venda == null || venda.getId() == null) {
            return false;
        }
        VendaBD.cancelar(venda);
        return true;
    }

}
