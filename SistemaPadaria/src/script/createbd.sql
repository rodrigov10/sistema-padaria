--
-- PostgreSQL database dump
--

-- Dumped from database version 10.6
-- Dumped by pg_dump version 11.2

-- Started on 2019-06-29 17:25:27

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE bd;
--
-- TOC entry 3910 (class 1262 OID 18854)
-- Name: bd; Type: DATABASE; Schema: -; Owner: -
--

CREATE DATABASE bd WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8';


\connect bd

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET default_with_oids = false;

--
-- TOC entry 196 (class 1259 OID 18855)
-- Name: tabenc_produto; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.tabenc_produto (
    id integer NOT NULL,
    id_entrega integer NOT NULL,
    id_produto integer NOT NULL,
    quantidade numeric(12,2) NOT NULL,
    observacao character varying(200),
    dt_inclusao timestamp without time zone NOT NULL,
    dt_alteracao timestamp without time zone NOT NULL,
    id_usu_inclusao integer NOT NULL,
    id_usu_alteracao integer NOT NULL
);


--
-- TOC entry 197 (class 1259 OID 18858)
-- Name: tabenc_produto_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.tabenc_produto_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3911 (class 0 OID 0)
-- Dependencies: 197
-- Name: tabenc_produto_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.tabenc_produto_id_seq OWNED BY public.tabenc_produto.id;


--
-- TOC entry 198 (class 1259 OID 18860)
-- Name: tabencomenda; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.tabencomenda (
    id integer NOT NULL,
    id_pessoa integer NOT NULL,
    codigo integer NOT NULL,
    datapedido date NOT NULL,
    dataparaentrega date NOT NULL,
    valor numeric(12,2) NOT NULL,
    observacao text,
    finalizado boolean DEFAULT false NOT NULL,
    dt_inclusao timestamp without time zone NOT NULL,
    dt_alteracao timestamp without time zone NOT NULL,
    id_usu_inclusao integer NOT NULL,
    id_usu_alteracao integer NOT NULL
);


--
-- TOC entry 199 (class 1259 OID 18867)
-- Name: tabencomenda_codigo_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.tabencomenda_codigo_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3912 (class 0 OID 0)
-- Dependencies: 199
-- Name: tabencomenda_codigo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.tabencomenda_codigo_seq OWNED BY public.tabencomenda.codigo;


--
-- TOC entry 200 (class 1259 OID 18869)
-- Name: tabencomenda_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.tabencomenda_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3913 (class 0 OID 0)
-- Dependencies: 200
-- Name: tabencomenda_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.tabencomenda_id_seq OWNED BY public.tabencomenda.id;


--
-- TOC entry 201 (class 1259 OID 18871)
-- Name: tabpessoa; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.tabpessoa (
    id integer NOT NULL,
    codigo integer NOT NULL,
    nome character varying(250) NOT NULL,
    cpf character varying(11),
    cnpj character varying(14),
    tipo_pessoa integer DEFAULT 0 NOT NULL,
    ativo boolean DEFAULT true NOT NULL,
    cliente boolean DEFAULT false NOT NULL,
    fornecedor boolean DEFAULT false NOT NULL,
    numero character varying(6),
    logradouro character varying(100),
    bairro character varying(60),
    cidade character varying(100),
    cep character varying(8),
    complemento character varying(250),
    telefone1 character varying(11),
    telefone2 character varying(11),
    email character varying(250),
    dt_inclusao timestamp without time zone NOT NULL,
    dt_alteracao timestamp without time zone NOT NULL,
    id_usu_inclusao integer NOT NULL,
    id_usu_alteracao integer NOT NULL
);


--
-- TOC entry 202 (class 1259 OID 18881)
-- Name: tabpessoa_codigo_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.tabpessoa_codigo_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3914 (class 0 OID 0)
-- Dependencies: 202
-- Name: tabpessoa_codigo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.tabpessoa_codigo_seq OWNED BY public.tabpessoa.codigo;


--
-- TOC entry 203 (class 1259 OID 18883)
-- Name: tabpessoa_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.tabpessoa_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3915 (class 0 OID 0)
-- Dependencies: 203
-- Name: tabpessoa_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.tabpessoa_id_seq OWNED BY public.tabpessoa.id;


--
-- TOC entry 204 (class 1259 OID 18885)
-- Name: tabproduto; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.tabproduto (
    id integer NOT NULL,
    id_pessoa integer,
    codigo integer NOT NULL,
    nome character varying(200) NOT NULL,
    marca character varying(200),
    valor numeric(12,2) NOT NULL,
    unidade_medida integer NOT NULL,
    cod_barra1 character varying(13),
    cod_barra2 character varying(13),
    dt_inclusao timestamp without time zone NOT NULL,
    dt_alteracao timestamp without time zone NOT NULL,
    id_usu_inclusao integer NOT NULL,
    id_usu_alteracao integer NOT NULL
);


--
-- TOC entry 205 (class 1259 OID 18888)
-- Name: tabproduto_codigo_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.tabproduto_codigo_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3916 (class 0 OID 0)
-- Dependencies: 205
-- Name: tabproduto_codigo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.tabproduto_codigo_seq OWNED BY public.tabproduto.codigo;


--
-- TOC entry 206 (class 1259 OID 18890)
-- Name: tabproduto_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.tabproduto_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3917 (class 0 OID 0)
-- Dependencies: 206
-- Name: tabproduto_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.tabproduto_id_seq OWNED BY public.tabproduto.id;


--
-- TOC entry 207 (class 1259 OID 18892)
-- Name: tabusuario; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.tabusuario (
    id integer NOT NULL,
    nomeusuario character varying(100) NOT NULL,
    senha character varying(32) NOT NULL,
    ativo boolean DEFAULT false NOT NULL,
    tipo integer DEFAULT 0 NOT NULL,
    master boolean DEFAULT false NOT NULL
);


--
-- TOC entry 208 (class 1259 OID 18898)
-- Name: tabusuario_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.tabusuario_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3918 (class 0 OID 0)
-- Dependencies: 208
-- Name: tabusuario_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.tabusuario_id_seq OWNED BY public.tabusuario.id;


--
-- TOC entry 209 (class 1259 OID 18900)
-- Name: tabvenda; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.tabvenda (
    id integer NOT NULL,
    id_pessoa integer NOT NULL,
    datavenda timestamp without time zone,
    valortotal numeric(12,2) NOT NULL,
    valordesconto numeric(12,2),
    valoracrescimo numeric(12,2),
    forma_pagamento integer DEFAULT 0 NOT NULL,
    finalizado boolean DEFAULT false NOT NULL,
    cancelado boolean DEFAULT false NOT NULL,
    dt_cancelamento timestamp without time zone,
    id_usu_cancelamento integer,
    dt_inclusao timestamp without time zone NOT NULL,
    id_usu_inclusao integer NOT NULL
);


--
-- TOC entry 210 (class 1259 OID 18906)
-- Name: tabvenda_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.tabvenda_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3919 (class 0 OID 0)
-- Dependencies: 210
-- Name: tabvenda_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.tabvenda_id_seq OWNED BY public.tabvenda.id;


--
-- TOC entry 211 (class 1259 OID 18908)
-- Name: tabvenda_produto; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.tabvenda_produto (
    id integer NOT NULL,
    id_produto integer NOT NULL,
    id_venda integer NOT NULL,
    quantidade numeric(12,2) NOT NULL,
    valor numeric(12,2) NOT NULL,
    dt_inclusao timestamp without time zone NOT NULL,
    dt_alteracao timestamp without time zone NOT NULL,
    id_usu_inclusao integer NOT NULL,
    id_usu_alteracao integer NOT NULL
);


--
-- TOC entry 212 (class 1259 OID 18911)
-- Name: tabvenda_produto_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.tabvenda_produto_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3920 (class 0 OID 0)
-- Dependencies: 212
-- Name: tabvenda_produto_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.tabvenda_produto_id_seq OWNED BY public.tabvenda_produto.id;


--
-- TOC entry 3709 (class 2604 OID 18913)
-- Name: tabenc_produto id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tabenc_produto ALTER COLUMN id SET DEFAULT nextval('public.tabenc_produto_id_seq'::regclass);


--
-- TOC entry 3711 (class 2604 OID 18914)
-- Name: tabencomenda id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tabencomenda ALTER COLUMN id SET DEFAULT nextval('public.tabencomenda_id_seq'::regclass);


--
-- TOC entry 3712 (class 2604 OID 18915)
-- Name: tabencomenda codigo; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tabencomenda ALTER COLUMN codigo SET DEFAULT nextval('public.tabencomenda_codigo_seq'::regclass);


--
-- TOC entry 3717 (class 2604 OID 18916)
-- Name: tabpessoa id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tabpessoa ALTER COLUMN id SET DEFAULT nextval('public.tabpessoa_id_seq'::regclass);


--
-- TOC entry 3718 (class 2604 OID 18917)
-- Name: tabpessoa codigo; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tabpessoa ALTER COLUMN codigo SET DEFAULT nextval('public.tabpessoa_codigo_seq'::regclass);


--
-- TOC entry 3719 (class 2604 OID 18918)
-- Name: tabproduto id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tabproduto ALTER COLUMN id SET DEFAULT nextval('public.tabproduto_id_seq'::regclass);


--
-- TOC entry 3723 (class 2604 OID 18919)
-- Name: tabusuario id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tabusuario ALTER COLUMN id SET DEFAULT nextval('public.tabusuario_id_seq'::regclass);


--
-- TOC entry 3727 (class 2604 OID 18920)
-- Name: tabvenda id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tabvenda ALTER COLUMN id SET DEFAULT nextval('public.tabvenda_id_seq'::regclass);


--
-- TOC entry 3728 (class 2604 OID 18921)
-- Name: tabvenda_produto id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tabvenda_produto ALTER COLUMN id SET DEFAULT nextval('public.tabvenda_produto_id_seq'::regclass);


--
-- TOC entry 3731 (class 2606 OID 18923)
-- Name: tabenc_produto tabenc_produto_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tabenc_produto
    ADD CONSTRAINT tabenc_produto_pkey PRIMARY KEY (id);


--
-- TOC entry 3734 (class 2606 OID 18925)
-- Name: tabencomenda tabencomenda_codigo_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tabencomenda
    ADD CONSTRAINT tabencomenda_codigo_key UNIQUE (codigo);


--
-- TOC entry 3737 (class 2606 OID 18927)
-- Name: tabencomenda tabencomenda_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tabencomenda
    ADD CONSTRAINT tabencomenda_pkey PRIMARY KEY (id);


--
-- TOC entry 3739 (class 2606 OID 18929)
-- Name: tabpessoa tabpessoa_cnpj_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tabpessoa
    ADD CONSTRAINT tabpessoa_cnpj_key UNIQUE (cnpj);


--
-- TOC entry 3742 (class 2606 OID 18931)
-- Name: tabpessoa tabpessoa_codigo_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tabpessoa
    ADD CONSTRAINT tabpessoa_codigo_key UNIQUE (codigo);


--
-- TOC entry 3744 (class 2606 OID 18933)
-- Name: tabpessoa tabpessoa_cpf_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tabpessoa
    ADD CONSTRAINT tabpessoa_cpf_key UNIQUE (cpf);


--
-- TOC entry 3747 (class 2606 OID 18935)
-- Name: tabpessoa tabpessoa_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tabpessoa
    ADD CONSTRAINT tabpessoa_pkey PRIMARY KEY (id);


--
-- TOC entry 3751 (class 2606 OID 18937)
-- Name: tabproduto tabproduto_codigo_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tabproduto
    ADD CONSTRAINT tabproduto_codigo_key UNIQUE (codigo);


--
-- TOC entry 3753 (class 2606 OID 18939)
-- Name: tabproduto tabproduto_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tabproduto
    ADD CONSTRAINT tabproduto_pkey PRIMARY KEY (id);


--
-- TOC entry 3755 (class 2606 OID 18941)
-- Name: tabusuario tabusuario_nomeusuario_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tabusuario
    ADD CONSTRAINT tabusuario_nomeusuario_key UNIQUE (nomeusuario);


--
-- TOC entry 3757 (class 2606 OID 18943)
-- Name: tabusuario tabusuario_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tabusuario
    ADD CONSTRAINT tabusuario_pkey PRIMARY KEY (id);


--
-- TOC entry 3761 (class 2606 OID 18945)
-- Name: tabvenda tabvenda_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tabvenda
    ADD CONSTRAINT tabvenda_pkey PRIMARY KEY (id);


--
-- TOC entry 3764 (class 2606 OID 18947)
-- Name: tabvenda_produto tabvenda_produto_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tabvenda_produto
    ADD CONSTRAINT tabvenda_produto_pkey PRIMARY KEY (id);


--
-- TOC entry 3729 (class 1259 OID 19046)
-- Name: tabenc_produto_id_entrega_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX tabenc_produto_id_entrega_index ON public.tabenc_produto USING btree (id_entrega);


--
-- TOC entry 3732 (class 1259 OID 19047)
-- Name: tabencomenda_codigo_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX tabencomenda_codigo_index ON public.tabencomenda USING btree (codigo);


--
-- TOC entry 3735 (class 1259 OID 19048)
-- Name: tabencomenda_dataparaentrega_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX tabencomenda_dataparaentrega_index ON public.tabencomenda USING btree (dataparaentrega);


--
-- TOC entry 3740 (class 1259 OID 19049)
-- Name: tabpessoa_codigo_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX tabpessoa_codigo_index ON public.tabpessoa USING btree (codigo);


--
-- TOC entry 3745 (class 1259 OID 19050)
-- Name: tabpessoa_nome_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX tabpessoa_nome_index ON public.tabpessoa USING btree (nome);


--
-- TOC entry 3748 (class 1259 OID 19045)
-- Name: tabproduto_cod_barra_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX tabproduto_cod_barra_index ON public.tabproduto USING btree (cod_barra1, cod_barra2);


--
-- TOC entry 3749 (class 1259 OID 19044)
-- Name: tabproduto_codigo_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX tabproduto_codigo_index ON public.tabproduto USING btree (codigo);


--
-- TOC entry 3758 (class 1259 OID 19052)
-- Name: tabvenda_datavenda_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX tabvenda_datavenda_index ON public.tabvenda USING btree (datavenda);


--
-- TOC entry 3759 (class 1259 OID 19051)
-- Name: tabvenda_id_pessoa_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX tabvenda_id_pessoa_index ON public.tabvenda USING btree (id_pessoa);


--
-- TOC entry 3762 (class 1259 OID 19053)
-- Name: tabvenda_produto_id_venda_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX tabvenda_produto_id_venda_index ON public.tabvenda_produto USING btree (id_venda);


--
-- TOC entry 3765 (class 2606 OID 18948)
-- Name: tabenc_produto tabenc_produto_id_entrega_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tabenc_produto
    ADD CONSTRAINT tabenc_produto_id_entrega_fkey FOREIGN KEY (id_entrega) REFERENCES public.tabencomenda(id);


--
-- TOC entry 3766 (class 2606 OID 18953)
-- Name: tabenc_produto tabenc_produto_id_produto_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tabenc_produto
    ADD CONSTRAINT tabenc_produto_id_produto_fkey FOREIGN KEY (id_produto) REFERENCES public.tabproduto(id);


--
-- TOC entry 3767 (class 2606 OID 18958)
-- Name: tabenc_produto tabenc_produto_id_usu_alteracao_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tabenc_produto
    ADD CONSTRAINT tabenc_produto_id_usu_alteracao_fkey FOREIGN KEY (id_usu_alteracao) REFERENCES public.tabusuario(id);


--
-- TOC entry 3768 (class 2606 OID 18963)
-- Name: tabenc_produto tabenc_produto_id_usu_inclusao_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tabenc_produto
    ADD CONSTRAINT tabenc_produto_id_usu_inclusao_fkey FOREIGN KEY (id_usu_inclusao) REFERENCES public.tabusuario(id);


--
-- TOC entry 3769 (class 2606 OID 18968)
-- Name: tabencomenda tabencomenda_id_pessoa_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tabencomenda
    ADD CONSTRAINT tabencomenda_id_pessoa_fkey FOREIGN KEY (id_pessoa) REFERENCES public.tabpessoa(id);


--
-- TOC entry 3770 (class 2606 OID 18973)
-- Name: tabencomenda tabencomenda_id_usu_alteracao_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tabencomenda
    ADD CONSTRAINT tabencomenda_id_usu_alteracao_fkey FOREIGN KEY (id_usu_alteracao) REFERENCES public.tabusuario(id);


--
-- TOC entry 3771 (class 2606 OID 18978)
-- Name: tabencomenda tabencomenda_id_usu_inclusao_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tabencomenda
    ADD CONSTRAINT tabencomenda_id_usu_inclusao_fkey FOREIGN KEY (id_usu_inclusao) REFERENCES public.tabusuario(id);


--
-- TOC entry 3772 (class 2606 OID 18983)
-- Name: tabpessoa tabpessoa_id_usu_alteracao_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tabpessoa
    ADD CONSTRAINT tabpessoa_id_usu_alteracao_fkey FOREIGN KEY (id_usu_alteracao) REFERENCES public.tabusuario(id);


--
-- TOC entry 3773 (class 2606 OID 18988)
-- Name: tabpessoa tabpessoa_id_usu_inclusao_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tabpessoa
    ADD CONSTRAINT tabpessoa_id_usu_inclusao_fkey FOREIGN KEY (id_usu_inclusao) REFERENCES public.tabusuario(id);


--
-- TOC entry 3774 (class 2606 OID 18993)
-- Name: tabproduto tabproduto_id_pessoa_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tabproduto
    ADD CONSTRAINT tabproduto_id_pessoa_fkey FOREIGN KEY (id_pessoa) REFERENCES public.tabpessoa(id);


--
-- TOC entry 3775 (class 2606 OID 18998)
-- Name: tabproduto tabproduto_id_usu_alteracao_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tabproduto
    ADD CONSTRAINT tabproduto_id_usu_alteracao_fkey FOREIGN KEY (id_usu_alteracao) REFERENCES public.tabusuario(id);


--
-- TOC entry 3776 (class 2606 OID 19003)
-- Name: tabproduto tabproduto_id_usu_inclusao_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tabproduto
    ADD CONSTRAINT tabproduto_id_usu_inclusao_fkey FOREIGN KEY (id_usu_inclusao) REFERENCES public.tabusuario(id);


--
-- TOC entry 3777 (class 2606 OID 19008)
-- Name: tabvenda tabvenda_id_pessoa_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tabvenda
    ADD CONSTRAINT tabvenda_id_pessoa_fkey FOREIGN KEY (id_pessoa) REFERENCES public.tabpessoa(id);


--
-- TOC entry 3778 (class 2606 OID 19013)
-- Name: tabvenda tabvenda_id_usu_inclusao_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tabvenda
    ADD CONSTRAINT tabvenda_id_usu_inclusao_fkey FOREIGN KEY (id_usu_inclusao) REFERENCES public.tabusuario(id);


--
-- TOC entry 3779 (class 2606 OID 19018)
-- Name: tabvenda tabvenda_iid_usu_cancelamento_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tabvenda
    ADD CONSTRAINT tabvenda_iid_usu_cancelamento_fkey FOREIGN KEY (id_usu_cancelamento) REFERENCES public.tabusuario(id);


--
-- TOC entry 3780 (class 2606 OID 19023)
-- Name: tabvenda_produto tabvenda_produto_id_produto_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tabvenda_produto
    ADD CONSTRAINT tabvenda_produto_id_produto_fkey FOREIGN KEY (id_produto) REFERENCES public.tabproduto(id);


--
-- TOC entry 3781 (class 2606 OID 19028)
-- Name: tabvenda_produto tabvenda_produto_id_usu_alteracao_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tabvenda_produto
    ADD CONSTRAINT tabvenda_produto_id_usu_alteracao_fkey FOREIGN KEY (id_usu_alteracao) REFERENCES public.tabusuario(id);


--
-- TOC entry 3782 (class 2606 OID 19033)
-- Name: tabvenda_produto tabvenda_produto_id_usu_inclusao_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tabvenda_produto
    ADD CONSTRAINT tabvenda_produto_id_usu_inclusao_fkey FOREIGN KEY (id_usu_inclusao) REFERENCES public.tabusuario(id);


--
-- TOC entry 3783 (class 2606 OID 19038)
-- Name: tabvenda_produto tabvenda_produto_id_venda_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tabvenda_produto
    ADD CONSTRAINT tabvenda_produto_id_venda_fkey FOREIGN KEY (id_venda) REFERENCES public.tabvenda(id);


-- Completed on 2019-06-29 17:25:53

--
-- PostgreSQL database dump complete
--

