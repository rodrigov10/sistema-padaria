--
-- PostgreSQL database dump
--

-- Dumped from database version 10.6
-- Dumped by pg_dump version 11.2

-- Started on 2019-06-29 17:27:15

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 3916 (class 0 OID 18892)
-- Dependencies: 207
-- Data for Name: tabusuario; Type: TABLE DATA; Schema: public; Owner: -
--

SET SESSION AUTHORIZATION DEFAULT;

ALTER TABLE public.tabusuario DISABLE TRIGGER ALL;

INSERT INTO public.tabusuario VALUES (0, 'Administrador', '606a4a9e6701f8d0f72b4de90f2a9e39', true, 0, true);


ALTER TABLE public.tabusuario ENABLE TRIGGER ALL;

--
-- TOC entry 3910 (class 0 OID 18871)
-- Dependencies: 201
-- Data for Name: tabpessoa; Type: TABLE DATA; Schema: public; Owner: -
--

ALTER TABLE public.tabpessoa DISABLE TRIGGER ALL;

INSERT INTO public.tabpessoa VALUES (0, 0, 'Cliente Eventual', NULL, NULL, 0, true, true, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NOW(), NOW(), 0, 0);


ALTER TABLE public.tabpessoa ENABLE TRIGGER ALL;

--
-- TOC entry 3907 (class 0 OID 18860)
-- Dependencies: 198
-- Data for Name: tabencomenda; Type: TABLE DATA; Schema: public; Owner: -
--

ALTER TABLE public.tabencomenda DISABLE TRIGGER ALL;



ALTER TABLE public.tabencomenda ENABLE TRIGGER ALL;

--
-- TOC entry 3913 (class 0 OID 18885)
-- Dependencies: 204
-- Data for Name: tabproduto; Type: TABLE DATA; Schema: public; Owner: -
--

ALTER TABLE public.tabproduto DISABLE TRIGGER ALL;

INSERT INTO public.tabproduto VALUES (0, NULL, 0, 'Produto Genérico', NULL, 1.00, 0, NULL, NULL, NOW(), NOW(), 0, 0);


ALTER TABLE public.tabproduto ENABLE TRIGGER ALL;

--
-- TOC entry 3905 (class 0 OID 18855)
-- Dependencies: 196
-- Data for Name: tabenc_produto; Type: TABLE DATA; Schema: public; Owner: -
--

ALTER TABLE public.tabenc_produto DISABLE TRIGGER ALL;



ALTER TABLE public.tabenc_produto ENABLE TRIGGER ALL;

--
-- TOC entry 3918 (class 0 OID 18900)
-- Dependencies: 209
-- Data for Name: tabvenda; Type: TABLE DATA; Schema: public; Owner: -
--

ALTER TABLE public.tabvenda DISABLE TRIGGER ALL;



ALTER TABLE public.tabvenda ENABLE TRIGGER ALL;

--
-- TOC entry 3920 (class 0 OID 18908)
-- Dependencies: 211
-- Data for Name: tabvenda_produto; Type: TABLE DATA; Schema: public; Owner: -
--

ALTER TABLE public.tabvenda_produto DISABLE TRIGGER ALL;



ALTER TABLE public.tabvenda_produto ENABLE TRIGGER ALL;

--
-- TOC entry 3927 (class 0 OID 0)
-- Dependencies: 197
-- Name: tabenc_produto_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.tabenc_produto_id_seq', 1, false);


--
-- TOC entry 3928 (class 0 OID 0)
-- Dependencies: 199
-- Name: tabencomenda_codigo_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.tabencomenda_codigo_seq', 1, false);


--
-- TOC entry 3929 (class 0 OID 0)
-- Dependencies: 200
-- Name: tabencomenda_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.tabencomenda_id_seq', 1, false);


--
-- TOC entry 3930 (class 0 OID 0)
-- Dependencies: 202
-- Name: tabpessoa_codigo_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.tabpessoa_codigo_seq', 1, false);


--
-- TOC entry 3931 (class 0 OID 0)
-- Dependencies: 203
-- Name: tabpessoa_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.tabpessoa_id_seq', 1, false);


--
-- TOC entry 3932 (class 0 OID 0)
-- Dependencies: 205
-- Name: tabproduto_codigo_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.tabproduto_codigo_seq', 1, false);


--
-- TOC entry 3933 (class 0 OID 0)
-- Dependencies: 206
-- Name: tabproduto_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.tabproduto_id_seq', 1, false);


--
-- TOC entry 3934 (class 0 OID 0)
-- Dependencies: 208
-- Name: tabusuario_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.tabusuario_id_seq', 1, false);


--
-- TOC entry 3935 (class 0 OID 0)
-- Dependencies: 210
-- Name: tabvenda_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.tabvenda_id_seq', 1, false);


--
-- TOC entry 3936 (class 0 OID 0)
-- Dependencies: 212
-- Name: tabvenda_produto_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.tabvenda_produto_id_seq', 1, false);


-- Completed on 2019-06-29 17:27:46

--
-- PostgreSQL database dump complete
--

